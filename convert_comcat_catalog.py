import datetime

# Convert catalog in USGS ComCat API format to a plain text catalog
#
# 2020-11-02	C. Yoon    First created

#in_comcat_file='../Catalog/test_catalog.txt'
#out_catalog_file='../Catalog/test_catalog_puerto_rico.txt'
#catalog_start_time='2020-01-07T00:00:00'

#in_comcat_file='../Catalog/catalog_puerto_rico_2018_2020.txt'
#out_catalog_file='../Catalog/catalog_new_puerto_rico_2018_2020.txt'
#catalog_start_time='2018-01-01T00:00:00'

#in_comcat_file='../Catalog/catalog_puerto_rico_2018_20191227.txt'
#out_catalog_file='../Catalog/catalog_new_puerto_rico_2018_20191227.txt'
#catalog_start_time='2018-01-01T00:00:00'

#in_comcat_file='../Catalog/catalog_puerto_rico_20191227_2020.txt'
#out_catalog_file='../Catalog/catalog_new_puerto_rico_20191227_2020.txt'
#catalog_start_time='2018-01-01T00:00:00'

#in_comcat_file='../Catalog/catalog_puerto_rico_20200107_20200114.txt'
#out_catalog_file='../Catalog/catalog_new_puerto_rico_20200107_20200114.txt'
#catalog_start_time='2020-01-07T00:00:00'

#in_comcat_file='../Catalog/EQT_20180101_20211001/catalog_puerto_rico_20180101_20211001_download20211020.txt'
#out_catalog_file='../Catalog/EQT_20180101_20211001/catalog_new_puerto_rico_20180101_20211001_download20211020.txt'
#catalog_start_time='2018-01-01T00:00:00'

#in_comcat_file='../Catalog/EQT_20180101_20220101/catalog_puerto_rico_20180101_20220101_download20220111.txt'
#out_catalog_file='../Catalog/EQT_20180101_20220101/catalog_new_puerto_rico_20180101_20220101_download20220111.txt'
#catalog_start_time='2018-01-01T00:00:00'

#in_comcat_file='../Catalog/EQT_20200107_20200108/catalog_puerto_rico_20200107_20200108_download20220111.txt'
#out_catalog_file='../Catalog/EQT_20200107_20200108/catalog_new_puerto_rico_20200107_20200108_download20220111.txt'
#catalog_start_time='2020-01-07T00:00:00'

in_comcat_file='../Catalog/EQT_20180101_20230101/catalog_puerto_rico_20180101_20230101_download20230206.txt'
out_catalog_file='../Catalog/EQT_20180101_20230101/catalog_new_puerto_rico_20180101_20230101_download20230206.txt'
catalog_start_time='2018-01-01T00:00:00'


catalog_ref_time = datetime.datetime.strptime(catalog_start_time, '%Y-%m-%dT%H:%M:%S')
fout = open(out_catalog_file, 'w')
with open(in_comcat_file, 'r') as fcat:
   for line in fcat:
      if (line[0] == '#'): # skip first line
         continue
      split_line = line.split('|')
      origin_time = datetime.datetime.strptime(split_line[1], '%Y-%m-%dT%H:%M:%S.%f')
      time_diff = (origin_time - catalog_ref_time).total_seconds()
      cat_lat = float(split_line[2])
      cat_lon = float(split_line[3])
      cat_depth = float(split_line[4])
      if (split_line[10] == ''): # no magnitude estimate
         print("Missing magnitude estimate, skip: ", split_line)
         continue
      cat_mag = float(split_line[10])
      ev_id = split_line[0].strip('.')
      # Output plain text catalog with columns: time(sec) since catalog_start_time, latitude, longitude, depth, magnitude, eventid
#      fout.write(('%15.5f %8.5f %8.5f %6.4f %5.3f %s\n') % (time_diff, cat_lat, cat_lon, cat_depth, cat_mag, ev_id))
      fout.write(('%s %15.5f %8.5f %8.5f %6.4f %5.3f %s\n') % (split_line[1], time_diff, cat_lat, cat_lon, cat_depth, cat_mag, ev_id))
fout.close()
