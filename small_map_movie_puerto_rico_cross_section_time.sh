#!/usr/bin/env bash
#               GMT ANIMATION 02
#
# Purpose:      Make simple animated GIF of an illuminated DEM grid
# GMT modules   math, makecpt, grdimage, plot, movie
# Unix progs:   cat
# Note:         Run with any argument to build movie; otherwise 1st frame is plotted only.

if [ $# -eq 0 ]; then   # Just make master PostScript frame 0
   opt="-Mps -Fnone"
else  # Make animated GIF
#   opt="-A+l"
   opt="-A" # no infinite loop
fi

# 1. Create files needed in the loop
cat << EOF > pre.sh
gmt begin
   gmt gmtset MAP_FRAME_TYPE plain     # map outline - single line with ticks
   gmt gmtset FORMAT_GEO_MAP DD        # labels on map - decimal degrees
   gmt gmtset FONT_ANNOT 24p
   gmt gmtset FONT_LABEL 24p

#   NDAY=1461
   NDAY=1826
   DELTA_DAY=1
   PLOT_DAY=3
#   PLOT_DAY=7 # original
   gmt math -T726/\${NDAY}/\${DELTA_DAY} T \${PLOT_DAY} ADD = timeframes.txt

   out_color_time_filename=timecolors.cpt
#   out_color_time_file=/Users/cyoon/Documents/PuertoRico/EQT_20180101_20220101/Plots/\${out_color_time_filename}
   out_color_time_file=/Users/cyoon/Documents/PuertoRico/EQT_20180101_20230101/Plots/\${out_color_time_filename}
   gmt makecpt -Cviridis -I -T726/\${NDAY}/0.01 -H > \${out_color_time_file}
   start_date=2018-01-01

   strike1_bproj=-JX30/15
   strike1_brange=-R-30/30/-30/0
   gmt basemap \${strike1_bproj} \${strike1_brange} -Ba10f5 -BneWS+g170 #-U
#   gmt colorbar -C\${out_color_time_file} -Dx11.9i/3.0i/2.5i/0.15i -Bxaf+l"Days since "\${start_date}

#   strike2_bproj=-JX40/15
#   strike2_brange=-R-40/40/-30/0
#   gmt basemap \${strike2_bproj} \${strike2_brange} -Ba10f5 -BneWS+g170 #-U
##   gmt colorbar -C\${out_color_time_file} -Dx15.9i/3.0i/2.5i/0.15i -Bxaf+l"Days since "\${start_date}
gmt end
EOF

# 2. Set up the main frame script
cat << EOF > main.sh
gmt begin
   gmt gmtset MAP_FRAME_TYPE plain     # map outline - single line with ticks
   gmt gmtset FORMAT_GEO_MAP DD        # labels on map - decimal degrees
   gmt gmtset FONT_ANNOT 24p
   gmt gmtset FONT_LABEL 24p

   out_color_time_filename=timecolors.cpt
#   out_color_time_file=/Users/cyoon/Documents/PuertoRico/EQT_20180101_20220101/Plots/\${out_color_time_filename}
   out_color_time_file=/Users/cyoon/Documents/PuertoRico/EQT_20180101_20230101/Plots/\${out_color_time_filename}

   DISP_DATE=\$(gdate -d "2017-12-31 +\${MOVIE_COL1} days" +%F) #PLOT_DAY=3
   echo "DISP_DATE="\${DISP_DATE}

#-----  NEW strike 13 -------
   strike1_bproj=-JX30/15
   strike1_brange=-R-30/30/-30/0
   gmt basemap \${strike1_bproj} \${strike1_brange} -Ba10f5 -BneWS+g170+t"Date: \${DISP_DATE}" #-U

#   in_event_file=/Users/cyoon/Documents/PuertoRico/EQT_20180101_20220101/Plots/catalog_new_puerto_rico_20180101_20220101_depth_0_1461_seis_proj13.txt
#   awk -v FD="0" -v LD="\${MOVIE_COL1}" '{if ((\$10/86400.) >= FD && (\$10/86400.) <= LD) printf "%10.8f %10.8f %6.4f\n", \$4, \$3*-1., \$11}' \${in_event_file} | gmt plot \${strike1_bproj} \${strike1_brange} -Bxa10+l"Length along cross-section A-A' (km)" -Bya10+l"Depth (km)" -BneWS -Sc -W0.001c,white #Catalog-past
#   awk -v FD="\${MOVIE_COL0}" -v LD="\${MOVIE_COL1}" '{if ((\$10/86400.) >= FD && (\$10/86400.) <= LD) printf "%10.8f %10.8f %10.8f %6.4f\n", \$4, \$3*-1., \$10/86400., \$11}' \${in_event_file} | gmt plot \${strike1_bproj} \${strike1_brange} -Bxa10+l"Length along cross-section A-A' (km)" -Bya10+l"Depth (km)" -BneWS -Sc -W1.2+cl -C\${out_color_time_file} #Catalog

#   in_event_file=/Users/cyoon/Documents/PuertoRico/EQT_20180101_20220101/Plots/EQT_20180101_20220101_3REAL_HYPOINVERSE_VELZHANG_combined_depth_0_1461_seis_proj13.txt
#   awk -v FD="0" -v LD="\${MOVIE_COL1}" '{if ((\$6/86400.) >= FD && (\$6/86400.) <= LD) printf "%10.8f %10.8f %6.4f\n", \$8, \$3*-1., \$7}' \${in_event_file} | gmt plot \${strike1_bproj} \${strike1_brange} -Bxa10+l"Length along cross-section A-A' (km)" -Bya10+l"Depth (km)" -BneWS -Sc -W0.001c,white #HYPOINVERSE-past
#   awk -v FD="\${MOVIE_COL0}" -v LD="\${MOVIE_COL1}" '{if ((\$6/86400.) >= FD && (\$6/86400.) <= LD) printf "%10.8f %10.8f %10.8f %6.4f\n", \$8, \$3*-1., \$6/86400., \$7}' \${in_event_file} | gmt plot \${strike1_bproj} \${strike1_brange} -Bxa10+l"Length along cross-section A-A' (km)" -Bya10+l"Depth (km)" -BneWS -Sc -W1.2+cl -C\${out_color_time_file} #HYPOINVERSE

#   in_event_file=/Users/cyoon/Documents/PuertoRico/EQT_20180101_20220101/Plots/EQT_20180101_20220101_3REAL_HYPOSVI_VELZHANG_depth_0_1461_seis_proj13.txt
#   awk -v FD="0" -v LD="\${MOVIE_COL1}" '{if ((\$6/86400.) >= FD && (\$6/86400.) <= LD) printf "%10.8f %10.8f %6.4f\n", \$11, \$3*-1., \$10}' \${in_event_file} | gmt plot \${strike1_bproj} \${strike1_brange} -Bxa10+l"Length along cross-section A-A' (km)" -Bya10+l"Depth (km)" -BneWS -Sc -W0.001c,white #HYPOSVI-past
#   awk -v FD="\${MOVIE_COL0}" -v LD="\${MOVIE_COL1}" '{if ((\$6/86400.) >= FD && (\$6/86400.) <= LD) printf "%10.8f %10.8f %10.8f %6.4f\n", \$11, \$3*-1., \$6/86400., \$10}' \${in_event_file} | gmt plot \${strike1_bproj} \${strike1_brange} -Bxa10+l"Length along cross-section A-A' (km)" -Bya10+l"Depth (km)" -BneWS -Sc -W1.2+cl -C\${out_color_time_file} #HYPOSVI

#   in_event_file=/Users/cyoon/Documents/PuertoRico/EQT_20180101_20220101/Plots/EQT_20180101_20220101_3REAL_GrowClust_VELZHANG_depth_0_1461_seis_proj_13.txt
#   in_eqt_nc_file=/Users/cyoon/Documents/PuertoRico/EQT_20180101_20220101/Plots/EQT_20180101_20220101_3REAL_NonCluster_GrowClust_VELZHANG_depth_0_1461_seis_proj13.txt
   in_event_file=/Users/cyoon/Documents/PuertoRico/EQT_20180101_20230101/Plots/EQT_20180101_20230101_REAL_VELZHANG_GrowClust_kNN5000_stasis_depth_0_1826_seis_proj_13.txt
   in_eqt_nc_file=/Users/cyoon/Documents/PuertoRico/EQT_20180101_20230101/Plots/EQT_20180101_20230101_REAL_VELZHANG_NonCluster_GrowClust_kNN5000_stasis_depth_0_1826_seis_proj13.txt
   awk -v FD="0" -v LD="\${MOVIE_COL1}" '{if ((\$6/86400.) >= FD && (\$6/86400.) <= LD) printf "%10.8f %10.8f %6.4f\n", \$8, \$3*-1., \$7}' \${in_event_file} | gmt plot \${strike1_bproj} \${strike1_brange} -Bxa10+l"Length along cross-section A-A' (km)" -Bya10+l"Depth (km)" -BneWS -Sc -W0.001c,130 #GrowClust-past
   awk -v FD="\${MOVIE_COL0}" -v LD="\${MOVIE_COL1}" '{if ((\$6/86400.) >= FD && (\$6/86400.) <= LD) printf "%10.8f %10.8f %10.8f %6.4f\n", \$8, \$3*-1., \$6/86400., \$7}' \${in_event_file} | gmt plot \${strike1_bproj} \${strike1_brange} -Bxa10+l"Length along cross-section A-A' (km)" -Bya10+l"Depth (km)" -BneWS -Sc -W1.2+cl -C\${out_color_time_file} #GrowClust
   awk -v FD="0" -v LD="\${MOVIE_COL1}" '{if ((\$6/86400.) >= FD && (\$6/86400.) <= LD) printf "%10.8f %10.8f %6.4f\n", \$8, \$3*-1., \$7}' \${in_eqt_nc_file} | gmt plot \${strike1_bproj} \${strike1_brange} -Bxa10+l"Length along cross-section A-A' (km)" -Bya10+l"Depth (km)" -BneWS -Sc -W0.001c,130 #GrowClust_NonCluster-past
   awk -v FD="\${MOVIE_COL0}" -v LD="\${MOVIE_COL1}" '{if ((\$6/86400.) >= FD && (\$6/86400.) <= LD) printf "%10.8f %10.8f %10.8f %6.4f\n", \$8, \$3*-1., \$6/86400., \$7}' \${in_eqt_nc_file} | gmt plot \${strike1_bproj} \${strike1_brange} -Bxa10+l"Length along cross-section A-A' (km)" -Bya10+l"Depth (km)" -BneWS -Sc -W1.2+cl -C\${out_color_time_file} #GrowClust_NonCluster


##-----  NEW strike 98 -------
#   strike2_bproj=-JX40/15
#   strike2_brange=-R-40/40/-30/0
#   gmt basemap \${strike2_bproj} \${strike2_brange} -Ba10f5 -BneWS+g170+t"Date: \${DISP_DATE}" #-U
#
##   in_event_file=/Users/cyoon/Documents/PuertoRico/EQT_20180101_20220101/Plots/catalog_new_puerto_rico_20180101_20220101_depth_0_1461_seis_proj98.txt
##   awk -v FD="0" -v LD="\${MOVIE_COL1}" '{if ((\$10/86400.) >= FD && (\$10/86400.) <= LD) printf "%10.8f %10.8f %6.4f\n", \$4, \$3*-1., \$11}' \${in_event_file} | gmt plot \${strike2_bproj} \${strike2_brange} -Bxa10+l"Length along cross-section B-B' (km)" -Bya10+l"Depth (km)" -BneWS -Sc -W0.001c,white #Catalog-past
##   awk -v FD="\${MOVIE_COL0}" -v LD="\${MOVIE_COL1}" '{if ((\$10/86400.) >= FD && (\$10/86400.) <= LD) printf "%10.8f %10.8f %10.8f %6.4f\n", \$4, \$3*-1., \$10/86400., \$11}' \${in_event_file} | gmt plot \${strike2_bproj} \${strike2_brange} -Bxa10+l"Length along cross-section B-B' (km)" -Bya10+l"Depth (km)" -BneWS -Sc -W1.2+cl -C\${out_color_time_file} #Catalog
#
##   in_event_file=/Users/cyoon/Documents/PuertoRico/EQT_20180101_20220101/Plots/EQT_20180101_20220101_3REAL_HYPOINVERSE_VELZHANG_combined_depth_0_1461_seis_proj98.txt
##   awk -v FD="0" -v LD="\${MOVIE_COL1}" '{if ((\$6/86400.) >= FD && (\$6/86400.) <= LD) printf "%10.8f %10.8f %6.4f\n", \$8, \$3*-1., \$7}' \${in_event_file} | gmt plot \${strike2_bproj} \${strike2_brange} -Bxa10+l"Length along cross-section B-B' (km)" -Bya10+l"Depth (km)" -BneWS -Sc -W0.001c,white #HYPOINVERSE-past
##   awk -v FD="\${MOVIE_COL0}" -v LD="\${MOVIE_COL1}" '{if ((\$6/86400.) >= FD && (\$6/86400.) <= LD) printf "%10.8f %10.8f %10.8f %6.4f\n", \$8, \$3*-1., \$6/86400., \$7}' \${in_event_file} | gmt plot \${strike2_bproj} \${strike2_brange} -Bxa10+l"Length along cross-section B-B' (km)" -Bya10+l"Depth (km)" -BneWS -Sc -W1.2+cl -C\${out_color_time_file} #HYPOINVERSE
#
##   in_event_file=/Users/cyoon/Documents/PuertoRico/EQT_20180101_20220101/Plots/EQT_20180101_20220101_3REAL_HYPOSVI_VELZHANG_depth_0_1461_seis_proj98.txt
##   awk -v FD="0" -v LD="\${MOVIE_COL1}" '{if ((\$6/86400.) >= FD && (\$6/86400.) <= LD) printf "%10.8f %10.8f %6.4f\n", \$11, \$3*-1., \$10}' \${in_event_file} | gmt plot \${strike2_bproj} \${strike2_brange} -Bxa10+l"Length along cross-section B-B' (km)" -Bya10+l"Depth (km)" -BneWS -Sc -W0.001c,white #HYPOSVI-past
##   awk -v FD="\${MOVIE_COL0}" -v LD="\${MOVIE_COL1}" '{if ((\$6/86400.) >= FD && (\$6/86400.) <= LD) printf "%10.8f %10.8f %10.8f %6.4f\n", \$11, \$3*-1., \$6/86400., \$10}' \${in_event_file} | gmt plot \${strike2_bproj} \${strike2_brange} -Bxa10+l"Length along cross-section B-B' (km)" -Bya10+l"Depth (km)" -BneWS -Sc -W1.2+cl -C\${out_color_time_file} #HYPOSVI
#
##   in_event_file=/Users/cyoon/Documents/PuertoRico/EQT_20180101_20220101/Plots/EQT_20180101_20220101_3REAL_GrowClust_VELZHANG_depth_0_1461_seis_proj_98.txt
##   in_eqt_nc_file=/Users/cyoon/Documents/PuertoRico/EQT_20180101_20220101/Plots/EQT_20180101_20220101_3REAL_NonCluster_GrowClust_VELZHANG_depth_0_1461_seis_proj98.txt
#   in_event_file=/Users/cyoon/Documents/PuertoRico/EQT_20180101_20230101/Plots/EQT_20180101_20230101_REAL_VELZHANG_GrowClust_kNN5000_stasis_depth_0_1826_seis_proj_98.txt
#   in_eqt_nc_file=/Users/cyoon/Documents/PuertoRico/EQT_20180101_20230101/Plots/EQT_20180101_20230101_REAL_VELZHANG_NonCluster_GrowClust_kNN5000_stasis_depth_0_1826_seis_proj98.txt
#   awk -v FD="0" -v LD="\${MOVIE_COL1}" '{if ((\$6/86400.) >= FD && (\$6/86400.) <= LD) printf "%10.8f %10.8f %6.4f\n", \$8, \$3*-1., \$7}' \${in_event_file} | gmt plot \${strike2_bproj} \${strike2_brange} -Bxa10+l"Length along cross-section B-B' (km)" -Bya10+l"Depth (km)" -BneWS -Sc -W0.001c,130 #GrowClust-past
#   awk -v FD="\${MOVIE_COL0}" -v LD="\${MOVIE_COL1}" '{if ((\$6/86400.) >= FD && (\$6/86400.) <= LD) printf "%10.8f %10.8f %10.8f %6.4f\n", \$8, \$3*-1., \$6/86400., \$7}' \${in_event_file} | gmt plot \${strike2_bproj} \${strike2_brange} -Bxa10+l"Length along cross-section B-B' (km)" -Bya10+l"Depth (km)" -BneWS -Sc -W1.2+cl -C\${out_color_time_file} #GrowClust
#   awk -v FD="0" -v LD="\${MOVIE_COL1}" '{if ((\$6/86400.) >= FD && (\$6/86400.) <= LD) printf "%10.8f %10.8f %6.4f\n", \$8, \$3*-1., \$7}' \${in_eqt_nc_file} | gmt plot \${strike2_bproj} \${strike2_brange} -Bxa10+l"Length along cross-section B-B' (km)" -Bya10+l"Depth (km)" -BneWS -Sc -W0.001c,130 #GrowClust_NonCluster-past
#   awk -v FD="\${MOVIE_COL0}" -v LD="\${MOVIE_COL1}" '{if ((\$6/86400.) >= FD && (\$6/86400.) <= LD) printf "%10.8f %10.8f %10.8f %6.4f\n", \$8, \$3*-1., \$6/86400., \$7}' \${in_eqt_nc_file} | gmt plot \${strike2_bproj} \${strike2_brange} -Bxa10+l"Length along cross-section B-B' (km)" -Bya10+l"Depth (km)" -BneWS -Sc -W1.2+cl -C\${out_color_time_file} #GrowClust_NonCluster

gmt end
EOF

# 3. Run the movie
#gmt movie main.sh -C14.0ix7.6ix120 -Nm_EQT_20180101_20220101_Catalog_3d_strike13 -Ttimeframes.txt -Sbpre.sh -D6 -Pe+ap $opt
#gmt movie main.sh -C14.0ix7.6ix120 -Nm_EQT_20180101_20220101_HYPOINVERSE_VELZHANG_3d_strike13 -Ttimeframes.txt -Sbpre.sh -D6 -Pe+ap $opt
#gmt movie main.sh -C14.0ix7.6ix120 -Nm_EQT_20180101_20220101_HYPOSVI_VELZHANG_3d_strike13 -Ttimeframes.txt -Sbpre.sh -D6 -Pe+ap $opt
#gmt movie main.sh -C14.0ix7.6ix120 -Nm_EQT_20180101_20220101_GrowClust_VELZHANG_3d_strike13 -Ttimeframes.txt -Sbpre.sh -D6 -Pe+ap $opt
gmt movie main.sh -C14.0ix7.6ix120 -Np_EQT_20180101_20230101_REAL_VELZHANG_GrowClust_3d_strike13 -Ttimeframes.txt -Sbpre.sh -D6 -Pe+ap $opt

#gmt movie main.sh -C18.0ix7.6ix120 -Nm_EQT_20180101_20220101_Catalog_3d_strike98 -Ttimeframes.txt -Sbpre.sh -D6 -Pe+ap $opt
#gmt movie main.sh -C18.0ix7.6ix120 -Nm_EQT_20180101_20220101_HYPOINVERSE_VELZHANG_3d_strike98 -Ttimeframes.txt -Sbpre.sh -D6 -Pe+ap $opt
#gmt movie main.sh -C18.0ix7.6ix120 -Nm_EQT_20180101_20220101_HYPOSVI_VELZHANG_3d_strike98 -Ttimeframes.txt -Sbpre.sh -D6 -Pe+ap $opt
#gmt movie main.sh -C18.0ix7.6ix120 -Nm_EQT_20180101_20220101_GrowClust_VELZHANG_3d_strike98 -Ttimeframes.txt -Sbpre.sh -D6 -Pe+ap $opt
#gmt movie main.sh -C18.0ix7.6ix120 -Np_EQT_20180101_20230101_REAL_VELZHANG_GrowClust_3d_strike98 -Ttimeframes.txt -Sbpre.sh -D6 -Pe+ap $opt
