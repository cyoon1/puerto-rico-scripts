import datetime
import json
import utils_hypoinverse as utils_hyp
# Convert NLL (NonLinLoc) phase output file to HYPOINVERSE phase input (arc) file format


def get_nll_arr_time(split_line):
   year = int(split_line[6][0:4])
   month = int(split_line[6][4:6])
   day = int(split_line[6][6:8])
   hour = int(split_line[7][0:2])
   minute = int(split_line[7][2:4])
   arr_time_nosec = datetime.datetime(year, month, day, hour, minute)
   arr_delta = datetime.timedelta(seconds=float(split_line[8]))
   arr_time = arr_time_nosec + arr_delta
   return arr_time



#in_station_file = '../LargeAreaEQTransformer/station_list_edited.json'
#in_nll_phase_file = '../LargeAreaEQTransformer/PhaseLink/LargeAreaEQTransformer_nmin_det4_merge3_nucl4_Model2.nlloc'
#out_hinv_phase_file = '../LargeAreaEQTransformer/PhaseLink/LargeAreaEQTransformer_Model2_PhaseLink.phs'

in_station_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/station_list_edited.json'
#in_nll_phase_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/PhaseLink/FullEQTransformer_nmin_det4_merge3_nucl4_Model2.nlloc'
in_nll_phase_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/PhaseLink/FullEQTransformer_nmin_det7_merge6_nucl7_Model2.nlloc'
out_hinv_phase_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/PhaseLink/FullEQTransformer_Model2_PhaseLink.phs'


# Get station data
stations_ = json.load(open(in_station_file))

# Initial event location - set to first phase pick station location
# Initial event origin time - set to first phase pick time

ev_id = 200001 # EQTransformer convention for event id
nevents = 0 # count number of events
nph_proc = 0 # count number of phases for each event
fout = open(out_hinv_phase_file, 'w')
with open(in_nll_phase_file, 'r') as fnll:
   for line in fnll:

      if (line.strip() == ''): # boundary between events in NLL file
         fout.write("{:<62}".format(' ')+"%10d"%(ev_id)+'\n');
         print("ev_id: ", ev_id, ", nph_proc = ", nph_proc)
         ev_id += 1
         nevents += 1
         nph_proc = 0
      else:
         split_line = line.split()
         if (nph_proc == 0): # first phase line - set to event line as well
            sta = split_line[0]
            arr_time = get_nll_arr_time(split_line)

            origin_time = arr_time
            origin_time_second = utils_hyp.get_rounded_seconds(origin_time)
            [lat_int, lat_char, lat_min] = utils_hyp.output_lat_hypoinverse_format(stations_[sta]['coords'][0]) # only for first phase line
            [lon_int, lon_char, lon_min] = utils_hyp.output_lon_hypoinverse_format(stations_[sta]['coords'][1]) # only for first phase line
            depth = 5.0 # same default depth as EQTransformer associator
            depth_out = round(100*float(depth))
            fout.write(('%4d%02d%02d%02d%02d%04d%2d%1s%4s%3d%1s%4s%5d%3d\n') % (origin_time.year, origin_time.month, origin_time.day, origin_time.hour, origin_time.minute, origin_time_second, lat_int, lat_char, lat_min, lon_int, lon_char, lon_min, depth_out, 0))

            net = split_line[1]
            ph = split_line[4]
            [chan, p_remark, s_remark, p_res, s_res, p_weight, s_weight, p_arr_time_sec, s_arr_time_sec] = utils_hyp.output_phase_hypoinverse_format(
               ph, arr_time, fm_p='I', fm_s='E', res=None, weight=None)
            nph_proc += 1
            fout.write(('%-5s%2s  %3s %2s %1d%4d%02d%02d%02d%02d%5d%4s   %5d%2s %1d%4s\n') % (sta, net, chan, p_remark, p_weight, arr_time.year, arr_time.month, arr_time.day, arr_time.hour, arr_time.minute, p_arr_time_sec, p_res, s_arr_time_sec, s_remark, s_weight, s_res))

         else: # phase line, but not the first
            sta = split_line[0]
            arr_time = get_nll_arr_time(split_line)
            net = split_line[1]
            ph = split_line[4]
            [chan, p_remark, s_remark, p_res, s_res, p_weight, s_weight, p_arr_time_sec, s_arr_time_sec] = utils_hyp.output_phase_hypoinverse_format(
               ph, arr_time, fm_p='I', fm_s='E', res=None, weight=None)
            nph_proc += 1
            fout.write(('%-5s%2s  %3s %2s %1d%4d%02d%02d%02d%02d%5d%4s   %5d%2s %1d%4s\n') % (sta, net, chan, p_remark, p_weight, arr_time.year, arr_time.month, arr_time.day, arr_time.hour, arr_time.minute, p_arr_time_sec, p_res, s_arr_time_sec, s_remark, s_weight, s_res))

fout.close()
print("nevents: ", nevents)
