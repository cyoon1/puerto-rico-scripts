# dtcc file input: network+station code (for HYPODD input from HYPOINVERSE)
# dtcc file output: only station code (max 5 characters for GrowClust input)

#base_dir = '/media/yoon/INT01/Ferndale2022/EQT_20221220_20221227/REAL/'
#in_dtcc_file = base_dir+'HYPODD/EQT_20221220_20221227_mendo_dtcc_net.txt'
#out_dtcc_file = base_dir+'GrowClust/IN/EQT_20221220_20221227_mendo_dtcc.txt'
#base_dir = '/media/yoon/INT01/Ferndale2022/EQT_20221220_20230103/REAL/'
#in_dtcc_file = base_dir+'HYPODD/EQT_20221220_20230103_hyposvi_mendo_dtcc_net.txt'
#out_dtcc_file = base_dir+'GrowClust/IN/EQT_20221220_20230103_hyposvi_mendo_dtcc.txt'

#base_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200108/'
##in_dtcc_file = base_dir+'REAL_VELZHANG/HYPODD_TEST10/EQT_20200107_20200108_combined_hyposvi_pr_dtcc_net.txt'
##out_dtcc_file = base_dir+'REAL_VELZHANG/GrowClust_TEST10/IN/VELZHANG/EQT_20200107_20200108_combined_hyposvi_pr_dtcc.txt'
#in_dtcc_file = base_dir+'stasis_out/VELZHANG/dtimes_hsvi_EQT_20200107_20200108_VELZHANG.grow'
#out_dtcc_file = base_dir+'REAL_VELZHANG/GrowClust_TEST10/IN/VELZHANG/dtimes_hsvi_EQT_20200107_20200108_VELZHANG.txt'

base_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20230101/'
in_dtcc_file = base_dir+'stasis_out/VELZHANG/kNN5000_dtimes_hsvi_EQT_20180101_20230101_VELZHANG.grow'
out_dtcc_file = base_dir+'GrowClust/IN/VELZHANG/kNN5000_dtimes_hsvi_EQT_20180101_20230101_VELZHANG.txt'
#in_dtcc_file = base_dir+'stasis_out/VELZHANG/kNN2000_dtimes_hsvi_EQT_20180101_20230101_VELZHANG.grow'
#out_dtcc_file = base_dir+'GrowClust/IN/VELZHANG/kNN2000_dtimes_hsvi_EQT_20180101_20230101_VELZHANG.txt'

fout = open(out_dtcc_file,'w')
with open(in_dtcc_file,'r') as fin:
   for line in fin:
      if (line[0] == '#'):
         fout.write(line)
      else:
         fout.write(line[2:]) # skip the first 2 letters (network code)
fout.close()
