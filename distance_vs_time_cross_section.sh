#!/bin/bash

# Plot Puerto Rico earthquakes on a map

#gmt gmtset BASEMAP_TYPE plain
#gmt gmtset HEADER_FONT_SIZE 24p
#gmt gmtset LABEL_FONT_SIZE 16p # smaller font for labels
##gmt gmtset PLOT_DEGREE_FORMAT +DF
#gmt gmtset PLOT_DEGREE_FORMAT -DF
#gmt gmtset GRID_PEN_PRIMARY 0.1p
#
gmt gmtset MAP_FRAME_TYPE plain     # map outline - single line with ticks
gmt gmtset FORMAT_GEO_MAP DD        # labels on map - decimal degrees

#gmt gmtset LABEL_FONT_SIZE 20p
#gmt gmtset FONT_ANNOT 20p
gmt gmtset LABEL_FONT_SIZE 48p
gmt gmtset FONT_ANNOT 48p
#gmt gmtset LABEL_FONT_SIZE 32p #poster
#gmt gmtset FONT_ANNOT 32p #poster

out_color_filename=depthcolors.cpt
out_color_file=../Plots/${out_color_filename}

# HYPOSVI
#in_file_strike1=../EQT_20180101_20220101/Plots/EQT_20180101_20220101_3REAL_HYPOSVI_VELZHANG_depth_0_1461_seis_proj13.txt
#out_map_strike1=../EQT_20180101_20220101/Plots/DT_EQT_20180101_20220101_3REAL_HYPOSVI_VELZHANG_depth_0_1461_seis_proj13
#in_file_strike2=../EQT_20180101_20220101/Plots/EQT_20180101_20220101_3REAL_HYPOSVI_VELZHANG_depth_0_1461_seis_proj98.txt
#out_map_strike2=../EQT_20180101_20220101/Plots/DT_EQT_20180101_20220101_3REAL_HYPOSVI_VELZHANG_depth_0_1461_seis_proj98
#in_file_depth=../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_HYPOSVI_VELZHANG_Catalog_diam.txt
#out_map_depth=../EQT_20180101_20220101/Plots/DT_EQT_20180101_20220101_3REAL_HYPOSVI_VELZHANG_depth_dim

# GrowClust
#in_file_strike1=../EQT_20180101_20220101/Plots/EQT_20180101_20220101_3REAL_GrowClust_VELZHANG_depth_0_1461_seis_proj_13.txt
#in_nc_file_strike1=../EQT_20180101_20220101/Plots/EQT_20180101_20220101_3REAL_NonCluster_GrowClust_VELZHANG_depth_0_1461_seis_proj13.txt
#out_map_strike1=../EQT_20180101_20220101/Plots/DT_EQT_20180101_20220101_3REAL_GrowClust_VELZHANG_depth_0_1461_seis_proj_13
#in_file_strike2=../EQT_20180101_20220101/Plots/EQT_20180101_20220101_3REAL_GrowClust_VELZHANG_depth_0_1461_seis_proj_98.txt
#in_nc_file_strike2=../EQT_20180101_20220101/Plots/EQT_20180101_20220101_3REAL_NonCluster_GrowClust_VELZHANG_depth_0_1461_seis_proj98.txt
#out_map_strike2=../EQT_20180101_20220101/Plots/DT_EQT_20180101_20220101_3REAL_GrowClust_VELZHANG_depth_0_1461_seis_proj_98
#in_file_depth=../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_GrowClust_VELZHANG_maxdt2_diam.txt
#in_nc_file_depth=../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_NonCluster_GrowClust_VELZHANG_maxdt2_diam.txt
#out_map_depth=../EQT_20180101_20220101/Plots/DT_EQT_20180101_20220101_3REAL_GrowClust_VELZHANG_depth_dim

#in_file_strike1=../EQT_20180101_20230101/Plots/catalog_new_puerto_rico_20180101_20230101_depth_0_1826_seis_proj13.txt
#out_map_strike1=../EQT_20180101_20230101/Plots/DT_catalog_new_puerto_rico_20180101_20230101_depth_0_1826_seis_proj13
#in_file_strike2=../EQT_20180101_20230101/Plots/catalog_new_puerto_rico_20180101_20230101_depth_0_1826_seis_proj98.txt
#out_map_strike2=../EQT_20180101_20230101/Plots/DT_catalog_new_puerto_rico_20180101_20230101_depth_0_1826_seis_proj98
#in_file_depth=../Catalog/EQT_20180101_20230101/catalog_new_puerto_rico_20180101_20230101_download20230206_diam.txt
#out_map_depth=../EQT_20180101_20230101/Plots/DT_catalog_new_puerto_rico_20180101_20230101_depth_dim

in_file_strike1=../EQT_20180101_20230101/Plots/EQT_20180101_20230101_REAL_VELZHANG_GrowClust_kNN5000_stasis_depth_0_1826_seis_proj_13.txt
in_nc_file_strike1=../EQT_20180101_20230101/Plots/EQT_20180101_20230101_REAL_VELZHANG_NonCluster_GrowClust_kNN5000_stasis_depth_0_1826_seis_proj13.txt
out_map_strike1=../EQT_20180101_20230101/Plots/DT_EQT_20180101_20230101_REAL_VELZHANG_GrowClust_kNN5000_stasis_depth_0_1826_seis_proj_13
in_file_strike2=../EQT_20180101_20230101/Plots/EQT_20180101_20230101_REAL_VELZHANG_GrowClust_kNN5000_stasis_depth_0_1826_seis_proj_98.txt
in_nc_file_strike2=../EQT_20180101_20230101/Plots/EQT_20180101_20230101_REAL_VELZHANG_NonCluster_GrowClust_kNN5000_stasis_depth_0_1826_seis_proj98.txt
out_map_strike2=../EQT_20180101_20230101/Plots/DT_EQT_20180101_20230101_REAL_VELZHANG_GrowClust_kNN5000_stasis_depth_0_1826_seis_proj_98
in_file_depth=../Catalog/EQT_20180101_20230101/EQT_20180101_20230101_REAL_VELZHANG_GrowClust_kNN5000_stasis_diam.txt
in_nc_file_depth=../Catalog/EQT_20180101_20230101/EQT_20180101_20230101_REAL_VELZHANG_NonCluster_GrowClust_kNN5000_stasis_diam.txt
out_map_depth=../EQT_20180101_20230101/Plots/DT_EQT_20180101_20230101_REAL_VELZHANG_GrowClust_kNN5000_stasis_depth_dim
mag_thresh=3

# Time slice ranges

FIRST_DAY=726 # 2019-12-28
LAST_DAY=750 # 2020-01-21
SCALE_FACTOR=1 # zoom in

cat << EOF >| xdateszoom.txt
726 a 2019-12-28
#732 a 2020-01-03
738 a 2020-01-09
#744 a 2020-01-15
750 a 2020-01-21
EOF

#FIRST_DAY=750 # 2020-01-21
#LAST_DAY=852 # 2020-05-02
#SCALE_FACTOR=2

#FIRST_DAY=852 # 2020-05-02
#LAST_DAY=894 # 2020-06-13
#SCALE_FACTOR=1 # zoom in

#FIRST_DAY=894 # 2020-06-13
#LAST_DAY=995 # 2020-09-22
#SCALE_FACTOR=2

#FIRST_DAY=995 # 2020-09-22
#LAST_DAY=1088 # 2020-12-24
#SCALE_FACTOR=2

#FIRST_DAY=1088 # 2020-12-24
#LAST_DAY=1102 # 2021-01-07
#SCALE_FACTOR=1

#FIRST_DAY=1102 # 2021-01-07
#LAST_DAY=1270 # 2021-06-24
#SCALE_FACTOR=2

#FIRST_DAY=1270 # 2021-06-24
#LAST_DAY=1331 # 2021-08-24
#SCALE_FACTOR=1

#FIRST_DAY=1270 # 2021-06-24
#LAST_DAY=1461 # 2022-01-01
#SCALE_FACTOR=2

FIRST_DAY=726 # 2019-12-28
#LAST_DAY=1461 # 2022-01-01
LAST_DAY=1826 # 2023-01-01
SCALE_FACTOR=8 # entire time span

cat << EOF >| xdateszoom.txt
730 a 2020-01-01
#790 a 2020-03-01
#851 a 2020-05-01
#912 a 2020-07-01
#974 a 2020-09-01
#1035 a 2020-11-01
1096 a 2021-01-01
#1155 a 2021-03-01
#1216 a 2021-05-01
#1277 a 2021-07-01
#1339 a 2021-09-01
#1400 a 2021-11-01
1461 a 2022-01-01
1826 a 2023-01-01
EOF

let NUMDAYS=(${LAST_DAY}-${FIRST_DAY}+1)/${SCALE_FACTOR}
echo ${NUMDAYS}

# Cross section parameters
strike1_bproj=-JX${NUMDAYS}/24
strike1_brange=-R${FIRST_DAY}/${LAST_DAY}/-30/30
strike2_bproj=-JX${NUMDAYS}/32
strike2_brange=-R${FIRST_DAY}/${LAST_DAY}/-40/40
depth_bproj=-JX${NUMDAYS}/12
depth_brange=-R${FIRST_DAY}/${LAST_DAY}/-30/0


##### COLOR BY DEPTH #####

gmt makecpt -Cinferno -I -T0/20/0.01 > ${out_color_file}

gmt begin ${out_map_strike1}_${FIRST_DAY}_${LAST_DAY}
   gmt basemap ${strike1_bproj} ${strike1_brange} -BneWS #-Ba15f -U
#   sort -nk6,6 ${in_file_strike1} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($6/86400.) >= FD && ($6/86400.) <= LD) print $6/86400., $8, $3, $5*$5*0.01 + 0.0200}' | gmt plot ${strike1_bproj} ${strike1_brange} -Bxa2+l"Event time along cross-section A-A'" -Bya2+l"Distance along A-A' (km)" -BneWS -Sc -W0.01c -G255 -C${out_color_file} # HYPOINVERSE
#   sort -nk6,6 ${in_file_strike1} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($6/86400.) >= FD && ($6/86400.) <= LD) print $6/86400., $11, $3, $10}' | gmt plot ${strike1_bproj} ${strike1_brange} -Bxa100+l"Number of days since 2018-01-01" -Bya10+l"Distance along A-A' (km)" -BneWS -Sy -W1.2+cl -C${out_color_file} # HYPOSVI 
#   sort -nk6,6 ${in_file_strike1} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($6/86400.) >= FD && ($6/86400.) <= LD) print $6/86400., $11, $3, $10}' | gmt plot ${strike1_bproj} ${strike1_brange} -Bpxcxdateszoom.txt+l"Date" -Bya10+l"Distance along A-A' (km)" -BneWS -Sy -W1.2+cl -C${out_color_file} # HYPOSVI 

#   sort -nk10,10 ${in_file_strike1} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($10/86400.) >= FD && ($10/86400.) <= LD) print $10/86400., $4, $3, $11}' | gmt plot ${strike1_bproj} ${strike1_brange} -Bpxcxdateszoom.txt+a0+l"Date" -Bya10+l"Distance along A-A' (km)" -BneWS -Sy -W1.2+cl -C${out_color_file} # ComCat

   sort -nk6,6 ${in_file_strike1} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($6/86400.) >= FD && ($6/86400.) <= LD) print $6/86400., $8, $3, $7}' | gmt plot ${strike1_bproj} ${strike1_brange} -Bpxcxdateszoom.txt+a0+l"Date" -Bya10+l"Distance along A-A' (km)" -BneWS -Sy -W1.2+cl -C${out_color_file} # GrowClust
   sort -nk6,6 ${in_nc_file_strike1} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($6/86400.) >= FD && ($6/86400.) <= LD) print $6/86400., $8, $3, $7}' | gmt plot ${strike1_bproj} ${strike1_brange} -Bpxcxdateszoom.txt+a0+l"Date" -Bya10+l"Distance along A-A' (km)" -BneWS -Sy -W1.2+cl -C${out_color_file} # GrowClust-NonCluster
#   gmt colorbar -C${out_color_file} -Dx11.9i/2.0i/2.5i/0.15i -Bxaf+l"Depth (km)"
gmt end show

gmt begin ${out_map_strike2}_${FIRST_DAY}_${LAST_DAY}
   gmt basemap ${strike2_bproj} ${strike2_brange} -BneWS #-Ba15f -U
#   sort -nk6,6 ${in_file_strike2} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($6/86400.) >= FD && ($6/86400.) <= LD) print $6/86400., $8, $3, $5*$5*0.01 + 0.0200}' | gmt plot ${strike2_bproj} ${strike2_brange} -Bxa2+l"Event time along cross-section B-B'" -Bya2+l"Distance along B-B' (km)" -BneWS -Sc -W0.01c -G255 -C${out_color_file} # HYPOINVERSE
#   sort -nk6,6 ${in_file_strike2} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($6/86400.) >= FD && ($6/86400.) <= LD) print $6/86400., $11, $3, $10}' | gmt plot ${strike2_bproj} ${strike2_brange} -Bxa100+l"Number of days since 2018-01-01" -Bya10+l"Distance along B-B' (km)" -BneWS -Sy -W1.2+cl -C${out_color_file} # HYPOSVI 
#   sort -nk6,6 ${in_file_strike2} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($6/86400.) >= FD && ($6/86400.) <= LD) print $6/86400., $11, $3, $10}' | gmt plot ${strike2_bproj} ${strike2_brange} -Bpxcxdateszoom.txt+l"Date" -Bya10+l"Distance along B-B' (km)" -BneWS -Sy -W1.2+cl -C${out_color_file} # HYPOSVI 

#   sort -nk10,10 ${in_file_strike2} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($10/86400.) >= FD && ($10/86400.) <= LD) print $10/86400., $4, $3, $11}' | gmt plot ${strike2_bproj} ${strike2_brange} -Bpxcxdateszoom.txt+a0+l"Date" -Bya10+l"Distance along B-B' (km)" -BneWS -Sy -W1.2+cl -C${out_color_file} # ComCat

   sort -nk6,6 ${in_file_strike2} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($6/86400.) >= FD && ($6/86400.) <= LD) print $6/86400., $8, $3, $7}' | gmt plot ${strike2_bproj} ${strike2_brange} -Bpxcxdateszoom.txt+a0+l"Date" -Bya10+l"Distance along B-B' (km)" -BneWS -Sy -W1.2+cl -C${out_color_file} # GrowClust
   sort -nk6,6 ${in_nc_file_strike2} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($6/86400.) >= FD && ($6/86400.) <= LD) print $6/86400., $8, $3, $7}' | gmt plot ${strike2_bproj} ${strike2_brange} -Bpxcxdateszoom.txt+a0+l"Date" -Bya10+l"Distance along B-B' (km)" -BneWS -Sy -W1.2+cl -C${out_color_file} # GrowClust-NonCluster
#   gmt colorbar -C${out_color_file} -Dx11.9i/2.0i/2.5i/0.15i -Bxaf+l"Depth (km)"
gmt end show

gmt begin ${out_map_depth}_${FIRST_DAY}_${LAST_DAY}
   gmt basemap ${depth_bproj} ${depth_brange} -BneWS #-Ba15f -U
#   sort -nk1,1 ${in_file_depth} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $1/86400., $4*(-1), $4, $10}' | gmt plot ${depth_bproj} ${depth_brange} -Bxa100+l"Number of days since 2018-01-01" -Bya10+l"Depth (km)" -BneWS -Sy -W1.2+cl -C${out_color_file} # HYPOSVI 
#   sort -nk1,1 ${in_file_depth} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $1/86400., $4*(-1), $4, $10}' | gmt plot ${depth_bproj} ${depth_brange} -Bpxcxdateszoom.txt+l"Date" -Bya10+l"Depth (km)" -BneWS -Sy -W1.2+cl -C${out_color_file} # HYPOSVI 
#   sort -nk1,1 ${in_file_depth} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $1/86400., $4*(-1), $4, $7}' | gmt plot ${depth_bproj} ${depth_brange} -Bpxcxdateszoom.txt+a0+l"Date" -Bya10+l"Depth (km)" -BneWS -Sy -W1.2+cl -C${out_color_file} # GrowClust
#   sort -nk1,1 ${in_nc_file_depth} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $1/86400., $4*(-1), $4, $7}' | gmt plot ${depth_bproj} ${depth_brange} -Bpxcxdateszoom.txt+a0+l"Date" -Bya10+l"Depth (km)" -BneWS -Sy -W1.2+cl -C${out_color_file} # GrowClust_NonCluster

#   sort -nk2,2 ${in_file_depth} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($2/86400.) >= FD && ($2/86400.) <= LD) print $2/86400., $5*(-1), $5, $8}' | gmt plot ${depth_bproj} ${depth_brange} -Bpxcxdateszoom.txt+a0+l"Date" -Bya10+l"Depth (km)" -BneWS -Sy -W1.2+cl -C${out_color_file} # ComCat

   sort -nk2,2 ${in_file_depth} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($2/86400.) >= FD && ($2/86400.) <= LD) print $2/86400., $5*(-1), $5, $8}' | gmt plot ${depth_bproj} ${depth_brange} -Bpxcxdateszoom.txt+a0+l"Date" -Bya10+l"Depth (km)" -BneWS -Sy -W1.2+cl -C${out_color_file} # GrowClust
   sort -nk2,2 ${in_nc_file_depth} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} -v MT=${mag_thresh} '{if (($2/86400.) >= FD && ($2/86400.) <= LD && $6 >= MT) print $2/86400., $5*(-1), $5, $8}' | gmt plot ${depth_bproj} ${depth_brange} -Bpxcxdateszoom.txt+a0+l"Date" -Bya10+l"Depth (km)" -BneWS -Sy -W1.2+cl -C${out_color_file} # GrowClust_NonCluster
#   gmt colorbar -C${out_color_file} -Dx11.9i/2.0i/2.5i/0.15i -Bxaf+l"Depth (km)"
gmt end show
