#!/bin/bash

# Plot Puerto Rico earthquakes on a map

#gmt gmtset BASEMAP_TYPE plain
#gmt gmtset HEADER_FONT_SIZE 24p
#gmt gmtset LABEL_FONT_SIZE 16p # smaller font for labels
##gmt gmtset PLOT_DEGREE_FORMAT +DF
#gmt gmtset PLOT_DEGREE_FORMAT -DF
#gmt gmtset GRID_PEN_PRIMARY 0.1p
#
gmt gmtset MAP_FRAME_TYPE plain     # map outline - single line with ticks
gmt gmtset FORMAT_GEO_MAP DD        # labels on map - decimal degrees

#gmt gmtset LABEL_FONT_SIZE 20p
#gmt gmtset FONT_ANNOT 20p
gmt gmtset LABEL_FONT_SIZE 16p
gmt gmtset FONT_ANNOT 16p
#gmt gmtset LABEL_FONT_SIZE 24p #poster
#gmt gmtset FONT_ANNOT 24p #poster

in_dir=../Catalog/EQT_20180101_20230101
in_fault_dir=../Catalog/Faults
out_dir=../EQT_20180101_20230101/Plots

out_color_file=${out_dir}/depthcolors.cpt

#in_sta_file=../Catalog/FullEQTransformer_pr_stations_large.txt
#in_temp_sta_file=../Catalog/FullEQTransformer_pr_stations_large_temporary.txt
#in_IU_sta_file=../Catalog/FullEQTransformer_pr_stations_large_IU.txt

in_temp_sta_file=../Catalog/FullEQTransformer_pr_stations_large_temporary.txt
in_IU_sta_file=../Catalog/FullEQTransformer_pr_stations_large_IU.txt
in_sta_file=${in_dir}/EQT_20180101_20230101_pr_stations.txt

in_fault_file=${in_fault_dir}/gem_active_faults_harmonized.gmt
in_carib_fault_file=${in_fault_dir}/flt6bg/flt6bg.gmt
in_pm_fault_file=${in_fault_dir}/PuntaMont_trace.txt

#out_map_fault_file=../EQT_20180101_20220101/Plots/EQT_20180101_20220101_pr_faults_map
out_map_fault_file=${out_dir}/EQT_20180101_20230101_pr_faults_map

#in_cat_file=../Catalog/EQT_20180101_20220101/catalog_new_puerto_rico_20180101_20220101_download20220111_diam.txt
in_cat_file=${in_dir}/catalog_new_puerto_rico_20180101_20230101_download20230206_diam.txt
gmt makecpt -Cinferno -I -T0/20/0.01 > ${out_color_file}
FIRST_DAY=0 # 2018-01-01
#LAST_DAY=1461 # 2022-01-01
LAST_DAY=1826 # 2023-01-01


#min_lat=17
#max_lat=19
##max_lat=20
#min_lon=-68
##min_lon=-69
#max_lon=-65
##max_lon=-64

#min_lat=17.5
#max_lat=18.5
#min_lon=-67.5
#max_lon=-66.0

min_lat=17.6
max_lat=18.3
min_lon=-67.3
max_lon=-66.4

reg=-R${min_lon}/${max_lon}/${min_lat}/${max_lat}
proj=-JM16
echo ${reg}
lat_lon_spacing=0.3

#region_inset=-R-75/-62/15/22
#region_inset=-R-105/-52/5/32
region_inset=-R-85/-60/8/27
projection_inset=-JM3.0i



gmt begin ${out_map_fault_file}
   gmt basemap ${proj} ${reg} -Ba${lat_lon_spacing}fg -BneWS #-U
   gmt coast ${proj} ${reg} -W0.25p,black -Na -G170 -Slightskyblue -Lg-66.6/17.68+c-66.5/17.68+w20+f+lkm
#   gmt grdimage ${proj} ${reg} pr_bathy.nc -Cterra
#   sort -nk1,1 ${in_cat_file} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $2, $3, $4, $7/6.26172 + 0.01}' | gmt plot ${proj} ${reg} -Sc -W0.3+cl -BneWS -: -C${out_color_file}
   sort -nk2,2 ${in_cat_file} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($2/86400.) >= FD && ($2/86400.) <= LD) print $3, $4, $5, $8/6.26172 + 0.01}' | gmt plot ${proj} ${reg} -Sc -W0.3+cl -BneWS -: -C${out_color_file}
   awk '{print $2 " " $3 " " $4}' ${in_sta_file} | gmt plot ${proj} ${reg} -Si0.4 -W0.1c,black -Gwhite -:
   awk '{print $2 " " $3 " " $4}' ${in_temp_sta_file} | gmt plot ${proj} ${reg} -Si0.4 -W0.1c,blue -Gyellow -:
   awk '{print $2 " " $3 " " $4}' ${in_IU_sta_file} | gmt plot ${proj} ${reg} -Si0.4 -W0.1c,magenta -Gyellow -:
   awk '{print $2 " " $3 " " $1}' ${in_sta_file} | gmt text ${proj} ${reg} -D0/0.35 -F+f10p,Helvetica -: # station names

#   gmt plot ${proj} ${reg} ${in_fault_file} -Sf+0.9+l+t -W0.03c,black -G0
   gmt plot ${proj} ${reg} ${in_fault_file} -Sf1/0.01+l -W0.06c,black -G0
#   gmt plot ${proj} ${reg} ${in_carib_fault_file} -Sf1/0.01+l -W0.03c,black -G0
   gmt plot ${proj} ${reg} ${in_pm_fault_file} -Sf1/0.01+l -W0.06c,blue -G0

   gmt plot ${proj} ${reg} -W6,red << EOF
${min_lon} ${min_lat}
${min_lon} ${max_lat}
${max_lon} ${max_lat}
${max_lon} ${min_lat}
${min_lon} ${min_lat}
EOF
   # M6.4 mainshock mechanism
   gmt meca ${proj} ${reg} -Sm0.8c+f14 -A -L0.03c,black -: -C${out_color_file} << EOF
17.88383 -66.79244 3.642 -3.7505 4.3290 -0.5786 -0.4846 -1.2931 2.6375 25
EOF

#   # Add inset
##   gmt basemap ${region_inset} ${projection_inset} -Bnews -X0.05i -Y0.05i
#   gmt basemap ${region_inset} ${projection_inset} -Bnews -X6.4i -Y0.05i
#   gmt coast ${region_inset} ${projection_inset} -Dl -G255 -N1 -W0.25 -Slightskyblue
#   gmt plot ${region_inset} ${projection_inset} -W2,blue << EOF
#${min_lon} ${min_lat}
#${min_lon} ${max_lat}
#${max_lon} ${max_lat}
#${max_lon} ${min_lat}
#${min_lon} ${min_lat}
#EOF
gmt end show

