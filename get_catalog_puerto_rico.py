from obspy import read
from obspy import UTCDateTime
from obspy.clients.fdsn import Client
import os

#def main():

# https://docs.obspy.org/packages/obspy.clients.fdsn.html#module-obspy.clients.fdsn
# https://docs.obspy.org/packages/autogen/obspy.clients.fdsn.client.Client.get_events.html#obspy.clients.fdsn.client.Client.get_events
# https://docs.obspy.org/packages/autogen/obspy.core.event.Catalog.write.html#obspy.core.event.Catalog.write

client = Client("USGS")
min_lat = 17
#max_lat = 19
max_lat = 20
min_lon = -68
max_lon = -65

#t_start = UTCDateTime("2020-01-07T00:00:00")
#t_end = UTCDateTime("2020-01-14T00:00:00")
#t_start = UTCDateTime("2018-01-01T00:00:00")
#t_start = UTCDateTime("2018-12-14T23:00:00")
#t_start = UTCDateTime("2020-01-04T09:20:00")
#t_end = UTCDateTime("2021-02-01T00:00:00")

#min_mag = 5
#min_mag = 0
min_mag = -2
#include_phases=True

#out_dir = '../Catalog/CatalogEvents/'
#out_dir = '../LargeAreaEQTransformer/CatalogEvents/'
#out_dir = '/media/yoon/INT01/PuertoRico/FullEQTransformer/CatalogEvents/'


#t_start = UTCDateTime("2020-01-07T00:00:00")
#t_end = UTCDateTime("2020-01-14T00:00:00")
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/CatalogEventsDownload20210601/'

#t_start = UTCDateTime("2019-12-28T00:00:00")
#t_end = UTCDateTime("2020-01-14T00:00:00")
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/CatalogEventsDownload20210608/'

## 20994 events - over 20000 event maximum - need to run in parts
#t_start = UTCDateTime("2018-01-01T00:00:00")
#t_end = UTCDateTime("2021-06-01T00:00:00")
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/CatalogEventsDownload20210610/'

## 20994 events - over 20000 event maximum - need to run in parts
#t_start = UTCDateTime("2018-01-01T00:00:00")
#t_end = UTCDateTime("2021-10-01T00:00:00")
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/CatalogEventsDownload20211020/'

## 23546 events - over 20000 event maximum - need to run in parts
#t_start = UTCDateTime("2018-01-01T00:00:00")
#t_end = UTCDateTime("2022-01-01T00:00:00")
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/CatalogEventsDownload20220111/'

#t_start = UTCDateTime("2020-01-07T00:00:00")
#t_end = UTCDateTime("2020-01-08T00:00:00")
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200108/CatalogEventsDownload20230131/'

# Need to run in parts
t_start = UTCDateTime("2018-01-01T00:00:00")
#t_end = UTCDateTime("2019-01-01T00:00:00")
#t_start = UTCDateTime("2019-01-01T00:00:00")
#t_end = UTCDateTime("2020-01-01T00:00:00")
#t_start = UTCDateTime("2020-01-01T00:00:00")
#t_start = UTCDateTime("2020-01-09T17:49:40")
#t_end = UTCDateTime("2021-01-01T00:00:00")
#t_start = UTCDateTime("2021-01-01T00:00:00")
#t_end = UTCDateTime("2022-01-01T00:00:00")
#t_start = UTCDateTime("2022-01-01T00:00:00")
t_end = UTCDateTime("2023-01-01T00:00:00")
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20230101/CatalogEventsDownload20230131/'
out_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20230101/CatalogEventsDownload20230211/'

if not os.path.exists(out_dir):
   os.makedirs(out_dir)

#-------------------------------------
# First, get list of events in catalog

#out_file = '../Catalog/test_puerto_rico_catalog.txt'
#cat = client.get_events(
#   starttime=t_start, endtime=t_end,
#   minlatitude=min_lat, maxlatitude=max_lat,
#   minlongitude=min_lon, maxlongitude=max_lon,
#   orderby='time-asc', filename=out_file)

cat = client.get_events(
   starttime=t_start, endtime=t_end,
   minlatitude=min_lat, maxlatitude=max_lat,
   minlongitude=min_lon, maxlongitude=max_lon,
   orderby='time-asc',
   minmagnitude=min_mag)
print("Number of events in catalog: ", len(cat))

#-------------------------------------
# Next, query each event by its event ID
# This is needed to get catalog phase picks and focal mechanism/moment tensor
# Save the event ID specific info to file (QUAKEML format)
no_event_list = []
for ev in cat:
   ev_id = ev.resource_id.id.split("eventid=")[1].split("&")[0]
   try:
      curr_ev = client.get_events(eventid=ev_id)
   except Exception:
      no_event_list.append(ev_id)
      print("WARNING: Unable to get catalog event: ", ev_id)
      continue
   print(curr_ev[0])
   print(curr_ev[0].focal_mechanisms)
   out_ev_file = out_dir+ev_id+'.xml'
   curr_ev[0].write(out_ev_file, format='QUAKEML')

print("Number of Events not downloaded: ", len(no_event_list))
print("Events not downloaded: ", no_event_list)



#if __name__ == "__main__":
#   main()
