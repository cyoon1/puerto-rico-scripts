# dtcc file input: dtcc file
# dtcc file output: dtcc file with increased CC threshold

in_dtcc_file = '../LargeAreaEQTransformer/20200107_20200114_Model1/HYPODD_OLD/maxdt2_clean_out_LargeAreaEQTransformer_puerto_rico_dt_cc.txt'
out_dtcc_file = '../LargeAreaEQTransformer/20200107_20200114_Model1/HYPODD_OLD/maxdt2_cc070_clean_out_LargeAreaEQTransformer_puerto_rico_dt_cc.txt'

cc_thresh = 0.7

fout = open(out_dtcc_file,'w')
with open(in_dtcc_file,'r') as fin:
   dtcc_list = []
   ev_line = ''
   for line in fin:
      if (line[0] == '#'):
         # From the previous event pair
         if (len(dtcc_list) > 0):
            fout.write(ev_line)
            for dtpair in dtcc_list:
               fout.write(dtpair)
         ev_line = line
         dtcc_list.clear()
      else:
         split_line = line.split()
         val_cc = float(split_line[2])
         if (abs(val_cc) <= cc_thresh):
            dtcc_list.append(line)

# Last remaining event
if (len(dtcc_list) > 0):
   fout.write(ev_line)
   for dtpair in dtcc_list:
      fout.write(dtpair)

fout.close()
