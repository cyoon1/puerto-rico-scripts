#dv='cuda:5'
dv='cuda'
# $ gpustat (find which GPUs free)
# $ export CUDA_VISIBLE_DEVICES=5,6,7
# $ echo $CUDA_VISIBLE_DEVICES
# $ CUDA_VISIBLE_DEVICES=5 python svi_zhang.py

# EikoNet
from EikoNet import model    as md
from EikoNet import database as db
from EikoNet import plot     as pt 

# HypoSVI
from HypoSVI import location as lc

# Downloading files from Google Drive
#from google_drive_downloader import GoogleDriveDownloader as gdd


# Additional Pacakges for projections etc
from IPython.display import Image
from IPython.display import display
from pyproj import Proj
#import pandas as pd
#import numpy as np
from glob import glob

import math
import numpy as np
import pandas as pd
import torch
import torch.autograd as autograd
import torch.optim as optim
import matplotlib.pylab as plt
import seaborn as sns
import os

#####PATH = '/atomic-data/cyoon/PuertoRico/Eiko_out'
#PATH = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/Eiko_out'
#PATH = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200108/Eiko_out'
PATH = '/media/yoon/INT01/PuertoRico/EQT_20180101_20230101/Eiko_out'
#PATH = '/media/yoon/INT01/Ferndale2022/EQT_20221220_20230103/Eiko_out'

#xmin               = [-67.5,17.5,-4] #Lat,Long,SDepth
#xmax               = [-65.0,18.5,30] #SDepth
#xmin               = [-67.5,17.5,-0.5] #Lat,Long,Depth
#xmax               = [-65.0,18.5,50]

xmin               = [-67.5,17.5,-2] #Lat,Long,Depth
#xmax               = [-65.0,18.5,20] #Lat,Long,Depth #PRSN
xmax               = [-65.0,18.5,41] #Lat,Long,Depth #ZHANG
###xmin               = [-67.3,17.6,-2] #Lat,Long,Depth #RESTRICT
###xmax               = [-66.4,18.3,41] #Lat,Long,Depth #RESTRICT
###xmin               = [-67.5,17.5,-2] #Lat,Long,Depth #RESTRICTDEPTH
###xmax               = [-65.0,18.5,25] #Lat,Long,Depth #RESTRICTDEPTH
projection         = "+proj=utm +zone=19 +north +ellps=WGS84 +datum=WGS84 +units=km +no_defs"

#xmin               = [-124.7,40.2,-2] #Lat,Long,Depth
#xmax               = [-123.7,40.8,41] #Lat,Long,Depth
#projection         = "+proj=utm +zone=10, +north +ellps=WGS84 +datum=WGS84 +units=km +no_defs"

#xmin               = [-125.0,39.5,-2] #Lat,Long,Depth
#xmax               = [-123.0,41.5,41] #Lat,Long,Depth
#projection         = "+proj=utm +zone=10, +north +ellps=WGS84 +datum=WGS84 +units=km +no_defs"

#vp_dir = 'VP_PRSN'
#vp_file = vp_dir+'/VP_PRSN.csv'
#vp_model_file = 'Model_Epoch_00014_ValLoss_0.006514014532663545.pt'
##vp_model_file = 'Model_Epoch_00015_ValLoss_0.006607915190769765.pt'
#vs_dir = 'VS_PRSN'
#vs_file = vs_dir+'/VS_PRSN.csv'
#vs_model_file = 'Model_Epoch_00014_ValLoss_0.0060166874305254085.pt'
##vs_model_file = 'Model_Epoch_00015_ValLoss_0.004795070604554245.pt'
##
##in_event_file = 'EQT_20200107_20200114_REAL_HYPOINVERSE_VELPRSN_combined_real_magcat_locate_pr_PickError010.nlloc'
##out_hyposvi_dir = 'EQT_20200107_20200114_REAL_HYPOINVERSE_VELPRSN_PickError010_Events'

vp_dir = 'VP_ZHANG'
vp_file = vp_dir+'/VP_ZHANG.csv'
vp_model_file = 'Model_Epoch_00015_ValLoss_0.0027984082313688625.pt'
vs_dir = 'VS_ZHANG'
vs_file = vs_dir+'/VS_ZHANG.csv'
vs_model_file = 'Model_Epoch_00015_ValLoss_0.004144702160983046.pt'

#vp_dir = 'VP_MENDO'
#vp_file = vp_dir+'/VP_MENDO.csv'
##vp_model_file = 'Model_Epoch_00012_ValLoss_0.0029753742589006074.pt'
#vp_model_file = 'Model_Epoch_00015_ValLoss_0.006774747649669871.pt'
#vs_dir = 'VS_MENDO'
#vs_file = vs_dir+'/VS_MENDO.csv'
##vs_model_file = 'Model_Epoch_00012_ValLoss_0.003173155303960456.pt'
#vs_model_file = 'Model_Epoch_00015_ValLoss_0.004194190198751657.pt'



#vp_dir = 'VP_ZHANG'
#vp_file = vp_dir+'/VP_ZHANG.csv'
#vp_model_file = 'Model_Epoch_00016_ValLoss_0.005435878705037267.pt'
#vs_dir = 'VS_ZHANG'
#vs_file = vs_dir+'/VS_ZHANG.csv'
#vs_model_file = 'Model_Epoch_00016_ValLoss_0.00243791271897411.pt'

#in_event_file = 'EQT_20200107_20200114_REAL_HYPOINVERSE_VELZHANG_combined_real_magcat_locate_pr_PickError010.nlloc'
###out_hyposvi_dir = 'EQT_20200107_20200114_REAL_HYPOINVERSE_VELZHANG_PickError010_Events'
###out_hyposvi_dir = 'RESTRICT_EQT_20200107_20200114_REAL_HYPOINVERSE_VELZHANG_PickError010_Events'
###out_hyposvi_dir = 'RESTRICTDEPTH_EQT_20200107_20200114_REAL_HYPOINVERSE_VELZHANG_PickError010_Events'
###out_hyposvi_dir = 'PARTICLE450_EQT_20200107_20200114_REAL_HYPOINVERSE_VELZHANG_PickError010_Events'
###out_hyposvi_dir = 'TTUNCERT_EQT_20200107_20200114_REAL_HYPOINVERSE_VELZHANG_PickError010_Events'
#out_hyposvi_dir = 'CONF68_EQT_20200107_20200114_REAL_HYPOINVERSE_VELZHANG_PickError010_Events'

#in_event_file = 'EQT_20180101_20210601_REAL_HYPOINVERSE_VELZHANG_combined_real_magcat_locate_pr_PickError010.nlloc'
#out_hyposvi_dir = 'CONF68_EQT_20180101_20210601_REAL_HYPOINVERSE_VELZHANG_PickError010_Events'
#in_event_file = 'EQT_20180101_20211001_REAL_HYPOINVERSE_VELZHANG_combined_real_magcat_locate_pr_PickError010.nlloc'
#out_hyposvi_dir = 'CONF68_EQT_20180101_20211001_REAL_HYPOINVERSE_VELZHANG_PickError010_Events'
#####in_event_file = 'EQT_20180101_20220101_3REAL_HYPOINVERSE_VELZHANG_combined_real_magcat_locate_pr_PickError010.nlloc'
#####out_hyposvi_dir = 'CONF68_EQT_20180101_20220101_3REAL_HYPOINVERSE_VELZHANG_PickError010_Events'

#in_event_file = 'EQT_20200107_20200114_REAL_HYPOINVERSE_VELPRSN_combined_real_magcat_locate_pr_PickError010.nlloc'
#out_hyposvi_dir = 'CONF68_EQT_20200107_20200114_REAL_HYPOINVERSE_VELPRSN_PickError010_Events'

#in_event_file = 'EQT_20180101_20220101_3REAL_HYPOINVERSE_VELPRSN_combined_real_magcat_locate_pr_PickError010.nlloc'
#out_hyposvi_dir = 'CONF68_EQT_20180101_20220101_3REAL_HYPOINVERSE_VELPRSN_PickError010_Events'

## Went through HYPOINVERSE first, before HYPOSVI location
#in_event_file = 'EQT_20221220_20230103_REAL_HYPOINVERSE_combined_real_magcat_locate_mendo_PickError010.nlloc'
#out_hyposvi_dir = 'EQT_20221220_20230103_REAL_HYPOINVERSE_PickError010_Events'
#
## Phases straight from REAL to HYPOSVI
#in_event_file = 'EQT_20221220_20230103_REAL_PickErrorRES.nlloc'
#out_hyposvi_dir = 'EQT_20221220_20230103_REAL_PickErrorRES_Events'

# EQTransformer-detected, REAL-associated events
#in_event_file = 'TEST10_EQT_20200107_20200108_REAL_VELZHANG_PickErrorRES.nlloc'
#out_hyposvi_dir = 'TEST10_EQT_20200107_20200108_REAL_VELZHANG_PickErrorRES_Events'
#start_event_id = 1000000
# Missed events from catalog
#in_event_file = 'TEST10_EQT_20200107_20200108_REAL_VELZHANG_MISSED_PickError010.nlloc'
#out_hyposvi_dir = 'TEST10_EQT_20200107_20200108_REAL_VELZHANG_MISSED_PickError010_Events'
#start_event_id = 4000001

## EQTransformer-detected, REAL-associated events
#in_event_file = 'EQT_20180101_20230101_REAL_VELZHANG_PickErrorRES.nlloc'
#out_hyposvi_dir = 'EQT_20180101_20230101_REAL_VELZHANG_PickErrorRES_Events'
#start_event_id = 1000000
# Missed events from catalog
in_event_file = 'EQT_20180101_20230101_REAL_VELZHANG_MISSED_PickError010.nlloc'
out_hyposvi_dir = 'EQT_20180101_20230101_REAL_VELZHANG_MISSED_PickError010_Events'
start_event_id = 4000001


# ------------- VP Velocity Model ----------
vm_vp = db.Graded1DVelocity(('{}/'+vp_file).format(PATH),xmin=xmin,xmax=xmax,projection=projection)
model_VP  = md.Model(('{}/'+vp_dir).format(PATH),vm_vp,device=dv)
model_VP.load(('{}/'+vp_dir+'/'+vp_model_file).format(PATH))

# ------------- VS Velocity Model ----------
vm_vs = db.Graded1DVelocity(('{}/'+vs_file).format(PATH),xmin=xmin,xmax=xmax,projection=projection)
model_VS  = md.Model(('{}/'+vs_dir).format(PATH),vm_vs,device=dv)
model_VS.load(('{}/'+vs_dir+'/'+vs_model_file).format(PATH))


# ------ HypoSVI -------
PATH_EVT = PATH+'/'+out_hyposvi_dir
if not os.path.exists(PATH_EVT):
   os.makedirs(PATH_EVT)

EVT = lc.IO_NLLoc2JSON(('{}/'+in_event_file).format(PATH), EVT={}, startEventID=start_event_id) # from REAL/HYPOINVERSE
lc.IO_JSON('{}/Picks.json'.format(PATH_EVT),Events=EVT,rw_type='w')
EVT  = lc.IO_JSON('{}/Picks.json'.format(PATH_EVT),rw_type='r')
print("len(EVT): ", len(EVT))

# ------------ Loading the station data ------------
Stations       = pd.read_csv('{}/pr_st.out'.format(PATH),sep=r'\s+')
#Stations       = pd.read_csv('{}/mendo_st.out'.format(PATH),sep=r'\s+')
Stations       = Stations.drop_duplicates(['Network','Station'],keep='last').reset_index(drop=True)
Stations

LocMethod = lc.HypoSVI([model_VP,model_VS],Phases=['P','S'],device=dv)
LocMethod.plot_info['EventPlot']['Traces']['Plot Traces']     = False
##LocMethod.plot_info['EventPlot']['Traces']['Plot Traces']     = True
##LocMethod.plot_info['EventPlot']['Traces']['Trace Host']      = '/atomic-data/cyoon/PuertoRico/downloads_mseeds'
#####LocMethod.plot_info['EventPlot']['Traces']['Trace Host']      = '/atomic-data/cyoon/PuertoRico/DATA/downloads_mseeds/2020_01'
#LocMethod.plot_info['EventPlot']['Traces']['Trace Host']      = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/downloads_mseeds/2020_01'
#LocMethod.plot_info['EventPlot']['Traces']['Trace Host']      = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200108/downloads_mseeds'
LocMethod.plot_info['EventPlot']['Traces']['Trace Host']      = '/media/yoon/INT01/PuertoRico/EQT_20180101_20230101/downloads_mseeds'
#LocMethod.plot_info['EventPlot']['Traces']['Trace Host']      = '/media/yoon/INT01/Ferndale2022/EQT_20221220_20230103/downloads_mseeds'
LocMethod.plot_info['EventPlot']['Traces']['Trace Host Type'] = 'EQTransformer'
LocMethod.plot_info['EventPlot']['Traces']['Channel Types']   = ['EH*','HH*','BH*','HN*']

#!CY
##LocMethod.location_info['Number of Particles']                  = 450
##LocMethod.location_info['Travel Time Uncertainty - [Gradient(km/s),Min(s),Max(s)]'] = [0.2,0.2,4.0]
##LocMethod.location_info['Travel Time Uncertainty - [Gradient(km/s),Min(s),Max(s)]'] = [0.2,0.2,8.0]
LocMethod.location_info['Location Uncertainty Percentile (%)']  = 68.0

##LocMethod.LocateEvents(EVT, Stations, output_plots=True, timer=True, output_path='{}'.format(PATH_EVT))
LocMethod.LocateEvents(EVT, Stations, output_plots=False, timer=True, output_path='{}'.format(PATH_EVT))

LocMethod.CataloguePlot(filepath='{}/CatalogPlot.png'.format(PATH_EVT),Events=lc.IO_JSON('{}/Catalogue.json'.format(PATH_EVT),rw_type='r'),user_xmin=[None,None,None],user_xmax=[None,None,None])
