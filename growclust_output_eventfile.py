
#in_phase_file = '../LargeAreaEQTransformer/HYPODD/pr_phases_hypoDD.txt'
#out_event_file = '../LargeAreaEQTransformer/GrowClust/IN/pr_evlist.txt'

#base_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200108/REAL_VELZHANG/'
#in_phase_file = base_dir+'HYPODD_TEST10/pr_phases_hypoDD.txt'
#out_event_file = base_dir+'GrowClust_TEST10/IN/pr_phases_evlist.txt'

base_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20230101/'
in_phase_file = base_dir+'REAL_VELZHANG/DIRECTHYPOSVI/pr_phases_hypoDD.txt'
out_event_file = base_dir+'GrowClust/IN/pr_phases_evlist.txt'

fout = open(out_event_file, 'w')
with open(in_phase_file, 'r') as fin:
   for line in fin:
      if (line[0] == '#'):
         fout.write(line[2:])
fout.close()
