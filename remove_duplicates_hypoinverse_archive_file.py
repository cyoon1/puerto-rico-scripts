import utils_hypoinverse as utils_hyp

# Read HYPOINVERSE archive output file, remove duplicate events
# Output HYPOINVERSE summary and archive files without duplicates

#in_hinv_arc_file = '../LargeAreaEQTransformer/association/locate_pr.arc'
#out_hinv_arc_file = '../LargeAreaEQTransformer/association/locate_pr_merge.arc'
#out_hinv_sum_file = '../LargeAreaEQTransformer/association/locate_pr_merge.sum'
#in_hinv_arc_file = '../LargeAreaEQTransformer/REAL/Events/real_locate_pr.arc'
#out_hinv_arc_file = '../LargeAreaEQTransformer/REAL/Events/real_locate_pr_merge.arc'
#out_hinv_sum_file = '../LargeAreaEQTransformer/REAL/Events/real_locate_pr_merge.sum'
#in_hinv_arc_file = '../LargeAreaEQTransformer/PhaseLink/pl_locate_pr.arc'
#out_hinv_arc_file = '../LargeAreaEQTransformer/PhaseLink/pl_locate_pr_merge.arc'
#out_hinv_sum_file = '../LargeAreaEQTransformer/PhaseLink/pl_locate_pr_merge.sum'
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/association/locate_pr.arc'
#out_hinv_arc_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/association/locate_pr_merge.arc'
#out_hinv_sum_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/association/locate_pr_merge.sum'
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/REAL/HYPOINVERSE/real_locate_pr.arc'
#out_hinv_arc_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/REAL/HYPOINVERSE/real_locate_pr_merge.arc'
#out_hinv_sum_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/REAL/HYPOINVERSE/real_locate_pr_merge.sum'
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/PhaseLink/pl_locate_pr.arc'
#out_hinv_arc_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/PhaseLink/pl_locate_pr_merge.arc'
#out_hinv_sum_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/PhaseLink/pl_locate_pr_merge.sum'

#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELPRSN/real_locate_pr.arc'
#out_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELPRSN/real_locate_pr_merge.arc'
#out_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELPRSN/real_locate_pr_merge.sum'

#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/real_locate_pr.arc'
#out_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/real_locate_pr_merge.arc'
#out_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/real_locate_pr_merge.sum'

#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELPRSN/real_locate_pr.arc'
#out_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELPRSN/real_locate_pr_merge.arc'
#out_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELPRSN/real_locate_pr_merge.sum'

#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELZHANG/real_locate_pr.arc'
#out_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELZHANG/real_locate_pr_merge.arc'
#out_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELZHANG/real_locate_pr_merge.sum'

#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELPRSN/real_locate_pr.arc'
#out_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELPRSN/real_locate_pr_merge.arc'
#out_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELPRSN/real_locate_pr_merge.sum'

#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/real_locate_pr.arc'
#out_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/real_locate_pr_merge.arc'
#out_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/real_locate_pr_merge.sum'

#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/real_locate_pr.arc'
#out_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/real_locate_pr_merge.arc'
#out_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/real_locate_pr_merge.sum'
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/real_locate_pr.arc'
#out_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/real_locate_pr_merge.arc'
#out_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/real_locate_pr_merge.sum'

#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200108/REAL_VELZHANG/DIRECTHYPOSVI_TEST10/hyposvi_real_locate_pr.arc'
#out_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200108/REAL_VELZHANG/DIRECTHYPOSVI_TEST10/hyposvi_real_locate_pr_merge.arc'
#out_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200108/REAL_VELZHANG/DIRECTHYPOSVI_TEST10/hyposvi_real_locate_pr_merge.sum'

base_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20230101/REAL_VELZHANG/DIRECTHYPOSVI/'
in_hinv_arc_file = base_dir+'combined_hyposvi_real_magcat_locate_keep_pr.arc'
out_hinv_arc_file = base_dir+'combined_hyposvi_real_magcat_locate_keep_pr_merge.arc'
out_hinv_sum_file = base_dir+'combined_hyposvi_real_magcat_locate_keep_pr_merge.sum'

#delta_origin_time = 5.0 # If origin times of events are less than 5 seconds apart, merge them into one event - works better for REAL
delta_origin_time = 4.0 # If origin times of events are less than 5 seconds apart, merge them into one event - works better for REAL
#delta_origin_time = 2.0 # If origin times of events are less than 2 seconds apart, merge them into one event
event_dict = {}
ev_prev_id = -1 
num_merged = 0
with open(in_hinv_arc_file, 'r') as fin:
   for line in fin:
      # Read in an event
      if ((line[0:2] == '19') or (line[0:2] == '20')):
         origin_time = utils_hyp.get_origin_time_hypoinverse_file(line)
         num_ph = int(line[119:122])
         ev_id = int(line[136:146])
         count_ph = 0
         flag_merge = False
         event_dict[ev_id] = {}
         event_dict[ev_id]['event'] = line
         event_dict[ev_id]['phases'] = []

         if (ev_prev_id != -1):
            origin_prev_time = utils_hyp.get_origin_time_hypoinverse_file(event_dict[ev_prev_id]['event'])
            diff_origin_time = abs(origin_time - origin_prev_time)
            if (diff_origin_time < delta_origin_time):
               flag_merge = True
               num_merged += 1
               event_dict.pop(ev_id) # Remove current event ID from event_dict since it is duplicate
               print("flag_merge = ", flag_merge)
               print(line)

      # Read line marking end of an event
      elif ((line[0:2]) == '  '):
         ev_id_end = int(line[62:72])
         if (ev_id != ev_id_end):
            print("WARNING: event ID does not match: ", ev_id, ev_id_end)
         if (count_ph != num_ph):
            print("WARNING: number of phases for this event does not match: ", ev_id, ev_id_end, count_ph, num_ph)
         if (flag_merge == False):
            event_dict[ev_id]['endline'] = line
            ev_prev_id = ev_id

      # Read phase for this event
      else:
         sta = line[0:4]
         net = line[5:7]
         chan = line[9:12]
         p_str = line[14:15]
         s_str = line[47:48]
         phase_str = sta.strip()+net.strip()+chan.strip()+p_str.strip()+s_str.strip()
         count_ph += 1
         if (flag_merge == True):
            event_dict[ev_prev_id]['phases'].append((phase_str, line)) # Added phases for duplicate event to previous event
         else:
            event_dict[ev_id]['phases'].append((phase_str, line))

# Output merged files from the event_dict
fout_arc = open(out_hinv_arc_file, 'w')
fout_sum = open(out_hinv_sum_file, 'w')
for ev_id in sorted(event_dict): # Loop over keys - event IDs
   fout_sum.write(("%s") % event_dict[ev_id]['event'])
   fout_arc.write(("%s") % event_dict[ev_id]['event'])
   for phase in event_dict[ev_id]['phases']: # Loop over phases for event ev_id
      fout_arc.write(("%s") % phase[1])
   fout_arc.write(("%s") % event_dict[ev_id]['endline'])
fout_arc.close()
fout_sum.close()

print("Number of merged events: ", num_merged)
