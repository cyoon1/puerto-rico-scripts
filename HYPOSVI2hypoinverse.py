import datetime
import utils_hypoinverse as utils_hyp
from HypoSVI import location as lc


#base_dir = '/media/yoon/INT01/Ferndale2022/EQT_20221220_20230103/'
#PATH_EVT = base_dir+'Eiko_out/EQT_20221220_20230103_REAL_HYPOINVERSE_PickError010_Events'
#in_magmap_file = '../Catalog/EQT_20221220_20230103/EQT_20221220_20230103_REAL_HYPOSVI_MagnitudeMap.txt'
#out_hinv_dir = base_dir+'REAL/HYPOSVI/'
#out_hinv_arc_file = out_hinv_dir+'hyposvi_combined_real_magcat_locate_mendo.arc'
#out_hinv_sum_file = out_hinv_dir+'hyposvi_combined_real_magcat_locate_mendo.sum'

#base_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200108/'
#PATH_EVT = base_dir+'Eiko_out/TEST10_EQT_20200107_20200108_REAL_VELZHANG_PickErrorRES_Events'
#out_hinv_dir = base_dir+'REAL_VELZHANG/DIRECTHYPOSVI_TEST10/'
#out_hinv_arc_file = out_hinv_dir+'hyposvi_real_locate_pr.arc'
#out_hinv_sum_file = out_hinv_dir+'hyposvi_real_locate_pr.sum'

#base_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200108/'
#PATH_EVT = base_dir+'Eiko_out/TEST10_EQT_20200107_20200108_REAL_VELZHANG_MISSED_PickError010_Events'
#out_hinv_dir = base_dir+'REAL_VELZHANG/DIRECTHYPOSVI_TEST10/'
#out_hinv_arc_file = out_hinv_dir+'hyposvi_cat_real_magmiss_locate_pr.arc'
#out_hinv_sum_file = out_hinv_dir+'hyposvi_cat_real_magmiss_locate_pr.sum'

#base_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20230101/'
#PATH_EVT = base_dir+'Eiko_out/EQT_20180101_20230101_REAL_VELZHANG_PickErrorRES_Events'
#out_hinv_dir = base_dir+'REAL_VELZHANG/DIRECTHYPOSVI/'
#out_hinv_arc_file = out_hinv_dir+'hyposvi_real_locate_pr.arc'
#out_hinv_sum_file = out_hinv_dir+'hyposvi_real_locate_pr.sum'

base_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20230101/'
PATH_EVT = base_dir+'Eiko_out/EQT_20180101_20230101_REAL_VELZHANG_MISSED_PickError010_Events'
out_hinv_dir = base_dir+'REAL_VELZHANG/DIRECTHYPOSVI/'
out_hinv_arc_file = out_hinv_dir+'hyposvi_cat_real_magmiss_locate_pr.arc'
out_hinv_sum_file = out_hinv_dir+'hyposvi_cat_real_magmiss_locate_pr.sum'

#base_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/'
#PATH_EVT = base_dir+'Eiko_out/CONF68_EQT_20180101_20220101_3REAL_HYPOINVERSE_VELZHANG_PickError010_Events'
#out_hinv_dir = base_dir+'3REAL/HYPOINVERSE_VELZHANG/'
#out_hinv_arc_file = out_hinv_dir+'hyposvi_combined_real_magcat_locate_pr.arc'
#out_hinv_sum_file = out_hinv_dir+'hyposvi_combined_real_magcat_locate_pr.sum'

#base_dir = '/media/yoon/INT01/Ferndale2022/EQT_20221220_20230103/'
#PATH_EVT = base_dir+'Eiko_out/EQT_20221220_20230103_REAL_PickErrorRES_Events'
##in_magmap_file = '../Catalog/EQT_20221220_20230103/EQT_20221220_20230103_REAL_HYPOSVI_MagnitudeMap.txt'
#out_hinv_dir = base_dir+'REAL/DIRECTHYPOSVI/'
#out_hinv_arc_file = out_hinv_dir+'hyposvi_real_locate_mendo.arc'
#out_hinv_sum_file = out_hinv_dir+'hyposvi_real_locate_mendo.sum'


## Read in magnitude map
#mag_map = {}
#with open(in_magmap_file, 'r') as fin:
#   for line in fin:
#      split_line = line.split()
#      hsvi_ev_id = int(split_line[0])
#      hinv_mag = float(split_line[3])
#      mag_map[hsvi_ev_id] = hinv_mag

# Read in HYPOSVI event locations and picks from JSON
EVT_org = lc.IO_JSON('{}/Catalogue.json'.format(PATH_EVT),rw_type='r')
keys = []
for key in EVT_org.keys():
   try:
      loc = EVT_org[key]['location']
      keys.append(key)
      continue
   except:
      continue
EVT= { your_key: EVT_org[your_key] for your_key in keys }

num_events = 0
fout_arc = open(out_hinv_arc_file, 'w')
fout_sum = open(out_hinv_sum_file, 'w')
for ik, key in enumerate(keys): # Loop over HYPOSVI events
   curr_ev = EVT[key]

   # Get event id, magnitude
   evid_out = int(key)
#   if (evid_out in mag_map):
#      mag_out = int(round(100*mag_map[evid_out]))
   # Check if event solution is good
   if ("OriginTime" in curr_ev["location"]): # check if event has a location
      mag_out = 1.00 # placeholder

      # Get latitude, longitude, depth, origin time, number of picks
      ev_lat = curr_ev["location"]["Hypocentre"][1]
      ev_lon = curr_ev["location"]["Hypocentre"][0]
      ev_depth = curr_ev["location"]["Hypocentre"][2]
      origin_time = datetime.datetime.strptime(curr_ev["location"]["OriginTime"][:-3], '%Y-%m-%d %H:%M:%S.%f')
      origin_time_second = utils_hyp.get_rounded_seconds(origin_time)
      num_picks = len(curr_ev["Picks"]["Network"])
      print(evid_out, origin_time, origin_time_second, ev_lat, ev_lon, ev_depth, mag_out, num_picks)

      # Output event info in HYPOINVERSE arc/sum files
      [lat_int, lat_char, lat_min] = utils_hyp.output_lat_hypoinverse_format(ev_lat)
      [lon_int, lon_char, lon_min] = utils_hyp.output_lon_hypoinverse_format(ev_lon)
      depth_out = round(100*float(ev_depth))
#      fout_arc.write(('%4d%02d%02d%02d%02d%04d%2d%1s%4s%3d%1s%4s%5d                                                                                                    %10d %3d\n') % (origin_time.year, origin_time.month, origin_time.day, origin_time.hour, origin_time.minute, origin_time_second, lat_int, lat_char, lat_min, lon_int, lon_char, lon_min, depth_out, evid_out, mag_out))
      fout_arc.write(('%4d%02d%02d%02d%02d%04d%2d%1s%4s%3d%1s%4s%5d                                                                                   %3d              %10d %3d\n') % (origin_time.year, origin_time.month, origin_time.day, origin_time.hour, origin_time.minute, origin_time_second, lat_int, lat_char, lat_min, lon_int, lon_char, lon_min, depth_out, num_picks, evid_out, mag_out))
      fout_sum.write(('%4d%02d%02d%02d%02d%04d%2d%1s%4s%3d%1s%4s%5d                                                                                   %3d              %10d %3d\n') % (origin_time.year, origin_time.month, origin_time.day, origin_time.hour, origin_time.minute, origin_time_second, lat_int, lat_char, lat_min, lon_int, lon_char, lon_min, depth_out, num_picks, evid_out, mag_out))

      # Get phase picks
      for ipick in range(num_picks):
         net = curr_ev["Picks"]["Network"][ipick]
         sta = curr_ev["Picks"]["Station"][ipick]
         ph = curr_ev["Picks"]["PhasePick"][ipick]
         arr_time = datetime.datetime.strptime(curr_ev["Picks"]["DT"][ipick], '%Y-%m-%d %H:%M:%S.%f')
         [chan, p_remark, s_remark, p_res, s_res, p_weight, s_weight, p_arr_time_sec, s_arr_time_sec] = utils_hyp.output_phase_hypoinverse_format(
            ph, arr_time, fm_p='I', fm_s='E', res=None, weight=None)

         # Output phase pick info in HYPOINVERSE arc files
         fout_arc.write(('%-5s%2s  %3s %2s %1d%4d%02d%02d%02d%02d%5d%4s   %5d%2s %1d%4s\n') % (sta, net, chan, p_remark, p_weight, arr_time.year, arr_time.month, arr_time.day, arr_time.hour, arr_time.minute, p_arr_time_sec, p_res, s_arr_time_sec, s_remark, s_weight, s_res))

      # Output event ID line in HYPOINVERSE arc files
      num_events += 1
      fout_arc.write("{:<62}".format(' ')+"%10d"%(evid_out)+'\n');
fout_arc.close()
fout_sum.close()
print("Number of HYPOSVI events written out to HYPOINVERSE arc file: ", num_events)
