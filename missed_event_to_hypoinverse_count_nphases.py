import datetime
import json
import utils_hypoinverse as utils_hyp
from obspy import read_events

# Missed event analysis - output HYPOINVERSE arc file for consistent processing
# Must run compare_catalog_eqtransformer_events.py before running this script

## Inputs
####base_dir = '/media/yoon/INT01/Ferndale2022/EQT_20221220_20221227/'
#base_dir = '/media/yoon/INT01/Ferndale2022/EQT_20221220_20230103/'
#hinv_dir = base_dir+'REAL/HYPOINVERSE/'
#in_station_file = base_dir+'station_list_edited.json'
####in_missed_event_xml_dir = base_dir+'CatalogEventsDownload20221227/'
####in_missed_event_file = hinv_dir+'events_MISSED_magcat_EQT_20221220_20221227.txt'
####out_missed_event_file = hinv_dir+'events_MISSED_magcat_EQT_EVID_20221220_20221227.txt'
######out_missed_us_event_file = hinv_dir+'us_events_MISSED_magcat_EQT_EVID_20221220_20221227.txt' # incomplete but thats ok
#in_missed_event_xml_dir = base_dir+'CatalogEventsDownload20230103/'
#in_missed_event_file = hinv_dir+'events_MISSED_magcat_EQT_20221220_20230103.txt'
#out_missed_event_file = hinv_dir+'events_MISSED_magcat_EQT_EVID_20221220_20230103.txt'
#out_hinv_arc_file = hinv_dir+'cat_real_magmiss_locate_mendo.arc'
#out_hinv_sum_file = hinv_dir+'cat_real_magmiss_locate_mendo.sum'

#base_dir = '/media/yoon/INT01/Ferndale2022/EQT_20221220_20230103/'
#hinv_dir = base_dir+'REAL/DIRECTHYPOSVI/'
#in_station_file = base_dir+'station_list_edited.json'
#in_missed_event_xml_dir = base_dir+'CatalogEventsDownload20230103/'
#in_missed_event_file = hinv_dir+'hyposvi_events_MISSED_magcat_EQT_20221220_20230103.txt'
#out_missed_event_file = hinv_dir+'hyposvi_events_MISSED_magcat_EQT_EVID_20221220_20230103.txt'
#out_hinv_arc_file = hinv_dir+'hyposvi_cat_real_magmiss_locate_mendo.arc'
#out_hinv_sum_file = hinv_dir+'hyposvi_cat_real_magmiss_locate_mendo.sum'

#base_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200108/'
#hinv_dir = base_dir+'REAL_VELZHANG/DIRECTHYPOSVI_TEST10/'
#in_station_file = base_dir+'station_list_edited.json'
#in_missed_event_xml_dir = base_dir+'CatalogEventsDownload20230131/'
#in_missed_event_file = hinv_dir+'events_MISSED_magcat_DIRECTHYPOSVI_TEST10_EQT_20200107_20200108.txt'
#out_missed_event_file = hinv_dir+'events_MISSED_magcat_DIRECTHYPOSVI_TEST10_EQT_EVID_20200107_20200108.txt'
#out_hinv_arc_file = hinv_dir+'cat_real_magmiss_locate_pr.arc'
#out_hinv_sum_file = hinv_dir+'cat_real_magmiss_locate_pr.sum'

base_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20230101/'
hinv_dir = base_dir+'REAL_VELZHANG/DIRECTHYPOSVI/'
in_station_file = base_dir+'station_list_edited.json'
in_missed_event_xml_dir = base_dir+'CatalogEventsDownload20230211/'
in_missed_event_file = hinv_dir+'events_MISSED_magcat_DIRECTHYPOSVI_EQT_20180101_20230101.txt'
out_missed_event_file = hinv_dir+'events_MISSED_magcat_DIRECTHYPOSVI_EQT_EVID_COUNT_20180101_20230101.txt'
#out_missed_event_file = hinv_dir+'events_MISSED_magcat_DIRECTHYPOSVI_EQT_EVID_20180101_20230101.txt'
#out_hinv_arc_file = hinv_dir+'cat_real_magmiss_locate_pr.arc'
#out_hinv_sum_file = hinv_dir+'cat_real_magmiss_locate_pr.sum'



stations_ = json.load(open(in_station_file))

# Loop over events in missed event file
nmissed = 0
nmissed_proc = 0
##nmissed_us_proc = 0
missed_start_evid = 4000000 # arbitrary, but do not want overlap with existing event ID
fout_miss = open(out_missed_event_file, 'w')
##fout_us_miss = open(out_missed_us_event_file, 'w')
#fout_arc = open(out_hinv_arc_file, 'w')
#fout_sum = open(out_hinv_sum_file, 'w')
with open(in_missed_event_file, 'r') as fin:
   for line in fin:
      split_line = line.split()
      origin_time = datetime.datetime.strptime(split_line[0], '%Y-%m-%dT%H:%M:%S.%f')
      origin_time_second = utils_hyp.get_rounded_seconds(origin_time)
      cat_lat = split_line[1]
      cat_lon = split_line[2]
      cat_depth = split_line[3]
      cat_mag = split_line[4]
      cat_evid = split_line[5]
      nmissed += 1
      print('\n', origin_time, cat_lat, cat_lon, cat_depth, cat_mag, cat_evid)

      # Read in event file from catalog xml file, includes phase pick info
      in_xml_file = in_missed_event_xml_dir+cat_evid+'.xml'
#      in_xml_file = in_missed_event_xml_dir+'nc'+cat_evid+'.xml'
      try:
         curr_ev = read_events(in_xml_file)
      except Exception:
         print("ERROR: Cannot find event xml file ", in_xml_file, ", skipping...", nmissed)
         continue

      # Assign new numeric event ID for this missed event
      missed_evid = missed_start_evid + nmissed

#      # Output event info in HYPOINVERSE arc/sum files for missed events
#      [lat_int, lat_char, lat_min] = utils_hyp.output_lat_hypoinverse_format(cat_lat)
#      [lon_int, lon_char, lon_min] = utils_hyp.output_lon_hypoinverse_format(cat_lon)
#      depth_out = round(100*float(cat_depth))
#      mag_out = round(100*float(cat_mag))
#
#      event_str = f'{origin_time.year:04}{origin_time.month:02}{origin_time.day:02}{origin_time.hour:02}{origin_time.minute:02}{origin_time.second:04}{lat_int:>2}{lat_char:1}{lat_min:4}{lon_int:>3}{lon_char:1}{lon_min:4}{depth_out:5}                                                                                                    {missed_evid:10} {mag_out:3}\n'
#      print(event_str)
#      fout_arc.write(('%4d%02d%02d%02d%02d%04d%2d%1s%4s%3d%1s%4s%5d                                                                                                    %10d %3d\n') % (origin_time.year, origin_time.month, origin_time.day, origin_time.hour, origin_time.minute, origin_time_second, lat_int, lat_char, lat_min, lon_int, lon_char, lon_min, depth_out, missed_evid, mag_out))
#      fout_sum.write(('%4d%02d%02d%02d%02d%04d%2d%1s%4s%3d%1s%4s%5d                                                                                                    %10d %3d\n') % (origin_time.year, origin_time.month, origin_time.day, origin_time.hour, origin_time.minute, origin_time_second, lat_int, lat_char, lat_min, lon_int, lon_char, lon_min, depth_out, missed_evid, mag_out))

      # Get phase picks
      phase_list = curr_ev[0].picks
      npicks_total = len(phase_list)
      print("npicks_total: ", npicks_total)
#      print(phase_list)
      npicks_proc = 0
      npicks_all_proc = 0
      npicks_sta_p = 0
      npicks_sta_s = 0
      npicks_all_p = 0
      npicks_all_s = 0
      nsta_inc = set()
      nsta_all = set()
      out_pick_list = []
      for pick in phase_list:
         sta = pick.waveform_id.station_code
         if (sta in stations_): # only output the phase if it is in our station list (json)
            net = pick.waveform_id.network_code
            chcode = pick.waveform_id.channel_code
            ph = pick.phase_hint
            arr_time = pick.time
            if (pick.onset == 'impulsive'):
               pick_type = 'I'
            else:
               pick_type = 'E'
            print(net, sta, chcode, ph, arr_time)

            # Possible phase names in catalog
            if ((ph == 'Pg') or (chcode[-1] == 'Z')):
               ph = 'P'
            if ((ph == 'Sg') or (chcode[-1] == 'E') or (chcode[-1] == 'N') or (chcode[-1] == '1') or (chcode[-1] == '2')):
               ph = 'S'

            # Phase specific output
            if ((ph != 'P') and (ph != 'S')): # ph == something else (not P or S)
               print("Not using this phase: ", ph, ", skipping...", chcode)
               continue
            [chan, p_remark, s_remark, p_res, s_res, p_weight, s_weight, p_arr_time_sec, s_arr_time_sec] = utils_hyp.output_phase_hypoinverse_format(
               ph, arr_time, fm_p=pick_type, fm_s=pick_type, res=None, weight=None)

            if (ph == 'P'):
               npicks_sta_p += 1
               npicks_all_p += 1
            if (ph == 'S'):
               npicks_sta_s += 1
               npicks_all_s += 1
            nsta_inc.add(sta)
            nsta_all.add(sta)

            # Output phase pick info in HYPOINVERSE arc files for missed events
            npicks_proc += 1
            npicks_all_proc += 1
            out_pick_str = f'{sta:5}{net:2}  {chan:3} {p_remark:2} {p_weight:1}{arr_time.year:4}{arr_time.month:02}{arr_time.day:02}{arr_time.hour:02}{arr_time.minute:02}{p_arr_time_sec:5}{p_res:4}   {s_arr_time_sec:5}{s_remark:2} {s_weight:1}{s_res:4}\n'
            out_pick_list.append(out_pick_str)
            print(p_arr_time_sec, s_arr_time_sec)
            print(out_pick_str)
#            fout_arc.write(('%-5s%2s  %3s %2s %1d%4d%02d%02d%02d%02d%5d%4s   %5d%2s %1d%4s\n') % (sta, net, chan, p_remark, p_weight, arr_time.year, arr_time.month, arr_time.day, arr_time.hour, arr_time.minute, p_arr_time_sec, p_res, s_arr_time_sec, s_remark, s_weight, s_res))

         else: # pick station not in our list, ignore
            # Possible phase names in catalog
            if ((ph == 'Pg') or (chcode[-1] == 'Z')):
               ph = 'P'
            if ((ph == 'Sg') or (chcode[-1] == 'E') or (chcode[-1] == 'N') or (chcode[-1] == '1') or (chcode[-1] == '2')):
               ph = 'S'

            # Phase specific output
            if ((ph != 'P') and (ph != 'S')): # ph == something else (not P or S)
               print("Not using this phase: ", ph, ", skipping...", chcode)
               continue

            if (ph == 'P'):
               npicks_all_p += 1
            if (ph == 'S'):
               npicks_all_s += 1
            nsta_all.add(sta)
            npicks_all_proc += 1

            # Output phase pick info in HYPOINVERSE arc files for missed events
            print("PICK AT THIS STATION NOT USED: ", sta)
      print("Number of phase picks processed in our station list: ", npicks_proc, "/", len(phase_list), "\n")
      print("npicks_total: ", npicks_total)
      print("npicks_proc: ", npicks_proc)
      print("npicks_all_proc: ", npicks_all_proc)
      print("npicks_sta_p: ", npicks_sta_p)
      print("npicks_sta_s: ", npicks_sta_s)
      print("npicks_all_p: ", npicks_all_p)
      print("npicks_all_s: ", npicks_all_s)
      print("len(nsta_inc): ", len(nsta_inc), nsta_inc)
      print("len(nsta_all): ", len(nsta_all), nsta_inc)

      # Output missed event file
      nmissed_proc += 1
      fout_miss.write(('%s %s %d %d %d %d %d %d %d %d %d\n') % (line.replace('\n',''), missed_evid, npicks_sta_p, npicks_sta_s, npicks_proc, len(nsta_inc), npicks_all_p, npicks_all_s, npicks_all_proc, len(nsta_all), npicks_total))
#      fout_miss.write(('%s %s\n') % (line.replace('\n',''), missed_evid))

##      if (cat_evid[0:2] == 'us'):
##         nmissed_us_proc += 1
##         fout_us_miss.write(('%s %s\n') % (line.replace('\n',''), missed_evid))

#      # Output event ID line in HYPOINVERSE arc files for missed events
#      npicks_str = str(npicks_proc).rjust(3)
#      out_event_str = event_str[0:119]+npicks_str+event_str[122:]
#      print(out_event_str)
#      fout_sum.write(out_event_str)
#      fout_arc.write(out_event_str)
#      for pickstr in out_pick_list:
#         fout_arc.write(pickstr)
#      fout_arc.write("{:<62}".format(' ')+"%10d"%(missed_evid)+'\n');

fout_miss.close()
##fout_us_miss.close()
#fout_arc.close()
#fout_sum.close()
print("Number of missed events processed with xml event file: ", nmissed_proc, "/", nmissed)
##print("Number of missed events with us: ", nmissed_us_proc)

