import json
import pickle
import time
import matplotlib.pyplot as plt
from matplotlib import rcParams
from matplotlib import colors
import numpy as np
from scipy import stats
import utils_hypoinverse as utils_hyp
from obspy import read_events
from obspy import UTCDateTime
from obspy.geodetics.base import gps2dist_azimuth

rcParams.update({'font.size': 24})
#rcParams.update({'font.size': 36}) #poster
###rcParams['pdf.fonttype'] = 42
rcParams['font.sans-serif'] = "Helvetica"
rcParams['font.family'] = "sans-serif"

# Return event and phase data from HYPOINVERSE arc file
#
# Output:
#    event_dict: dictionary, key is event ID as integer
#    event_dict[ev_id]['event'] -> [origin time (UTCDateTime), lat (deg), lon (deg), depth (km), mag]
#    event_dict[ev_id]['P']['netsta'] -> list ['channel', arrival time (UTCDateTime), ph_res]
#    event_dict[ev_id]['S']['netsta'] -> list ['channel', arrival time (UTCDateTime), ph_res]
#    ev_id_list: event IDs in order
#
def get_selected_event_phase_data_hypoinverse_file(in_hinv_arc_file, select_ev_id):
   event_dict = {}
   ev_id_list = []
   num_bad_wt = 0
   with open(in_hinv_arc_file, 'r') as fin:
      for line in fin:
         # Read in an event
         if ((line[0:2] == '19') or (line[0:2] == '20')):
            flag_read = False
            ev_id = int(utils_hyp.get_event_id_hypoinverse_file(line))
            if (ev_id in select_ev_id):
               flag_read = True
               origin_time = utils_hyp.get_origin_time_hypoinverse_file(line)
               [lat_deg, lon_deg, depth_km] = utils_hyp.get_lat_lon_depth_hypoinverse_file(line) #CY
               mag = 0.01*int(line[147:150])
               num_ph = int(line[119:122])
               count_ph = 0
               ev_id_list.append(ev_id) # store in order
               event_dict[ev_id] = {}
               event_dict[ev_id]['event'] = [origin_time, lat_deg, lon_deg, depth_km, mag] #CY
               event_dict[ev_id]['P'] = {}
               event_dict[ev_id]['S'] = {}
#               print(line)

         # Read line marking end of an event
         elif ((line[0:2]) == '  '):
            if (flag_read):
               ev_id_end = int(line[62:72])
               if (ev_id != ev_id_end):
                  print("WARNING: event ID does not match: ", ev_id, ev_id_end)
               if (count_ph != num_ph):
                  print("WARNING: number of phases for this event does not match: ", ev_id, ev_id_end, count_ph, num_ph)

         # Read phase for this event
         else:
            if (flag_read):
#               [flag_good_phase, ph_str, net_sta, chan, ph_UTC_time, ph_res] = utils_hyp.get_one_phase_hypoinverse_file(line, ev_id)
               [flag_good_phase, ph_str, net_sta, chan, ph_UTC_time, ph_res, n_bad_wt] = utils_hyp.get_one_phase_hypoinverse_file(line, ev_id)
               num_bad_wt += n_bad_wt
               if (flag_good_phase):
                  event_dict[ev_id][ph_str][net_sta] = [chan, ph_UTC_time, ph_res]
                  count_ph += 1

#            print(year, month, day, hour, minute, ph_time, ph_str, ph_UTC_time, ph_wt)

   #         phase_str = sta.strip()+net.strip()+chan.strip()+p_str.strip()+s_str.strip()
   #         event_dict[ev_id]['phases'].append((phase_str, line))
   print("Number of events from HYPOINVERSE arc file: ", len(event_dict), len(ev_id_list))
   print("Number of bad phase weights: ", num_bad_wt)
   return [event_dict, ev_id_list]

def print_stats(t_res, label):
   t_mean = np.mean(t_res)
   t_std = np.std(t_res)
   t_median = np.median(t_res)
   t_mad = stats.median_abs_deviation(t_res)
   print(label, ", Min:", min(t_res), ", Max:", max(t_res), ", Mean:", t_mean, ", Std:", t_std,
      ", Median:", t_median, ", MAD:", t_mad)
   return [t_mean, t_std, t_median, t_mad]

def get_phase_list_time_period(input_phase_list, IND_OT, min_ot_arr, max_ot_arr, flag_inside_time=True):
   output_phase_list = []
   if (flag_inside_time):
      for ph in input_phase_list:
         flag_include = False
         for idxot, ot in enumerate(min_ot_arr):
            if ((ph[IND_OT] >= min_ot_arr[idxot]) and (ph[IND_OT] < max_ot_arr[idxot])):
               flag_include = True
               break
         if (flag_include):
            output_phase_list.append(ph)
#      output_phase_list = [ph for ph in input_phase_list for min_t, max_t in zip(min_ot_arr, max_ot_arr) if ((ph[IND_OT] >= min_t) and (ph[IND_OT] < max_t))]
   else:
      for ph in input_phase_list:
         flag_include = True
         for idxot, ot in enumerate(min_ot_arr):
            if ((ph[IND_OT] >= min_ot_arr[idxot]) and (ph[IND_OT] < max_ot_arr[idxot])):
               flag_include = False
               break
         if (flag_include):
            output_phase_list.append(ph)
#      output_phase_list = [ph for ph in input_phase_list for min_t, max_t in zip(min_ot_arr, max_ot_arr) if not((ph[IND_OT] >= min_t) and (ph[IND_OT] < max_t))]
   return output_phase_list

def plot_pick_time_residual_hist(t_res, label_str, title_str, out_plot_file, text_res='', text_nomatch=''):
   plt.figure(num=0, figsize=(10,8))
   plt.clf()
#   n, bins, patches = plt.hist([t_res], bins=np.arange(-2, 2, 0.01), label=[label_str], color=['yellow'], edgecolor='black', linewidth=0.2)
   n, bins, patches = plt.hist([t_res], bins=np.arange(-2, 2, 0.01), label=[label_str], color=['white'], edgecolor='black', linewidth=0.1)
   plt.xlim([-1, 1])
   plt.ylim([0, 9000]) # P
#   plt.ylim([0, 4000]) # S
   plt.xlabel("Pick time residual (s): $t_{cat} - t_{EQT}$")
   plt.ylabel("Number of picks")
   plt.title(title_str)
   plt.legend(prop={'size':20})
   plt.figtext(0.17, 0.75, text_res, fontsize=18)
   plt.figtext(0.17, 0.45, text_nomatch, fontsize=18)
   plt.tight_layout()
   plt.savefig(out_plot_file)



#=============================
# Script to compare picks from Comcat-Catalog vs EQT-HYPOINVERSE

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/station_list_edited.json'
#in_cat_xml_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/CatalogEventsDownload20220111/'
#
#
##in_match_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/events_MATCH_magcat_EQT_20180101_20220101.txt'
##in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/merged_real_magcat_locate_pr.arc'
###out_plot_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/MagPlots/'
##out_plot_dir = '../EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/MagPlots/'
#
#in_match_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/events_MATCH_magcat_EQT_20180101_20220101.txt'
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/merged_real_magcat_locate_pr.arc'
##out_plot_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/MagPlots/'
#out_plot_dir = '../EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/MagPlots/'
#
#out_match_list_file = out_plot_dir+'phase_list_MATCH_EQT_20180101_20220101.npz'
#out_cat_dict_file = out_plot_dir+'phase_cat_dict_MATCH_EQT_20180101_20220101.npz'
#out_eqt_dict_file = out_plot_dir+'phase_eqt_dict_MATCH_EQT_20180101_20220101.npz'
#out_comp_list_file = out_plot_dir+'phase_comp_list_MATCH_EQT_20180101_20220101.npz'



base_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20230101/'
in_station_file = base_dir+'station_list_edited.json'
in_cat_xml_dir = base_dir+'CatalogEventsDownload20230211/'
hinv_dir = base_dir+'REAL_VELZHANG/DIRECTHYPOSVI/'
in_match_catalog_file = hinv_dir+'events_MATCH_magcat_DIRECTHYPOSVI_EQT_20180101_20230101.txt'
in_hinv_arc_file = hinv_dir+'hyposvi_real_magcat_locate_keep_pr.arc'
out_plot_dir = hinv_dir+'MagPlots/'
out_plot_dir = '../EQT_20180101_20230101/REAL_VELZHANG/DIRECTHYPOSVI/MagPlots/' # only for local plots
out_match_list_file = out_plot_dir+'phase_list_MATCH_EQT_20180101_20230101.npz'
out_cat_dict_file = out_plot_dir+'phase_cat_dict_MATCH_EQT_20180101_20230101.npz'
out_eqt_dict_file = out_plot_dir+'phase_eqt_dict_MATCH_EQT_20180101_20230101.npz'
out_comp_list_file = out_plot_dir+'phase_comp_list_MATCH_EQT_20180101_20230101.npz'
in_station_file = out_plot_dir+'station_list_edited.json' # only for local plots


stations_ = json.load(open(in_station_file))

t0 = time.time()
# Read in event ids for matching events, in order: catalog and EQT
ev_cat_id_list = []
ev_eqt_id_list = []
event_cat_dict = {}
with open(in_match_catalog_file, 'r') as fev:
   for line in fev:
      ev_id = int(utils_hyp.get_event_id_hypoinverse_file(line)) # eqt ev_id
      ev_eqt_id_list.append(ev_id)

#      cat_line = line[180:].split() # cat ev_id
#      print(line[180:], cat_line)
      cat_line = line[151:].split() # cat ev_id
      print(line[151:], cat_line)
      ev_cat_id = cat_line[5]
      ev_cat_id_list.append(ev_cat_id)

      # Read in events for matching events: catalog
      cat_origin_time = UTCDateTime.strptime(cat_line[0], "%Y-%m-%dT%H:%M:%S.%f")
      cat_ev_lat = float(cat_line[1])
      cat_ev_lon = float(cat_line[2])
      cat_depth = float(cat_line[3])
      cat_mag = float(cat_line[4])
      event_cat_dict[ev_cat_id] = {}
      event_cat_dict[ev_cat_id]['event'] = [cat_origin_time, cat_ev_lat, cat_ev_lon, cat_depth, cat_mag]
      event_cat_dict[ev_cat_id]['P'] = {}
      event_cat_dict[ev_cat_id]['S'] = {}


# Read in events and phases for matching events: EQT
[event_eqt_dict, ev_eqt_id_list_list] = get_selected_event_phase_data_hypoinverse_file(in_hinv_arc_file, ev_eqt_id_list)
print("ev_eqt_id_list == ev_eqt_id_list_list", np.array_equal(ev_eqt_id_list, ev_eqt_id_list_list))

# Read in phases for matching events: catalog
for cat_evid in ev_cat_id_list:
   # Read in event file from catalog xml file, includes phase pick info
   in_xml_file = in_cat_xml_dir+cat_evid+'.xml'
   try:
      curr_ev = read_events(in_xml_file)
   except Exception:
      print("ERROR: Cannot find event xml file ", in_xml_file, ", skipping...", cat_evid)
      continue

   # Get phase picks
   phase_list = curr_ev[0].picks
   for pick in phase_list:
      sta = pick.waveform_id.station_code
      if (sta in stations_): # only output the phase if it is in our station list (json)
         net = pick.waveform_id.network_code
         chcode = pick.waveform_id.channel_code
         ph = pick.phase_hint
         print(cat_evid, net, sta, chcode, ph, pick.time)

         # Possible phase names in catalog
         if ((ph == 'Pg') or (chcode[-1] == 'Z')):
            ph = 'P'
         if ((ph == 'Sg') or (chcode[-1] == 'E') or (chcode[-1] == 'N') or (chcode[-1] == '1') or (chcode[-1] == '2')):
            ph = 'S'

         net_sta = net.strip()+sta.strip()
         ph_res = 0.0 # dummy
         event_cat_dict[cat_evid][ph][net_sta] = [chcode, pick.time, ph_res] # Add phase to dict

tfinal = time.time() - t0
print("Runtime for reading in catalog and EQT phases:", tfinal)

# Save for later
np.savez(out_match_list_file, ev_cat_id_list=ev_cat_id_list, ev_eqt_id_list=ev_eqt_id_list,
   event_cat_dict=event_cat_dict, event_eqt_dict=event_eqt_dict)
with open(out_cat_dict_file, 'wb') as handle:
   pickle.dump(event_cat_dict, handle, protocol=pickle.HIGHEST_PROTOCOL)
with open(out_eqt_dict_file, 'wb') as handle:
   pickle.dump(event_eqt_dict, handle, protocol=pickle.HIGHEST_PROTOCOL)

#==== Finished reading in phases into dicts=====

## Load precomputed dicts with phases
#data = np.load(out_match_list_file, allow_pickle=True)
#ev_cat_id_list = data['ev_cat_id_list']
#ev_eqt_id_list = data['ev_eqt_id_list']
#with open(out_cat_dict_file, 'rb') as handle:
#   event_cat_dict = pickle.load(handle)
#with open(out_eqt_dict_file, 'rb') as handle:
#   event_eqt_dict = pickle.load(handle)

t0 = time.time()
# Prep net_sta array
net_sta_arr = []
for sta in stations_:
   network = stations_[sta]['network']
   net_sta = network+sta
   net_sta_arr.append(net_sta)

# Now, for each common event between catalog and EQT, compare phases
phase_comp_list = []
for idx, ev_cat in enumerate(ev_cat_id_list):
   # curr_ev_info has 13 elements: [idx, cat_evid, cat_ot, cat_lat, cat_lon, cat_depth, cat_mag, eqt_evid, eqt_ot, eqt_lat, eqt_lon, eqt_depth, eqt_mag]
   curr_ev_info = [idx, ev_cat, event_cat_dict[ev_cat]['event'][0], event_cat_dict[ev_cat]['event'][1], 
      event_cat_dict[ev_cat]['event'][2], event_cat_dict[ev_cat]['event'][3], event_cat_dict[ev_cat]['event'][4],  
      ev_eqt_id_list[idx], event_eqt_dict[ev_eqt_id_list[idx]]['event'][0], event_eqt_dict[ev_eqt_id_list[idx]]['event'][1],
      event_eqt_dict[ev_eqt_id_list[idx]]['event'][2], event_eqt_dict[ev_eqt_id_list[idx]]['event'][3],
      event_eqt_dict[ev_eqt_id_list[idx]]['event'][4]]
   ev_lat = event_cat_dict[ev_cat]['event'][1]
   ev_lon = event_cat_dict[ev_cat]['event'][2]
   print("Event:", idx, ev_cat, ev_eqt_id_list[idx])
#   print("curr_ev_info:", curr_ev_info)
   for phase in ['P','S']:
      curr_cat_ph_list = event_cat_dict[ev_cat][phase]
      curr_eqt_ph_list = event_eqt_dict[ev_eqt_id_list[idx]][phase]
#      print(curr_cat_ph_list, curr_eqt_ph_list)
#      print(len(curr_cat_ph_list), len(curr_eqt_ph_list))

      num_tp = 0
      num_fp = 0
      num_fn = 0
      num_tn = 0
      for net_sta in net_sta_arr:
         sta_lat = stations_[net_sta[2:]]['coords'][0]
         sta_lon = stations_[net_sta[2:]]['coords'][1]
         t_res = np.nan # residual pick time t_eqt - t_catalog; not a nan only for (flag_pick_comp == 3)
         epi_dist = np.nan # epicentral distance (km) from event to station; compute only if t_res exists
         if ((net_sta in curr_cat_ph_list) and (net_sta in curr_eqt_ph_list)):
            flag_pick_comp = 3 # catalog - yes pick, EQT - yes pick (true positive)
            num_tp += 1
            # Compute residual pick time
            t_eqt = curr_eqt_ph_list[net_sta][1] # EQT pick time, UTCDateTime
            t_cat = curr_cat_ph_list[net_sta][1] # catalog pick time, UTCDateTime
            t_res = t_cat - t_eqt
#            print(idx, ev_cat, ev_eqt_id_list[idx], phase, net_sta, t_res)
            # Compute epicentral distance, using catalog event location
            [dist_ev_sta, azAB, azBA] = gps2dist_azimuth(ev_lat, ev_lon, sta_lat, sta_lon)
            epi_dist = 0.001*dist_ev_sta
#            print(net_sta, sta_lat, sta_lon, ev_lat, ev_lon, epi_dist) 
            curr_phase_info = [phase, net_sta, flag_pick_comp, t_res, epi_dist]
            curr_phase_comp_line = curr_ev_info + curr_phase_info
            phase_comp_list.append(curr_phase_comp_line)
#            print(curr_phase_comp_line)
         elif (net_sta in curr_eqt_ph_list):
            flag_pick_comp = 2 # catalog - no pick, EQT - yes pick (false positive - new EQT pick)
            num_fp += 1
            # Compute epicentral distance, using catalog event location
            [dist_ev_sta, azAB, azBA] = gps2dist_azimuth(ev_lat, ev_lon, sta_lat, sta_lon)
            epi_dist = 0.001*dist_ev_sta
            curr_phase_info = [phase, net_sta, flag_pick_comp, t_res, epi_dist]
            curr_phase_comp_line = curr_ev_info + curr_phase_info
            phase_comp_list.append(curr_phase_comp_line)
#            print(curr_phase_comp_line)
         elif (net_sta in curr_cat_ph_list):
            flag_pick_comp = 1 # catalog - yes pick, EQT - no pick (false negative - missed catalog pick)
            num_fn += 1
            # Compute epicentral distance, using catalog event location
            [dist_ev_sta, azAB, azBA] = gps2dist_azimuth(ev_lat, ev_lon, sta_lat, sta_lon)
            epi_dist = 0.001*dist_ev_sta
            curr_phase_info = [phase, net_sta, flag_pick_comp, t_res, epi_dist]
            curr_phase_comp_line = curr_ev_info + curr_phase_info
            phase_comp_list.append(curr_phase_comp_line)
#            print(curr_phase_comp_line)
         else:
            flag_pick_comp = 0 # catalog - no pick, EQT - no pick
            num_tn += 1

      print(idx, ev_cat, ev_eqt_id_list[idx], phase, "TP:", num_tp, ", FP:", num_fp, ", FN:", num_fn, ", TN:", num_tn)
print("Number of lines in phase_comp_list:", len(phase_comp_list))
tfinal = time.time() - t0
print("Runtime for comparing catalog and EQT phases:", tfinal)
np.savez(out_comp_list_file, phase_comp_list=phase_comp_list)

# ======== Filter the phase_comp_list ============

#data = np.load(out_comp_list_file, allow_pickle=True)
#phase_comp_list = data['phase_comp_list']

# Filter for phases where both catalog and EQT had a pick (true positive), and the residual exists
min_lat = 17.6
max_lat = 18.3
min_lon = -67.3
max_lon = -66.4
min_depth=-1.
max_depth=40.

IND_IDX = 0
IND_CAT_OT = 2
IND_CAT_LAT = 3
IND_CAT_LON = 4
IND_CAT_DEPTH = 5
IND_EQT_MAG = 12
IND_PHASE = 13
IND_FLAG_PICK_COMP = 15
IND_T_RES = 16
IND_EPI_DIST = 17

#======= All phases (either P or S) =========

#phase_type = 'P'
phase_type = 'S'

#tp_phases_sel = [ph for ph in phase_comp_list if (ph[IND_PHASE]==phase_type and ph[IND_FLAG_PICK_COMP]==3)]
tp_phases_sel = [ph for ph in phase_comp_list if ((ph[IND_PHASE]==phase_type) and (ph[IND_FLAG_PICK_COMP]==3) and 
   (ph[IND_CAT_LAT] >= min_lat) and (ph[IND_CAT_LAT] <= max_lat) and (ph[IND_CAT_LON] >= min_lon) and (ph[IND_CAT_LON] <= max_lon) and
   (ph[IND_CAT_DEPTH] >= min_depth) and (ph[IND_CAT_DEPTH] <= max_depth))]
t_res_sel = [ph[IND_T_RES] for ph in tp_phases_sel]
[t_mean, t_std, t_median, t_mad] = print_stats(t_res_sel, phase_type+" t_res")

fp_new_phases_sel = [ph for ph in phase_comp_list if ((ph[IND_PHASE]==phase_type) and (ph[IND_FLAG_PICK_COMP]==2) and 
   (ph[IND_CAT_LAT] >= min_lat) and (ph[IND_CAT_LAT] <= max_lat) and (ph[IND_CAT_LON] >= min_lon) and (ph[IND_CAT_LON] <= max_lon) and
   (ph[IND_CAT_DEPTH] >= min_depth) and (ph[IND_CAT_DEPTH] <= max_depth))]
fn_miss_phases_sel = [ph for ph in phase_comp_list if ((ph[IND_PHASE]==phase_type) and (ph[IND_FLAG_PICK_COMP]==1) and 
   (ph[IND_CAT_LAT] >= min_lat) and (ph[IND_CAT_LAT] <= max_lat) and (ph[IND_CAT_LON] >= min_lon) and (ph[IND_CAT_LON] <= max_lon) and
   (ph[IND_CAT_DEPTH] >= min_depth) and (ph[IND_CAT_DEPTH] <= max_depth))]
num_tp_sel = len(tp_phases_sel)
num_fp_new_sel = len(fp_new_phases_sel)
num_fn_miss_sel = len(fn_miss_phases_sel)
new_pick_ratio_sel = float(num_tp_sel + num_fp_new_sel) / float(num_tp_sel)
recall_sel = float(num_tp_sel) / float(num_tp_sel + num_fn_miss_sel)
print(phase_type, " phases --- TP match:", num_tp_sel, ", FP new:", num_fp_new_sel, ", New Pick Ratio: ", new_pick_ratio_sel, ", FN miss:", num_fn_miss_sel, ", Recall:", recall_sel, "\n")

out_plot_file = out_plot_dir+'EQT_20180101_20230101_t_res_'+phase_type+'.pdf'
text_res = "$\mu$: "+f"{t_mean:.2f}"+" s\n$\sigma$: "+f"{t_std:.2f}"+" s\nmedian: "+f"{t_median:.2f}"+" s\nMAD: "+f"{t_mad:.2f}"+" s"
text_nomatch = "New EQT picks: "+str(num_fp_new_sel)+"\nNew Pick Ratio: "+f"{new_pick_ratio_sel:.2f}"+"\n\nMissed catalog picks: "+str(num_fn_miss_sel)+"\nRecall: "+f"{recall_sel:.2f}"
plot_pick_time_residual_hist(t_res_sel, 'All '+phase_type, str(num_tp_sel)+' total '+phase_type+' picks', out_plot_file, text_res, text_nomatch)

#======= Phases within a certain epicentral distance (either P or S) =========

min_epi_dist = 0
max_epi_dist = 20
#min_epi_dist = 20
#max_epi_dist = 40
#min_epi_dist = 40
#max_epi_dist = 60
#min_epi_dist = 60
#max_epi_dist = 80
#min_epi_dist = 80
#max_epi_dist = 100
#min_epi_dist = 100
#max_epi_dist = 250

tp_epi_dist_phases = [ph for ph in tp_phases_sel if ((ph[IND_EPI_DIST] >= min_epi_dist) and (ph[IND_EPI_DIST] < max_epi_dist))]
t_res_epi_dist = [ph[IND_T_RES] for ph in tp_epi_dist_phases]
[t_mean_epi_dist, t_std_epi_dist, t_median_epi_dist, t_mad_epi_dist] = print_stats(t_res_epi_dist, phase_type+" t_res_epi_dist, min_epi_dist:"+str(min_epi_dist)+", max_epi_dist:"+str(max_epi_dist))

fp_new_epi_dist_phases = [ph for ph in fp_new_phases_sel if ((ph[IND_EPI_DIST] >= min_epi_dist) and (ph[IND_EPI_DIST] < max_epi_dist))]
fn_miss_epi_dist_phases = [ph for ph in fn_miss_phases_sel if ((ph[IND_EPI_DIST] >= min_epi_dist) and (ph[IND_EPI_DIST] < max_epi_dist))]

num_tp_epi_dist_sel = len(tp_epi_dist_phases)
num_fp_new_epi_dist_sel = len(fp_new_epi_dist_phases)
num_fn_miss_epi_dist_sel = len(fn_miss_epi_dist_phases)
new_pick_ratio_epi_dist_sel = float(num_tp_epi_dist_sel + num_fp_new_epi_dist_sel) / float(num_tp_epi_dist_sel)
recall_epi_dist_sel = float(num_tp_epi_dist_sel) / float(num_tp_epi_dist_sel + num_fn_miss_epi_dist_sel)
print(phase_type, " phases, min_epi_dist:", min_epi_dist, ", max_epi_dist:", max_epi_dist,
   ", --- TP match:", num_tp_epi_dist_sel, ", FP new:", num_fp_new_epi_dist_sel, ", New Pick Ratio: ", new_pick_ratio_epi_dist_sel, 
   ", FN miss:", num_fn_miss_epi_dist_sel, ", Recall:", recall_epi_dist_sel, "\n")

out_plot_file = out_plot_dir+'EQT_20180101_20230101_t_res_'+phase_type+'_epi_dist_'+str(min_epi_dist)+'_'+str(max_epi_dist)+'.pdf'
text_res = "$\mu$: "+f"{t_mean_epi_dist:.2f}"+" s\n$\sigma$: "+f"{t_std_epi_dist:.2f}"+" s\nmedian: "+f"{t_median_epi_dist:.2f}"+" s\nMAD: "+f"{t_mad_epi_dist:.2f}"+" s"
text_nomatch = "New EQT picks: "+str(num_fp_new_epi_dist_sel)+"\nNew Pick Ratio: "+f"{new_pick_ratio_epi_dist_sel:.2f}"+"\n\nMissed catalog picks: "+str(num_fn_miss_epi_dist_sel)+"\nRecall: "+f"{recall_epi_dist_sel:.2f}"
plot_pick_time_residual_hist(t_res_epi_dist, str(min_epi_dist)+'-'+str(max_epi_dist)+' km', str(num_tp_epi_dist_sel)+' '+phase_type+' picks', out_plot_file, text_res, text_nomatch)


#======= Phases within a certain magnitude range (either P or S) =========

#min_mag = -1
#max_mag = 1
#min_mag = 1
#max_mag = 2
#min_mag = 2
#max_mag = 3
min_mag = 3
max_mag = 4
#min_mag = 4
#max_mag = 7

tp_mag_phases = [ph for ph in tp_phases_sel if ((ph[IND_EQT_MAG] >= min_mag) and (ph[IND_EQT_MAG] < max_mag))]
t_res_mag = [ph[IND_T_RES] for ph in tp_mag_phases]
[t_mean_mag, t_std_mag, t_median_mag, t_mad_mag] = print_stats(t_res_mag, phase_type+" t_res_mag, min_mag:"+str(min_mag)+", max_mag:"+str(max_mag))

fp_new_mag_phases = [ph for ph in fp_new_phases_sel if ((ph[IND_EQT_MAG] >= min_mag) and (ph[IND_EQT_MAG] < max_mag))]
fn_miss_mag_phases = [ph for ph in fn_miss_phases_sel if ((ph[IND_EQT_MAG] >= min_mag) and (ph[IND_EQT_MAG] < max_mag))]

num_tp_mag_sel = len(tp_mag_phases)
num_fp_new_mag_sel = len(fp_new_mag_phases)
num_fn_miss_mag_sel = len(fn_miss_mag_phases)
new_pick_ratio_mag_sel = float(num_tp_mag_sel + num_fp_new_mag_sel) / float(num_tp_mag_sel)
recall_mag_sel = float(num_tp_mag_sel) / float(num_tp_mag_sel + num_fn_miss_mag_sel)
print(phase_type, " phases, min_mag:", min_mag, ", max_mag:", max_mag,
   ", --- TP match:", num_tp_mag_sel, ", FP new:", num_fp_new_mag_sel, ", New Pick Ratio: ", new_pick_ratio_mag_sel,
   ", FN miss:", num_fn_miss_mag_sel, ", Recall:", recall_mag_sel, "\n")

out_plot_file = out_plot_dir+'EQT_20180101_20230101_t_res_'+phase_type+'_mag_'+str(min_mag)+'_'+str(max_mag)+'.pdf'
text_res = "$\mu$: "+f"{t_mean_mag:.2f}"+" s\n$\sigma$: "+f"{t_std_mag:.2f}"+" s\nmedian: "+f"{t_median_mag:.2f}"+" s\nMAD: "+f"{t_mad_mag:.2f}"+" s"
text_nomatch = "New EQT picks: "+str(num_fp_new_mag_sel)+"\nNew Pick Ratio: "+f"{new_pick_ratio_mag_sel:.2f}"+"\n\nMissed catalog picks: "+str(num_fn_miss_mag_sel)+"\nRecall: "+f"{recall_mag_sel:.2f}"
plot_pick_time_residual_hist(t_res_mag, str(min_mag)+' < M < '+str(max_mag), str(num_tp_mag_sel)+' '+phase_type+' picks', out_plot_file, text_res, text_nomatch)


#======= Phases within a certain time period (either P or S) =========

min_ot_arr = [UTCDateTime(2018,1,1,0,0,0), UTCDateTime(2020,11,18,0,0,0), UTCDateTime(2021,1,1,0,0,0)]
max_ot_arr = [UTCDateTime(2020,1,8,7,23,0), UTCDateTime(2020,11,19,0,0,0), UTCDateTime(2021,1,31,0,0,0)] #20230206
#max_ot_arr = [UTCDateTime(2020,1,8,7,23,0), UTCDateTime(2020,11,19,0,0,0), UTCDateTime(2021,1,28,0,0,0)] #20220111
flag_inside_time = True
#flag_inside_time = False

tp_ot_phases = get_phase_list_time_period(tp_phases_sel, IND_CAT_OT, min_ot_arr, max_ot_arr, flag_inside_time)
t_res_ot = [ph[IND_T_RES] for ph in tp_ot_phases]
[t_mean_ot, t_std_ot, t_median_ot, t_mad_ot] = print_stats(t_res_ot, phase_type+" t_res_ot, flag_inside_time:"+str(flag_inside_time))

fp_new_ot_phases = get_phase_list_time_period(fp_new_phases_sel, IND_CAT_OT, min_ot_arr, max_ot_arr, flag_inside_time)
fn_miss_ot_phases = get_phase_list_time_period(fn_miss_phases_sel, IND_CAT_OT, min_ot_arr, max_ot_arr, flag_inside_time)

num_tp_ot_sel = len(tp_ot_phases)
num_fp_new_ot_sel = len(fp_new_ot_phases)
num_fn_miss_ot_sel = len(fn_miss_ot_phases)
new_pick_ratio_ot_sel = float(num_tp_ot_sel + num_fp_new_ot_sel) / float(num_tp_ot_sel)
recall_ot_sel = float(num_tp_ot_sel) / float(num_tp_ot_sel + num_fn_miss_ot_sel)
print(phase_type, " phases, flag_inside_time:", flag_inside_time,
   ", --- TP match:", num_tp_ot_sel, ", FP new:", num_fp_new_ot_sel, ", New Pick Ratio: ", new_pick_ratio_ot_sel,
   ", FN miss:", num_fn_miss_ot_sel, ", Recall:", recall_ot_sel, "\n")

out_plot_file = out_plot_dir+'EQT_20180101_20230101_t_res_'+phase_type+'_ot_'+str(flag_inside_time)+'.pdf'
text_res = "$\mu$: "+f"{t_mean_ot:.2f}"+" s\n$\sigma$: "+f"{t_std_ot:.2f}"+" s\nmedian: "+f"{t_median_ot:.2f}"+" s\nMAD: "+f"{t_mad_ot:.2f}"+" s"
text_nomatch = "New EQT picks: "+str(num_fp_new_ot_sel)+"\nNew Pick Ratio: "+f"{new_pick_ratio_ot_sel:.2f}"+"\n\nMissed catalog picks: "+str(num_fn_miss_ot_sel)+"\nRecall: "+f"{recall_ot_sel:.2f}"
plot_pick_time_residual_hist(t_res_ot, 'analyst: '+str(flag_inside_time), str(num_tp_ot_sel)+' '+phase_type+' picks', out_plot_file, text_res, text_nomatch)


# ========= Compare number of phases (either P or S) from catalog and EQT, for the same event ===========
cat_phases_sel_temp = [ph for ph in phase_comp_list if ((ph[IND_PHASE]==phase_type) and ((ph[IND_FLAG_PICK_COMP]==3) or (ph[IND_FLAG_PICK_COMP]==1)) and
   (ph[IND_CAT_LAT] >= min_lat) and (ph[IND_CAT_LAT] <= max_lat) and (ph[IND_CAT_LON] >= min_lon) and (ph[IND_CAT_LON] <= max_lon) and
   (ph[IND_CAT_DEPTH] >= min_depth) and (ph[IND_CAT_DEPTH] <= max_depth))]
#cat_phases_sel_temp = [ph for ph in phase_comp_list if (((ph[IND_FLAG_PICK_COMP]==3) or (ph[IND_FLAG_PICK_COMP]==1)) and
#   (ph[IND_CAT_LAT] >= min_lat) and (ph[IND_CAT_LAT] <= max_lat) and (ph[IND_CAT_LON] >= min_lon) and (ph[IND_CAT_LON] <= max_lon) and
#   (ph[IND_CAT_DEPTH] >= min_depth) and (ph[IND_CAT_DEPTH] <= max_depth))] # P and S
#cat_phases_sel = cat_phases_sel_temp # do not separate by time
cat_phases_sel = get_phase_list_time_period(cat_phases_sel_temp, IND_CAT_OT, min_ot_arr, max_ot_arr, flag_inside_time) # filter further by flag_inside_time
cat_phases_info = []
idx = 0
num_ph = 0
for ph in cat_phases_sel:
   if (idx == ph[IND_IDX]):
      num_ph += 1
   else:
      if (num_ph > 0):
         add_phase = [idx, num_ph, ph[IND_EQT_MAG]]
      else:
         add_phase = [idx, np.nan, ph[IND_EQT_MAG]]
      cat_phases_info.append(add_phase)
      num_ph = 0
      idx += 1
num_cat_phases = [el[1] for el in cat_phases_info]

eqt_phases_sel_temp = [ph for ph in phase_comp_list if ((ph[IND_PHASE]==phase_type) and ((ph[IND_FLAG_PICK_COMP]==3) or (ph[IND_FLAG_PICK_COMP]==2)) and
   (ph[IND_CAT_LAT] >= min_lat) and (ph[IND_CAT_LAT] <= max_lat) and (ph[IND_CAT_LON] >= min_lon) and (ph[IND_CAT_LON] <= max_lon) and
   (ph[IND_CAT_DEPTH] >= min_depth) and (ph[IND_CAT_DEPTH] <= max_depth))]
#eqt_phases_sel_temp = [ph for ph in phase_comp_list if (((ph[IND_FLAG_PICK_COMP]==3) or (ph[IND_FLAG_PICK_COMP]==2)) and
#   (ph[IND_CAT_LAT] >= min_lat) and (ph[IND_CAT_LAT] <= max_lat) and (ph[IND_CAT_LON] >= min_lon) and (ph[IND_CAT_LON] <= max_lon) and
#   (ph[IND_CAT_DEPTH] >= min_depth) and (ph[IND_CAT_DEPTH] <= max_depth))] # P and S
#eqt_phases_sel = eqt_phases_sel_temp # do not separate by time
eqt_phases_sel = get_phase_list_time_period(eqt_phases_sel_temp, IND_CAT_OT, min_ot_arr, max_ot_arr, flag_inside_time) # filter further by flag_inside_time
eqt_phases_info = []
idx = 0
num_ph = 0
for ph in eqt_phases_sel:
   if (idx == ph[IND_IDX]):
      num_ph += 1
   else:
      if (num_ph > 0):
         add_phase = [idx, num_ph, ph[IND_EQT_MAG]]
      else:
         add_phase = [idx, np.nan, ph[IND_EQT_MAG]]
      eqt_phases_info.append(add_phase)
      num_ph = 0
      idx += 1
num_eqt_phases = [el[1] for el in eqt_phases_info]
mag_array = [el[2] for el in eqt_phases_info]

# Plot number of picks histogram for common events: catalog vs eqt
print("num_cat_phases:", len(num_cat_phases), "np.nanmin: ", np.nanmin(num_cat_phases), ", np.nanmax:", np.nanmax(num_cat_phases))
print("num_eqt_phases:", len(num_eqt_phases), "np.nanmin: ", np.nanmin(num_eqt_phases), ", np.nanmax:", np.nanmax(num_eqt_phases))
size_plot_mag_sq = np.multiply(mag_array, mag_array)
size_plot_mag_arr = np.multiply(size_plot_mag_sq, 5)

min_npicks = 0
max_npicks = 30
#max_npicks = 60
equal_line = np.linspace(min_npicks, max_npicks, 100)
phase_bins = np.arange(min_npicks-0.5, max_npicks+1.5, 1)

plt.figure(num=1, figsize=(6,6))
plt.clf()
[hist, xedges, yedges, mesh] = plt.hist2d(num_cat_phases, num_eqt_phases, bins=[phase_bins, phase_bins], cmap='cool', cmin=1, vmin=0, vmax=150)
#plt.scatter(num_cat_phases, num_eqt_phases, size_plot_mag_arr, facecolors='none', edgecolors='k', linewidths=0.1)
plt.plot(equal_line, equal_line, '--', color='k', linewidth=0.8)
plt.axis('scaled')
plt.xlim([min_npicks, max_npicks])
plt.ylim([min_npicks, max_npicks])
plt.xticks([0, 10, 20, 30])
plt.yticks([0, 10, 20, 30])
#----
#phase_type = 'P+S'
#plt.xticks([0, 20, 40, 60])
#plt.yticks([0, 20, 40, 60])
#----
plt.colorbar(orientation="horizontal", label="Number of events", pad=0.25)
plt.xlabel('Number of '+phase_type+' picks, catalog')
plt.ylabel('Number of '+phase_type+' picks,\n EQT')
plt.tight_layout()
plt.savefig(out_plot_dir+'EQT_20180101_20230101_compare_numpicks_'+phase_type+'.pdf')
#plt.savefig(out_plot_dir+'EQT_20180101_20230101_compare_numpicks_'+phase_type+'_ot_'+str(flag_inside_time)+'.pdf')

