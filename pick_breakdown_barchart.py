import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rcParams

rcParams.update({'font.size': 24})
#rcParams.update({'font.size': 36}) #poster
###rcParams['pdf.fonttype'] = 42
rcParams['font.sans-serif'] = "Helvetica"
rcParams['font.family'] = "sans-serif"

# Adapted from
# https://matplotlib.org/stable/gallery/lines_bars_and_markers/horizontal_barchart_distribution.html

#out_dir = '../EQT_20180101_20220101/Plots/'
#
#out_file = out_dir+'breakdown_barchart_distance.pdf'
#category_names = ['0-20 km', '20-40 km', '40-60 km', '60-80 km', '80-100 km', '100-250 km']
#results = {
#    '159419 total P': [42767, 54149, 26234, 19350, 11690, 5229],
#    '133035 total S': [36760, 47059, 22216, 15418, 8492, 3090]
#}

#out_file = out_dir+'breakdown_barchart_magnitude.pdf'
#category_names = ['-1 < M < 1', '1 < M < 2', '2 < M < 3', '3 < M < 4', '4 < M < 7']
#results = {
#    '159419 total P': [5315, 68388, 69338, 14473, 1905],
#    '133035 total S': [5082, 61516, 54877, 10375, 1185] 
#}

#out_file = out_dir+'breakdown_barchart_time.pdf'
#category_names = ['Analyst complete times', 'Analyst incomplete times']
#results = {
#    '159419 total P': [28890, 130529],
#    '133035 total S': [26390, 106645]
#}

out_dir = '../EQT_20180101_20230101/Plots/'

#out_file = out_dir+'breakdown_barchart_distance.pdf'
#category_names = ['0-20 km', '20-40 km', '40-60 km', '60-80 km', '80-100 km', '100-250 km']
#results = {
#    '192119 total P': [57741, 66368, 30571, 20472, 12280, 4687],
#    '160584 total S': [53021, 58536, 24497, 14359, 7675, 2496]
#}

out_file = out_dir+'breakdown_barchart_magnitude.pdf'
category_names = ['-1 < M < 1', '1 < M < 2', '2 < M < 3', '3 < M < 4', '4 < M < 7']
results = {
    '192119 total P': [12713, 88146, 74401, 14967, 1892],
    '160584 total S': [12601, 79586, 56877, 10345, 1175]
}


def survey(results, category_names):
   """
   Parameters
   ----------
   results : dict
     A mapping from question labels to a list of answers per category.
     It is assumed all lists contain the same number of entries and that
     it matches the length of *category_names*.
   category_names : list of str
     The category labels.
   """
   labels = list(results.keys())
   data = np.array(list(results.values()))
   data_cum = data.cumsum(axis=1)
   category_colors = plt.colormaps['RdYlGn'](
     np.linspace(0.15, 0.85, data.shape[1]))

   fig, ax = plt.subplots(figsize=(20, 4))
   ax.invert_yaxis()
   ax.xaxis.set_visible(False)
   ax.set_xlim(0, np.sum(data, axis=1).max())

   for i, (colname, color) in enumerate(zip(category_names, category_colors)):
      widths = data[:, i]
      starts = data_cum[:, i] - widths
      rects = ax.barh(labels, widths, left=starts, height=0.5,
                     label=colname, color=color, alpha=0.8)

      r, g, b, _ = color
      text_color = 'white' if r * g * b < 0.5 else 'darkgrey'
      ax.bar_label(rects, label_type='center', color=text_color)

   ax.legend(ncol=len(category_names), bbox_to_anchor=(0, 0.37),
           loc='lower left', fontsize='small')

   return fig, ax


survey(results, category_names)
plt.savefig(out_file)
