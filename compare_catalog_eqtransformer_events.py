from obspy.geodetics.base import gps2dist_azimuth
from collections import defaultdict
import utils_hypoinverse as utils_hyp
import math
import numpy as np


# Read in catalog, ComCat format
def get_comcat_catalog_map(in_catalog_file):
   map_catalog = defaultdict(lambda: defaultdict(int))
   nitem = 0
   with open(in_catalog_file, 'r') as fcat:
      for line in fcat:
         if (line[0] == '#'): # skip first line
            continue
         split_line = line.split('|')
         string_day = split_line[1][0:10].replace('-','') #YYYYMMDD
         hour = split_line[1][11:13]
         minute = split_line[1][14:16]
         second = split_line[1][17:]
         nsec = int(round(3600*int(hour) + 60*int(minute) + float(second))) # number of seconds within the day (0..86400)
#         print(string_day, hour, minute, second, nsec)
         map_catalog[string_day][nsec] = split_line
         nitem += 1
   print("Number of events read in:", nitem)
   return map_catalog


# Script to compare catalog events with EQTransformer events

#in_catalog_file = '../Catalog/catalog_puerto_rico_20200107_20200114.txt' # Comcat text file format
#in_hinv_sum_eqt_file = '../LargeAreaEQTransformer/association/merged_locate_pr.sum' # HYPOINVERSE summary format
#out_dir = '../LargeAreaEQTransformer/association/'
#out_match_file = out_dir+'events_MATCH_eqt_20200107_20200114.txt'
#out_missed_file = out_dir+'events_MISSED_eqt_20200107_20200114.txt'
#out_new_file = out_dir+'events_NEW_eqt_20200107_20200114.txt'

#in_catalog_file = '../Catalog/catalog_puerto_rico_20200107_20200114.txt' # Comcat text file format
#in_hinv_sum_eqt_file = '../LargeAreaEQTransformer/REAL/Events/merged_real_locate_pr.sum' # HYPOINVERSE summary format
#out_dir = '../LargeAreaEQTransformer/REAL/Events/'
#out_match_file = out_dir+'events_MATCH_eqt_20200107_20200114.txt'
#out_missed_file = out_dir+'events_MISSED_eqt_20200107_20200114.txt'
#out_new_file = out_dir+'events_NEW_eqt_20200107_20200114.txt'

#in_catalog_file = '../Catalog/catalog_puerto_rico_20200107_20200114.txt' # Comcat text file format
#in_hinv_sum_eqt_file = '../LargeAreaEQTransformer/PhaseLink/merged_pl_locate_pr.sum' # HYPOINVERSE summary format
#out_dir = '../LargeAreaEQTransformer/PhaseLink/'
#out_match_file = out_dir+'events_MATCH_eqt_20200107_20200114.txt'
#out_missed_file = out_dir+'events_MISSED_eqt_20200107_20200114.txt'
#out_new_file = out_dir+'events_NEW_eqt_20200107_20200114.txt'

#in_catalog_file = '../Catalog/catalog_puerto_rico_20200107_20200114.txt' # Comcat text file format
#in_hinv_sum_eqt_file = '../LargeAreaEQTransformer/association/merged_magcat_locate_pr.sum' # HYPOINVERSE summary format
#out_dir = '../LargeAreaEQTransformer/association/'
#out_match_file = out_dir+'events_MATCH_magcat_eqt_20200107_20200114.txt'
#out_missed_file = out_dir+'events_MISSED_magcat_eqt_20200107_20200114.txt'
#out_new_file = out_dir+'events_NEW_magcat_eqt_20200107_20200114.txt'

#in_catalog_file = '../Catalog/catalog_puerto_rico_20200107_20200114.txt' # Comcat text file format
#in_hinv_sum_eqt_file = '../LargeAreaEQTransformer/REAL/Events/merged_real_magcat_locate_pr.sum' # HYPOINVERSE summary format
#out_dir = '../LargeAreaEQTransformer/REAL/Events/'
#out_match_file = out_dir+'events_MATCH_magcat_eqt_20200107_20200114.txt'
#out_missed_file = out_dir+'events_MISSED_magcat_eqt_20200107_20200114.txt'
#out_new_file = out_dir+'events_NEW_magcat_eqt_20200107_20200114.txt'

#in_catalog_file = '../Catalog/catalog_puerto_rico_2018_2020.txt' # Comcat text file format
#in_hinv_sum_eqt_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/association/merged_locate_pr.sum' # HYPOINVERSE summary format
#out_dir = '/media/yoon/INT01/PuertoRico/FullEQTransformer/association/'
#out_match_file = out_dir+'events_MATCH_eqt_2018_2020.txt'
#out_missed_file = out_dir+'events_MISSED_eqt_2018_2020.txt'
#out_new_file = out_dir+'events_NEW_eqt_2018_2020.txt'

#in_catalog_file = '../Catalog/catalog_puerto_rico_2018_2020.txt' # Comcat text file format
#in_hinv_sum_eqt_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/REAL/HYPOINVERSE/merged_real_locate_pr.sum' # HYPOINVERSE summary format
#out_dir = '/media/yoon/INT01/PuertoRico/FullEQTransformer/REAL/HYPOINVERSE/'
#out_match_file = out_dir+'events_MATCH_eqt_2018_2020.txt'
#out_missed_file = out_dir+'events_MISSED_eqt_2018_2020.txt'
#out_new_file = out_dir+'events_NEW_eqt_2018_2020.txt'

#in_catalog_file = '../Catalog/catalog_puerto_rico_20180101_20210201.txt' # Comcat text file format
##in_hinv_sum_eqt_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/PhaseLink/merged_pl_locate_pr.sum' # HYPOINVERSE summary format
##out_dir = '/media/yoon/INT01/PuertoRico/FullEQTransformer/PhaseLink/'
#in_hinv_sum_eqt_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/REAL/HYPOINVERSE/merged_real_locate_pr.sum' # HYPOINVERSE summary format
#out_dir = '/media/yoon/INT01/PuertoRico/FullEQTransformer/REAL/HYPOINVERSE/'
#out_match_file = out_dir+'events_MATCH_eqt_20180101_20210201.txt'
#out_missed_file = out_dir+'events_MISSED_eqt_20180101_20210201.txt'
#out_new_file = out_dir+'events_NEW_eqt_20180101_20210201.txt'

#in_catalog_file = '../Catalog/catalog_puerto_rico_2018_2020.txt' # Comcat text file format
#in_hinv_sum_eqt_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/association/merged_magcat_locate_pr.sum' # HYPOINVERSE summary format
#out_dir = '/media/yoon/INT01/PuertoRico/FullEQTransformer/association/'
#out_match_file = out_dir+'events_MATCH_magcat_eqt_2018_2020.txt'
#out_missed_file = out_dir+'events_MISSED_magcat_eqt_2018_2020.txt'
#out_new_file = out_dir+'events_NEW_magcat_eqt_2018_2020.txt'

#in_catalog_file = '../Catalog/catalog_puerto_rico_2018_2020.txt' # Comcat text file format
#in_hinv_sum_eqt_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/REAL/HYPOINVERSE/merged_real_magcat_locate_pr.sum' # HYPOINVERSE summary format
#out_dir = '/media/yoon/INT01/PuertoRico/FullEQTransformer/REAL/HYPOINVERSE/'
#out_match_file = out_dir+'events_MATCH_magcat_eqt_2018_2020.txt'
#out_missed_file = out_dir+'events_MISSED_magcat_eqt_2018_2020.txt'
#out_new_file = out_dir+'events_NEW_magcat_eqt_2018_2020.txt'

#--------------------------
#in_catalog_file = '../Catalog/EQT_20200107_20200114/catalog_puerto_rico_20200107_20200114_download20210601.txt' # Comcat text file format
#in_hinv_sum_eqt_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELPRSN/merged_real_locate_pr.sum' # HYPOINVERSE summary format
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELPRSN/'
#out_match_file = out_dir+'events_MATCH_EQT_20200107_20200114.txt'
#out_missed_file = out_dir+'events_MISSED_EQT_20200107_20200114.txt'
#out_new_file = out_dir+'events_NEW_EQT_20200107_20200114.txt'

#in_catalog_file = '../Catalog/EQT_20200107_20200114/catalog_puerto_rico_20200107_20200114_catalogus_contributorus_download20210601.txt' # Comcat text file format - us only (for magnitudes)
#in_hinv_sum_eqt_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELPRSN/merged_real_locate_pr.sum' # HYPOINVERSE summary format
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELPRSN/'
#out_match_file = out_dir+'us_events_MATCH_EQT_20200107_20200114.txt'
#out_missed_file = out_dir+'us_events_MISSED_EQT_20200107_20200114.txt'
#out_new_file = out_dir+'us_events_NEW_EQT_20200107_20200114.txt'

#in_catalog_file = '../Catalog/EQT_20200107_20200114/catalog_puerto_rico_20200107_20200114_download20210601.txt' # Comcat text file format
#in_hinv_sum_eqt_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELPRSN/merged_real_magcat_locate_pr.sum' # HYPOINVERSE summary format
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELPRSN/'
#out_match_file = out_dir+'events_MATCH_magcat_EQT_20200107_20200114.txt'
#out_missed_file = out_dir+'events_MISSED_magcat_EQT_20200107_20200114.txt'
#out_new_file = out_dir+'events_NEW_magcat_EQT_20200107_20200114.txt'

#in_catalog_file = '../Catalog/EQT_20200107_20200114/catalog_puerto_rico_20200107_20200114_download20210601.txt' # Comcat text file format
#in_hinv_sum_eqt_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/merged_real_locate_pr.sum' # HYPOINVERSE summary format
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/'
#out_match_file = out_dir+'events_MATCH_EQT_20200107_20200114.txt'
#out_missed_file = out_dir+'events_MISSED_EQT_20200107_20200114.txt'
#out_new_file = out_dir+'events_NEW_EQT_20200107_20200114.txt'

#in_catalog_file = '../Catalog/EQT_20200107_20200114/catalog_puerto_rico_20200107_20200114_catalogus_contributorus_download20210601.txt' # Comcat text file format - us only (for magnitudes)
#in_hinv_sum_eqt_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/merged_real_locate_pr.sum' # HYPOINVERSE summary format
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/'
#out_match_file = out_dir+'us_events_MATCH_EQT_20200107_20200114.txt'
#out_missed_file = out_dir+'us_events_MISSED_EQT_20200107_20200114.txt'
#out_new_file = out_dir+'us_events_NEW_EQT_20200107_20200114.txt'

#in_catalog_file = '../Catalog/EQT_20200107_20200114/catalog_puerto_rico_20200107_20200114_download20210601.txt' # Comcat text file format
#in_hinv_sum_eqt_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/merged_real_magcat_locate_pr.sum' # HYPOINVERSE summary format
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/'
#out_match_file = out_dir+'events_MATCH_magcat_EQT_20200107_20200114.txt'
#out_missed_file = out_dir+'events_MISSED_magcat_EQT_20200107_20200114.txt'
#out_new_file = out_dir+'events_NEW_magcat_EQT_20200107_20200114.txt'

#in_catalog_file = '../Catalog/EQT_20191228_20200114/catalog_puerto_rico_20191228_20200114_download20210608.txt' # Comcat text file format
#in_hinv_sum_eqt_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELPRSN/merged_real_locate_pr.sum' # HYPOINVERSE summary format
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELPRSN/'
#out_match_file = out_dir+'events_MATCH_EQT_20191228_20200114.txt'
#out_missed_file = out_dir+'events_MISSED_EQT_20191228_20200114.txt'
#out_new_file = out_dir+'events_NEW_EQT_20191228_20200114.txt'

#in_catalog_file = '../Catalog/EQT_20191228_20200114/catalog_puerto_rico_20191228_20200114_catalogus_contributorus_download20210608.txt' # Comcat text file format - us only (for magnitudes)
#in_hinv_sum_eqt_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELPRSN/merged_real_locate_pr.sum' # HYPOINVERSE summary format
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELPRSN/'
#out_match_file = out_dir+'us_events_MATCH_EQT_20191228_20200114.txt'
#out_missed_file = out_dir+'us_events_MISSED_EQT_20191228_20200114.txt'
#out_new_file = out_dir+'us_events_NEW_EQT_20191228_20200114.txt'

#in_catalog_file = '../Catalog/EQT_20191228_20200114/catalog_puerto_rico_20191228_20200114_download20210608.txt' # Comcat text file format
#in_hinv_sum_eqt_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELPRSN/merged_real_magcat_locate_pr.sum' # HYPOINVERSE summary format
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELPRSN/'
#out_match_file = out_dir+'events_MATCH_magcat_EQT_20191228_20200114.txt'
#out_missed_file = out_dir+'events_MISSED_magcat_EQT_20191228_20200114.txt'
#out_new_file = out_dir+'events_NEW_magcat_EQT_20191228_20200114.txt'

#in_catalog_file = '../Catalog/EQT_20191228_20200114/catalog_puerto_rico_20191228_20200114_download20210608.txt' # Comcat text file format
#in_hinv_sum_eqt_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELZHANG/merged_real_locate_pr.sum' # HYPOINVERSE summary format
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELZHANG/'
#out_match_file = out_dir+'events_MATCH_EQT_20191228_20200114.txt'
#out_missed_file = out_dir+'events_MISSED_EQT_20191228_20200114.txt'
#out_new_file = out_dir+'events_NEW_EQT_20191228_20200114.txt'

#in_catalog_file = '../Catalog/EQT_20191228_20200114/catalog_puerto_rico_20191228_20200114_catalogus_contributorus_download20210608.txt' # Comcat text file format - us only (for magnitudes)
#in_hinv_sum_eqt_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELZHANG/merged_real_locate_pr.sum' # HYPOINVERSE summary format
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELZHANG/'
#out_match_file = out_dir+'us_events_MATCH_EQT_20191228_20200114.txt'
#out_missed_file = out_dir+'us_events_MISSED_EQT_20191228_20200114.txt'
#out_new_file = out_dir+'us_events_NEW_EQT_20191228_20200114.txt'

#in_catalog_file = '../Catalog/EQT_20191228_20200114/catalog_puerto_rico_20191228_20200114_download20210608.txt' # Comcat text file format
#in_hinv_sum_eqt_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELZHANG/merged_real_magcat_locate_pr.sum' # HYPOINVERSE summary format
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELZHANG/'
#out_match_file = out_dir+'events_MATCH_magcat_EQT_20191228_20200114.txt'
#out_missed_file = out_dir+'events_MISSED_magcat_EQT_20191228_20200114.txt'
#out_new_file = out_dir+'events_NEW_magcat_EQT_20191228_20200114.txt'

#in_catalog_file = '../Catalog/EQT_20180101_20210601/catalog_puerto_rico_20180101_20210601_download20210610.txt' # Comcat text file format
#in_hinv_sum_eqt_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELPRSN/merged_real_locate_pr.sum' # HYPOINVERSE summary format
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELPRSN/'
#out_match_file = out_dir+'events_MATCH_EQT_20180101_20210601.txt'
#out_missed_file = out_dir+'events_MISSED_EQT_20180101_20210601.txt'
#out_new_file = out_dir+'events_NEW_EQT_20180101_20210601.txt'

#in_catalog_file = '../Catalog/EQT_20180101_20210601/catalog_puerto_rico_20180101_20210601_catalogus_contributorus_download20210610.txt' # Comcat text file format - us only (for magnitudes)
#in_hinv_sum_eqt_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELPRSN/merged_real_locate_pr.sum' # HYPOINVERSE summary format
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELPRSN/'
#out_match_file = out_dir+'us_events_MATCH_EQT_20180101_20210601.txt'
#out_missed_file = out_dir+'us_events_MISSED_EQT_20180101_20210601.txt'
#out_new_file = out_dir+'us_events_NEW_EQT_20180101_20210601.txt'

#in_catalog_file = '../Catalog/EQT_20180101_20210601/catalog_puerto_rico_20180101_20210601_download20210610.txt' # Comcat text file format
#in_hinv_sum_eqt_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELPRSN/merged_real_magcat_locate_pr.sum' # HYPOINVERSE summary format
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELPRSN/'
#out_match_file = out_dir+'events_MATCH_magcat_EQT_20180101_20210601.txt'
#out_missed_file = out_dir+'events_MISSED_magcat_EQT_20180101_20210601.txt'
#out_new_file = out_dir+'events_NEW_magcat_EQT_20180101_20210601.txt'

#in_catalog_file = '../Catalog/EQT_20180101_20210601/catalog_puerto_rico_20180101_20210601_download20210610.txt' # Comcat text file format
#in_hinv_sum_eqt_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/merged_real_locate_pr.sum' # HYPOINVERSE summary format
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/'
#out_match_file = out_dir+'events_MATCH_EQT_20180101_20210601.txt'
#out_missed_file = out_dir+'events_MISSED_EQT_20180101_20210601.txt'
#out_new_file = out_dir+'events_NEW_EQT_20180101_20210601.txt'

#in_catalog_file = '../Catalog/EQT_20180101_20210601/catalog_puerto_rico_20180101_20210601_catalogus_contributorus_download20210610.txt' # Comcat text file format - us only (for magnitudes)
#in_hinv_sum_eqt_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/merged_real_locate_pr.sum' # HYPOINVERSE summary format
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/'
#out_match_file = out_dir+'us_events_MATCH_EQT_20180101_20210601.txt'
#out_missed_file = out_dir+'us_events_MISSED_EQT_20180101_20210601.txt'
#out_new_file = out_dir+'us_events_NEW_EQT_20180101_20210601.txt'

#in_catalog_file = '../Catalog/EQT_20180101_20210601/catalog_puerto_rico_20180101_20210601_download20210610.txt' # Comcat text file format
#in_hinv_sum_eqt_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/merged_real_magcat_locate_pr.sum' # HYPOINVERSE summary format
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/'
#out_match_file = out_dir+'events_MATCH_magcat_EQT_20180101_20210601.txt'
#out_missed_file = out_dir+'events_MISSED_magcat_EQT_20180101_20210601.txt'
#out_new_file = out_dir+'events_NEW_magcat_EQT_20180101_20210601.txt'

#in_catalog_file = '../Catalog/EQT_20180101_20211001/catalog_puerto_rico_20180101_20211001_download20211020.txt' # Comcat text file format
#in_hinv_sum_eqt_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/merged_real_locate_pr.sum' # HYPOINVERSE summary format
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/'
#out_match_file = out_dir+'events_MATCH_EQT_20180101_20211001.txt'
#out_missed_file = out_dir+'events_MISSED_EQT_20180101_20211001.txt'
#out_new_file = out_dir+'events_NEW_EQT_20180101_20211001.txt'

#in_catalog_file = '../Catalog/EQT_20180101_20211001/catalog_puerto_rico_20180101_20211001_catalogus_contributorus_download20211020.txt' # Comcat text file format - us only (for magnitudes)
#in_hinv_sum_eqt_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/merged_real_locate_pr.sum' # HYPOINVERSE summary format
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/'
#out_match_file = out_dir+'us_events_MATCH_EQT_20180101_20211001.txt'
#out_missed_file = out_dir+'us_events_MISSED_EQT_20180101_20211001.txt'
#out_new_file = out_dir+'us_events_NEW_EQT_20180101_20211001.txt'

#in_catalog_file = '../Catalog/EQT_20180101_20211001/catalog_puerto_rico_20180101_20211001_download20211020.txt' # Comcat text file format
#in_hinv_sum_eqt_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/merged_real_magcat_locate_pr.sum' # HYPOINVERSE summary format
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/'
#out_match_file = out_dir+'events_MATCH_magcat_EQT_20180101_20211001.txt'
#out_missed_file = out_dir+'events_MISSED_magcat_EQT_20180101_20211001.txt'
#out_new_file = out_dir+'events_NEW_magcat_EQT_20180101_20211001.txt'
#--------------------------

#in_catalog_file = '../Catalog/EQT_20180101_20220101/catalog_puerto_rico_20180101_20220101_catalogus_contributorus_download20220111.txt' # Comcat text file format - us only (for magnitudes)
#in_hinv_sum_eqt_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/merged_real_locate_pr.sum' # HYPOINVERSE summary format
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/'
#out_match_file = out_dir+'us_events_MATCH_EQT_20180101_20220101.txt'
#out_missed_file = out_dir+'us_events_MISSED_EQT_20180101_20220101.txt'
#out_new_file = out_dir+'us_events_NEW_EQT_20180101_20220101.txt'

#in_catalog_file = '../Catalog/EQT_20180101_20220101/catalog_puerto_rico_20180101_20220101_catalogus_contributorus_download20220111.txt' # Comcat text file format - us only (for magnitudes)
#in_hinv_sum_eqt_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/merged_real_locate_pr.sum' # HYPOINVERSE summary format
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/'
#out_match_file = out_dir+'us_events_MATCH_EQT_20180101_20220101.txt'
#out_missed_file = out_dir+'us_events_MISSED_EQT_20180101_20220101.txt'
#out_new_file = out_dir+'us_events_NEW_EQT_20180101_20220101.txt'

#in_catalog_file = '../Catalog/EQT_20180101_20220101/catalog_puerto_rico_20180101_20220101_download20220111.txt' # Comcat text file format
#in_hinv_sum_eqt_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/merged_real_magcat_locate_pr.sum' # HYPOINVERSE summary format
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/'
#out_match_file = out_dir+'events_MATCH_magcat_EQT_20180101_20220101.txt'
#out_missed_file = out_dir+'events_MISSED_magcat_EQT_20180101_20220101.txt'
#out_new_file = out_dir+'events_NEW_magcat_EQT_20180101_20220101.txt'

#in_catalog_file = '../Catalog/EQT_20180101_20220101/catalog_puerto_rico_20180101_20220101_download20220111.txt' # Comcat text file format
#in_hinv_sum_eqt_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/merged_real_magcat_locate_pr.sum' # HYPOINVERSE summary format
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/'
#out_match_file = out_dir+'events_MATCH_magcat_EQT_20180101_20220101.txt'
#out_missed_file = out_dir+'events_MISSED_magcat_EQT_20180101_20220101.txt'
#out_new_file = out_dir+'events_NEW_magcat_EQT_20180101_20220101.txt'

#--------------------------

#in_catalog_file = '../Catalog/EQT_20200107_20200108/catalog_puerto_rico_20200107_20200108_download20220111.txt' # Comcat text file format
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200108/REAL_VELZHANG/Events_TEST23/'
#in_hinv_sum_eqt_file = out_dir+'TEST23_EQT_20200107_20200108.sum' # HYPOINVERSE summary format
#out_match_file = out_dir+'events_MATCH_TEST23_EQT_20200107_20200108.txt'
#out_missed_file = out_dir+'events_MISSED_TEST23_EQT_20200107_20200108.txt'
#out_new_file = out_dir+'events_NEW_TEST23_EQT_20200107_20200108.txt'

#in_catalog_file = '../Catalog/EQT_20200107_20200108/catalog_puerto_rico_20200107_20200108_download20220111.txt' # Comcat text file format
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200108/REAL_VELZHANG/HYPOINVERSE_TEST10/'
#in_hinv_sum_eqt_file = out_dir+'real_locate_pr.sum' # HYPOINVERSE summary format
#out_match_file = out_dir+'events_MATCH_HYPOINVERSE_TEST10_EQT_20200107_20200108.txt'
#out_missed_file = out_dir+'events_MISSED_HYPOINVERSE_TEST10_EQT_20200107_20200108.txt'
#out_new_file = out_dir+'events_NEW_HYPOINVERSE_TEST10_EQT_20200107_20200108.txt'

#in_catalog_file = '../Catalog/EQT_20200107_20200108/catalog_puerto_rico_20200107_20200108_download20220111.txt' # Comcat text file format
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200108/REAL_VELZHANG/DIRECTHYPOSVI_TEST10/'
#in_hinv_sum_eqt_file = out_dir+'hyposvi_real_locate_pr.sum' # HYPOINVERSE summary format
#out_match_file = out_dir+'events_MATCH_DIRECTHYPOSVI_TEST10_EQT_20200107_20200108.txt'
#out_missed_file = out_dir+'events_MISSED_DIRECTHYPOSVI_TEST10_EQT_20200107_20200108.txt'
#out_new_file = out_dir+'events_NEW_DIRECTHYPOSVI_TEST10_EQT_20200107_20200108.txt'

#in_catalog_file = '../Catalog/EQT_20200107_20200108/catalog_puerto_rico_20200107_20200108_catalogus_contributorus_download20220111.txt' # Comcat text file format - us only (for magnitudes)
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200108/REAL_VELZHANG/DIRECTHYPOSVI_TEST10/'
#in_hinv_sum_eqt_file = out_dir+'hyposvi_real_locate_pr.sum' # HYPOINVERSE summary format
#out_match_file = out_dir+'us_events_MATCH_TEST10_EQT_20200107_20200108.txt'
#out_missed_file = out_dir+'us_events_MISSED_TEST10_EQT_20200107_20200108.txt'
#out_new_file = out_dir+'us_events_NEW_TEST10_EQT_20200107_20200108.txt'

#in_catalog_file = '../Catalog/EQT_20200107_20200108/catalog_puerto_rico_20200107_20200108_download20220111.txt' # Comcat text file format
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200108/REAL_VELZHANG/DIRECTHYPOSVI_TEST10/'
#in_hinv_sum_eqt_file = out_dir+'hyposvi_real_magcat_locate_pr.sum' # HYPOINVERSE summary format
#out_match_file = out_dir+'events_MATCH_magcat_DIRECTHYPOSVI_TEST10_EQT_20200107_20200108.txt'
#out_missed_file = out_dir+'events_MISSED_magcat_DIRECTHYPOSVI_TEST10_EQT_20200107_20200108.txt'
#out_new_file = out_dir+'events_NEW_magcat_DIRECTHYPOSVI_TEST10_EQT_20200107_20200108.txt'

in_catalog_file = '../Catalog/EQT_20180101_20230101/catalog_puerto_rico_20180101_20230101_catalogus_contributorus_download20230306.txt' # Comcat text file format - us only (for magnitudes)
out_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20230101/REAL_VELZHANG/DIRECTHYPOSVI/'
in_hinv_sum_eqt_file = out_dir+'hyposvi_real_locate_pr.sum' # HYPOINVERSE summary format
out_match_file = out_dir+'us_events_MATCH_EQT_20180101_20230101.txt'
out_missed_file = out_dir+'us_events_MISSED_EQT_20180101_20230101.txt'
out_new_file = out_dir+'us_events_NEW_EQT_20180101_20230101.txt'

#in_catalog_file = '../Catalog/EQT_20180101_20230101/catalog_puerto_rico_20180101_20230101_download20230206.txt' # Comcat text file format
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20230101/REAL_VELZHANG/DIRECTHYPOSVI/'
#in_hinv_sum_eqt_file = out_dir+'hyposvi_real_magcat_locate_pr.sum' # HYPOINVERSE summary format
#out_match_file = out_dir+'events_MATCH_magcat_DIRECTHYPOSVI_EQT_20180101_20230101.txt'
#out_missed_file = out_dir+'events_MISSED_magcat_DIRECTHYPOSVI_EQT_20180101_20230101.txt'
#out_new_file = out_dir+'events_NEW_magcat_DIRECTHYPOSVI_EQT_20180101_20230101.txt'



# Get map (dictionary) with catalog event info
# Key is YYYYMMDD for faster search
map_catalog = get_comcat_catalog_map(in_catalog_file)

# Criteria for matching a catalog event with eqtransformer event
#delta_match_sec = 3.0 # seconds
#delta_match_sec = 5.0 # seconds
delta_match_sec = 8.0 # seconds
#delta_match_sec = 10.0 # seconds
#delta_distance_thresh = 10.0 # km
#delta_distance_thresh = 25.0 # km
delta_distance_thresh = 40.0 # km
#delta_distance_thresh = 50.0 # km

# Loop over EQTransformer events in HYPOINVERSE file
diff_cat_arr = []
fmatch_out = open(out_match_file, 'w')
num_match = 0
fnew_out = open(out_new_file, 'w')
num_new = 0
with open(in_hinv_sum_eqt_file, 'r') as fin:
   for line in fin:
      hour = int(line[8:10])
      minute = int(line[10:12])
      second = 0.01*float(line[12:16])
      cur_nsec = 3600*hour + 60*minute + second
      string_day = line[0:8] #YYYYMMDD

      flag_in_catalog = False
      if (string_day in map_catalog):
         cat_day = map_catalog[string_day] # Contains all catalog events with same YYYYMMDD as eqt event

         tmp_dict = defaultdict(int)
         for icat in cat_day:
            diff_cat_det = cur_nsec - icat
            if (abs(diff_cat_det) <= delta_match_sec):
               tmp_dict[icat] = abs(diff_cat_det)

         # Now tmp_dict has all catalog events with origin time within delta_match_sec of eqt event
         if (len(tmp_dict) > 0):
            for imatch in sorted(tmp_dict, key=tmp_dict.get): # traverse in time order
               item_sec = imatch
               print(imatch, map_catalog[string_day][imatch])
               print(line.strip('\n'))
               print("imatch = ", imatch, ", time difference: ", tmp_dict[item_sec])

               cat_lat = float(map_catalog[string_day][item_sec][2])
               cat_lon = float(map_catalog[string_day][item_sec][3])
               cat_depth = float(map_catalog[string_day][item_sec][4])
               if (map_catalog[string_day][item_sec][10] == ''): # no magnitude estimate
                  print("Missing magnitude estimate, skip: ", map_catalog[string_day][item_sec])
                  continue
               cat_mag = float(map_catalog[string_day][item_sec][10])
               print("catalog values: ", cat_lat, cat_lon, cat_depth, cat_mag)

               [eqt_lat, eqt_lon, eqt_depth] = utils_hyp.get_lat_lon_depth_hypoinverse_file(line)
               print("eqtransformer values: ", eqt_lat, eqt_lon, eqt_depth)

               # Check epicentral distance between catalog event and eqt event
               [epi_dist, azAB, azBA] = gps2dist_azimuth(cat_lat, cat_lon, eqt_lat, eqt_lon)
               epi_dist_km = 0.001*epi_dist
               diff_depth_km = cat_depth - eqt_depth
               print("epi_dist_km = ", epi_dist_km, ", diff_depth_km = ", diff_depth_km, "\n")
               
               # If epicentral distance exceeds threshold, do not declare a match
               if (epi_dist_km > delta_distance_thresh):
                  print("WARNING: epi_dist_km above threshold: ", delta_distance_thresh, "\n")
                  continue

               # Found a match between catalog event and eqt event
               flag_in_catalog = True
               num_match += 1
               fmatch_out.write(('%s %s %7.4f %7.4f %5.3f %4.2f %s\n') % (line.strip('\n'), map_catalog[string_day][item_sec][1], cat_lat, cat_lon, cat_depth, cat_mag, map_catalog[string_day][item_sec][0].strip('.')))
               diff_cat_arr.append(cur_nsec-item_sec)
#               print("cur_nsec = ", cur_nsec, ", item_sec = ", item_sec)
               map_catalog[string_day].pop(item_sec) # Remove matching items from catalog map

               if (flag_in_catalog): # only want one matching item with lowest abs(diff_cat_det)
                  break

      # Did not find a match with catalog event; eqt event must be new
      if (not flag_in_catalog): # Detection not found in catalog
         num_new += 1
         fnew_out.write(('%s') % line)

fmatch_out.close()
fnew_out.close()

diff_cat_arr = np.asarray(diff_cat_arr)
print("len(diff_cat_arr) = ", len(diff_cat_arr))
print("min(diff_cat_arr) = ", min(diff_cat_arr))
print("max(diff_cat_arr) = ", max(diff_cat_arr))
print("max(abs(diff_cat_arr)) = ", max(abs(diff_cat_arr)))
#np.savetxt('diff_'+network_str+'.txt', np.sort(diff_cat_arr), fmt='%d')

# Only missed events should remain in catalog map, so write them out
num_missed = 0
fmissed_out = open(out_missed_file, 'w')
for iday in map_catalog:
   for event in map_catalog[iday]:
      # Write: datetime, lat, lon, depth, mag, evid
      if (map_catalog[iday][event][10] == ''): # no magnitude estimate
         print("Missing magnitude estimate, skip: ", map_catalog[iday][event])
         continue
      num_missed += 1
      fmissed_out.write(('%s %7.4f %7.4f %5.3f %4.2f %s\n') % (map_catalog[iday][event][1], float(map_catalog[iday][event][2]), float(map_catalog[iday][event][3]), float(map_catalog[iday][event][4]), float(map_catalog[iday][event][10]), map_catalog[iday][event][0].strip('.')))
fmissed_out.close()

print("Number of match catalog events: ", num_match)
print("Number of new events not in catalog: ", num_new)
print("Number of missed catalog events: ", num_missed)

