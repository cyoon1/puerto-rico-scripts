import numpy as np
import utils_hypoinverse as utils_hyp

# Remove false events from hypoinverse format arc/sum files
# Output "new_keep" sum file: only new events from EQT

hinv_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20230101/REAL_VELZHANG/DIRECTHYPOSVI/'
in_false_file = hinv_dir+'events_NEW_magcat_false_DIRECTHYPOSVI_EQT_20180101_20230101.txt'
in_new_keep_file = hinv_dir+'events_NEW_magcat_keep_DIRECTHYPOSVI_EQT_20180101_20230101.txt'
in_hinv_arc_file = hinv_dir+'hyposvi_real_magcat_locate_pr.arc'
in_hinv_sum_file = hinv_dir+'hyposvi_real_magcat_locate_pr.sum'
out_hinv_arc_file = hinv_dir+'hyposvi_real_magcat_locate_keep_pr.arc' # no false events
out_hinv_sum_file = hinv_dir+'hyposvi_real_magcat_locate_keep_pr.sum' # no false events
out_hinv_new_keep_sum_file = hinv_dir+'hyposvi_real_magcat_locate_new_keep_pr.sum' # no false events, needed for catalog text files

false_id_list = np.loadtxt(in_false_file, usecols=(6), dtype='int', unpack=True)
print("len(false_id_list): ", len(false_id_list))

new_keep_id_list = np.loadtxt(in_new_keep_file, usecols=(6), dtype='int', unpack=True)
print("len(new_keep_id_list): ", len(new_keep_id_list))

num_keep_events = 0
num_lines_out = 0
num_new_keep_events = 0
fout_arc = open(out_hinv_arc_file, 'w')
fout_sum = open(out_hinv_sum_file, 'w')
fout_new_keep_sum = open(out_hinv_new_keep_sum_file, 'w')
with open(in_hinv_arc_file, 'r') as fin:
   for line in fin:
      if ((line[0:2] == '19') or (line[0:2] == '20')):
         ev_id = int(utils_hyp.get_event_id_hypoinverse_file(line))
         if (ev_id in false_id_list):
            flag_out_line = False
         else:
            flag_out_line = True

         if (ev_id in new_keep_id_list):
            flag_new_keep_line = True
         else:
            flag_new_keep_line = False

         if (flag_out_line):
            fout_arc.write(('%s') % line)
            fout_sum.write(('%s') % line)
            num_keep_events += 1
            num_lines_out += 1

         if (flag_new_keep_line):
            fout_new_keep_sum.write(('%s') % line)
            num_new_keep_events += 1
      else:
         if (flag_out_line):
            fout_arc.write(('%s') % line)
            num_lines_out += 1

fout_arc.close()
fout_sum.close()
print("num_keep_events: ", num_keep_events)
print("num_lines_out: ", num_lines_out)

