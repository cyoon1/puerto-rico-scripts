import matplotlib.pyplot as plt
import numpy as np
from matplotlib import rcParams

rcParams.update({'font.size': 24})
#rcParams.update({'font.size': 36}) #poster
###rcParams['pdf.fonttype'] = 42
rcParams['font.sans-serif'] = "Helvetica"
rcParams['font.family'] = "sans-serif"

out_dir = '../EQT_20200107_20200108/Plots/'
out_base_str = out_dir+'param_test_EQT_20200107_20200108_'
marker_size = 100

# Constant thresholds, vary overlap
out_vary_str = 'vary_overlap'
x_overlap = [0.3, 0.75, 0.9]
#DET_THRESH = 0.3
#P_THRESH = 0.1
#S_THRESH = 0.1
consv_num_picks_p = [8372, 12129, 14862]
consv_num_picks_s = [8524, 12606, 16335]
consv_num_events = [930, 1188, 1325]
consv_num_match_events = [538, 622, 639]
consv_num_new_events = [392, 566, 686]
consv_num_missed_events = [286, 202, 185]
#DET_THRESH = 0.5
#P_THRESH = 0.3
#S_THRESH = 0.3
orig_num_picks_p = [17770, 27139, 36671]
orig_num_picks_s = [16357, 24870, 34682]
orig_num_events = [1296, 1529, 1612]
orig_num_match_events = [641, 697, 692]
orig_num_new_events = [655, 832, 920]
orig_num_missed_events = [183, 127, 132]

out_plot_str = 'number_picks_'
plt.figure(num=1, figsize=(10,8))
plt.clf()
plt.scatter(x_overlap, consv_num_picks_p, s=marker_size, marker='s', facecolors='k', edgecolors='k', label='conservative, P') 
plt.scatter(x_overlap, consv_num_picks_s, s=marker_size, marker='s', facecolors='none', edgecolors='k', label='conservative, S') 
plt.scatter(x_overlap, orig_num_picks_p, s=marker_size, marker='^', facecolors='k', edgecolors='k', label='original, P') 
plt.scatter(x_overlap, orig_num_picks_s, s=marker_size, marker='^', facecolors='none', edgecolors='k', label='original, S') 
plt.xlabel("Overlap parameter")
plt.xlim([0, 1])
plt.xticks([0, 0.3, 0.75, 0.9, 1], [0, 0.3, 0.75, 0.9, 1])
plt.ylabel("Number of EQTransformer picks")
plt.ylim([0, 40000])
plt.legend(loc='upper left', prop={'size':20})
plt.tight_layout()
plt.savefig(out_base_str+out_plot_str+out_vary_str+'.pdf')

out_plot_str = 'number_events_'
plt.figure(num=2, figsize=(10,8))
plt.clf()
plt.scatter(x_overlap, consv_num_events, s=marker_size, marker='s', facecolors='k', edgecolors='k', label='conservative EQT model') 
plt.scatter(x_overlap, orig_num_events, s=marker_size, marker='^', facecolors='k', edgecolors='k', label='original EQT model') 
plt.xlabel("Overlap parameter")
plt.xlim([0, 1])
plt.xticks([0, 0.3, 0.75, 0.9, 1], [0, 0.3, 0.75, 0.9, 1])
plt.ylabel("Number of REAL-associated events")
plt.ylim([0, 2000])
plt.legend(loc='upper left', prop={'size':20})
plt.tight_layout()
plt.savefig(out_base_str+out_plot_str+out_vary_str+'.pdf')

out_plot_str = 'number_catalogcomp_events_'
plt.figure(num=2, figsize=(10,8))
plt.clf()
plt.scatter(x_overlap, consv_num_match_events, s=marker_size, marker='s', facecolors='blue', edgecolors='k', label='conservative EQT model, match') 
plt.scatter(x_overlap, consv_num_new_events, s=marker_size, marker='s', facecolors='cyan', edgecolors='k', label='conservative EQT model, new') 
plt.scatter(x_overlap, consv_num_missed_events, s=marker_size, marker='s', facecolors='red', edgecolors='k', label='conservative EQT model, missed') 
plt.scatter(x_overlap, orig_num_match_events, s=marker_size, marker='^', facecolors='blue', edgecolors='k', label='original EQT model, match') 
plt.scatter(x_overlap, orig_num_new_events, s=marker_size, marker='^', facecolors='cyan', edgecolors='k', label='original EQT model, new') 
plt.scatter(x_overlap, orig_num_missed_events, s=marker_size, marker='^', facecolors='red', edgecolors='k', label='original EQT model, missed') 
plt.xlabel("Overlap parameter")
plt.xlim([0, 1])
plt.xticks([0, 0.3, 0.75, 0.9, 1], [0, 0.3, 0.75, 0.9, 1])
plt.ylabel("Number of REAL-associated events")
plt.ylim([0, 2000])
plt.legend(loc='upper left', prop={'size':20})
plt.tight_layout()
plt.savefig(out_base_str+out_plot_str+out_vary_str+'.pdf')

#----------
# Constant overlap=0.75, vary threshold
out_vary_str = 'vary_threshold'
x_threshold = [1, 2, 3]
#OVER_LAP = 0.75
consv_num_picks_p = [14973, 13754, 12129]
consv_num_picks_s = [16188, 14889, 12606]
consv_num_events = [1419, 1354, 1188]
consv_num_match_events = [666, 651, 622]
consv_num_new_events = [753, 703, 566]
consv_num_missed_events = [158, 173, 202]
#OVER_LAP = 0.75
orig_num_picks_p = [32523, 27139, 22948]
orig_num_picks_s = [30936, 24870, 19492]
orig_num_events = [1774, 1529, 1257]
orig_num_match_events = [743, 697, 616]
orig_num_new_events = [1031, 832, 641]
orig_num_missed_events = [81, 127, 208]

out_plot_str = 'number_picks_'
plt.figure(num=1, figsize=(10,8))
plt.clf()
plt.scatter(x_threshold, consv_num_picks_p, s=marker_size, marker='s', facecolors='k', edgecolors='k', label='conservative, P') 
plt.scatter(x_threshold, consv_num_picks_s, s=marker_size, marker='s', facecolors='none', edgecolors='k', label='conservative, S') 
plt.scatter(x_threshold, orig_num_picks_p, s=marker_size, marker='^', facecolors='k', edgecolors='k', label='original, P') 
plt.scatter(x_threshold, orig_num_picks_s, s=marker_size, marker='^', facecolors='none', edgecolors='k', label='original, S') 
plt.xlabel("Probability threshold")
plt.xlim([0.5, 3.5])
#plt.xticks([1, 2, 3], ['low: 0.05,0.03', 'medium: 0.1,0.05', 'high: 0.3,0.1'])
plt.xticks([1, 2, 3], ['low', 'medium', 'high'])
plt.ylabel("Number of EQTransformer picks")
plt.ylim([0, 40000])
plt.legend(loc='upper right', prop={'size':20})
plt.tight_layout()
plt.savefig(out_base_str+out_plot_str+out_vary_str+'.pdf')

out_plot_str = 'number_events_'
plt.figure(num=2, figsize=(10,8))
plt.clf()
plt.scatter(x_threshold, consv_num_events, s=marker_size, marker='s', facecolors='k', edgecolors='k', label='conservative EQT model') 
plt.scatter(x_threshold, orig_num_events, s=marker_size, marker='^', facecolors='k', edgecolors='k', label='original EQT model') 
plt.xlabel("Probability threshold")
plt.xlim([0.5, 3.5])
#plt.xticks([1, 2, 3], ['low: 0.4,0.2', 'medium: 0.5,0.3', 'high: 0.6,0.4'])
plt.xticks([1, 2, 3], ['low', 'medium', 'high'])
plt.ylabel("Number of REAL-associated events")
plt.ylim([0, 2000])
plt.legend(loc='upper right', prop={'size':20})
plt.tight_layout()
plt.savefig(out_base_str+out_plot_str+out_vary_str+'.pdf')

out_plot_str = 'number_catalogcomp_events_'
plt.figure(num=2, figsize=(10,8))
plt.clf()
plt.scatter(x_threshold, consv_num_match_events, s=marker_size, marker='s', facecolors='blue', edgecolors='k', label='conservative EQT model, match') 
plt.scatter(x_threshold, consv_num_new_events, s=marker_size, marker='s', facecolors='cyan', edgecolors='k', label='conservative EQT model, new') 
plt.scatter(x_threshold, consv_num_missed_events, s=marker_size, marker='s', facecolors='red', edgecolors='k', label='conservative EQT model, missed') 
plt.scatter(x_threshold, orig_num_match_events, s=marker_size, marker='^', facecolors='blue', edgecolors='k', label='original EQT model, match') 
plt.scatter(x_threshold, orig_num_new_events, s=marker_size, marker='^', facecolors='cyan', edgecolors='k', label='original EQT model, new') 
plt.scatter(x_threshold, orig_num_missed_events, s=marker_size, marker='^', facecolors='red', edgecolors='k', label='original EQT model, missed') 
plt.xlabel("Probability threshold")
plt.xlim([0.5, 3.5])
#plt.xticks([1, 2, 3], ['low: 0.4,0.2', 'medium: 0.5,0.3', 'high: 0.6,0.4'])
plt.xticks([1, 2, 3], ['low', 'medium', 'high'])
plt.ylabel("Number of REAL-associated events")
plt.ylim([0, 2000])
plt.legend(loc='upper right', prop={'size':20})
plt.tight_layout()
plt.savefig(out_base_str+out_plot_str+out_vary_str+'.pdf')

