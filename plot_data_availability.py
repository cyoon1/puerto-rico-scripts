import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rcParams

rcParams.update({'font.size': 24})
#rcParams.update({'font.size': 36}) #poster
###rcParams['pdf.fonttype'] = 42
rcParams['font.sans-serif'] = "Helvetica"
rcParams['font.family'] = "sans-serif"

# Data availability for each Network.Station.Channel with time
# Previously ran:
## $ obspy-scan ./downloads_mseeds/ -f MSEED -v -w data_availability_EQT_20180101_20220101.npz --no-x --no-gaps -o ./data_availability_EQT_20180101_20220101.pdf
#in_dir = '../EQT_20180101_20220101/'
#in_file = in_dir+'data_availability_EQT_20180101_20220101.npz'
#out_file = in_dir+'data_availability_EQT_20180101_20220101_bar_plot.png'

# $ obspy-scan ./downloads_mseeds/ -f MSEED -v -w data_availability_EQT_20180101_20230101.npz --no-x --no-gaps -o ./data_availability_EQT_20180101_20230101.pdf
in_dir = '../EQT_20180101_20230101/'
in_file = in_dir+'data_availability_EQT_20180101_20230101.npz'
out_file = in_dir+'Plots/data_availability_EQT_20180101_20230101_bar_plot.png'

data = np.load(in_file, allow_pickle=True)
ind = 0
label_arr = []
ytick_arr = []
fig, ax = plt.subplots(figsize=(50,30))
for sta in sorted(data.keys()):
   if (('_SAMP' not in sta) and ('version' not in sta)):
      ind += 1
      ybarmin = ind-0.1
      ybarmax = 0.2
      xbar = [(a[0], a[1]-a[0]) for a in data[sta]]
#      print(xbar)
      ax.broken_barh(xbar, (ybarmin, ybarmax))
      ytick_arr.append(ind)
      label_arr.append(sta)
      print(sta)
#ax.set_xlim(17532,18993)
#ax.set_xticks([17532, 17897, 18262, 18628, 18993], labels=['2018-01-01', '2019-01-01', '2020-01-01', '2021-01-01', '2022-01-01'])
#ax.set_ylim(0,181)
#ax.set_xlim(736695, 738528)
ax.set_xticks([736694, 737059, 737424, 737790, 738155, 738520], labels=['2018-01-01', '2019-01-01', '2020-01-01', '2021-01-01', '2022-01-01', '2023-01-01'])
ax.set_xlim(736694, 738520)
ax.set_ylim(0,184)
ax.set_yticks(ytick_arr, labels=label_arr)
ax.xaxis.set_tick_params(labelsize=30)
ax.yaxis.set_tick_params(labelsize=12)
plt.xlabel('Date', fontsize=30)
plt.ylabel('Network.Station.Channel', fontsize=30)
plt.tight_layout()
plt.savefig(out_file)
print("Total channels: ", ind)
