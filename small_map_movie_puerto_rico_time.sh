#!/usr/bin/env bash
#               GMT ANIMATION 02
#
# Purpose:      Make simple animated GIF of an illuminated DEM grid
# GMT modules   math, makecpt, grdimage, plot, movie
# Unix progs:   cat
# Note:         Run with any argument to build movie; otherwise 1st frame is plotted only.

if [ $# -eq 0 ]; then   # Just make master PostScript frame 0
   opt="-Mps -Fnone"
else  # Make animated GIF
#   opt="-A+l"
#   opt="-A -H2" # no infinite loop
#   opt="-A -H2 -Fgif" # no infinite loop
   opt="-A" # no infinite loop
fi

# 1. Create files needed in the loop
cat << EOF > pre.sh
gmt begin
   gmt gmtset MAP_FRAME_TYPE plain     # map outline - single line with ticks
   gmt gmtset FORMAT_GEO_MAP DD        # labels on map - decimal degrees
   gmt gmtset FONT_TITLE 16p
   gmt gmtset MAP_TITLE_OFFSET 0p

#   NDAY=1461
   NDAY=1826
   DELTA_DAY=1
#   PLOT_DAY=1
   PLOT_DAY=3
#   PLOT_DAY=7 # original
   gmt math -T726/\${NDAY}/\${DELTA_DAY} T \${PLOT_DAY} ADD = timeframes.txt

   out_color_filename=depthcolors.cpt
#   out_color_file=/Users/cyoon/Documents/PuertoRico/EQT_20180101_20220101/Plots/\${out_color_filename}
   out_color_file=/Users/cyoon/Documents/PuertoRico/EQT_20180101_20230101/Plots/\${out_color_filename}
   gmt makecpt -Cinferno -I -T0/20/0.01 -H > \${out_color_file}
#   gmt colorbar -C\${out_color_file} -Dx6.4i/2.4i/2.5i/0.15i -Bxaf+l"Depth (km)"

#   out_color_time_filename=timecolors.cpt
#   out_color_time_file=/Users/cyoon/Documents/PuertoRico/EQT_20180101_20230101/Plots/\${out_color_time_filename}
#   gmt makecpt -Cviridis -I -T726/\${NDAY}/0.01 -H > \${out_color_time_file}
#   start_date=2018-01-01

   min_lat=17.6
   max_lat=18.3
   min_lon=-67.3
   max_lon=-66.4

   reg=-R\${min_lon}/\${max_lon}/\${min_lat}/\${max_lat}
#   proj=-JM16
   proj=-JM17
   echo \${reg}
   lat_lon_spacing=0.3

   gmt basemap \${proj} \${reg} -Ba\${lat_lon_spacing}f0.1g -BneWS #-U
#   gmt coast \${proj} \${reg} -W0.25p,black -Na -G225 -Slightskyblue -Lg-66.6/17.68+c-66.5/17.68+w20+f+lkm
   gmt coast \${proj} \${reg} -W0.25p,black -Na -G210 -Slightskyblue -Lg-66.6/17.68+c-66.5/17.68+w20+f+lkm

#   in_sta_file=/Users/cyoon/Documents/PuertoRico/Catalog/EQT_20180101_20220101/EQT_20180101_20220101_pr_stations.txt
   in_sta_file=/Users/cyoon/Documents/PuertoRico/Catalog/EQT_20180101_20230101/EQT_20180101_20230101_pr_stations.txt
   in_temp_sta_file=/Users/cyoon/Documents/PuertoRico/Catalog/FullEQTransformer_pr_stations_large_temporary.txt
   awk '{print \$2 " " \$3 " " \$4}' \${in_sta_file} | gmt plot \${proj} \${reg} -Si0.4 -W0.1c,black -Gwhite -:
   awk '{print \$2 " " \$3 " " \$4}' \${in_temp_sta_file} | gmt plot \${proj} \${reg} -Si0.4 -W0.1c,blue -Gyellow -:

gmt end
EOF

# 2. Set up the main frame script
cat << EOF > main.sh
gmt begin
   gmt gmtset MAP_FRAME_TYPE plain     # map outline - single line with ticks
   gmt gmtset FORMAT_GEO_MAP DD        # labels on map - decimal degrees
   gmt gmtset FONT_TITLE 16p
   gmt gmtset MAP_TITLE_OFFSET 0p

   out_color_filename=depthcolors.cpt
#   out_color_file=/Users/cyoon/Documents/PuertoRico/EQT_20180101_20220101/Plots/\${out_color_filename}
   out_color_file=/Users/cyoon/Documents/PuertoRico/EQT_20180101_20230101/Plots/\${out_color_filename}

#   out_color_time_filename=timecolors.cpt
#   out_color_time_file=/Users/cyoon/Documents/PuertoRico/EQT_20180101_20230101/Plots/\${out_color_time_filename}

#   in_event_file=/Users/cyoon/Documents/PuertoRico/Catalog/EQT_20180101_20220101/catalog_new_puerto_rico_20180101_20220101_download20220111_diam.txt
#   in_event_file=/Users/cyoon/Documents/PuertoRico/Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_HYPOINVERSE_VELZHANG_combined_real_magcat_locate_pr_diam.txt
#   in_event_file=/Users/cyoon/Documents/PuertoRico/Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_HYPOSVI_VELZHANG_Catalog_diam.txt
#   in_event_file=/Users/cyoon/Documents/PuertoRico/Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_GrowClust_VELZHANG_maxdt2_diam.txt
   in_event_file=/Users/cyoon/Documents/PuertoRico/Catalog/EQT_20180101_20230101/EQT_20180101_20230101_REAL_VELZHANG_GrowClust_kNN5000_stasis_diam.txt

#   in_eqt_nc_file=/Users/cyoon/Documents/PuertoRico/Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_NonCluster_GrowClust_VELZHANG_maxdt2_diam.txt
   in_eqt_nc_file=/Users/cyoon/Documents/PuertoRico/Catalog/EQT_20180101_20230101/EQT_20180101_20230101_REAL_VELZHANG_NonCluster_GrowClust_kNN5000_stasis_diam.txt
   mag_thresh=3 # GrowClust with NonClust M3+ HYPOSVI

   min_lat=17.6
   max_lat=18.3
   min_lon=-67.3
   max_lon=-66.4

   # Cross section parameters (for projection lines only)
   strike1_angle=13
   strike1_length=30
   strike1_center_lat=17.93
   strike1_center_lon=-66.85
   strike2_angle=98
   strike2_length=40
   strike2_center_lat=17.93
   strike2_center_lon=-66.85

   reg=-R\${min_lon}/\${max_lon}/\${min_lat}/\${max_lat}
#   proj=-JM16
   proj=-JM17
   lat_lon_spacing=0.3

   DISP_DATE=\$(gdate -d "2017-12-31 +\${MOVIE_COL1} days" +%F) #PLOT_DAY=3
   echo "DISP_DATE="\${DISP_DATE}
   gmt basemap \${proj} \${reg} -Ba\${lat_lon_spacing}f0.1g -BneWS+t"Date: \${DISP_DATE}" #-U
#   gmt coast \${proj} \${reg} -W0.25p,black -Na -G225 -Slightskyblue -Lg-66.6/17.68+c-66.5/17.68+w20+f+lkm
   gmt coast \${proj} \${reg} -W0.25p,black -Na -G210 -Slightskyblue -Lg-66.6/17.68+c-66.5/17.68+w20+f+lkm

#   in_sta_file=/Users/cyoon/Documents/PuertoRico/Catalog/EQT_20180101_20220101/EQT_20180101_20220101_pr_stations.txt
   in_sta_file=/Users/cyoon/Documents/PuertoRico/Catalog/EQT_20180101_20230101/EQT_20180101_20230101_pr_stations.txt
   in_temp_sta_file=/Users/cyoon/Documents/PuertoRico/Catalog/FullEQTransformer_pr_stations_large_temporary.txt
   awk '{print \$2 " " \$3 " " \$4}' \${in_sta_file} | gmt plot \${proj} \${reg} -Si0.4 -W0.1c,black -Gwhite -:
   awk '{print \$2 " " \$3 " " \$4}' \${in_temp_sta_file} | gmt plot \${proj} \${reg} -Si0.4 -W0.1c,blue -Gyellow -:

#   Color by time
#   awk -v FD="\${MOVIE_COL0}" -v LD="\${MOVIE_COL1}" '{if ((\$1/86400.) >= FD && (\$1/86400.) <= LD) printf "%10.8f %10.8f %10.8f %6.4f\n", \$2, \$3, \$1/86400., \$7/6.26172 + 0.03}' \${in_event_file} | gmt plot \${proj} \${reg} -Sc -W0.3+cl -BneWS -: -C\${out_color_file} # HYPOINVERSE
###   awk -v FD="\${MOVIE_COL0}" -v LD="\${MOVIE_COL1}" '{if ((\$1/86400.) >= FD && (\$1/86400.) <= LD) printf "%10.8f %10.8f %10.8f %6.4f\n", \$2, \$3, \$1/86400., \$10/6.26172}' \${in_event_file} | gmt plot \${proj} \${reg} -Sc -W0.01c -G255 -BneWS -: -C\${out_color_file} # HYPOSVI

#  Color by depth
#   awk -v FD="0" -v LD="\${MOVIE_COL1}" '{if ((\$1/86400.) >= FD && (\$1/86400.) <= LD) printf "%10.8f %10.8f %10.8f %6.4f\n", \$2, \$3, \$4, \$10/6.26172}' \${in_event_file} | gmt plot \${proj} \${reg} -Sc0.01 -W0.005,160 -BneWS -: # HYPOSVI-past
#   awk -v FD="\${MOVIE_COL0}" -v LD="\${MOVIE_COL1}" '{if ((\$1/86400.) >= FD && (\$1/86400.) <= LD) printf "%10.8f %10.8f %10.8f %6.4f\n", \$2, \$3, \$4, \$10/6.26172 + 0.05}' \${in_event_file} | gmt plot \${proj} \${reg} -Sc -W0.005c -G255 -BneWS -: -C\${out_color_file} # HYPOSVI
#   awk -v FD="0" -v LD="\${MOVIE_COL1}" '{if ((\$1/86400.) >= FD && (\$1/86400.) <= LD) printf "%10.8f %10.8f %10.8f %6.4f\n", \$2, \$3, \$4, \$7/6.26172}' \${in_event_file} | gmt plot \${proj} \${reg} -Sc0.01 -W0.005,160 -BneWS -: # GrowClust-past
#   awk -v FD="\${MOVIE_COL0}" -v LD="\${MOVIE_COL1}" '{if ((\$1/86400.) >= FD && (\$1/86400.) <= LD) printf "%10.8f %10.8f %10.8f %6.4f\n", \$2, \$3, \$4, \$7/6.26172 + 0.05}' \${in_event_file} | gmt plot \${proj} \${reg} -Sc -W0.005c -G255 -BneWS -: -C\${out_color_file} # GrowClust
   awk -v FD="0" -v LD="\${MOVIE_COL1}" '{if ((\$2/86400.) >= FD && (\$2/86400.) <= LD) printf "%10.8f %10.8f %10.8f %6.4f\n", \$3, \$4, \$5, \$8/6.26172}' \${in_event_file} | gmt plot \${proj} \${reg} -Sc0.01 -W0.005,160 -BneWS -: # GrowClust-past
   awk -v FD="\${MOVIE_COL0}" -v LD="\${MOVIE_COL1}" '{if ((\$2/86400.) >= FD && (\$2/86400.) <= LD) printf "%10.8f %10.8f %10.8f %6.4f\n", \$3, \$4, \$5, \$8/6.26172 + 0.05}' \${in_event_file} | gmt plot \${proj} \${reg} -Sc -W0.005c -G255 -BneWS -: -C\${out_color_file} # GrowClust

   awk -v FD="0" -v LD="\${MOVIE_COL1}" -v MT="\${mag_thresh}" '{if ((\$2/86400.) >= FD && (\$2/86400.) <= LD && (\$6 >= MT)) printf "%10.8f %10.8f %10.8f %6.4f\n", \$3, \$4, \$5, \$8/6.26172 + 0.05}' \${in_eqt_nc_file} | gmt plot \${proj} \${reg} -Sc0.01 -W0.005,160 -BneWS -: # GrowClust_NonCluster-past
   awk -v FD="\${MOVIE_COL0}" -v LD="\${MOVIE_COL1}" -v MT="\${mag_thresh}" '{if ((\$2/86400.) >= FD && (\$2/86400.) <= LD && (\$6 >= MT)) printf "%10.8f %10.8f %10.8f %6.4f\n", \$3, \$4, \$5, \$8/6.26172 + 0.05}' \${in_eqt_nc_file} | gmt plot \${proj} \${reg} -Sc -W0.005c -G255 -BneWS -: -C\${out_color_file} # GrowClust_NonCluster

   # Projection lines
   gmt project -Q -G1 -C\${strike1_center_lon}/\${strike1_center_lat} -A\${strike1_angle} -L-\${strike1_length}/\${strike1_length} | gmt plot \${proj} \${reg} -W1,0/0/0,-. -Gblack
   gmt text a_label.txt \${proj} \${reg} -F+f24
   gmt project -Q -G1 -C\${strike2_center_lon}/\${strike2_center_lat} -A\${strike2_angle} -L-\${strike2_length}/\${strike2_length} | gmt plot \${proj} \${reg} -W1,0/0/0,-. -Gblack
   gmt text b_label.txt \${proj} \${reg} -F+f24

gmt end
EOF

# 3. Run the movie
#gmt movie main.sh -Csxga+ -Nm_EQT_20180101_20220101_Catalog_3d -Ttimeframes.txt -Sbpre.sh -D6 -Pe+ap $opt
#gmt movie main.sh -Csxga+ -Nm_EQT_20180101_20220101_HYPOINVERSE_VELZHANG_3d -Ttimeframes.txt -Sbpre.sh -D6 -Pe+ap $opt
#gmt movie main.sh -Csxga+ -Nm_EQT_20180101_20220101_HYPOSVI_VELZHANG_3d -Ttimeframes.txt -Sbpre.sh -D6 -Pe+ap $opt
#gmt movie main.sh -Csxga+ -Nm_EQT_20180101_20220101_GrowClust_VELZHANG_3d -Ttimeframes.txt -Sbpre.sh -D6 -Pe+ap $opt
gmt movie main.sh -Csxga+ -Nm_EQT_20180101_20230101_REAL_VELZHANG_GrowClust_VELZHANG_3d -Ttimeframes.txt -Sbpre.sh -D6 -Pe+ap $opt
