import pandas as pd
import utils_eqt

# EQTransform2GPD.py: Script to convert picks from EQTransformer csv format to GPD format (for input to PhaseLink)

#--------------------------START OF INPUTS------------------------
#in_eqt_dir = '../LargeAreaEQTransformer/mseed_detections/'
#out_gpd_file = '../LargeAreaEQTransformer/PhaseLink/GPD_LargeAreaEQTransformer_Model2_picks.out'
##out_gpd_file = '../LargeAreaEQTransformer/PhaseLink/GPD_LargeAreaEQTransformer_picks.out'

#in_eqt_dir = '/media/yoon/INT01/PuertoRico/FullEQTransformer/mseed_detections/'
#out_gpd_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/PhaseLink/GPD_FullEQTransformer_Model2_picks.out'
##out_gpd_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/PhaseLink/GPD_FullEQTransformer_picks.out'

#in_eqt_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/mseed_detections/'
#out_gpd_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/PhaseLink/GPD_EQT_20200107_20200114_picks.out'

#in_eqt_dir = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/mseed_detections/'
#out_gpd_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/PhaseLink/GPD_EQT_20191228_20200114_picks.out'

in_eqt_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/mseed_detections/'
out_gpd_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/PhaseLink/GPD_EQT_20180101_20220101_picks.out'

#--------------------------END OF INPUTS------------------------

pcks_gpd = utils_eqt.EQTransform2DataFrame(in_eqt_dir)
pcks_gpd.to_csv(out_gpd_file,header=False,index=False,sep=' ')
