import matplotlib.pyplot as plt
import math
import numpy as np
from matplotlib import rcParams
import utils_distance as utils_dist

rcParams.update({'font.size': 24})
#rcParams.update({'font.size': 36}) #poster
###rcParams['pdf.fonttype'] = 42
rcParams['font.sans-serif'] = "Helvetica"
rcParams['font.family'] = "sans-serif"



#in_ev_file = '../Catalog/FullEQTransformer_REAL_HYPOINVERSE_puerto_rico_catalog_combined_2018_2020_sort_by_origin_time.txt'
#out_plot_numevents_moment_file = '../Plots/numevents_moment_vs_time_box_REAL_HYPOINVERSE_20180101_20201101.pdf'
#start_nsec = 60393600
#end_nsec = 89424000

#in_ev_file = '../Catalog/EQT_20180101_20211001/EQT_20180101_20211001_REAL_HYPOINVERSE_VELZHANG_combined_real_magcat_locate_pr.txt'
#out_plot_numevents_moment_file = '../EQT_20180101_20211001/Plots/numevents_moment_vs_time_box_REAL_HYPOINVERSE_EQT_20180101_20211001.pdf'
#start_nsec = 60393600
#end_nsec = 118281600

#in_ev_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_HYPOINVERSE_VELPRSN_combined_real_magcat_locate_pr.txt'
#out_plot_numevents_moment_file = '../EQT_20180101_20220101/Plots/numevents_moment_vs_time_box_3REAL_HYPOINVERSE_EQT_VELPRSN_20180101_20220101.pdf'
#in_ev_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_HYPOINVERSE_VELZHANG_combined_real_magcat_locate_pr.txt'
#out_plot_numevents_moment_file = '../EQT_20180101_20220101/Plots/numevents_moment_vs_time_box_3REAL_HYPOINVERSE_EQT_VELZHANG_20180101_20220101.pdf'
#start_nsec = 60393600
#end_nsec = 126230400

in_ev_file = '../Catalog/EQT_20180101_20230101/EQT_20180101_20230101_REAL_VELZHANG_DIRECTHYPOSVI.txt'
out_plot_numevents_moment_file = '../EQT_20180101_20230101/Plots/numevents_moment_vs_time_box_REAL_VELZHANG_DIRECTHYPOSVI_20180101_20230101.pdf'
start_nsec = 60393600
end_nsec = 157766400

# Boundaries of swarm area box
min_lat=17.6
max_lat=18.3
min_lon=-67.3
max_lon=-66.4
min_depth=-1.
max_depth=40.

# Read in event data
#[ev_times, ev_lat, ev_lon, ev_depth, ev_mag] = np.loadtxt(in_ev_file, usecols=(0,1,2,3,4), unpack=True)
[ev_times, ev_lat, ev_lon, ev_depth, ev_mag] = np.loadtxt(in_ev_file, usecols=(1,2,3,4,5), unpack=True)
print("len(ev_times) = ", len(ev_times))

# Plot only events inside swarm area box - not the entire catalog
#[ev_keep_times, ev_keep_mag] = utils_dist.return_events_inside_box(ev_times, ev_mag, ev_lat, ev_lon, min_lat, max_lat, min_lon, max_lon)
[ev_keep_times, ev_keep_mag] = utils_dist.return_events_inside_box(ev_times, ev_mag, ev_lat, ev_lon, ev_depth, min_lat, max_lat, min_lon, max_lon, min_depth, max_depth)
print("len(ev_keep_times) = ", len(ev_keep_times))

in_bins = np.arange(start_nsec, end_nsec, 86400)
tot_bins = 0
moment_bins = np.zeros(len(in_bins)-1)
for ibin in range(len(in_bins[:-1])):
   ind_bin = np.where(np.logical_and(ev_keep_times >= in_bins[ibin], ev_keep_times <= in_bins[ibin+1]))
   moment_bin = 0.0
   for curr_ind in ind_bin[0]:
      moment_bin += math.pow(10, 1.5*(ev_keep_mag[curr_ind] + 6.07))
      moment_bins[ibin] = moment_bin
   tot_bins += 1
#   print(ibin, in_bins[ibin], in_bins[ibin+1], tot_bins, len(ind_bin[0]))

# Compute cumulative number of events
x_rot = 0
#fig = plt.figure(num=0, figsize=(24,6))
#x_rot = 20
fig = plt.figure(num=0, figsize=(32,7))
plt.clf()
ax1 = fig.add_subplot(111)
n_bin, count_bins, count_patches = plt.hist(ev_keep_times, bins=in_bins, color='white', edgecolor='black', linewidth=0.1)
#n_bin, count_bins, count_patches = plt.hist(ev_keep_times, bins=in_bins, color='yellow', edgecolor='black', linewidth=0.5)
#plt.xticks([60393600, 63072000, 65750400, 68256000, 70934400, 73526400, 76204800, 78796800, 81475200, 84153600, 86745600, 89424000],
#   ['2019-12-01', '2020-01-01', '2020-02-01', '2020-03-01', '2020-04-01', '2020-05-01', '2020-06-01', '2020-07-01', '2020-08-01', '2020-09-01', '2020-10-01', '2020-11-01'], rotation=x_rot)
#plt.xlim([60393600, 89424000])
#plt.xticks([60393600, 63072000, 65750400, 68256000, 70934400, 73526400, 76204800, 78796800, 81475200, 84153600, 86745600, 89424000, 92016000, 94694400, 97372800, 99792000, 102470400, 105062400, 107740800, 110332800, 113011200, 115689600, 118281600],
#   ['2019-12-01', '2020-01-01', '2020-02-01', '2020-03-01', '2020-04-01', '2020-05-01', '2020-06-01', '2020-07-01', '2020-08-01', '2020-09-01', '2020-10-01', '2020-11-01', '2020-12-01', '2021-01-01', '2021-02-01', '2021-03-01', '2021-04-01', '2021-05-01', '2021-06-01', '2021-07-01', '2021-08-01', '2021-09-01', '2021-10-01'], rotation=x_rot)
#plt.xticks([60393600, 63072000, 65750400, 68256000, 70934400, 73526400, 76204800, 78796800, 81475200, 84153600, 86745600, 89424000, 92016000, 94694400, 97372800, 99792000, 102470400, 105062400, 107740800, 110332800, 113011200, 115689600, 118281600, 120960000, 123552000, 126230400],
#   ['2019-12-01', '2020-01-01', '2020-02-01', '2020-03-01', '2020-04-01', '2020-05-01', '2020-06-01', '2020-07-01', '2020-08-01', '2020-09-01', '2020-10-01', '2020-11-01', '2020-12-01', '2021-01-01', '2021-02-01', '2021-03-01', '2021-04-01', '2021-05-01', '2021-06-01', '2021-07-01', '2021-08-01', '2021-09-01', '2021-10-01', '2021-11-01', '2021-12-01', '2022-01-01'], rotation=x_rot)
plt.xticks([63072000, 94694400, 126230400, 157766400], ['2020-01-01', '2021-01-01', '2022-12-01', '2023-01-01'], rotation=x_rot)
plt.xlim([start_nsec, end_nsec])
plt.xlabel('Date')
plt.ylim([0, 2000])
plt.ylabel('Number of events\n per day')
ax2 = ax1.twinx()
ax2.set_ylabel('Cumulative Seismic\n Moment (N m)', color='blue')
ax2.plot(in_bins[1:], np.cumsum(moment_bins), linewidth=2, color='blue')
ax2.tick_params(axis='y', colors='blue')
ax2.set_ylim([0, 7e18])
#ax2.set_ylim([0, 1.25e19])
plt.tight_layout()
plt.savefig(out_plot_numevents_moment_file)

## Compute cumulative moment
#plt.figure(num=1, figsize=(24,6))
#plt.clf()
#plt.plot(in_bins[1:], np.cumsum(moment_bins))
#plt.xticks([60393600, 63072000, 65750400, 68256000, 70934400, 73526400, 76204800, 78796800, 81475200, 84153600, 86745600, 89424000],
#   ['2019-12-01', '2020-01-01', '2020-02-01', '2020-03-01', '2020-04-01', '2020-05-01', '2020-06-01', '2020-07-01', '2020-08-01', '2020-09-01', '2020-10-01', '2020-11-01'])
#plt.xlim([60393600, 89424000])
#plt.xlabel('Date')
#plt.ylim([0, 1.25e19])
#plt.ylabel('Cumulative Seismic Moment (N m)')
#plt.tight_layout()
#plt.savefig(out_plot_moment_file)

