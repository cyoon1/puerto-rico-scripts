import matplotlib.pyplot as plt
import numpy as np
from matplotlib import rcParams

rcParams.update({'font.size': 24})
#rcParams.update({'font.size': 36}) #poster
###rcParams['pdf.fonttype'] = 42
rcParams['font.sans-serif'] = "Helvetica"
rcParams['font.family'] = "sans-serif"

in_dir = '../Catalog/EQT_20180101_20230101/'
in_npick_eqt_file = in_dir+'EQT_20180101_20230101_REAL_VELZHANG_DIRECTHYPOSVI_events_MISSED_magcalcml_inregion_diam_npickseqt.txt'
in_npick_comcat_file = in_dir+'events_MISSED_magcat_DIRECTHYPOSVI_EQT_EVID_COUNT_20180101_20230101.txt'
out_dir = '../EQT_20180101_20230101/Plots/'

[list_ev_id, list_npicks_eqt_p, list_npicks_eqt_s, list_npicks_eqt_total, list_nsta_eqt] = np.loadtxt(in_npick_eqt_file, dtype='int', usecols=(6,8,9,10,11), unpack=True)
list_local_mag = np.loadtxt(in_npick_eqt_file, dtype='float', usecols=5, unpack=True)

num_comcat_ev = 0
list_npicks_comcat_sta_p = []
list_npicks_comcat_sta_s = []
list_npicks_comcat_sta_total = []
list_nsta_comcat_sta = []
list_npicks_comcat_all_p = []
list_npicks_comcat_all_s = []
list_npicks_comcat_all_total = []
list_nsta_comcat_all = []
with open(in_npick_comcat_file, 'r') as fin:
   for line in fin:
      split_line = line.split()
      curr_evid = int(split_line[6])
      if (curr_evid in list_ev_id):
         num_comcat_ev += 1
         list_npicks_comcat_sta_p.append(int(split_line[7]))
         list_npicks_comcat_sta_s.append(int(split_line[8]))
         list_npicks_comcat_sta_total.append(int(split_line[9]))
         list_nsta_comcat_sta.append(int(split_line[10]))
         list_npicks_comcat_all_p.append(int(split_line[11]))
         list_npicks_comcat_all_s.append(int(split_line[12]))
         list_npicks_comcat_all_total.append(int(split_line[13]))
         list_nsta_comcat_all.append(int(split_line[14]))
#         print(curr_evid)
print("num_comcat_ev = ", num_comcat_ev)

marker_size = 50
equal_line = np.linspace(-1, 40, 100)

min_npicks = 0
max_npicks = 40
phase_bins = np.arange(min_npicks-0.5, max_npicks+1.5, 1)

plt.figure(num=1, figsize=(10,8))
plt.clf()
[hist, xedges, yedges, mesh] = plt.hist2d(list_npicks_comcat_sta_p, list_npicks_comcat_all_p, bins=[phase_bins, phase_bins], cmap='cool', cmin=1, vmin=0, vmax=150)
#plt.scatter(list_npicks_comcat_sta_p, list_npicks_comcat_all_p, s=marker_size)
plt.plot(equal_line, equal_line, '--', color='k')
plt.axis('scaled')
plt.xlabel("Number of P picks, catalog, continuous stations")
plt.ylabel("Number of P picks, catalog, all stations")
plt.xlim([0, 30])
plt.ylim([0, 30])
#plt.colorbar(orientation="vertical", label="Number of events", pad=0.05, shrink=0.5)
plt.tight_layout()
plt.savefig(out_dir+'missed_events_compare_comcat_vs_comcat_picks_p.pdf')

plt.figure(num=2, figsize=(10,8))
plt.clf()
[hist, xedges, yedges, mesh] = plt.hist2d(list_npicks_comcat_sta_s, list_npicks_comcat_all_s, bins=[phase_bins, phase_bins], cmap='cool', cmin=1, vmin=0, vmax=150)
#plt.scatter(list_npicks_comcat_sta_s, list_npicks_comcat_all_s, s=marker_size)
plt.plot(equal_line, equal_line, '--', color='k')
plt.axis('scaled')
plt.xlabel("Number of S picks, catalog, continuous stations")
plt.ylabel("Number of S picks, catalog, all stations")
plt.xlim([0, 30])
plt.ylim([0, 30])
#plt.colorbar(orientation="vertical", label="Number of events", pad=0.05, shrink=0.5)
plt.tight_layout()
plt.savefig(out_dir+'missed_events_compare_comcat_vs_comcat_picks_s.pdf')

plt.figure(num=3, figsize=(10,8))
plt.clf()
[hist, xedges, yedges, mesh] = plt.hist2d(list_nsta_comcat_sta, list_nsta_comcat_all, bins=[phase_bins, phase_bins], cmap='cool', cmin=1, vmin=0, vmax=150)
#plt.scatter(list_nsta_comcat_sta, list_nsta_comcat_all, s=marker_size)
plt.plot(equal_line, equal_line, '--', color='k')
plt.axis('scaled')
plt.xlabel("Number of continuous stations, catalog")
plt.ylabel("Number of total stations, catalog")
plt.xlim([0, 30])
plt.ylim([0, 30])
#plt.colorbar(orientation="vertical", label="Number of events", pad=0.05, shrink=0.5)
plt.tight_layout()
plt.savefig(out_dir+'missed_events_compare_comcat_vs_comcat_sta.pdf')

plt.figure(num=4, figsize=(10,8))
plt.clf()
[hist, xedges, yedges, mesh] = plt.hist2d(list_npicks_comcat_sta_p, list_npicks_eqt_p, bins=[phase_bins, phase_bins], cmap='cool', cmin=1, vmin=0, vmax=150)
#plt.scatter(list_npicks_comcat_sta_p, list_npicks_eqt_p, s=marker_size)
plt.plot(equal_line, equal_line, '--', color='k')
plt.axis('scaled')
plt.xlabel("Number of P picks, catalog, continuous stations")
plt.ylabel("Number of P picks, EQTransformer")
plt.xlim([0, 30])
plt.ylim([0, 30])
#plt.colorbar(orientation="vertical", label="Number of events", pad=0.05, shrink=0.5)
plt.tight_layout()
plt.savefig(out_dir+'missed_events_compare_comcat_vs_eqt_picks_p.pdf')

plt.figure(num=5, figsize=(10,8))
plt.clf()
[hist, xedges, yedges, mesh] = plt.hist2d(list_npicks_comcat_sta_s, list_npicks_eqt_s, bins=[phase_bins, phase_bins], cmap='cool', cmin=1, vmin=0, vmax=150)
#plt.scatter(list_npicks_comcat_sta_s, list_npicks_eqt_s, s=marker_size)
plt.plot(equal_line, equal_line, '--', color='k')
plt.axis('scaled')
plt.xlabel("Number of S picks, catalog, continuous stations")
plt.ylabel("Number of S picks, EQTransformer")
plt.xlim([0, 30])
plt.ylim([0, 30])
#plt.colorbar(orientation="vertical", label="Number of events", pad=0.05, shrink=0.5)
plt.tight_layout()
plt.savefig(out_dir+'missed_events_compare_comcat_vs_eqt_picks_s.pdf')

plt.figure(num=6, figsize=(10,8))
plt.clf()
[hist, xedges, yedges, mesh] = plt.hist2d(list_nsta_comcat_sta, list_nsta_eqt, bins=[phase_bins, phase_bins], cmap='cool', cmin=1, vmin=0, vmax=150)
#plt.scatter(list_nsta_comcat_sta, list_nsta_eqt, s=marker_size)
plt.plot(equal_line, equal_line, '--', color='k')
plt.axis('scaled')
plt.xlabel("Number of continuous stations, catalog")
plt.ylabel("Number of stations, EQTransformer")
plt.xlim([0, 30])
plt.ylim([0, 30])
#plt.colorbar(orientation="vertical", label="Number of events", pad=0.05, shrink=0.5)
plt.tight_layout()
plt.savefig(out_dir+'missed_events_compare_comcat_vs_eqt_sta.pdf')


