import datetime

in_mt_file = '../Catalog/SLU_MT_2018_2020_puerto_rico.txt'
catalog_start_time = '2018-01-01 00:00:00'
out_mt_file = '../Catalog/SLU_MT_2018_2020_puerto_rico_times.txt'

catalog_ref_time = datetime.datetime.strptime(catalog_start_time, '%Y-%m-%d %H:%M:%S')
fout = open(out_mt_file, 'w')
with open(in_mt_file, 'r') as fin:
   for line in fin:
      split_line = line.strip('\n').split()
      origin_time = datetime.datetime.strptime(split_line[0], '%Y%m%d_%H%M%S')
      num_sec = (origin_time - catalog_ref_time).total_seconds()
      fout.write(("%s %f\n") % (line.strip('\n'), num_sec))
      print(split_line, origin_time, num_sec)
fout.close()

