#!/bin/bash

# Plot Puerto Rico earthquakes on a map

#gmt gmtset BASEMAP_TYPE plain
#gmt gmtset HEADER_FONT_SIZE 24p
#gmt gmtset LABEL_FONT_SIZE 16p # smaller font for labels
##gmt gmtset PLOT_DEGREE_FORMAT +DF
#gmt gmtset PLOT_DEGREE_FORMAT -DF
#gmt gmtset GRID_PEN_PRIMARY 0.1p

gmt gmtset MAP_FRAME_TYPE plain     # map outline - single line with ticks
gmt gmtset FORMAT_GEO_MAP DD        # labels on map - decimal degrees

gmt gmtset LABEL_FONT_SIZE 16p
gmt gmtset FONT_ANNOT 16p
#gmt gmtset LABEL_FONT_SIZE 24p #poster
#gmt gmtset FONT_ANNOT 24p #poster

in_dir=../Catalog/EQT_20180101_20230101
in_fault_dir=../Catalog/Faults
out_dir=../EQT_20180101_20230101/Plots

out_color_file=${out_dir}/depthcolors.cpt

#in_sta_file=../Catalog/FullEQTransformer_pr_stations_large.txt
#in_temp_sta_file=../Catalog/FullEQTransformer_pr_stations_large_temporary.txt
#in_IU_sta_file=../Catalog/FullEQTransformer_pr_stations_large_IU.txt
#out_map_sta_file=../Plots/FullEQTransformer_pr_stations_large

in_temp_sta_file=../Catalog/FullEQTransformer_pr_stations_large_temporary.txt
in_IU_sta_file=../Catalog/FullEQTransformer_pr_stations_large_IU.txt
in_sta_file=${in_dir}/EQT_20180101_20230101_pr_stations.txt

in_fault_file=${in_fault_dir}/gem_active_faults_harmonized.gmt
in_pb_trench_file=${in_fault_dir}/trench.gmt
in_pb_trench_cocos_file=${in_fault_dir}/trench_cocos.gmt
in_pb_trench_carib_file=${in_fault_dir}/trench_carib.gmt
in_pb_ridge_file=${in_fault_dir}/ridge.gmt
in_pb_transform_file=${in_fault_dir}/transform.gmt
in_pr_trench_fault_file=${in_fault_dir}/gem_puerto_rico_trench.gmt
in_muertos_fault_file=${in_fault_dir}/gem_muertos.gmt
in_mona_west_fault_file=${in_fault_dir}/gem_mona_west.gmt
in_mona_east_fault_file=${in_fault_dir}/gem_mona_east.gmt
in_anegada_north_fault_file=${in_fault_dir}/gem_anegada_north.gmt
in_anegada_south_fault_file=${in_fault_dir}/gem_anegada_south.gmt

#out_map_sta_file=../Plots/EQT_20180101_20210601_pr_stations_map
#out_map_sta_file=../EQT_20180101_20210601/Plots/EQT_20180101_20210601_pr_stations_map
#out_map_sta_file=../EQT_20180101_20211001/Plots/EQT_20180101_20211001_pr_stations_map
#out_map_sta_file=../EQT_20180101_20220101/Plots/EQT_20180101_20220101_pr_tectonics_map
out_map_sta_file=${out_dir}/EQT_20180101_20230101_pr_tectonics_map

#in_cat_file=../Catalog/EQT_20180101_20220101/catalog_new_puerto_rico_20180101_20220101_download20220111_diam.txt
in_cat_file=${in_dir}/catalog_new_puerto_rico_20180101_20230101_download20230206_diam.txt
gmt makecpt -Cinferno -I -T0/20/0.01 > ${out_color_file}
#FIRST_DAY=0 # 2018-01-01
#LAST_DAY=1461 # 2022-01-01
#LAST_DAY=1826 # 2023-01-01


#min_lat=17
#max_lat=19
##max_lat=20
#min_lon=-68
##min_lon=-69
#max_lon=-65
##max_lon=-64

min_lat=16
max_lat=21
min_lon=-71
max_lon=-61

#min_lat=17.5
#max_lat=18.5
#min_lon=-67.5
#max_lon=-66.0

minbox_lat=17
#maxbox_lat=19
maxbox_lat=20
minbox_lon=-68
maxbox_lon=-65

reg=-R${min_lon}/${max_lon}/${min_lat}/${max_lat}
proj=-JM16
echo ${reg}
lat_lon_spacing=2

#region_inset=-R-75/-62/15/22
#region_inset=-R-105/-52/5/32
region_inset=-R-95/-55/2/28
projection_inset=-JM3.0i



gmt begin ${out_map_sta_file}
   gmt basemap ${proj} ${reg} -Ba${lat_lon_spacing}fg -BneWS #-U
   gmt coast ${proj} ${reg} -W0.5p,black -Na -G255 -Slightskyblue -Lg-65.8/17.25+c-65.7/17.25+w100+f+lkm

#   #https://docs.generic-mapping-tools.org/6.1/tutorial/session-3.html
#   gmt grdcut @earth_relief_15s -R-71/-60/15/22 -Gpr_bathy.nc -V
#   https://docs.generic-mapping-tools.org/6.0/datasets/earth_relief.html
   gmt grdimage ${proj} ${reg} pr_bathy.nc -Cglobe
   gmt colorbar -Dx-2.0c/8c+w8c/0.5c+jTC+m+e -Bx2000f+l"Elevation (m)"
   gmt coast ${proj} ${reg} -Na -Lg-65.7/16.45+c-65.7/16.45+w400+f+lkm

#   sort -nk1,1 ${in_cat_file} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $2, $3, $4, $7/6.26172 + 0.01}' | gmt plot ${proj} ${reg} -Sc -W0.3+cl -BneWS -: -C${out_color_file}
#   awk '{print $2 " " $3 " " $4}' ${in_sta_file} | gmt plot ${proj} ${reg} -Si0.4 -W0.1c,black -Gwhite -:
#   awk '{print $2 " " $3 " " $4}' ${in_temp_sta_file} | gmt plot ${proj} ${reg} -Si0.4 -W0.1c,blue -Gyellow -:
#   awk '{print $2 " " $3 " " $4}' ${in_IU_sta_file} | gmt plot ${proj} ${reg} -Si0.4 -W0.1c,magenta -Gyellow -:

   # Plot Faults
   # https://docs.generic-mapping-tools.org/6.1/plot.html
   # https://www.pygmt.org/dev/gallery/lines/linefronts.html
   #gmt plot ${proj} ${reg} ${in_fault_file} -Sf+0.9+l+t -W0.03c,black -G0
   gmt plot ${proj} ${reg} ${in_fault_file} -Sf1/0.01+l -W0.03c,black -G0
   gmt plot ${proj} ${reg} ${in_pr_trench_fault_file} -Sf1/0.2+r+t -W0.03c,black -G0
   gmt plot ${proj} ${reg} ${in_muertos_fault_file} -Sf1/0.2+l+t -W0.03c,black -G0
   gmt plot ${proj} ${reg} ${in_mona_west_fault_file} -Sf0.2/0.2+l -W0.03c,black -G0
   gmt plot ${proj} ${reg} ${in_mona_east_fault_file} -Sf0.2/0.2+r -W0.03c,black -G0
   gmt plot ${proj} ${reg} ${in_anegada_north_fault_file} -Sf0.2/0.2+r -W0.03c,black -G0
   gmt plot ${proj} ${reg} ${in_anegada_south_fault_file} -Sf0.2/0.2+l -W0.03c,black -G0
#   gmt plot ${proj} ${reg} ${in_fault_file} -Sf1/0.01+l -W0.03c,yellow -G0
#   gmt plot ${reg} ${proj} ${in_pb_trench_file} -Sf0.5/0.1+r+t -W0.03c,black -G0
#   gmt plot ${reg} ${proj} ${in_pb_ridge_file} -Sf0.5/0.1+l -W0.03c,red -G0
#   gmt plot ${reg} ${proj} ${in_pb_transform_file} -Sf0.5/0.01+l -W0.03c,blue -G0

   gmt plot ${proj} ${reg} -W2,blue << EOF
${minbox_lon} ${minbox_lat}
${minbox_lon} ${maxbox_lat}
${maxbox_lon} ${maxbox_lat}
${maxbox_lon} ${minbox_lat}
${minbox_lon} ${minbox_lat}
EOF
#   gmt plot ${proj} ${reg} -W2,blue << EOF
#${min_lon} ${min_lat}
#${min_lon} ${max_lat}
#${max_lon} ${max_lat}
#${max_lon} ${min_lat}
#${min_lon} ${min_lat}
#EOF
#   # M6.4 mainshock mechanism
#   gmt meca ${proj} ${reg} -Sm0.3c+f14 -A -L0.03c,black -: -C${out_color_file} << EOF
#17.88383 -66.79244 3.642 -3.7505 4.3290 -0.5786 -0.4846 -1.2931 2.6375 25
#EOF
#   # Mechanism, M6.4, 2014-01-13, https://earthquake.usgs.gov/earthquakes/eventpage/usc000m1w9/moment-tensor
#   gmt meca ${proj} ${reg} -Sm0.3c+f14 -A -: << EOF
#19.043 -66.81 20 0.988 -1.097 0.11 -1.731 3.541 1.253 25
#EOF
#   # Mechanism, M6.0, 2019-09-24, https://earthquake.usgs.gov/earthquakes/eventpage/pr2019267000/moment-tensor
#   gmt meca ${proj} ${reg} -Sa0.3c+f14 -A -: << EOF
#19.077 -67.22 10 25 51 -73 6.0
#EOF
#   # Mechanism, M7.2, 1918-10-11, Doser et al 2005, https://earthquake.usgs.gov/earthquakes/eventpage/iscgem913306/executive
#   gmt meca ${proj} ${reg} -Sa0.3c+f14 -A -: << EOF
#18.7 -67.189 15 207 54 -127 7.2
#EOF
#   # Mechanism, M6.4, 1915-10-11, Doser et al 2005, https://earthquake.usgs.gov/earthquakes/eventpage/iscgem913947/executive
#   gmt meca ${proj} ${reg} -Sa0.3c+f14 -A -: << EOF
#19.583 -67.17 15 85 40 55 6.4
#EOF
#   # Mechanism, M6.5, 1920-02-10, Doser et al 2005, https://earthquake.usgs.gov/earthquakes/eventpage/iscgem912429/executive
#   gmt meca ${proj} ${reg} -Sa0.3c+f14 -A -: << EOF
#18.616 -67.384 15 65 30 50 6.5
#EOF
#   # Mechanism, M7.8, 1943-07-29, Doser et al 2005, https://earthquake.usgs.gov/earthquakes/eventpage/iscgem899944/executive
#   gmt meca ${proj} ${reg} -Sa0.3c+f14 -A -: << EOF
#18.907 -67.118 15 50 30 30 7.8
#EOF
#   # Mechanism, M7.2, 1867-11-18, Barkan and ten Brink 2010, https://earthquake.usgs.gov/earthquakes/eventpage/eqh18671118184500000/executive
#   gmt meca ${proj} ${reg} -Sa0.3c+f14 -A -: << EOF
#18.2 -65 10 135 45 -45 7.2
#EOF


   # Add inset
#   gmt basemap ${region_inset} ${projection_inset} -Bnews -X0.05i -Y0.05i
   gmt basemap ${region_inset} ${projection_inset} -Bnews -X6.4i -Y0.05i
   gmt coast ${region_inset} ${projection_inset} -Dl -G255 -N1 -W0.25 -Slightskyblue
#   gmt plot ${region_inset} ${projection_inset} ${in_pb_trench_file} -Sf0.5/0.1+r+t -W0.03c,black -G0
   gmt plot ${region_inset} ${projection_inset} ${in_pb_trench_cocos_file} -Sf0.5/0.1+l+t -W0.03c,black -G0
   gmt plot ${region_inset} ${projection_inset} ${in_pb_trench_carib_file} -Sf0.5/0.1+r+t -W0.03c,black -G0
   gmt plot ${region_inset} ${projection_inset} ${in_pb_ridge_file} -Sf0.5/0.1+l -W0.03c,red -G0
   gmt plot ${region_inset} ${projection_inset} ${in_pb_transform_file} -Sf0.5/0.01+l -W0.03c,blue -G0
   gmt plot ${region_inset} ${projection_inset} -W2,black << EOF
${min_lon} ${min_lat}
${min_lon} ${max_lat}
${max_lon} ${max_lat}
${max_lon} ${min_lat}
${min_lon} ${min_lat}
EOF
gmt end show

