import numpy as np
import json
import os
import time
import datetime
import matplotlib # interactive plot only
matplotlib.use('TkAgg') # interactive plot only
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import matplotlib.markers as mk
import matplotlib.cm as cm
import matplotlib.colors as colors
from matplotlib import rcParams

rcParams.update({'font.size': 16})
rcParams['pdf.fonttype'] = 42
rcParams['font.sans-serif'] = "Helvetica"
rcParams['font.family'] = "sans-serif"


#in_station_file = '../LargeAreaEQTransformer/station_list_edited.json'
#in_event_file = '../Catalog/catalog_new_puerto_rico_20200107_20200114.txt'
#in_event_file = '../Catalog/LargeAreaEQTransformer_HYPOINVERSE_puerto_rico_catalog_20200107_20200114.txt'
#in_event_file = '../Catalog/LargeAreaEQTransformer_HYPODD_puerto_rico_catalog_20200107_20200114.txt'
#in_event_file = '../Catalog/LargeAreaEQTransformer_GrowClust_puerto_rico_catalog_20200107_20200114.txt'
#in_event_file = '../Catalog/catalog_new_puerto_rico_2018_2020.txt'
#in_event_file = '../Catalog/FullEQTransformer_HYPOINVERSE_puerto_rico_catalog_2018_2020.txt'

#out_movie_dir = '../Plots/movie_LargeAreaEQTransformer_HYPOINVERSE/'
#out_movie_dir = '../Plots/movie_LargeAreaEQTransformer_HYPODD/'

#in_event_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_HYPOSVI_VELZHANG_Catalog.txt'
#out_movie_dir = '../EQT_20180101_20220101/Movies/movie3D_EQT_20180101_20220101_3REAL_HYPOSVI_VELZHANG/'

mag_thresh = 3
#in_nc_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_NonCluster_GrowClust_VELZHANG_maxdt2_diam.txt'
#in_event_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_GrowClust_VELZHANG_maxdt2_diam.txt'
#out_movie_dir = '../EQT_20180101_20220101/Movies/movie3D_EQT_20180101_20220101_3REAL_GrowClust_VELZHANG/'
in_nc_file = '../Catalog/EQT_20180101_20230101/EQT_20180101_20230101_REAL_VELZHANG_NonCluster_GrowClust_kNN5000_stasis_diam.txt'
in_event_file = '../Catalog/EQT_20180101_20230101/EQT_20180101_20230101_REAL_VELZHANG_GrowClust_kNN5000_stasis_diam.txt'
out_movie_dir = '../EQT_20180101_20230101/Movies/movie3D_EQT_20180101_20230101_REAL_VELZHANG_GrowClust/'

if not os.path.exists(out_movie_dir):
   os.makedirs(out_movie_dir)
flag_movie = True

# Get station data
#stations_ = json.load(open(in_station_file))

# Read in event data
###[times, lat, lon, depth, mag] = np.loadtxt(in_event_file, usecols=(0,1,2,3,4), unpack=True)

#[times1, lat1, lon1, depth1, mag1, diam1] = np.loadtxt(in_event_file, usecols=(0,1,2,3,4,6), unpack=True)
#[times2, lat2, lon2, depth2, mag2, diam2] = np.loadtxt(in_nc_file, usecols=(0,1,2,3,4,6), unpack=True)
[times1, lat1, lon1, depth1, mag1, diam1] = np.loadtxt(in_event_file, usecols=(1,2,3,4,5,7), unpack=True)
[times2, lat2, lon2, depth2, mag2, diam2] = np.loadtxt(in_nc_file, usecols=(1,2,3,4,5,7), unpack=True)
ind_keep = np.where(mag2 >= mag_thresh)
times = np.concatenate((times1, times2[ind_keep[0]]))
lat = np.concatenate((lat1, lat2[ind_keep[0]]))
lon = np.concatenate((lon1, lon2[ind_keep[0]]))
depth = np.concatenate((depth1, depth2[ind_keep[0]]))
mag = np.concatenate((mag1, mag2[ind_keep[0]]))
diam = np.concatenate((diam1, diam2[ind_keep[0]]))

day = times/86400.
print("len(times) = ", len(times))

depth = -1*depth
mag_markers = 100*np.multiply(diam,diam)
#mag_markers = 2*np.multiply(mag, mag)
#mag_markers = 0.2


## Read in station locations
#[sta_lat, sta_lon, sta_depth] = np.loadtxt(input_dir+"threestations.dat", unpack=True)
#sta_depth = -1*sta_depth
#sta_lon = sta_lon-360

fig = plt.figure(figsize=(16,12))
ax = fig.add_subplot(111, projection='3d')

## Plot station locations
#ax.scatter(sta_lon, sta_lat, zs=sta_depth, s=150, c="black", marker="^")
#sta_names = ["WHAR", "ARK1", "ARK2"]
#for i in range(len(sta_names)):
#   ax.text(sta_lon[i], sta_lat[i], sta_depth[i], sta_names[i], size=15)

ax.set_xlabel('Longitude (deg)', labelpad=15)
ax.set_ylabel('Latitude (deg)', labelpad=10)
ax.set_zlabel('Depth (km)', labelpad=10)

min_lat = 17.6
max_lat = 18.3
min_lon = -67.3
max_lon = -66.4
min_depth = -30
max_depth = 0

ax.set_xlim3d([min_lon, max_lon])
ax.set_ylim3d([min_lat, max_lat])
ax.set_zlim3d([min_depth, max_depth])
plt.xticks([-67.3, -67.0, -66.7, -66.4])
plt.yticks([17.6, 17.8, 18.0, 18.2])

ax.tick_params(axis='x', pad=7)
ax.tick_params(axis='y', pad=2)
ax.set_box_aspect(aspect = (9,7,3))

ax.view_init(elev=30.0, azim=-108)

first_day = 726 # 20191228 - days since 20180101
#last_day = 1461 # 20220101 - days since 20180101
last_day = 1826 # 20230101 - days since 20180101
##rot_angle = 720 # 2 full rotations; 1 full rotation with the evolution
rot_angle = 1440 # 4 full rotations; 3 full rotation with the time evolution
disp_first_date = datetime.datetime.strptime('2018-01-01', '%Y-%m-%d')
t0 = time.time()
if (flag_movie):
   # Test movie (comment out plt.show())
#   days_per_frame = (last_day - first_day + 1) / float(rot_angle/2) # 20191228 to 20220101
   days_per_frame = (last_day - first_day + 1) / float(rot_angle * 0.75) # 20191228 to 20230101
   for ii in range(0,rot_angle,1):
      ax.view_init(elev=5, azim=ii+270)
      ax.set_facecolor((0.66, 0.66, 0.66))
#      ax.view_init(elev=23.0, azim=ii+270)
#      p = ax.scatter(lon, lat, zs=depth, s=mag_markers, c=day, vmin=first_day, vmax=last_day, cmap='viridis_r', facecolors='none') # color by time (days since 20180101)

      # --- EVOLUTION MOVIE ---
      curr_frame = ii * days_per_frame
      ind_select = (day >= (first_day + ii*days_per_frame)) & (day <= (first_day + (ii+1)*days_per_frame))
      print(ii, curr_frame)
      p = ax.scatter(lon[ind_select], lat[ind_select], zs=depth[ind_select], s=mag_markers[ind_select], c=day[ind_select], vmin=first_day, vmax=last_day, cmap='viridis_r', marker="$\u25EF$") # color by time (days since 20180101)
#      p = ax.scatter(lon[ind_select], lat[ind_select], zs=depth[ind_select], s=mag_markers, c=day[ind_select], vmin=first_day, vmax=last_day, cmap='viridis_r', facecolors='none') # color by time (days since 20180101)

      #if (ii < 360):
      if (ii < 1079):
         date_to_show = day[ind_select][0]
         print(date_to_show)
         disp_curr_date = disp_first_date + datetime.timedelta(date_to_show)
         disp_title_date = datetime.datetime.strftime(disp_curr_date, '%Y-%m-%d')
         plt.title('Date: '+disp_title_date, y=0.75)
      else:
         plt.title('All events, 2019-12-28 to 2023-01-01', y=0.75)
#         plt.title('All events, 2019-12-28 to 2022-01-01', y=0.75)

      # --- EVOLUTION MOVIE ---

      if (ii == 0):
         cbar = fig.colorbar(p, ticks=[730, 1096, 1461, 1826], shrink=0.5)
         cbar.ax.set_yticklabels(['2020-01-01', '2021-01-01', '2022-01-01', '2023-01-01'])
#         cbar = fig.colorbar(p, ticks=[730, 1096, 1461], shrink=0.5)
#         cbar.ax.set_yticklabels(['2020-01-01', '2021-01-01', '2022-01-01'])
         cbar.set_label("Date")

      plt.tight_layout()
      plt.savefig(out_movie_dir+"movie"+str(ii).zfill(4)+".png")

#      if (ii == 360):
#         plt.show()

   ## After this script, run:
   # $ ffmpeg -framerate 24 -i movie%04d.png -vcodec libx264 -pix_fmt yuv420p movie.mp4
   # $ ffmpeg -i movie.mp4 -filter:v "setpts=3.0*PTS" movie_final.mp4
else:
   plt.show()

tfinal = time.time() - t0
print("Runtime for plot_event_location_3d.py:", tfinal)
