import matplotlib.pyplot as plt
import numpy as np
import math
from matplotlib import rcParams

rcParams.update({'font.size': 24})
#rcParams.update({'font.size': 36}) #poster
#rcParams['pdf.fonttype'] = 42
rcParams['font.sans-serif'] = "Helvetica"
rcParams['font.family'] = "sans-serif"

#out_plot_file = '../Plots/hector_kamer_velocity_model.pdf'
#out_plot_file = '../Plots/puerto_rico_velocity_model.pdf'
#out_plot_file = '../EQT_20180101_20211001/Plots/puerto_rico_velocity_model_zhang.pdf'
out_plot_file = '../EQT_20180101_20230101/Plots/puerto_rico_velocity_model_both.pdf'

## Hector Mine velocity model - SCSN
#depth = np.array([0, 5.5, 5.5, 16, 16, 32, 32])
#vp = np.array([5.5, 5.5, 6.3, 6.3, 6.7, 6.7, 7.8])

## Hector Mine velocity model - Kamer et al (2017)
#depth = np.array([0, 2, 2, 4, 4, 6, 6, 8, 8, 10, 10, 12, 12, 15, 15, 18, 18, 23, 23, 30, 30, 50, 50])
#vp = np.array([5.28, 5.28, 5.68, 5.68, 6.05, 6.05, 6.13, 6.13, 6.21, 6.21, 6.3, 6.3, 6.36, 6.36, 6.53, 6.53, 6.59, 6.59, 6.83, 6.83, 7.84, 7.84, 8.2])
#vs = vp / math.sqrt(3.0)

# Puerto Rico 1D velocity model - Huerfano and Bataille 1994
depth_pr = np.array([0, 2.81, 2.81, 8.86, 8.86, 14.74, 14.74, 19.18, 19.18, 30])
vp_pr = np.array([4.59, 4.59, 6.35, 6.35, 6.84, 6.84, 7.31, 7.31, 7.92, 7.92])
vs_pr = vp_pr / 1.78

# Puerto Rico 1D velocity model - Zhang 2014
depth = np.array([0, 2, 2, 5, 5, 8, 8, 14, 14, 20, 20, 25, 25, 31])
vp = np.array([4.43, 4.43, 4.97, 4.97, 5.53, 5.53, 6.40, 6.40, 6.86, 6.86, 7.21, 7.21, 7.56, 7.56])
vs = vp / 1.75

plt.figure(figsize=(8,9))
plt.plot(vp, depth, color="b", label="Vp, Zhang", linewidth=3)
plt.plot(vs, depth, color="r", label="Vs, Zhang", linewidth=3)
plt.plot(vp_pr, depth_pr, color="b", label="Vp, PRSN", linewidth=3, linestyle='--')
plt.plot(vs_pr, depth_pr, color="r", label="Vs, PRSN", linewidth=3, linestyle='--')
plt.xlim([2,10])
plt.ylim([0,30])
plt.gca().invert_yaxis()
plt.legend(loc='upper right', prop={'size':20})
plt.xlabel("Velocity (km/s)")
plt.ylabel("Depth (km)")
plt.title("1D Velocity Model", y=1.02)
plt.tight_layout()
plt.savefig(out_plot_file)
