#!/bin/bash

in_dir=../Catalog/EQT_20180101_20230101
s1_file=${in_dir}/catalog_new_puerto_rico_20180101_20230101_download20230206_diam.txt
s2_file=${in_dir}/EQT_20180101_20230101_REAL_VELZHANG_DIRECTHYPOSVI_diam_uncertainty.txt
s3_file=${in_dir}/EQT_20180101_20230101_REAL_VELZHANG_GrowClust_kNN5000_stasis_diam.txt
s4_file=${in_dir}/EQT_20180101_20230101_REAL_VELZHANG_NonCluster_GrowClust_kNN5000_stasis_diam.txt
s5_file=../EQT_20180101_20230101/REAL_VELZHANG/DIRECTHYPOSVI/events_MATCH_magcat_DIRECTHYPOSVI_EQT_20180101_20230101.txt

out_dir=../Manuscript_MachineLearning/Data
sort -nk2,2 ${s1_file} > ${out_dir}/Data01_supplement.txt
sort -nk2,2 ${s2_file} > ${out_dir}/Data02_supplement.txt
sort -nk2,2 ${s3_file} > ${out_dir}/Data03_supplement.txt
sort -nk2,2 ${s4_file} > ${out_dir}/Data04_supplement.txt
cp ${s5_file} ${out_dir}/Data05_supplement.txt
