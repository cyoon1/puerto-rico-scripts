import json
import pickle
import time
import matplotlib.pyplot as plt
from matplotlib import rcParams
import numpy as np
from obspy import read_events
from obspy import UTCDateTime

rcParams.update({'font.size': 24})
#rcParams.update({'font.size': 36}) #poster
###rcParams['pdf.fonttype'] = 42
rcParams['font.sans-serif'] = "Helvetica"
rcParams['font.family'] = "sans-serif"

def plot_num_picks_hist(num_picks, x_max, label_str, title_str, out_plot_file, text_res='', text_nomatch=''):
   plt.figure(num=0, figsize=(10,8))
   plt.clf()
   n, bins, patches = plt.hist([num_picks], bins=np.arange(-0.5, x_max+1.5, 1), label=[label_str], color=['yellow'], edgecolor='black', linewidth=0.2)
   plt.xlim([0, x_max])
   plt.yscale('log', nonpositive='clip')
   plt.ylim([0.1, 2000])
   plt.xlabel("Number of picks")
   plt.ylabel("Number of catalog events missed by EQT")
   plt.title(title_str)
   plt.legend(prop={'size':20})
   plt.figtext(0.17, 0.75, text_res, fontsize=18)
   plt.figtext(0.17, 0.45, text_nomatch, fontsize=18)
   plt.tight_layout()
   plt.savefig(out_plot_file)


#=============================
# Script to count number of picks for catalog events missed by EQT-HYPOINVERSE

in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/station_list_edited.json'
in_cat_xml_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/CatalogEventsDownload20220111/'

#in_missed_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/events_MISSED_magcat_EQT_20180101_20220101.txt'
##out_plot_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/MagPlots/'
#out_plot_dir = '../EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/MagPlots/'

in_missed_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/events_MISSED_magcat_EQT_20180101_20220101.txt'
#out_plot_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/MagPlots/'
out_plot_dir = '../EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/MagPlots/'

in_station_file = out_plot_dir+'station_list_edited.json'
out_missed_list_file = out_plot_dir+'phase_list_MISSED_EQT_20180101_20220101.npz'
out_cat_dict_file = out_plot_dir+'phase_cat_dict_MISSED_EQT_20180101_20220101.npz'

stations_ = json.load(open(in_station_file))

t0 = time.time()
# Read in event ids for missed events, in order
ev_cat_id_list = []
event_cat_dict = {}
with open(in_missed_catalog_file, 'r') as fev:
   for line in fev:
      cat_line = line.split() # cat ev_id
      ev_cat_id = cat_line[5]

      # Read in events for missed events: catalog
      cat_origin_time = UTCDateTime.strptime(cat_line[0], "%Y-%m-%dT%H:%M:%S.%f")
      cat_ev_lat = float(cat_line[1])
      cat_ev_lon = float(cat_line[2])
      cat_depth = float(cat_line[3])
      cat_mag = float(cat_line[4])
#      if ((cat_ev_lat >= min_lat) and (cat_ev_lat <= max_lat) and (cat_ev_lon >= min_lon) and (cat_ev_lon < max_lon)):
      event_cat_dict[ev_cat_id] = {}
      event_cat_dict[ev_cat_id]['event'] = [cat_origin_time, cat_ev_lat, cat_ev_lon, cat_depth, cat_mag]
      event_cat_dict[ev_cat_id]['P'] = {}
      event_cat_dict[ev_cat_id]['S'] = {}
      ev_cat_id_list.append(ev_cat_id)

# Read in phases for missed events: catalog
for cat_evid in ev_cat_id_list:
   # Read in event file from catalog xml file, includes phase pick info
   in_xml_file = in_cat_xml_dir+cat_evid+'.xml'
   try:
      curr_ev = read_events(in_xml_file)
   except Exception:
      print("ERROR: Cannot find event xml file ", in_xml_file, ", skipping...", cat_evid)
      continue

   # Get phase picks
   phase_list = curr_ev[0].picks
   for pick in phase_list:
      sta = pick.waveform_id.station_code
      if (sta in stations_): # only output the phase if it is in our station list (json)
         net = pick.waveform_id.network_code
         chcode = pick.waveform_id.channel_code
         ph = pick.phase_hint
         print(cat_evid, net, sta, chcode, ph, pick.time)

         # Possible phase names in catalog
         if ((ph == 'Pg') or (chcode[-1] == 'Z')):
            ph = 'P'
         if ((ph == 'Sg') or (chcode[-1] == 'E') or (chcode[-1] == 'N') or (chcode[-1] == '1') or (chcode[-1] == '2')):
            ph = 'S'

         net_sta = net.strip()+sta.strip()
         ph_res = 0.0 # dummy
         event_cat_dict[cat_evid][ph][net_sta] = [chcode, pick.time, ph_res] # Add phase to dict

tfinal = time.time() - t0
print("Runtime for reading in missed catalog phases:", tfinal)
print("Number of missed events:", len(ev_cat_id_list))

# Save for later
np.savez(out_missed_list_file, ev_cat_id_list=ev_cat_id_list)
with open(out_cat_dict_file, 'wb') as handle:
   pickle.dump(event_cat_dict, handle, protocol=pickle.HIGHEST_PROTOCOL)

#==== Finished reading in phases into dicts=====

## Load precomputed dicts with phases
#data = np.load(out_missed_list_file, allow_pickle=True)
#ev_cat_id_list = data['ev_cat_id_list']
#with open(out_cat_dict_file, 'rb') as handle:
#   event_cat_dict = pickle.load(handle)

t0 = time.time()
# Prep net_sta array
net_sta_arr = []
for sta in stations_:
   network = stations_[sta]['network']
   net_sta = network+sta
   net_sta_arr.append(net_sta)

# Now, for each common event between catalog and EQT, compare phases
phase_comp_list = []
for idx, ev_cat in enumerate(ev_cat_id_list):
   # curr_ev_info has 7 elements: [idx, cat_evid, cat_ot, cat_lat, cat_lon, cat_depth, cat_mag]
   ev_lat = event_cat_dict[ev_cat]['event'][1]
   ev_lon = event_cat_dict[ev_cat]['event'][2]
   curr_ev_info = [idx, ev_cat, event_cat_dict[ev_cat]['event'][0], ev_lat, ev_lon, event_cat_dict[ev_cat]['event'][3], event_cat_dict[ev_cat]['event'][4]]
   print("Event:", idx, ev_cat)
#   print("curr_ev_info:", curr_ev_info)
   num_all_phases = 0
   curr_phase_comp_line = curr_ev_info
   for phase in ['P','S']:
      curr_cat_ph_list = event_cat_dict[ev_cat][phase]
      num_phases = 0
      for net_sta in net_sta_arr:
         if (net_sta in curr_cat_ph_list):
            num_phases += 1
            num_all_phases += 1
      curr_phase_info = [phase, num_phases]
      curr_phase_comp_line += curr_phase_info
   curr_all_phase_info = ['all', num_all_phases]
   curr_phase_comp_line += curr_all_phase_info
   print(curr_phase_comp_line)
   # curr_phase_comp_line has 13 elements: [idx, cat_evid, cat_ot, cat_lat, cat_lon, cat_depth, cat_mag, 'P', num_P_phases, 'S', num_S_phases, 'all', num_all_phases]
   phase_comp_list.append(curr_phase_comp_line)

print("Number of lines in phase_comp_list:", len(phase_comp_list))
tfinal = time.time() - t0
print("Runtime for computing number of phases in missed catalog events:", tfinal)
#np.savez(out_comp_list_file, phase_comp_list=phase_comp_list)

# ======== Filter the phase_comp_list ============

# Filter for phases within the lat/lon box
min_lat = 17.6
max_lat = 18.3
min_lon = -67.3
max_lon = -66.4

IND_IDX = 0
IND_CAT_OT = 2
IND_CAT_LAT = 3
IND_CAT_LON = 4
IND_NPHASE_P = 8
IND_NPHASE_S = 10
IND_NPHASE_ALL = 12

#ev_sel = [ph for ph in phase_comp_list]
#ev_out_str = ''

ev_sel = [ph for ph in phase_comp_list if ((ph[IND_CAT_LAT] >= min_lat) and (ph[IND_CAT_LAT] <= max_lat) and
   (ph[IND_CAT_LON] >= min_lon) and (ph[IND_CAT_LON] <= max_lon))] # inside lat/lon box
ev_out_str = '_box'

print("len(ev_sel):", len(ev_sel))
num_p_sel = [ph[IND_NPHASE_P] for ph in ev_sel]
num_s_sel = [ph[IND_NPHASE_S] for ph in ev_sel]
num_all_sel = [ph[IND_NPHASE_ALL] for ph in ev_sel]

out_plot_file = out_plot_dir+'EQT_20180101_20220101_MISSED_NUM_P_picks_histogram'+ev_out_str+'.pdf'
plot_num_picks_hist(num_p_sel, 25, 'P picks', str(len(num_p_sel))+' total events', out_plot_file)
out_plot_file = out_plot_dir+'EQT_20180101_20220101_MISSED_NUM_S_picks_histogram'+ev_out_str+'.pdf'
plot_num_picks_hist(num_s_sel, 25, 'S picks', str(len(num_s_sel))+' total events', out_plot_file)
out_plot_file = out_plot_dir+'EQT_20180101_20220101_MISSED_NUM_ALL_picks_histogram'+ev_out_str+'.pdf'
plot_num_picks_hist(num_all_sel, 50, 'All P+S picks', str(len(num_all_sel))+' total events', out_plot_file)
