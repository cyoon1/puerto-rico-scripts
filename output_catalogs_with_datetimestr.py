import datetime

in_dir = '../Manuscript_MachineLearning/Data/'
catalog_start_time = '2018-01-01T00:00:00'
catalog_ref_time = datetime.datetime.strptime(catalog_start_time, '%Y-%m-%dT%H:%M:%S')

#in_file = 'catalog_new_puerto_rico_20180101_20220101_download20220111_diam.txt'
#out_file = in_dir+'Data01_'+in_file

#in_file = 'EQT_20180101_20220101_3REAL_HYPOINVERSE_VELZHANG_combined_real_magcat_locate_pr_diam.txt'
#out_file = in_dir+'Data02_'+in_file

#in_file = 'EQT_20180101_20220101_3REAL_HYPOSVI_VELZHANG_Catalog_diam.txt'
#out_file = in_dir+'Data03_'+in_file

#in_file = 'EQT_20180101_20220101_3REAL_GrowClust_VELZHANG_maxdt2_diam.txt'
#out_file = in_dir+'Data04_'+in_file

in_file = 'EQT_20180101_20220101_3REAL_NonCluster_GrowClust_VELZHANG_maxdt2_diam.txt'
out_file = in_dir+'Data05_'+in_file

fout = open(out_file, 'w')
with open(in_dir+in_file, 'r') as fin:
   for line in fin:
      split_line = line.split()
      num_total_sec = float(split_line[0])
      current_time = catalog_ref_time + datetime.timedelta(seconds=num_total_sec)
      datetime_str = datetime.datetime.strftime(current_time, "%Y-%m-%dT%H:%M:%S.%f")
      print(datetime_str)
      fout.write(("%s %s") % (datetime_str, line))
fout.close()
