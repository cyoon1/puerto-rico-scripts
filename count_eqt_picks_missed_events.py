import datetime
import glob
import os

# Count number of EQTransformer picks for missed ComCat events
in_cat_dir = '../Catalog/EQT_20180101_20230101/'
in_missed_event_file = in_cat_dir+'EQT_20180101_20230101_REAL_VELZHANG_DIRECTHYPOSVI_events_MISSED_magcalcml_inregion_diam.txt'
base_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20230101/'
in_pick_dir = base_dir+'REAL_VELZHANG/Pick/'
out_num_picks_file = in_cat_dir+'EQT_20180101_20230101_REAL_VELZHANG_DIRECTHYPOSVI_events_MISSED_magcalcml_inregion_diam_npickseqt.txt'
start_time = 0
end_time = 15

fout_picks = open(out_num_picks_file, 'w')
with open(in_missed_event_file, 'r') as fin:
   for line in fin:
      split_line = line.strip('\n').split()
      origin_time = datetime.datetime.strptime(split_line[0], '%Y-%m-%dT%H:%M:%S.%f')
      ymd_str = origin_time.strftime('%Y%m%d')
      ymd_val = datetime.datetime.strptime(ymd_str, '%Y%m%d')
      ot_num_sec = (origin_time - ymd_val).total_seconds()
      start_from_ot_time = ot_num_sec + start_time
      end_from_ot_time = ot_num_sec + end_time
      ev_id = split_line[6]
      print(ev_id, origin_time, ymd_str, ot_num_sec)

      npicks_sta_p = 0
      npicks_sta_s = 0
      npicks_sta_total = 0
      sta_inc = set()
      all_pick_files = glob.glob(in_pick_dir+ymd_str+'/*.txt')
#      print('Number of pick files: ', len(all_pick_files))
      for pick_file in all_pick_files:
         file_name = os.path.basename(pick_file)
         curr_net, curr_sta, curr_ph, curr_ext = file_name.split('.')
#         print(pick_file, file_name, curr_net, curr_sta, curr_ph, curr_ext)

         f_pick_in = open(pick_file, 'r')
         for pick_line in f_pick_in:
            split_pick_line = pick_line.split()
            num_sec = float(split_pick_line[0])
            prob_val = float(split_pick_line[1])
#            print(pick_line, num_sec, prob_val)
            if ((num_sec >= start_from_ot_time) and (num_sec <= end_from_ot_time)):
               npicks_sta_total += 1
               sta_inc.add(curr_sta)
               if (curr_ph == 'P'):
                  npicks_sta_p += 1
               if (curr_ph == 'S'):
                  npicks_sta_s += 1
         f_pick_in.close()
      print(ev_id, npicks_sta_p, npicks_sta_s, npicks_sta_total, len(sta_inc), sta_inc)
      fout_picks.write(('%s %d %d %d %d\n') % (line.replace('\n',''), npicks_sta_p, npicks_sta_s, npicks_sta_total, len(sta_inc)))
fout_picks.close()


