import matplotlib.pyplot as plt
import numpy as np
from matplotlib import rcParams
from matplotlib.gridspec import GridSpec

rcParams.update({'font.size': 20})
#rcParams.update({'font.size': 24})
#rcParams.update({'font.size': 30}) #poster
#rcParams['pdf.fonttype'] = 42
rcParams['font.family'] = "sans-serif"
rcParams['font.sans-serif'] = "Helvetica"



#in_hinv_sum_file = '../FullEQTransformer/20180101_20201101_Model1/REAL/HYPOINVERSE/events_MATCH_magcat_eqt_2018_2020.txt'
#out_mag_plot_dir = '../Plots/'
#out_calib_plot_file = 'FullEQTransformer_Magnitude_Calibration.pdf'

#in_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/us_events_MATCH_magcat_EQT_20180101_20211001.txt'
#out_mag_plot_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/MagPlots/'
#out_calib_plot_file = 'final_magnitude_calibration_us_EQT_20180101_20211001.pdf'
#out_resid_plot_file = 'final_magnitude_residual_us_EQT_20180101_20211001.pdf'

#in_hinv_sum_file = '../EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/us_events_MATCH_magcat_EQT_20180101_20220101.txt'
#out_mag_plot_dir = '../EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/CompPlots/'
#out_calib_plot_file = 'final_magnitude_calibration_us_EQT_20180101_20220101.pdf'
#out_resid_plot_file = 'final_magnitude_residual_us_EQT_20180101_20220101.pdf'

#in_hinv_sum_file = '../EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/events_MATCH_magcat_EQT_20180101_20220101.txt'
#out_mag_plot_dir = '../EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/CompPlots/'
#out_calib_plot_file = 'final_magnitude_calibration_EQT_20180101_20220101.pdf'
#out_resid_plot_file = 'final_magnitude_residual_EQT_20180101_20220101.pdf'

#in_hinv_sum_file = '../EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/us_events_MATCH_magcat_EQT_20180101_20220101.txt'
#out_mag_plot_dir = '../EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/CompPlots/'
#out_calib_plot_file = 'final_magnitude_calibration_us_EQT_20180101_20220101.pdf'
#out_resid_plot_file = 'final_magnitude_residual_us_EQT_20180101_20220101.pdf'

#in_hinv_sum_file = '../EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/events_MATCH_magcat_EQT_20180101_20220101.txt'
#out_mag_plot_dir = '../EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/CompPlots/'
#out_calib_plot_file = 'final_magnitude_calibration_EQT_20180101_20220101.pdf'
#out_resid_plot_file = 'final_magnitude_residual_EQT_20180101_20220101.pdf'

in_hinv_sum_file = '../EQT_20180101_20230101/REAL_VELZHANG/DIRECTHYPOSVI/us_events_MATCH_magcat_EQT_20180101_20230101.txt'
out_mag_plot_dir = '../EQT_20180101_20230101/REAL_VELZHANG/DIRECTHYPOSVI/CompPlots/'
out_calib_plot_file = 'final_magnitude_calibration_us_EQT_20180101_20230101.pdf'
out_resid_plot_file = 'final_magnitude_residual_us_EQT_20180101_20230101.pdf'

#in_hinv_sum_file = '../EQT_20180101_20230101/REAL_VELZHANG/DIRECTHYPOSVI/events_MATCH_magcat_DIRECTHYPOSVI_EQT_20180101_20230101.txt'
#out_mag_plot_dir = '../EQT_20180101_20230101/REAL_VELZHANG/DIRECTHYPOSVI/CompPlots/'
#out_calib_plot_file = 'final_magnitude_calibration_EQT_20180101_20230101.pdf'
#out_resid_plot_file = 'final_magnitude_residual_EQT_20180101_20230101.pdf'


all_catalog_mag = []
all_catalog_ML = []
with open(in_hinv_sum_file, 'r') as fin:
   for line in fin:
      ml_mag = 0.01*float(line[147:150])
#      cat_line = line[179:]
      cat_line = line[151:]
      split_line = cat_line.split()
      cat_mag = float(split_line[4])
      all_catalog_mag.append(cat_mag)
      all_catalog_ML.append(ml_mag)
#      print(ml_mag, cat_mag)
print(len(all_catalog_mag), len(all_catalog_ML))


# After magnitude calibration, show that magnitudes match: plot catalog magnitude vs calculated ML
equal_line = np.linspace(-1, 7, 100)
plt.figure(figsize=(6,6))
plt.scatter(all_catalog_mag, all_catalog_ML, facecolors='none', edgecolors='k', linewidths=0.1)
plt.plot(equal_line, equal_line, '--', color='k')
plt.axis('scaled')
plt.xlim([0, 7])
plt.ylim([0, 7])
plt.xticks([0, 1, 2, 3, 4, 5, 6, 7])
plt.yticks([0, 1, 2, 3, 4, 5, 6, 7])
plt.xlabel('Catalog magnitude $M_{cat}$')
plt.ylabel('Local magnitude $M_{L}$')
plt.tight_layout()
plt.savefig(out_mag_plot_dir+out_calib_plot_file)

# Compute residuals and statistics
resid_mag = np.subtract(all_catalog_ML, all_catalog_mag)
mean_res_arr = np.mean(resid_mag)
std_res_arr = np.std(resid_mag)
print("Min residual: ", min(resid_mag))
print("Max residual: ", max(resid_mag))
print("Mean residual: ", mean_res_arr)
print("Std residual: ", std_res_arr)
x_min = 0
x_max = 7
#y_min = -1
#y_max = 1
y_min = -2
y_max = 2
equal_line = np.linspace(x_min, x_max, 100)
zero_line = np.zeros(100)

# Plot residuals
fig = plt.figure(figsize=(8,6))
# https://stackoverflow.com/questions/37008112/matplotlib-plotting-histogram-plot-just-above-scatter-plot
gs = GridSpec(5,8)
ax_joint = fig.add_subplot(gs[0:5,0:6])
ax_marg_y = fig.add_subplot(gs[0:5,6:8], sharey=ax_joint)

ax_joint.scatter(all_catalog_mag, resid_mag, facecolors='none', edgecolors='k', linewidths=0.1)
ax_joint.plot(equal_line, zero_line, '--', color='k', linewidth=0.8)
ax_marg_y.hist(resid_mag, bins=np.arange(y_min, y_max+0.0001, 0.05), orientation="horizontal", color=['white'], edgecolor='black', linewidth=0.3)

ax_joint.set_xlim([x_min, x_max])
ax_joint.set_ylim([y_min, y_max])
ax_joint.set_xlabel('Catalog magnitude $M_{cat}$')
ax_joint.set_ylabel('Residual: $M_{L}$ - $M_{cat}$')

# Turn off tick labels on marginals
plt.setp(ax_marg_y.get_yticklabels(), visible=False)

# Statistics
text_res = "Mean: "+f"{mean_res_arr:.2f}"+"\nStd: "+f"{std_res_arr:.2f}"
plt.figtext(0.85, 0.72, text_res, rotation=270, fontsize=16)

# Set labels on marginals
ax_marg_y.set_xlabel('Number of\n events')

plt.tight_layout()
plt.savefig(out_mag_plot_dir+out_resid_plot_file)

