import numpy as np
import glob
from obspy import read
from obspy import read_inventory
from obspy import UTCDateTime
from obspy.geodetics.base import gps2dist_azimuth
from statistics import median
import utils_hypoinverse as utils_hyp

# Utilities for the local magnitude calculation


# Get station inventory for instrument response
def get_station_inventory(in_inv_dir, stations_):
   inv_map = {}
   for sta,val in stations_.items():
      in_inv_file = in_inv_dir+sta+'/'+val['network']+'.'+sta+'.xml'
      curr_inv = read_inventory(in_inv_file)
      inv_map[sta] = curr_inv
   return inv_map


# Get map of distances to each station from event
def get_event_station_distance_map(stations_, ev_lat, ev_lon):
   sta_dist_map = {}
   for sta,val in stations_.items():
      sta_lat = val['coords'][0]
      sta_lon = val['coords'][1]
      [dist_ev_sta, azAB, azBA] = gps2dist_azimuth(ev_lat, ev_lon, sta_lat, sta_lon)
      dist_ev_sta_km = 0.001*dist_ev_sta
      sta_dist_map[sta] = np.log10(dist_ev_sta_km)
   return sta_dist_map


# Get map (dictionary) of catalog events
def get_catalog_events_map(in_match_catalog_file, stations_):
   map_cat_events = {} # contain event data from catalog
   with open(in_match_catalog_file, 'r') as fev:
      for line in fev:
         ev_id = int(utils_hyp.get_event_id_hypoinverse_file(line)) # eqt ev_id
#         cat_line = line[180:].split()
         cat_line = line[151:].split() #hyposvi
         cat_lat = float(cat_line[1])
         cat_lon = float(cat_line[2])
         cat_depth = float(cat_line[3])
         cat_mag = float(cat_line[4])
         sta_dist_map = get_event_station_distance_map(stations_, cat_lat, cat_lon)
         map_cat_events[ev_id] = [cat_lat, cat_lon, cat_depth, cat_mag, sta_dist_map, line]
   print("Number of catalog events for calibration: ", len(map_cat_events))
   return map_cat_events
 

# Get map (dictionary) of missed catalog events
def get_missed_catalog_events_map(in_missed_catalog_file, stations_):
   map_cat_events = {} # contain event data from catalog
   with open(in_missed_catalog_file, 'r') as fev:
      for line in fev:
         split_line = line.split()
         ev_id = int(split_line[6])
         cat_lat = float(split_line[1])
         cat_lon = float(split_line[2])
         cat_depth = float(split_line[3])
         cat_mag = float(split_line[4])
         sta_dist_map = get_event_station_distance_map(stations_, cat_lat, cat_lon)
         map_cat_events[ev_id] = [cat_lat, cat_lon, cat_depth, cat_mag, sta_dist_map, line]
   print("Number of catalog events for calibration: ", len(map_cat_events))
   return map_cat_events


# Get map (dictionary) of events from HYPOINVERSE arc file
def get_events_map(in_hinv_arc_file, stations_):
   map_events = {} # contain event data
   with open(in_hinv_arc_file, 'r') as fev:
      for line in fev:
         if ((line[0:2] == '19') or (line[0:2] == '20')): # an event line
            ev_id = int(utils_hyp.get_event_id_hypoinverse_file(line)) # eqt ev_id
            [ev_lat, ev_lon, ev_depth] = utils_hyp.get_lat_lon_depth_hypoinverse_file(line)
            ev_mag = 0.01*float(line[147:150]) # magnitude should be 0 - not used
            sta_dist_map = get_event_station_distance_map(stations_, ev_lat, ev_lon)
            map_events[ev_id] = [ev_lat, ev_lon, ev_depth, ev_mag, sta_dist_map, line]
         else:
            continue # not an event line
   print("Number of events to calculate magnitude: ", len(map_events))
   return map_events


# Get Wood-Anderson response waveform data
def get_wood_anderson_data(in_event_file_dir, ev_origin_time, start_time, end_time, sta, inv):
   # https://eqcorrscan.readthedocs.io/en/latest/_modules/eqcorrscan/utils/mag_calc.html#_sim_WA
   # Helpers for local magnitude estimation
   # Note Wood anderson sensitivity is 2080 as per Uhrhammer & Collins 1990
   PAZ_WA_ = {'poles': [-6.283 + 4.7124j, -6.283 - 4.7124j],
             'zeros': [0 + 0j], 'gain': 1.0, 'sensitivity': 2080}
   #paz_wa = copy.deepcopy(PAZ_WA_)

   # Read this event file, get slice around event at this station
   ev_origin_date = UTCDateTime(ev_origin_time.year, ev_origin_time.month, ev_origin_time.day)
   origin_date_str = UTCDateTime.strftime(ev_origin_date, '%Y%m%d')
   mseed_files = glob.glob(in_event_file_dir+'*/'+sta+'/*.'+sta+'*__'+origin_date_str+'T000000Z'+'__*')
   if (len(mseed_files) > 0):
      st_full = read(in_event_file_dir+'*/'+sta+'/*.'+sta+'*__'+origin_date_str+'T000000Z'+'__*mseed')
      st = st_full.slice(start_time, end_time)

#   st.plot(outfile=out_mag_plot_dir+'event'+str(ev_id)+'.'+sta+'.png')

   # Remove instrument response
   st.remove_response(inventory=inv, output='VEL')
#   st.plot(outfile=out_mag_plot_dir+'vel_event'+str(ev_id)+'.'+sta+'.png')

   # Get Wood-Anderson displacement waveforms for this event
   st.simulate(paz_remove=None, paz_simulate=PAZ_WA_, simulate_sensitivity=True)
#   st.plot(outfile=out_mag_plot_dir+'wa_event'+str(ev_id)+'.'+sta+'.png')
 
   return st


# Get max amplitude for this component of waveform data, for magnitude calculation
def get_max_amp_component(wf_comp, start_time, end_time, min_dur):
   flag_comp = False
   max_amp_comp = 0.
   if len(wf_comp):
      wf_mag_comp = wf_comp.slice(start_time, end_time) # time window for magnitude computation
      if len(wf_mag_comp):
         wf_mag_comp_time = wf_mag_comp[0].stats.endtime - wf_mag_comp[0].stats.starttime
         if (wf_mag_comp_time >= min_dur):
            max_amp_comp = max(abs(wf_mag_comp[0].data)) # get maximum amplitude
            if (max_amp_comp > 0):
               flag_comp = True
   return [max_amp_comp, flag_comp]


# Get max amplitude for this station, for magnitude calculation
def get_max_amp_station(st, start_time, end_time, min_dur):
   # Get each component
   wf_e = st.select(component='E')
   [max_amp_e, flag_e] = get_max_amp_component(wf_e, start_time, end_time, min_dur)
   wf_n = st.select(component='N')
   [max_amp_n, flag_n] = get_max_amp_component(wf_n, start_time, end_time, min_dur)
   wf_z = st.select(component='Z')
   [max_amp_z, flag_z] = get_max_amp_component(wf_z, start_time, end_time, min_dur)

   flag_sta_amp = False
   mag_amp_term = 0.0
   if (flag_e and flag_n): # preferred scenario
      # ML_Richter: compute magnitude first (take log10), then average them
      mag_amp_term = 0.5*(np.log10(max_amp_e) + np.log10(max_amp_n))
      flag_sta_amp = True
   elif (flag_e):
      mag_amp_term = np.log10(max_amp_e)
      flag_sta_amp = True
   elif (flag_n):
      mag_amp_term = np.log10(max_amp_n)
      flag_sta_amp = True
   elif (flag_z):
      mag_amp_term = np.log10(max_amp_z)
      flag_sta_amp = True
   return [mag_amp_term, flag_sta_amp]

# Get phase info -> time window length for magnitude calculation
def get_phase_time_info_flags(event_dict, ev_id, sta, phases_list):
   t_p = np.nan
   t_s = np.nan
   s_minus_p = np.nan
   flag_tp = False
   flag_ts = False
   flag_s_minus_p = False
   flag_phase = False
   for phase in phases_list:
      cur_sta = phase[2:]
      if (cur_sta == sta):
         flag_phase = True
         # Save phase arrival times (UTCDateTime)
         if (phase in event_dict[ev_id]['P']):
            t_p = event_dict[ev_id]['P'][phase][1]
            flag_tp = True
         if (phase in event_dict[ev_id]['S']):
            t_s = event_dict[ev_id]['S'][phase][1]
            flag_ts = True
         if (flag_tp and flag_ts):
            s_minus_p = t_s - t_p
            flag_s_minus_p = True
         break
   return [t_p, t_s, s_minus_p, flag_tp, flag_ts, flag_s_minus_p, flag_phase]


def calculate_max_amplitude_station(in_event_file_dir, event_dict, ev_id, ev_origin_time, sta, phases_list, inv_map):
   mag_amp_term = 0.0
   flag_sta_amp = False

   # Define event time window to look for peak amplitude
   start_time = ev_origin_time
#   default_win_duration = 40.0
   default_win_duration = 30.0
   end_time = start_time + default_win_duration
   min_win_duration = 5.0
   min_end_time = start_time + min_win_duration
   flag_filter = False # flag for bandpass filter

   # Define time window for Wood Anderson response calculation
   # Need extra padding around start and end (otherwise, can lead to incorrect magnitude estimate outliers)
   start_wa_time = ev_origin_time - 15.0
   end_wa_time = ev_origin_time + 105.0

   # Select only stations where a phase pick was made
   # Do not want to use noisy stations for ML magnitude estimate
   [t_p, t_s, s_minus_p, flag_tp, flag_ts, flag_s_minus_p, flag_phase] = get_phase_time_info_flags(event_dict, ev_id, sta, phases_list)

   if (flag_phase):
      try:
         st = get_wood_anderson_data(in_event_file_dir, ev_origin_time, start_wa_time, end_wa_time, sta, inv_map[sta])
      except:
         # It is possible that event file does not exist for this station at this time
         print("Warning: file not found", ev_id, ev_origin_time, sta)
         return [mag_amp_term, flag_sta_amp]
#         continue

      # Filter if needed
      if (flag_filter):
         st.detrend(type='demean')
         st.detrend(type='linear')
         st.filter('highpass', freq=1.0, corners=2, zerophase=False) # remove low frequency noise that messes with amplitude measurement of small events
#         st.plot(outfile=out_mag_plot_dir+'wafilt_event'+str(ev_id)+'.'+sta+'.png')

      # Get end time: ts + 3*(ts-tp), if both tp and ts exist for this station
      # Constrain time window to search for maximum amplitude
      if (flag_ts and flag_s_minus_p):
         end_calc_time = t_s + 3.0*s_minus_p
         end_input_time = max(min_end_time, min(end_calc_time, end_time))
      else:
         end_input_time = end_time
#         print("ev_id = ", ev_id, ", sta = ", sta, ", end_input_time = ", end_input_time)

      # Get maximum amplitude at this station
      [mag_amp_term, flag_sta_amp] = get_max_amp_station(st, start_time, end_input_time, min_win_duration)

   return [mag_amp_term, flag_sta_amp]


def calculate_local_magnitude(in_event_file_dir, event_dict, ev_id, order_sta, map_events, inv_map, dist_corr_slope, dist_corr_icpt):
   arr_ML_Richter = []
   median_ML_Richter = 0.0

   # Get event origin time
   ev_origin_time = event_dict[ev_id]['event'][0]

   # Get list of HYPOINVERSE phases for this event ev_id
   phases_list = set().union(event_dict[ev_id]['P'].keys(), event_dict[ev_id]['S'].keys())

   # Loop over stations for this event ev_id
   for ista,sta in enumerate(order_sta):

      # Calculate maximum amplitude at this station
      [mag_amp_term, flag_sta_amp] = calculate_max_amplitude_station(in_event_file_dir, event_dict, ev_id,
         ev_origin_time, sta, phases_list, inv_map)

      # This station will be used in ML calculation
      if (flag_sta_amp):
         ML_Richter = mag_amp_term + dist_corr_slope*map_events[ev_id][4][sta] + dist_corr_icpt[ista] # with distance correction
         arr_ML_Richter.append(ML_Richter)

   if (len(arr_ML_Richter) > 0):
      median_ML_Richter = median(arr_ML_Richter)
   else:
      print("WARNING: arr_ML_Richter is empty for event: ", ev_id)

   return median_ML_Richter

