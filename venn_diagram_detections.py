import matplotlib.pyplot as plt
from matplotlib import rcParams
from matplotlib_venn import venn2, venn2_circles # conda install matplotlib-venn

rcParams.update({'font.size': 14})
#rcParams.update({'font.size': 20})
rcParams['font.sans-serif'] = "Helvetica"
rcParams['font.family'] = "sans-serif"


#catalog_str = 'PRSN ComCat'
#catalt_str = 'EQT-REAL-HYPOINVERSE'
#data_str = 'VELPRSN EQT_20180101_20191228'
#num_missed = 1287
#num_new = 196
#num_match = 932
#out_file = '../EQT_20180101_20220101/Plots/venn_diagram_EQT_20180101_20191228_3REAL_HYPOINVERSE_VELPRSN.pdf'

#catalog_str = 'PRSN ComCat'
#catalt_str = 'EQT-REAL-HYPOINVERSE'
#data_str = 'VELZHANG EQT_20180101_20191228'
#num_missed = 1350
#num_new = 207
#num_match = 965
#out_file = '../EQT_20180101_20220101/Plots/venn_diagram_EQT_20180101_20191228_3REAL_HYPOINVERSE_VELZHANG.pdf'

#catalog_str = 'PRSN ComCat'
#catalt_str = 'EQT-REAL-HYPOINVERSE'
#data_str = 'VELPRSN EQT_20191228_20220101'
#num_missed = 1972
#num_new = 123986
#num_match = 15502
#out_file = '../EQT_20180101_20220101/Plots/venn_diagram_EQT_20191228_20220101_3REAL_HYPOINVERSE_VELPRSN.pdf'

#catalog_str = 'PRSN ComCat'
#catalt_str = 'EQT-REAL-HYPOINVERSE'
#data_str = 'VELZHANG EQT_20191228_20220101'
#num_missed = 1965
#num_new = 124485
#num_match = 15516
#out_file = '../EQT_20180101_20220101/Plots/venn_diagram_EQT_20191228_20220101_3REAL_HYPOINVERSE_VELZHANG.pdf'

#catalog_str = 'PRSN ComCat'
#catalt_str = 'EQT-REAL-HYPOINVERSE'
#data_str = 'VELPRSN EQT_20180101_20220101'
#num_missed = 3259
#num_new = 124182
#num_match = 16434
#out_file = '../EQT_20180101_20220101/Plots/venn_diagram_EQT_20180101_20220101_3REAL_HYPOINVERSE_VELPRSN.pdf'

#catalog_str = 'PRSN ComCat'
#catalt_str = 'EQT-REAL-HYPOINVERSE'
#data_str = 'VELZHANG EQT_20180101_20220101'
#num_missed = 3315
#num_new = 124692
#num_match = 16481
#out_file = '../EQT_20180101_20220101/Plots/venn_diagram_EQT_20180101_20220101_3REAL_HYPOINVERSE_VELZHANG.pdf'

#catalog_str = 'PRSN ComCat'
#catalt_str = 'EQT-REAL-HYPOSVI'
#data_str = 'VELZHANG EQT_20180101_20230101'
#num_missed = 997
#num_new = 299
#num_match = 1212
#out_file = '../EQT_20180101_20230101/Plots/venn_diagram_EQT_20180101_20230101_REAL_VELZHANG_DIRECTHYPOSVI_before.pdf'

catalog_str = 'PRSN ComCat'
catalt_str = 'EQT-REAL-HYPOSVI'
data_str = 'VELZHANG EQT_20180101_20230101'
num_missed = 1608
num_new = 158520
num_match = 20496
out_file = '../EQT_20180101_20230101/Plots/venn_diagram_EQT_20180101_20230101_REAL_VELZHANG_DIRECTHYPOSVI_during.pdf'

#catalog_str = 'PRSN ComCat'
#catalt_str = 'QuakeFlow GAMMA'
#data_str = 'ComCat vs QuakeFlow GAMMA 20180501_20191228'
#num_missed = 1339
#num_new = 199
#num_match = 717
#out_file = '../EQT_20180101_20220101/QuakeFlow/venn_diagram_20180501_20191228_ComCat_vs_QuakeFlow_GAMMA.pdf'

#catalog_str = 'PRSN ComCat'
#catalt_str = 'QuakeFlow HYPODD'
#data_str = 'ComCat vs QuakeFlow HYPODD 20180501_20191228'
#num_missed = 1507
#num_new = 652
#num_match = 529
#out_file = '../EQT_20180101_20220101/QuakeFlow/venn_diagram_20180501_20191228_ComCat_vs_QuakeFlow_HYPODD.pdf'

#catalog_str = 'PRSN ComCat'
#catalt_str = 'QuakeFlow GAMMA'
#data_str = 'ComCat vs QuakeFlow GAMMA 20191228_20211101'
#num_missed = 2617
#num_new = 143507
#num_match = 14547
#out_file = '../EQT_20180101_20220101/QuakeFlow/venn_diagram_20191228_20211101_ComCat_vs_QuakeFlow_GAMMA.pdf'

#catalog_str = 'PRSN ComCat'
#catalt_str = 'QuakeFlow HYPODD'
#data_str = 'ComCat vs QuakeFlow HYPODD 20191228_20211101'
#num_missed = 2802
#num_new = 132581
#num_match = 14356
#out_file = '../EQT_20180101_20220101/QuakeFlow/venn_diagram_20191228_20211101_ComCat_vs_QuakeFlow_HYPODD.pdf'

#catalog_str = 'QuakeFlow_GAMMA'
#catalt_str = 'EQT-REAL-HYPOINVERSE'
#data_str = 'QuakeFlow_GAMMA vs EQT-REAL-HYPOINVERSE 20180501_20191228'
#num_missed = 277
#num_new = 395
#num_match = 607
#out_file = '../EQT_20180101_20220101/QuakeFlow/venn_diagram_20180501_20191228_QuakeFlow_GAMMA_vs_EQT.pdf'

#catalog_str = 'QuakeFlow_HYPODD'
#catalt_str = 'EQT-REAL-HYPOINVERSE'
#data_str = 'QuakeFlow_HYPODD vs EQT-REAL-HYPOINVERSE 20180501_20191228'
#num_missed = 691
#num_new = 527
#num_match = 475
#out_file = '../EQT_20180101_20220101/QuakeFlow/venn_diagram_20180501_20191228_QuakeFlow_HYPODD_vs_EQT.pdf'

#catalog_str = 'QuakeFlow_GAMMA'
#catalt_str = 'EQT-REAL-HYPOINVERSE'
#data_str = 'QuakeFlow_GAMMA vs EQT-REAL-HYPOINVERSE 20191228_20211101'
#num_missed = 47447
#num_new = 27177
#num_match = 110220
#out_file = '../EQT_20180101_20220101/QuakeFlow/venn_diagram_20191228_20211101_QuakeFlow_GAMMA_vs_EQT.pdf'

#catalog_str = 'QuakeFlow_HYPODD'
#catalt_str = 'EQT-REAL-HYPOINVERSE'
#data_str = 'QuakeFlow_HYPODD vs EQT-REAL-HYPOINVERSE 20191228_20211101'
#num_missed = 41273
#num_new = 32166
#num_match = 105231
#out_file = '../EQT_20180101_20220101/QuakeFlow/venn_diagram_20191228_20211101_QuakeFlow_HYPODD_vs_EQT.pdf'




num_catalog = num_match + num_missed
num_catalt = num_match + num_new
num_total = num_match + num_missed + num_new

# Subset sizes
s = (
      float(num_missed)/num_total,  # Ab
      float(num_new)/num_total,  # aB
      float(num_match)/num_total,  # AB
)

v = venn2(subsets=s, set_labels=(catalog_str+':\n '+str(num_catalog)+' events', catalt_str+':\n '+str(num_catalt)+' events'))

# Subset labels
v.get_label_by_id('10').set_text(str(num_missed)+'\n'+catalog_str+'\nonly\nevents')
v.get_label_by_id('01').set_text(str(num_new)+'\n'+catalt_str+'\nonly\nevents')
v.get_label_by_id('11').set_text(str(num_match)+'\noverlapping\nevents')

# Subset colors
v.get_patch_by_id('10').set_color('red')
v.get_patch_by_id('01').set_color('cyan')
v.get_patch_by_id('11').set_color('blue')

# Subset alphas
v.get_patch_by_id('10').set_alpha(0.4)
v.get_patch_by_id('01').set_alpha(1.0)
v.get_patch_by_id('11').set_alpha(0.7)

# Border styles
c = venn2_circles(subsets=s, linestyle='solid')
#c[0].set_ls('dashed')  # Line style
#c[0].set_lw(2.0)       # Line width

plt.title('Total: '+str(num_total)+' earthquakes,\n '+data_str)
plt.savefig(out_file)
