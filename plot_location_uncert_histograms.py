import matplotlib.pyplot as plt
from matplotlib import rcParams
import numpy as np
from scipy import stats
from obspy import UTCDateTime

rcParams.update({'font.size': 24})
#rcParams.update({'font.size': 36}) #poster
###rcParams['pdf.fonttype'] = 42
rcParams['font.sans-serif'] = "Helvetica"
rcParams['font.family'] = "sans-serif"

def print_stats(t_res, label):
   t_mean = np.mean(t_res)
   t_std = np.std(t_res)
   t_median = np.median(t_res)
   t_mad = stats.median_abs_deviation(t_res)
   print(label, ", Min:", min(t_res), ", Max:", max(t_res), ", Mean:", t_mean, ", Std:", t_std,
      ", Median:", t_median, ", MAD:", t_mad)
   return [t_mean, t_std, t_median, t_mad]

###def plot_location_uncert_hist(num_picks, bin_size, x_max, x_label_str, title_str, out_plot_file, text_res='', text_nomatch=''):
def plot_location_uncert_hist(num_picks, mag, bin_size, x_max, x_label_str, title_str, out_plot_file, text_res='', text_nomatch=''):
   plt.figure(num=0, figsize=(10,8))
   plt.clf()
#   n, bins, patches = plt.hist([num_picks], bins=np.arange(-0.5, x_max+1.5, 0.1), color=['yellow'], edgecolor='black', linewidth=0.2)
#   n, bins, patches = plt.hist([num_picks], bins=np.arange(-0.5, x_max+1.5, 0.1), color=['white'], edgecolor='black', linewidth=0.3)
#   n, bins, patches = plt.hist([num_picks], bins=np.arange(-0.5, x_max+1.5, 0.02), color=['white'], edgecolor='black', linewidth=0.3)
###   n, bins, patches = plt.hist([num_picks], bins=np.arange(-0.5, x_max+1.5, bin_size), color=['white'], edgecolor='black', linewidth=0.3)

   num_picks_gt_2 = num_picks[mag > 2.]
   num_picks_1_2 = num_picks[np.where((mag > 1.) & (mag <= 2.))]
   num_picks_lt_1 = num_picks[mag <= 1.]
   n, bins, patches = plt.hist([num_picks_gt_2, num_picks_1_2, num_picks_lt_1], bins=np.arange(-0.5, x_max+1.5, bin_size), label=["M > 2", "1 <= M < 2", "M <= 1"], color=['blue', 'cyan', 'white'], edgecolor='black', linewidth=0.3, stacked=True)

   plt.xlim([0, x_max])
#   plt.ylim([0, 15000])
#   plt.yticks([0, 5000, 10000, 15000])
   plt.ylim([0, 20000])
   plt.yticks([0, 5000, 10000, 15000, 20000])
   plt.xlabel(x_label_str)
   plt.ylabel("Number of events")
   plt.title(title_str)
   plt.legend(prop={'size':20}, loc='center right')
   plt.figtext(0.7, 0.75, text_res, fontsize=20)
#   plt.figtext(0.17, 0.45, text_nomatch, fontsize=18)
   plt.tight_layout()
   plt.savefig(out_plot_file)


#=============================
# Script to plot histograms of location uncertainties from HYPOSVI

##in_hyposvi_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_HYPOSVI_VELPRSN_Catalog_diam.txt'
#in_hyposvi_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_HYPOSVI_VELZHANG_Catalog_diam.txt'
#out_plot_dir = '../EQT_20180101_20220101/Plots/'

in_hyposvi_file = '../Catalog/EQT_20180101_20230101/EQT_20180101_20230101_REAL_VELZHANG_DIRECTHYPOSVI_diam_uncertainty.txt'
out_plot_dir = '../EQT_20180101_20230101/Plots/'

#[delta_x, delta_y, delta_z] = np.loadtxt(in_hyposvi_file, usecols=(6,7,8), unpack=True)
###[delta_x, delta_y, delta_z, delta_t] = np.loadtxt(in_hyposvi_file, usecols=(8,9,10,11), unpack=True)
[mag, delta_x, delta_y, delta_z, delta_t] = np.loadtxt(in_hyposvi_file, usecols=(5,8,9,10,11), unpack=True)
print(len(delta_x), len(delta_y), len(delta_z), len(delta_t))
print(max(delta_x), max(delta_y), max(delta_z), max(delta_t))
[x_mean, x_std, x_median, x_mad] = print_stats(delta_x, " x_res")
[y_mean, y_std, y_median, y_mad] = print_stats(delta_y, " y_res")
[z_mean, z_std, z_median, z_mad] = print_stats(delta_z, " z_res")
[t_mean, t_std, t_median, t_mad] = print_stats(delta_t, " t_res")


out_plot_file = out_plot_dir+'HYPOSVI_DeltaX_histogram_EQT_20180101_20230101.pdf'
text_res = "$\mu$: "+f"{x_mean:.2f}"+" km\n$\sigma$: "+f"{x_std:.2f}"+" km\nmedian: "+f"{x_median:.2f}"+" km\nMAD: "+f"{x_mad:.2f}"+" km"
###plot_location_uncert_hist(delta_x, 0.1, 10, 'east-west location uncertainty (km)', 'HYPOSVI x uncertainty distribution', out_plot_file, text_res)
plot_location_uncert_hist(delta_x, mag, 0.1, 10, 'east-west location uncertainty (km)', 'HYPOSVI x uncertainty distribution', out_plot_file, text_res)

out_plot_file = out_plot_dir+'HYPOSVI_DeltaY_histogram_EQT_20180101_20230101.pdf'
text_res = "$\mu$: "+f"{y_mean:.2f}"+" km\n$\sigma$: "+f"{y_std:.2f}"+" km\nmedian: "+f"{y_median:.2f}"+" km\nMAD: "+f"{y_mad:.2f}"+" km"
###plot_location_uncert_hist(delta_y, 0.1, 10, 'north-south location uncertainty (km)', 'HYPOSVI y uncertainty distribution', out_plot_file, text_res)
plot_location_uncert_hist(delta_y, mag, 0.1, 10, 'north-south location uncertainty (km)', 'HYPOSVI y uncertainty distribution', out_plot_file, text_res)

out_plot_file = out_plot_dir+'HYPOSVI_DeltaZ_histogram_EQT_20180101_20230101.pdf'
text_res = "$\mu$: "+f"{z_mean:.2f}"+" km\n$\sigma$: "+f"{z_std:.2f}"+" km\nmedian: "+f"{z_median:.2f}"+" km\nMAD: "+f"{z_mad:.2f}"+" km"
###plot_location_uncert_hist(delta_z, 0.1, 10, 'depth uncertainty (km)', 'HYPOSVI z uncertainty distribution', out_plot_file, text_res)
plot_location_uncert_hist(delta_z, mag, 0.1, 10, 'depth uncertainty (km)', 'HYPOSVI z uncertainty distribution', out_plot_file, text_res)

out_plot_file = out_plot_dir+'HYPOSVI_DeltaT_histogram_EQT_20180101_20230101.pdf'
text_res = "$\mu$: "+f"{t_mean:.2f}"+" s\n$\sigma$: "+f"{t_std:.2f}"+" s\nmedian: "+f"{t_median:.2f}"+" s\nMAD: "+f"{t_mad:.2f}"+" s"
#plot_location_uncert_hist(delta_t, 0.1, 10, 'origin time uncertainty (s)', 'HYPOSVI t uncertainty distribution', out_plot_file, text_res)
###plot_location_uncert_hist(delta_t, 0.02, 2, 'origin time uncertainty (s)', 'HYPOSVI t uncertainty distribution', out_plot_file, text_res)
plot_location_uncert_hist(delta_t, mag, 0.02, 2, 'origin time uncertainty (s)', 'HYPOSVI t uncertainty distribution', out_plot_file, text_res)

