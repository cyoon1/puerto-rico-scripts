#!/bin/bash

cd /media/yoon/INT01/PuertoRico/EQT_20180101_20230101/downloads_mseeds

# List how many station subdirectories in each directory
for i in 2*; do echo -n "$i "; ls "$i" | wc -l; done > number_of_station_directories.txt

# List how many mseed files in each directory
# https://unix.stackexchange.com/questions/34325/sorting-the-output-of-find-print0-by-piping-to-the-sort-command
find . -maxdepth 2 -mindepth 1 -type d -exec sh -c 'echo "{} : $(find "{}" -type f | wc -l)" file\(s\)' \; | sort > number_of_mseed_files.txt


