import datetime

# Write out relocated QuakeFlow GAMMA event catalogs for plotting in GMT

#catalog_start_time = '2018-01-01T00:00:00'
#in_file = '../Catalog/QuakeFlow/merged_catalog.csv'
#out_file = '../Catalog/QuakeFlow/20180501_20211101_QuakeFlow_GAMMA_Catalog.txt'

#catalog_start_time = '2018-01-01T00:00:00'
#in_file = '../Catalog/QuakeFlow/events_MATCH_magcat_QuakeFlow_GAMMA_20180501_20211101.txt'
#out_file = '../Catalog/QuakeFlow/QuakeFlow_GAMMA_20180501_20211101_events_MATCH_magcat.txt'

#catalog_start_time = '2018-01-01T00:00:00'
#in_file = '../Catalog/QuakeFlow/events_NEW_magcat_QuakeFlow_GAMMA_20180501_20211101.txt'
#out_file = '../Catalog/QuakeFlow/QuakeFlow_GAMMA_20180501_20211101_events_NEW_magcat.txt'

#catalog_start_time = '2018-01-01T00:00:00'
#in_file = '../Catalog/QuakeFlow/us_events_MATCH_magcat_QuakeFlow_GAMMA_20180501_20211101.txt'
#out_file = '../Catalog/QuakeFlow/QuakeFlow_GAMMA_20180501_20211101_us_events_MATCH_magcat.txt'

catalog_start_time = '2018-01-01T00:00:00'
in_file = '../Catalog/QuakeFlow/us_events_NEW_magcat_QuakeFlow_GAMMA_20180501_20211101.txt'
out_file = '../Catalog/QuakeFlow/QuakeFlow_GAMMA_20180501_20211101_us_events_NEW_magcat.txt'

catalog_ref_time = datetime.datetime.strptime(catalog_start_time, '%Y-%m-%dT%H:%M:%S')
fout = open(out_file, 'w')
with open(in_file, 'r') as fin:
   for line in fin:
      if line.startswith('time'): # skip first line
         continue
      split_line = line.split()
      origin_time = datetime.datetime.strptime(split_line[0], '%Y-%m-%dT%H:%M:%S.%f')
      num_sec = (origin_time - catalog_ref_time).total_seconds()
      lat_deg = split_line[3]
      lon_deg = split_line[2]
      depth_km = 0.001*float(split_line[4])
      mag = split_line[1]
      evid = split_line[7]
      cov = split_line[5]
      sigma = float(cov.split(',')[0]) # residual arrival time (sec)
      if (sigma < 0.5):
         fout.write(("%15.5f %s %s %6.4f %s %s\n") % (num_sec, lat_deg, lon_deg, depth_km, mag, evid))
fout.close()
