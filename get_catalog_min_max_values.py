import utils_hypoinverse as utils_hyp

#in_match_catalog_file = '../LargeAreaEQTransformer/REAL/HYPOINVERSE/events_MATCH_eqt_20200107_20200114.txt'
#in_hinv_sum_file = '../LargeAreaEQTransformer/REAL/HYPOINVERSE/merged_locate_pr.sum'
#in_match_catalog_file = '../LargeAreaEQTransformer/REAL/Events/events_MATCH_eqt_20200107_20200114.txt'
#in_hinv_sum_file = '../LargeAreaEQTransformer/REAL/Events/merged_real_locate_pr.sum'
#in_match_catalog_file = '../LargeAreaEQTransformer/REAL/Events/events_MATCH_eqt_20200107_20200114.txt'
#in_hinv_sum_file = '../LargeAreaEQTransformer/REAL/Events/merged_real_locate_pr.sum'
#in_match_catalog_file = '../LargeAreaEQTransformer/PhaseLink/events_MATCH_eqt_20200107_20200114.txt'
#in_hinv_sum_file = '../LargeAreaEQTransformer/PhaseLink/merged_pl_locate_pr.sum'

#in_match_catalog_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/REAL/HYPOINVERSE/events_MATCH_eqt_2018_2020.txt'
#in_hinv_sum_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/REAL/HYPOINVERSE/merged_locate_pr.sum'
#in_match_catalog_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/REAL/HYPOINVERSE/events_MATCH_eqt_2018_2020.txt'
#in_match_catalog_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/REAL/HYPOINVERSE/events_MATCH_eqt_20180101_20210201.txt'
#in_hinv_sum_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/REAL/HYPOINVERSE/merged_real_locate_pr.sum'
#in_match_catalog_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/PhaseLink/events_MATCH_eqt_20180101_20210201.txt'
#in_hinv_sum_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/PhaseLink/merged_pl_locate_pr.sum'

#in_match_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELPRSN/events_MATCH_EQT_20200107_20200114.txt'
#in_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELPRSN/merged_real_locate_pr.sum'
#in_match_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/events_MATCH_EQT_20200107_20200114.txt'
#in_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/merged_real_locate_pr.sum'

#in_match_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELPRSN/events_MATCH_EQT_20191228_20200114.txt'
#in_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELPRSN/merged_real_locate_pr.sum'
#in_match_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELZHANG/events_MATCH_EQT_20191228_20200114.txt'
#in_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELZHANG/merged_real_locate_pr.sum'

#in_match_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELPRSN/events_MATCH_EQT_20180101_20210601.txt'
#in_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELPRSN/merged_real_locate_pr.sum'
#in_match_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/events_MATCH_EQT_20180101_20210601.txt'
#in_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/merged_real_locate_pr.sum'

#in_match_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/events_MATCH_EQT_20180101_20211001.txt'
#in_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/merged_real_locate_pr.sum'

#in_match_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/events_MATCH_magcat_EQT_20180101_20220101.txt'
#in_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/merged_real_magcat_locate_pr.sum'
in_match_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/events_MATCH_magcat_EQT_20180101_20220101.txt'
in_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/merged_real_magcat_locate_pr.sum'

min_lat = 17.6
max_lat = 18.3
min_lon = -67.3
max_lon = -66.4

count_in_bounds = 0
count_cat = 0
ev_id_all = []
cat_lat = []
cat_lon = []
cat_depth = []
cat_mag = []
with open(in_match_catalog_file, 'r') as fev:
   for line in fev:
      ev_id = int(utils_hyp.get_event_id_hypoinverse_file(line)) # eqt ev_id
      ev_id_all.append(ev_id)
      cat_line = line[180:].split()
      cat_ev_lat = float(cat_line[1])
      cat_ev_lon = float(cat_line[2])
      cat_lat.append(cat_ev_lat)
      cat_lon.append(cat_ev_lon)
      cat_depth.append(float(cat_line[3]))
      cat_mag.append(float(cat_line[4]))
      count_cat += 1
      if ((cat_ev_lat >= min_lat) and (cat_ev_lat <= max_lat) and (cat_ev_lon >= min_lon) and (cat_ev_lon <= max_lon)):
         count_in_bounds += 1

num_ev_in_bounds = 0
num_ev = 0
with open(in_hinv_sum_file, 'r') as fin:
   for line in fin:
      [ev_lat, ev_lon, depth_km] = utils_hyp.get_lat_lon_depth_hypoinverse_file(line)
      num_ev += 1
      if ((ev_lat >= min_lat) and (ev_lat <= max_lat) and (ev_lon >= min_lon) and (ev_lon <= max_lon)):
         num_ev_in_bounds += 1

print("Minimum catalog event latitude = ", min(cat_lat))
print("Maximum catalog event latitude = ", max(cat_lat))
print("Minimum catalog event longitude = ", min(cat_lon))
print("Maximum catalog event longitude = ", max(cat_lon))
print("Minimum catalog event depth = ", min(cat_depth))
print("Maximum catalog event depth = ", max(cat_depth))
print("Minimum catalog event magnitude = ", min(cat_mag))
print("Maximum catalog event magnitude = ", max(cat_mag))
print("Number of catalog events in bounds = ", count_in_bounds, " out of ", count_cat)
print("Number of EQTransformer events in bounds = ", num_ev_in_bounds, " out of ", num_ev)

