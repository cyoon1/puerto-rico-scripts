from obspy.core.event import read_events
import glob
import os
import numpy as np

#https://docs.obspy.org/packages/autogen/obspy.core.event.read_events.html#obspy.core.event.read_events

def get_exponent(in_number):
   out_exp = int(np.floor(np.log10(np.abs(in_number))))
   return out_exp


# Read events from QuakeML files named with event ID (manual download from USGS event webpage)
# This has focal mechanism/moment tensor
# Convert to format expected for GMT 6.4.0 meca to plot moment tensors with -Sm
#out_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_pr_temp_moment_tensor_display.txt'
#in_ev_files = sorted(glob.glob('../EQT_20180101_20220101/CatalogEventsMT/*.xml'))
out_file = '../Catalog/EQT_20180101_20230101/EQT_20180101_20230101_pr_temp_moment_tensor_display.txt'
in_ev_files = sorted(glob.glob('../EQT_20180101_20230101/CatalogEventsMT/*.xml'))

print(len(in_ev_files))
fout = open(out_file, 'w')
for infile in in_ev_files:
   ev_id = os.path.basename(infile).replace('_quakeml.xml','')
   print(infile)
   print(ev_id)

   cat = read_events(infile, format='QUAKEML')
   for ev in cat:
      if (len(ev.focal_mechanisms) > 0):
         m_tensor = ev.focal_mechanisms[0].moment_tensor.tensor 
         print(m_tensor)
         m_exp = get_exponent(m_tensor.m_tt)
         m_exp_val = 10.**m_exp
         print(m_exp_val)
         m_rr = m_tensor.m_rr/m_exp_val
         m_tt = m_tensor.m_tt/m_exp_val
         m_pp = m_tensor.m_pp/m_exp_val
         m_rt = m_tensor.m_rt/m_exp_val
         m_rp = m_tensor.m_rp/m_exp_val
         m_tp = m_tensor.m_tp/m_exp_val
         m_exp += 7 # convert from N-m to dyn-cm
         print(m_rr, m_tt, m_pp, m_rt, m_rp, m_tp, m_exp, ev_id)
         fout.write('%5.4f %5.4f %5.4f %5.4f %5.4f %5.4f %02d %s\n' % (m_rr, m_tt, m_pp, m_rt, m_rp, m_tp, m_exp, ev_id))
fout.close()
