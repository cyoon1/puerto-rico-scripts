import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rcParams

rcParams.update({'font.size': 24})
#rcParams.update({'font.size': 36}) #poster
###rcParams['pdf.fonttype'] = 42
rcParams['font.sans-serif'] = "Helvetica"
rcParams['font.family'] = "sans-serif"

# Adapted from
# https://matplotlib.org/stable/gallery/lines_bars_and_markers/horizontal_barchart_distribution.html

out_dir = '../EQT_20180101_20230101/Plots/'
out_file = out_dir+'breakdown_barchart_picks_stations.pdf'
category_names = ['0', '1', '2', '3', '4', '5+ stations']
results = {
    '2605 total\n missed events': [164,411,938,634,258,200]
}


def survey(results, category_names):
   """
   Parameters
   ----------
   results : dict
     A mapping from question labels to a list of answers per category.
     It is assumed all lists contain the same number of entries and that
     it matches the length of *category_names*.
   category_names : list of str
     The category labels.
   """
   labels = list(results.keys())
   data = np.array(list(results.values()))
   data_cum = data.cumsum(axis=1)
   category_colors = plt.colormaps['RdYlGn'](
     np.linspace(0.15, 0.85, data.shape[1]))

   fig, ax = plt.subplots(figsize=(20, 2))
   ax.invert_yaxis()
   ax.xaxis.set_visible(False)
   ax.set_xlim(0, np.sum(data, axis=1).max())

   for i, (colname, color) in enumerate(zip(category_names, category_colors)):
      widths = data[:, i]
      starts = data_cum[:, i] - widths
      rects = ax.barh(labels, widths, left=starts, height=0.3,
                     label=colname, color=color, alpha=0.8)

      r, g, b, _ = color
      text_color = 'white' if r * g * b < 0.5 else 'darkgrey'
      ax.bar_label(rects, label_type='center', color=text_color)

   ax.legend(ncol=len(category_names), bbox_to_anchor=(0.5, -0.1),
           loc='lower center', fontsize='small')

   return fig, ax


survey(results, category_names)
plt.savefig(out_file)
