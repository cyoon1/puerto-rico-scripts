import matplotlib.pyplot as plt
import numpy as np
from matplotlib import rcParams

rcParams.update({'font.size': 24})
#rcParams.update({'font.size': 36}) #poster
###rcParams['pdf.fonttype'] = 42
rcParams['font.sans-serif'] = "Helvetica"
rcParams['font.family'] = "sans-serif"



#out_dir = '../EQT_20180101_20220101/Plots/'
#out_base_str = out_dir+'comparison_stats_EQT_20180101_20220101_'

out_dir = '../EQT_20180101_20230101/Plots/'
out_base_str = out_dir+'comparison_stats_EQT_20180101_20230101_'

## Binned by epicentral distance ------------------
#bin_labels = ['0-20 km', '20-40 km', '40-60 km', '60-80 km', '80-100 km', '100-250 km']
#out_bin_str = 'bin_epidist'
#x_label = 'Epicentral distance bin range (km)'
#
### VELPRSN
##out_vel_str = 'VELPRSN_'
##t_mean_p = [0.12, 0.11, 0.12, 0.12, 0.20, 0.15] # mean pick time residual (s), P
##t_median_p = [0.07, 0.08, 0.08, 0.08, 0.14, 0.08] # median pick time residual (s), P
##t_mean_s = [0.03, 0.00, -0.02, -0.08, -0.08, -0.29] # mean pick time residual (s), S
##t_median_s = [0.04, 0.03, 0.03, 0.00, 0.01, -0.12] # median pick time residual (s), S
##t_std_p = [0.28, 0.38, 0.74, 0.97, 0.66, 1.83] # std pick time residual (s), P
##t_mad_p = [0.05, 0.06, 0.06, 0.08, 0.12, 0.10] # mad pick time residual (s), P
##t_std_s = [0.23, 0.30, 0.35, 0.46, 0.61, 0.58] # std pick time residual (s), S
##t_mad_s = [0.08, 0.11, 0.12, 0.19, 0.25, 0.24] # mad pick time residual (s), S
##recall_p = [0.92, 0.89, 0.81, 0.72, 0.70, 0.33] # recall, P
##recall_s = [0.78, 0.78, 0.70, 0.64, 0.58, 0.25] # recall, S
##npr_p = [1.29, 1.33, 1.22, 1.48, 1.42, 1.28] # new pick ratio, P
##npr_s = [1.31, 1.36, 1.28, 1.61, 1.58, 1.45] # new pick ratio, S
##t_overall_mean_p = np.full(6, 0.12)
##t_overall_median_p = np.full(6, 0.08)
##t_overall_mean_s = np.full(6, -0.01)
##t_overall_median_s = np.full(6, 0.03)
##t_overall_std_p = np.full(6, 0.61)
##t_overall_mad_p = np.full(6, 0.06)
##t_overall_std_s = np.full(6, 0.35)
##t_overall_mad_s = np.full(6, 0.11)
##t_overall_recall_p = np.full(6, 0.82)
##t_overall_recall_s = np.full(6, 0.71)
##t_overall_npr_p = np.full(6, 1.32)
##t_overall_npr_s = np.full(6, 1.38)
### VELPRSN
#
### VELZHANG
##out_vel_str = 'VELZHANG_'
##t_mean_p = [0.12, 0.11, 0.12, 0.12, 0.19, 0.09] # mean pick time residual (s), P
##t_median_p = [0.07, 0.08, 0.08, 0.08, 0.14, 0.07] # median pick time residual (s), P
##t_mean_s = [0.04, 0.00, -0.02, -0.07, -0.07, -0.33] # mean pick time residual (s), S
##t_median_s = [0.04, 0.03, 0.03, 0.01, 0.01, -0.14] # median pick time residual (s), S
##t_std_p = [0.29, 0.38, 0.74, 0.97, 0.67, 1.46] # std pick time residual (s), P
##t_mad_p = [0.05, 0.06, 0.06, 0.08, 0.12, 0.10] # mad pick time residual (s), P
##t_std_s = [0.24, 0.30, 0.35, 0.47, 0.61, 0.65] # std pick time residual (s), S
##t_mad_s = [0.08, 0.11, 0.12, 0.18, 0.25, 0.27] # mad pick time residual (s), S
##recall_p = [0.93, 0.89, 0.81, 0.72, 0.73, 0.55] # recall, P
##recall_s = [0.82, 0.80, 0.71, 0.63, 0.60, 0.37] # recall, S
##npr_p = [1.29, 1.33, 1.22, 1.48, 1.42, 1.26] # new pick ratio, P
##npr_s = [1.31, 1.36, 1.28, 1.62, 1.54, 1.41] # new pick ratio, S
##t_overall_mean_p = np.full(6, 0.12)
##t_overall_median_p = np.full(6, 0.08)
##t_overall_mean_s = np.full(6, -0.01)
##t_overall_median_s = np.full(6, 0.03)
##t_overall_std_p = np.full(6, 0.61)
##t_overall_mad_p = np.full(6, 0.06)
##t_overall_std_s = np.full(6, 0.36)
##t_overall_mad_s = np.full(6, 0.11)
##t_overall_recall_p = np.full(6, 0.83)
##t_overall_recall_s = np.full(6, 0.73)
##t_overall_npr_p = np.full(6, 1.32)
##t_overall_npr_s = np.full(6, 1.38)
### VELZHANG
#
## VELZHANG EQT_20180101_20230101
#out_vel_str = 'VELZHANG_'
#t_mean_p = [0.10, 0.11, 0.13, 0.13, 0.20, 0.11] # mean pick time residual (s), P
#t_median_p = [0.06, 0.08, 0.08, 0.09, 0.14, 0.08] # median pick time residual (s), P
#t_mean_s = [0.03, 0.00, -0.01, -0.06, -0.07, -0.35] # mean pick time residual (s), S
#t_median_s = [0.09, 0.02, 0.02, 0.00, 0.00, -0.16] # median pick time residual (s), S
#t_std_p = [0.35, 0.44, 0.74, 0.96, 0.68, 1.33] # std pick time residual (s), P
#t_mad_p = [0.05, 0.06, 0.06, 0.08, 0.12, 0.10] # mad pick time residual (s), P
#t_std_s = [0.37, 0.44, 0.51, 0.55, 0.70, 0.75] # std pick time residual (s), S
#t_mad_s = [0.09, 0.12, 0.13, 0.20, 0.27, 0.30] # mad pick time residual (s), S
#recall_p = [0.92, 0.88, 0.82, 0.70, 0.72, 0.46] # recall, P
#recall_s = [0.86, 0.80, 0.67, 0.53, 0.51, 0.27] # recall, S
#npr_p = [1.28, 1.34, 1.27, 1.53, 1.46, 1.34] # new pick ratio, P
#npr_s = [1.30, 1.36, 1.31, 1.67, 1.50, 1.52] # new pick ratio, S
#t_overall_mean_p = np.full(6, 0.12)
#t_overall_median_p = np.full(6, 0.08)
#t_overall_mean_s = np.full(6, -0.01)
#t_overall_median_s = np.full(6, 0.03)
#t_overall_std_p = np.full(6, 0.60)
#t_overall_mad_p = np.full(6, 0.06)
#t_overall_std_s = np.full(6, 0.47)
#t_overall_mad_s = np.full(6, 0.12)
#t_overall_recall_p = np.full(6, 0.83)
#t_overall_recall_s = np.full(6, 0.72)
#t_overall_npr_p = np.full(6, 1.34)
#t_overall_npr_s = np.full(6, 1.38)
## VELZHANG EQT_20180101_20230101
## Binned by epicentral distance ------------------

# Binned by magnitude ------------------
bin_labels = ['-1<M<1', '1<M<2', '2<M<3', '3<M<4', 'M>4']
out_bin_str = 'bin_mag'
x_label = 'Magnitude bin range'

## VELPRSN
#out_vel_str = 'VELPRSN_'
#t_mean_p = [0.06, 0.10, 0.14, 0.15, 0.15] # mean pick time residual (s), P
#t_median_p = [0.06, 0.08, 0.08, 0.07, 0.10] # median pick time residual (s), P
#t_mean_s = [0.03, 0.00, -0.03, -0.03, -0.08] # mean pick time residual (s), S
#t_median_s = [0.04, 0.04, 0.02, 0.01, -0.02] # median pick time residual (s), S
#t_std_p = [0.22, 0.30, 0.77, 0.91, 0.25] # std pick time residual (s), P
#t_mad_p = [0.05, 0.06, 0.07, 0.07, 0.10] # mad pick time residual (s), P
#t_std_s = [0.21, 0.32, 0.39, 0.41, 0.41] # std pick time residual (s), S
#t_mad_s = [0.07, 0.10, 0.13, 0.13, 0.14] # mad pick time residual (s), S
#recall_p = [0.84, 0.79, 0.83, 0.85, 0.86] # recall, P
#recall_s = [0.79, 0.70, 0.71, 0.71, 0.70] # recall, S
#npr_p = [1.08, 1.21, 1.41, 1.49, 1.57] # new pick ratio, P
#npr_s = [1.08, 1.22, 1.51, 1.70, 1.84] # new pick ratio, S
#t_overall_mean_p = np.full(5, 0.12)
#t_overall_median_p = np.full(5, 0.08)
#t_overall_mean_s = np.full(5, -0.01)
#t_overall_median_s = np.full(5, 0.03)
#t_overall_std_p = np.full(5, 0.61)
#t_overall_mad_p = np.full(5, 0.06)
#t_overall_std_s = np.full(5, 0.35)
#t_overall_mad_s = np.full(5, 0.11)
#t_overall_recall_p = np.full(5, 0.82)
#t_overall_recall_s = np.full(5, 0.71)
#t_overall_npr_p = np.full(5, 1.32)
#t_overall_npr_s = np.full(5, 1.38)
## VELPRSN

## VELZHANG
#out_vel_str = 'VELZHANG_'
#t_mean_p = [0.06, 0.10, 0.14, 0.16, 0.14] # mean pick time residual (s), P
#t_median_p = [0.06, 0.08, 0.08, 0.07, 0.10] # median pick time residual (s), P
#t_mean_s = [0.03, 0.00, -0.03, -0.02, -0.08] # mean pick time residual (s), S
#t_median_s = [0.04, 0.04, 0.02, 0.01, -0.02] # median pick time residual (s), S
#t_std_p = [0.20, 0.30, 0.77, 0.93, 0.30] # std pick time residual (s), P
#t_mad_p = [0.05, 0.06, 0.07, 0.07, 0.08] # mad pick time residual (s), P
#t_std_s = [0.23, 0.32, 0.40, 0.41, 0.39] # std pick time residual (s), S
#t_mad_s = [0.08, 0.10, 0.13, 0.13, 0.14] # mad pick time residual (s), S
#recall_p = [0.84, 0.80, 0.86, 0.88, 0.87] # recall, P
#recall_s = [0.79, 0.72, 0.74, 0.73, 0.71] # recall, S
#npr_p = [1.08, 1.22, 1.40, 1.49, 1.55] # new pick ratio, P
#npr_s = [1.08, 1.23, 1.50, 1.70, 1.84] # new pick ratio, S
#t_overall_mean_p = np.full(5, 0.12)
#t_overall_median_p = np.full(5, 0.08)
#t_overall_mean_s = np.full(5, -0.01)
#t_overall_median_s = np.full(5, 0.03)
#t_overall_std_p = np.full(5, 0.61)
#t_overall_mad_p = np.full(5, 0.06)
#t_overall_std_s = np.full(5, 0.36)
#t_overall_mad_s = np.full(5, 0.11)
#t_overall_recall_p = np.full(5, 0.83)
#t_overall_recall_s = np.full(5, 0.73)
#t_overall_npr_p = np.full(5, 1.32)
#t_overall_npr_s = np.full(5, 1.38)
## VELZHANG

# VELZHANG EQT_20180101_20230101
out_vel_str = 'VELZHANG_'
t_mean_p = [0.06, 0.10, 0.14, 0.17, 0.16] # mean pick time residual (s), P
t_median_p = [0.04, 0.07, 0.08, 0.08, 0.10] # median pick time residual (s), P
t_mean_s = [0.03, 0.00, -0.02, -0.01, -0.10] # mean pick time residual (s), S
t_median_s = [0.04, 0.03, 0.02, 0.01, -0.03] # median pick time residual (s), S
t_std_p = [0.40, 0.36, 0.75, 0.97, 0.32] # std pick time residual (s), P
t_mad_p = [0.05, 0.06, 0.07, 0.07, 0.08] # mad pick time residual (s), P
t_std_s = [0.47, 0.43, 0.49, 0.59, 0.56] # std pick time residual (s), S
t_mad_s = [0.08, 0.11, 0.14, 0.14, 0.15] # mad pick time residual (s), S
recall_p = [0.87, 0.82, 0.83, 0.85, 0.84] # recall, P
recall_s = [0.84, 0.74, 0.69, 0.69, 0.68] # recall, S
npr_p = [1.17, 1.26, 1.42, 1.52, 1.58] # new pick ratio, P
npr_s = [1.17, 1.27, 1.50, 1.70, 1.88] # new pick ratio, S
t_overall_mean_p = np.full(5, 0.12)
t_overall_median_p = np.full(5, 0.08)
t_overall_mean_s = np.full(5, -0.01)
t_overall_median_s = np.full(5, 0.03)
t_overall_std_p = np.full(5, 0.60)
t_overall_mad_p = np.full(5, 0.06)
t_overall_std_s = np.full(5, 0.47)
t_overall_mad_s = np.full(5, 0.12)
t_overall_recall_p = np.full(5, 0.83)
t_overall_recall_s = np.full(5, 0.72)
t_overall_npr_p = np.full(5, 1.34)
t_overall_npr_s = np.full(5, 1.38)
# VELZHANG EQT_20180101_20230101
# Binned by magnitude ------------------

line_zero = np.zeros(len(bin_labels))
line_one = np.ones(len(bin_labels))
x_val = np.arange(len(bin_labels))

# Plot pick time residual - mean
plt.figure(num=1, figsize=(10,8))
plt.clf()
plt.plot(t_mean_p, 'bo-', label='P, binned mean')
plt.plot(t_overall_mean_p, 'b--', label='P, overall mean')
plt.plot(t_mean_s, 'ro-', label='S, binned mean')
plt.plot(t_overall_mean_s, 'r--', label='S, overall mean')
plt.plot(line_zero, 'k--')
plt.xticks(x_val, bin_labels, rotation=-30)
plt.xlim(x_val[0], x_val[-1])
plt.ylim(-0.4, 0.4)
plt.legend(loc='lower left', prop={'size':20})
plt.xlabel(x_label)
plt.ylabel("Pick time residual (s): $t_{cat} - t_{EQT}$")
plt.tight_layout()
plt.savefig(out_base_str+out_vel_str+out_bin_str+'_picktimeresidual_mean.pdf')

# Plot pick time residual - median
plt.figure(num=2, figsize=(10,8))
plt.clf()
plt.plot(t_median_p, 'bo-', label='P, binned median')
plt.plot(t_overall_median_p, 'b--', label='P, overall median')
plt.plot(t_median_s, 'ro-', label='S, binned median')
plt.plot(t_overall_median_s, 'r--', label='S, overall median')
plt.plot(line_zero, 'k--')
plt.xticks(x_val, bin_labels, rotation=-30)
plt.xlim(x_val[0], x_val[-1])
plt.ylim(-0.4, 0.4)
plt.legend(loc='lower left', prop={'size':20})
plt.xlabel(x_label)
plt.ylabel("Pick time residual (s): $t_{cat} - t_{EQT}$")
plt.tight_layout()
plt.savefig(out_base_str+out_vel_str+out_bin_str+'_picktimeresidual_median.pdf')

# Plot pick time residual - std
plt.figure(num=3, figsize=(10,8))
plt.clf()
plt.plot(t_std_p, 'bo-', label='P, binned std')
plt.plot(t_overall_std_p, 'b--', label='P, overall std')
plt.plot(t_std_s, 'ro-', label='S, binned std')
plt.plot(t_overall_std_s, 'r--', label='S, overall std')
plt.xticks(x_val, bin_labels, rotation=-30)
plt.yticks([0, 0.5, 1, 1.5])
plt.xlim(x_val[0], x_val[-1])
plt.ylim(0, 1.5)
plt.legend(loc='upper left', prop={'size':20})
plt.xlabel(x_label)
plt.ylabel("Pick time residual (s): $t_{cat} - t_{EQT}$")
plt.tight_layout()
plt.savefig(out_base_str+out_vel_str+out_bin_str+'_picktimeresidual_std.pdf')

# Plot pick time residual - mad
plt.figure(num=4, figsize=(10,8))
plt.clf()
plt.plot(t_mad_p, 'bo-', label='P, binned mad')
plt.plot(t_overall_mad_p, 'b--', label='P, overall mad')
plt.plot(t_mad_s, 'ro-', label='S, binned mad')
plt.plot(t_overall_mad_s, 'r--', label='S, overall mad')
plt.xticks(x_val, bin_labels, rotation=-30)
plt.yticks([0, 0.5, 1, 1.5])
plt.xlim(x_val[0], x_val[-1])
plt.ylim(0, 1.5)
plt.legend(loc='upper left', prop={'size':20})
plt.xlabel(x_label)
plt.ylabel("Pick time residual (s): $t_{cat} - t_{EQT}$")
plt.tight_layout()
plt.savefig(out_base_str+out_vel_str+out_bin_str+'_picktimeresidual_mad.pdf')

# Plot recall
plt.figure(num=5, figsize=(10,8))
plt.clf()
plt.plot(recall_p, 'bo-', label='P, binned recall')
plt.plot(t_overall_recall_p, 'b--', label='P, overall recall')
plt.plot(recall_s, 'ro-', label='S, binned recall')
plt.plot(t_overall_recall_s, 'r--', label='S, overall recall')
plt.xticks(x_val, bin_labels, rotation=-30)
plt.xlim(x_val[0], x_val[-1])
plt.ylim(0, 1)
plt.legend(loc='lower left', prop={'size':20})
plt.xlabel(x_label)
plt.ylabel("Recall (fraction of catalog picks\n found by EQT)")
plt.tight_layout()
plt.savefig(out_base_str+out_vel_str+out_bin_str+'_recall.pdf')

# Plot new pick ratio
plt.figure(num=6, figsize=(10,8))
plt.clf()
plt.plot(npr_p, 'bo-', label='P, binned new pick ratio')
plt.plot(t_overall_npr_p, 'b--', label='P, overall new pick ratio')
plt.plot(npr_s, 'ro-', label='S, binned new pick ratio')
plt.plot(t_overall_npr_s, 'r--', label='S, overall new pick ratio')
plt.plot(line_one, 'k--')
plt.xticks(x_val, bin_labels, rotation=-30)
plt.xlim(x_val[0], x_val[-1])
plt.ylim(0, 2)
plt.legend(loc='lower left', prop={'size':20})
plt.xlabel(x_label)
plt.ylabel("New pick ratio (number of EQT picks\n divided by number of catalog picks)")
plt.tight_layout()
plt.savefig(out_base_str+out_vel_str+out_bin_str+'_npr.pdf')
