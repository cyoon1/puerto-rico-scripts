from EQTransformer.utils.downloader import makeStationList
from EQTransformer.utils.downloader import downloadMseeds
from EQTransformer.utils.hdf5_maker import preprocessor
from EQTransformer.core.predictor import predictor
from EQTransformer.core.mseed_predictor import mseed_predictor
from EQTransformer.utils.plot import plot_data_chart
from EQTransformer.utils.plot import plot_detections, plot_helicorder
from EQTransformer.utils.associator import run_associator
import os
import json
import time
import datetime

# Puerto Rico data
clientlist=["IRIS"]
minlat=17
maxlat=19
minlon=-68
maxlon=-65
#tstart="2018-01-01 00:00:00.00"
#tend="2020-11-01 00:00:00.00"
tstart="2020-01-07 00:00:00.00"
tend="2020-01-09 00:00:00.00"
#tend="2020-01-14 00:00:00.00"
#out_data_dir="../Data/downloads_mseeds/"
out_data_dir="downloads_mseeds/"
model_path="/home/yoon/Documents/EQTransformer/ModelsAndSampleData/"

# Get list of stations
makeStationList(client_list=clientlist,
   min_lat=minlat, max_lat=maxlat, min_lon=minlon, max_lon=maxlon,
   start_time=tstart, end_time=tend, 
#   channel_list=["HH[ZNE]", "HH[Z21]", "BH[ZNE]"], 
   channel_list=["HH[ZNE]", "HH[Z21]", "BH[ZNE]", "EH[ZNE]", "HN[ZNE]", "HN[Z21]"], 
   filter_network=["SY"], filter_station=[])
#
## Download MiniSEED continuous waveform data
#downloadMseeds(client_list=clientlist, stations_json='station_list.json', 
#   output_dir=out_data_dir,
#   min_lat=minlat, max_lat=maxlat, min_lon=minlon, max_lon=maxlon,
#   start_time=tstart, end_time=tend, 
#   chunk_size=1, channel_list=[], n_processor=8)

## Preprocess MiniSEED --> 1-min-long numpy in hdf
#preprocessor(mseed_dir=out_data_dir, stations_json='station_list.json', overlap=0.3, n_processor=8)

## Run the predictor on hdf data
#predictor(input_dir=out_data_dir+'_processed_hdfs', input_model=model_path+'EqT_model.h5', output_dir='detections_hdf',
#   detection_threshold=0.3, P_threshold=0.1, S_threshold=0.1, number_of_plots=100, plot_mode='time')

## Run the predictor directly on MiniSEED data
#mseed_predictor(input_dir=out_data_dir, input_model=model_path+'EqT_model.h5', stations_json='station_list.json',
#   output_dir='mseed_detections', detection_threshold=0.3, P_threshold=0.1, S_threshold=0.1,
#   number_of_plots=500, plot_mode='time_frequency', overlap=0.3, batch_size=500)

## Plot seismic data availability
#plot_data_chart('time_tracks.pkl', time_interval=12)
#
## Plot detected events on helicorder plots
#delta = datetime.timedelta(days=1)
#json_file = open('station_list_edited.json')
#stations_ = json.load(json_file)
#input_dir_mseed = 'downloads_mseeds/'
#for sta,val in stations_.items():
#   input_dir_sta_mseed = input_dir_mseed+sta+'/'
#   input_dir_sta_det_file = 'mseed_detections/'+sta+'_outputs/X_prediction_results.csv'
#   print(input_dir_sta_det_file)
##   if (sta == 'PR03'):
##      continue
#   for chan in val['channels']:
#      print(chan)
##      input_dir_sta_chan_mseed = input_dir_sta_mseed+val['network']+'.'+sta+'.00.'+chan+'__'
#      input_dir_sta_chan_mseed = input_dir_sta_mseed+val['network']+'.'+sta+'..'+chan+'__'
#      print(input_dir_sta_chan_mseed)
#      start_date = datetime.datetime.strptime(tstart, "%Y-%m-%d %H:%M:%S.%f")
#      end_date = datetime.datetime.strptime(tend, "%Y-%m-%d %H:%M:%S.%f")
#      while (start_date <= end_date):
#         next_date = start_date + delta
#         input_file_mseed = input_dir_sta_chan_mseed+datetime.datetime.strftime(start_date, "%Y%m%dT%H%M%SZ")+'__'+datetime.datetime.strftime(next_date, "%Y%m%dT%H%M%SZ")+'.mseed'
#         if (os.path.isfile(input_file_mseed)):
#            plot_helicorder(input_mseed=input_file_mseed, input_csv=input_dir_sta_det_file, save_plot=True)
#         else:
#            print(input_file_mseed, " not found")
#         if (next_date == end_date):
#            break
#         start_date = next_date

## Plot detections map at different stations
#plot_detections(input_dir="mseed_detections", input_json="station_list.json", plot_type='station_map', marker_size=50)
#
## Plot detection histogram for each station
#plot_detections(input_dir="mseed_detections", input_json="station_list.json", plot_type='hist', time_window=720)

## Associate detected events from different stations
#os.system("mkdir association")
#run_associator(input_dir='mseed_detections', start_time=tstart, end_time=tend,  moving_window=15, pair_n=3, output_dir="association")

