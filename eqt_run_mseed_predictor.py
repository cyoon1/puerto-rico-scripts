from EQTransformer.core.mseed_predictor import mseed_predictor
import sys

# eqt_run_mseed_predictor.py: script to run EQTransformer detector/picker on MiniSEED waveform data
# --> To run in parallel: called by eqt_parallel_run_mseed_predictor.py

if len(sys.argv) != 3:
   print("Usage: python eqt_run_mseed_predictor.py <in_mseed_dir> <out_det_dir>")
   print("Usage Example: python eqt_run_mseed_predictor.py /media/yoon/INT01/PuertoRico/EQT_20200107_20200114/downloads_mseeds/20200107/ /media/yoon/INT01/PuertoRico/EQT_20200107_20200114/mseed_detections/20200107/")
   print("Exiting, not enough arguments...")
   sys.exit(1)

IN_MSEED_DIR = str(sys.argv[1])
OUT_DET_DIR = str(sys.argv[2])
print("PROCESSING:", IN_MSEED_DIR, OUT_DET_DIR)
   

#--------------------------START OF INPUTS------------------------
# Model parameters
model_path="/home/yoon/Documents/EQTransformer/ModelsAndSampleData/"
#MODEL_FILE='EqT_model.h5' # initial model - minimize false positives
#MODEL_FILE='EqT_model2.h5' # new model - minimize false negatives

# Predictor parameters
#OVER_LAP = 0.75 # overlap fraction between adjacent time windows.  Do not use default value OVER_LAP=0.3 -> missing many picks
#NUM_PLOTS = 1000

## TEST00: DEFAULT in manuscript, intermediate overlap, highest detection threshold (constant), conservative model
#MODEL_FILE='EqT_model_conservative.h5' # initial model - minimize false positives
#DET_THRESH = 0.3
#P_THRESH = 0.1
#S_THRESH = 0.1
#OVER_LAP = 0.75 # overlap fraction between adjacent time windows

## TEST01: highest overlap, highest detection threshold (constant), conservative model - will slow down runtime
#MODEL_FILE='EqT_model_conservative.h5' # initial model - minimize false positives
#DET_THRESH = 0.3
#P_THRESH = 0.1
#S_THRESH = 0.1
#OVER_LAP = 0.9 # overlap fraction between adjacent time windows

## TEST02: lowest overlap, highest detection threshold (constant), conservative model - run faster but miss events
#MODEL_FILE='EqT_model_conservative.h5' # initial model - minimize false positives
#DET_THRESH = 0.3
#P_THRESH = 0.1
#S_THRESH = 0.1
#OVER_LAP = 0.3 # overlap fraction between adjacent time windows

## TEST03: intermediate overlap (constant), intermediate detection threshold, conservative model
#MODEL_FILE='EqT_model_conservative.h5' # initial model - minimize false positives
#DET_THRESH = 0.1
#P_THRESH = 0.05
#S_THRESH = 0.05
#OVER_LAP = 0.75 # overlap fraction between adjacent time windows

## TEST04: intermediate overlap (constant), lowest detection threshold, conservative model
#MODEL_FILE='EqT_model_conservative.h5' # initial model - minimize false positives
#DET_THRESH = 0.05
#P_THRESH = 0.03
#S_THRESH = 0.03
#OVER_LAP = 0.75 # overlap fraction between adjacent time windows

## TEST05: intermediate overlap, lowest detection threshold (constant), original model
#MODEL_FILE='EqT_original_model.h5' # original model - minimize false negatives
#DET_THRESH = 0.5
#P_THRESH = 0.3
#S_THRESH = 0.3
#OVER_LAP = 0.75 # overlap fraction between adjacent time windows

## TEST06: highest overlap, lowest detection threshold (constant), original model - will slow down runtime
#MODEL_FILE='EqT_original_model.h5' # original model - minimize false negatives
#DET_THRESH = 0.5
#P_THRESH = 0.3
#S_THRESH = 0.3
#OVER_LAP = 0.9 # overlap fraction between adjacent time windows

## TEST07: lowest overlap, lowest detection threshold (constant), original model - run faster but miss events
#MODEL_FILE='EqT_original_model.h5' # original model - minimize false negatives
#DET_THRESH = 0.5
#P_THRESH = 0.3
#S_THRESH = 0.3
#OVER_LAP = 0.3 # overlap fraction between adjacent time windows

## TEST08: intermediate overlap (constant), highest detection threshold, original model
#MODEL_FILE='EqT_original_model.h5' # original model - minimize false negatives
#DET_THRESH = 0.7
#P_THRESH = 0.5
#S_THRESH = 0.5
#OVER_LAP = 0.75 # overlap fraction between adjacent time windows

## TEST09: intermediate overlap (constant), intermediate detection threshold, original model
#MODEL_FILE='EqT_original_model.h5' # original model - minimize false negatives
#DET_THRESH = 0.6
#P_THRESH = 0.4
#S_THRESH = 0.4
#OVER_LAP = 0.75 # overlap fraction between adjacent time windows

# TEST10: highest overlap, lowest detection threshold, conservative model - 'optimal' - preferred based on associator runtime
MODEL_FILE='EqT_model_conservative.h5' # initial model - minimize false positives
DET_THRESH = 0.05
P_THRESH = 0.03
S_THRESH = 0.03
OVER_LAP = 0.9 # overlap fraction between adjacent time windows

## TEST11: highest overlap, highest detection threshold (otherwise there are false positives), original model
#MODEL_FILE='EqT_original_model.h5' # original model - minimize false negatives
#DET_THRESH = 0.7
#P_THRESH = 0.5
#S_THRESH = 0.5
#OVER_LAP = 0.9 # overlap fraction between adjacent time windows

## TEST12: intermediate overlap (constant), ever-lowest detection threshold, original model
#MODEL_FILE='EqT_original_model.h5' # original model - minimize false negatives
#DET_THRESH = 0.4
#P_THRESH = 0.2
#S_THRESH = 0.2
#OVER_LAP = 0.75


# Consistent parameters
BATCH = 500
NUM_PLOTS = 0
#NUM_PLOTS = 20000

# Station file (input)
#in_station_json_file='/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/station_list.json'
#in_station_json_file='/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/station_list.json'
#in_station_json_file='/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/station_list.json'
#in_station_json_file='/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/station_list.json'
#in_station_json_file='/media/yoon/INT01/PuertoRico/EQT_20200107_20200108/station_list.json'
in_station_json_file='/media/yoon/INT01/PuertoRico/EQT_20180101_20230101/station_list.json'

#--------------------------END OF INPUTS------------------------

# Run the predictor directly on MiniSEED data
mseed_predictor(input_dir=IN_MSEED_DIR, input_model=model_path+MODEL_FILE, stations_json=in_station_json_file,
   output_dir=OUT_DET_DIR, detection_threshold=DET_THRESH, P_threshold=P_THRESH, S_threshold=S_THRESH,
   number_of_plots=NUM_PLOTS, plot_mode='time_frequency', overlap=OVER_LAP, batch_size=BATCH)
