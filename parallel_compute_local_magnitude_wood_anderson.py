import numpy as np
import json
import os
import multiprocessing
import matplotlib.pyplot as plt
import matplotlib
import utils_hypoinverse as utils_hyp
import utils_magnitude as utils_mag
import PARTIALcompute_local_magnitude_wood_anderson as comp_local_mag
from matplotlib import rcParams

matplotlib.use('Agg')
rcParams.update({'font.size': 24})
rcParams['pdf.fonttype'] = 42
rcParams['font.sans-serif'] = "Helvetica"
rcParams['font.family'] = "sans-serif"



def get_list_of_block_indices(i_start, i_end, nblock_i):
   block_indices = []
   for iblock in range(nblock_i):
      i_now_start = i_start[iblock]
      i_now_end = i_end[iblock]
      print("\n # i_now_start = ", i_now_start, ", i_now_end = ", i_now_end)

      block_indices.append([i_now_start, i_now_end])
   return block_indices


def run_compute_local_magnitude(block_index):
   print("block_index = ", block_index)
   result = comp_local_mag.main(block_index[0], block_index[1]) # This also works, but stdout output is mixed up
   return result


# PARALLEL Compute local magnitude (Wood-Anderson) for EQTransformer events, following magnitude calibration from matching catalog events

#in_hinv_arc_file = '../LargeAreaEQTransformer/association/merged_locate_pr.arc' # use the merged file
##in_hinv_arc_file = '../LargeAreaEQTransformer/association/SMALLTEST_merged_magcat_locate_pr.arc' # use the merged file
#in_station_file = '../LargeAreaEQTransformer/station_list_edited.json'
#in_match_catalog_file = '../LargeAreaEQTransformer/association/events_MATCH_eqt_20200107_20200114.txt'

#in_hinv_arc_file = '../LargeAreaEQTransformer/REAL/Events/merged_real_locate_pr.arc' # use the merged file
#in_station_file = '../LargeAreaEQTransformer/station_list_edited.json'
#in_match_catalog_file = '../LargeAreaEQTransformer/REAL/Events/events_MATCH_eqt_20200107_20200114.txt'

#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/association/merged_locate_pr.arc' # use the merged file
#in_station_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/station_list_edited.json'
#in_match_catalog_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/association/events_MATCH_eqt_2018_2020.txt'

#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/REAL/HYPOINVERSE/merged_real_locate_pr.arc' # use the merged file
#in_station_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/station_list_edited.json'
#in_match_catalog_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/REAL/HYPOINVERSE/events_MATCH_eqt_2018_2020.txt'

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/station_list_edited.json'
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELPRSN/merged_real_locate_pr.arc' # use the merged file
#in_match_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELPRSN/us_events_MATCH_EQT_20200107_20200114.txt'

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/station_list_edited.json'
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/merged_real_locate_pr.arc' # use the merged file
#in_match_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/us_events_MATCH_EQT_20200107_20200114.txt'

##in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/station_list_edited.json'
##in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELPRSN/merged_real_locate_pr.arc' # use the merged file
##in_match_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELPRSN/us_events_MATCH_EQT_20191228_20200114.txt'
#
##in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/station_list_edited.json'
##in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELZHANG/merged_real_locate_pr.arc' # use the merged file
##in_match_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELZHANG/us_events_MATCH_EQT_20191228_20200114.txt'
#
#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/station_list_edited.json'
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELPRSN/merged_real_locate_pr.arc' # use the merged file
#in_match_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELPRSN/us_events_MATCH_EQT_20180101_20210601.txt'

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/station_list_edited.json'
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/merged_real_locate_pr.arc' # use the merged file
#in_match_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/us_events_MATCH_EQT_20180101_20210601.txt'

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/station_list_edited.json'
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/merged_real_locate_pr.arc' # use the merged file
#in_match_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/us_events_MATCH_EQT_20180101_20211001.txt'

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/station_list_edited.json'
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/merged_real_locate_pr.arc' # use the merged file
#in_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/merged_real_locate_pr.sum' # use the merged file
#in_match_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/us_events_MATCH_EQT_20180101_20220101.txt'

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/station_list_edited.json'
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/merged_real_locate_pr.arc' # use the merged file
#in_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/merged_real_locate_pr.sum' # use the merged file
#in_match_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/us_events_MATCH_EQT_20180101_20220101.txt'

#out_mag_plot_dir = '../LargeAreaEQTransformer/MagPlots/'
#out_calib_plot_file = 'constrainwindow120_median_calib_NOFILTERPHASETEST_20200107_20200114_catalog_match_event_mag_comparison.png'
#out_match_catalog_file = '../LargeAreaEQTransformer/association/events_MATCH_magcat_eqt_20200107_20200114.txt'
#out_hinv_arc_file = '../LargeAreaEQTransformer/association/merged_magcat_locate_pr.arc' # catalog magnitudes filled into HYPOINVERSE arc file
#out_hinv_sum_file = '../LargeAreaEQTransformer/association/merged_magcat_locate_pr.sum' # catalog magnitudes filled into HYPOINVERSE sum file

#out_mag_plot_dir = '../LargeAreaEQTransformer/MagPlots/'
#out_calib_plot_file = 'real_constrainwindow120_median_calib_NOFILTERPHASETEST_20200107_20200114_catalog_match_event_mag_comparison.png'
#out_match_catalog_file = '../LargeAreaEQTransformer/REAL/Events/events_MATCH_magcat_eqt_20200107_20200114.txt'
#out_hinv_arc_file = '../LargeAreaEQTransformer/REAL/Events/merged_real_magcat_locate_pr.arc' # catalog magnitudes filled into HYPOINVERSE arc file
#out_hinv_sum_file = '../LargeAreaEQTransformer/REAL/Events/merged_real_magcat_locate_pr.sum' # catalog magnitudes filled into HYPOINVERSE sum file

#out_mag_plot_dir = '/media/yoon/INT01/PuertoRico/FullEQTransformer/MagPlots/'
#out_calib_plot_file = 'constrainwindow120_FULL_median_calib_NOFILTERPHASETEST_2018_2020_catalog_match_event_mag_comparison.png'
#out_match_catalog_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/association/events_MATCH_magcat_eqt_2018_2020.txt'
#out_hinv_arc_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/association/merged_magcat_locate_pr.arc' # catalog magnitudes filled into HYPOINVERSE arc file
#out_hinv_sum_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/association/merged_magcat_locate_pr.sum' # catalog magnitudes filled into HYPOINVERSE sum file

#out_mag_plot_dir = '/media/yoon/INT01/PuertoRico/FullEQTransformer/MagPlots/'
#out_calib_plot_file = 'real_constrainwindow120_FULL_median_calib_NOFILTERPHASETEST_2018_2020_catalog_match_event_mag_comparison.png'
#out_match_catalog_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/REAL/HYPOINVERSE/events_MATCH_magcat_eqt_2018_2020.txt'
#out_hinv_arc_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/REAL/HYPOINVERSE/merged_real_magcat_locate_pr.arc' # catalog magnitudes filled into HYPOINVERSE arc file
#out_hinv_sum_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/REAL/HYPOINVERSE/merged_real_magcat_locate_pr.sum' # catalog magnitudes filled into HYPOINVERSE sum file

#out_mag_plot_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELPRSN/MagPlots/'
#out_calib_plot_file = 'magnitude_calibration_us_EQT_20200107_20200114.pdf'
#out_match_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELPRSN/us_events_MATCH_magcat_EQT_20200107_20200114.txt'
#out_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELPRSN/merged_real_magcat_locate_pr.arc' # catalog magnitudes filled into HYPOINVERSE arc file
#out_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELPRSN/merged_real_magcat_locate_pr.sum' # catalog magnitudes filled into HYPOINVERSE sum file

#out_mag_plot_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/MagPlots/'
#out_calib_plot_file = 'magnitude_calibration_us_EQT_20200107_20200114.pdf'
#out_match_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/us_events_MATCH_magcat_EQT_20200107_20200114.txt'
#out_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/merged_real_magcat_locate_pr.arc' # catalog magnitudes filled into HYPOINVERSE arc file
#out_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/merged_real_magcat_locate_pr.sum' # catalog magnitudes filled into HYPOINVERSE sum file

#out_mag_plot_dir = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELPRSN/MagPlots/'
#out_calib_plot_file = 'magnitude_calibration_us_EQT_20191228_20200114.pdf'
#out_match_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELPRSN/us_events_MATCH_magcat_EQT_20191228_20200114.txt'
#out_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELPRSN/merged_real_magcat_locate_pr.arc' # catalog magnitudes filled into HYPOINVERSE arc file
#out_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELPRSN/merged_real_magcat_locate_pr.sum' # catalog magnitudes filled into HYPOINVERSE sum file

#out_mag_plot_dir = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELZHANG/MagPlots/'
#out_calib_plot_file = 'magnitude_calibration_us_EQT_20191228_20200114.pdf'
#out_match_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELZHANG/us_events_MATCH_magcat_EQT_20191228_20200114.txt'
#out_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELZHANG/merged_real_magcat_locate_pr.arc' # catalog magnitudes filled into HYPOINVERSE arc file
#out_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELZHANG/merged_real_magcat_locate_pr.sum' # catalog magnitudes filled into HYPOINVERSE sum file

#out_mag_plot_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELPRSN/MagPlots/'
#out_calib_plot_file = 'magnitude_calibration_us_EQT_20180101_20210601.pdf'
#out_match_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELPRSN/us_events_MATCH_magcat_EQT_20180101_20210601.txt'
#out_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELPRSN/merged_real_magcat_locate_pr.arc' # catalog magnitudes filled into HYPOINVERSE arc file
#out_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELPRSN/merged_real_magcat_locate_pr.sum' # catalog magnitudes filled into HYPOINVERSE sum file

#out_mag_plot_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/MagPlots/'
#out_calib_plot_file = 'magnitude_calibration_us_EQT_20180101_20210601.pdf'
#out_match_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/us_events_MATCH_magcat_EQT_20180101_20210601.txt'
#out_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/merged_real_magcat_locate_pr.arc' # catalog magnitudes filled into HYPOINVERSE arc file
#out_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/merged_real_magcat_locate_pr.sum' # catalog magnitudes filled into HYPOINVERSE sum file

#out_mag_plot_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/MagPlots/'
#out_calib_plot_file = 'magnitude_calibration_us_EQT_20180101_20211001.pdf'
#out_match_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/us_events_MATCH_magcat_EQT_20180101_20211001.txt'
#out_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/merged_real_magcat_locate_pr.arc' # catalog magnitudes filled into HYPOINVERSE arc file
#out_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/merged_real_magcat_locate_pr.sum' # catalog magnitudes filled into HYPOINVERSE sum file

#out_mag_plot_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/MagPlots/'
#out_calib_plot_file = 'magnitude_calibration_us_EQT_20180101_20220101.pdf'
#out_match_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/us_events_MATCH_magcat_EQT_20180101_20220101.txt'
#out_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/merged_real_magcat_locate_pr.arc' # catalog magnitudes filled into HYPOINVERSE arc file
#out_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/merged_real_magcat_locate_pr.sum' # catalog magnitudes filled into HYPOINVERSE sum file

#out_mag_plot_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/MagPlots/'
#out_calib_plot_file = 'magnitude_calibration_us_EQT_20180101_20220101.pdf'
#out_match_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/us_events_MATCH_magcat_EQT_20180101_20220101.txt'
#out_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/merged_real_magcat_locate_pr.arc' # catalog magnitudes filled into HYPOINVERSE arc file
#out_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/merged_real_magcat_locate_pr.sum' # catalog magnitudes filled into HYPOINVERSE sum file

#base_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200108/'
#hinv_dir = base_dir+'REAL_VELZHANG/DIRECTHYPOSVI_TEST10/'
#in_station_file = base_dir+'station_list_edited.json'
#in_hinv_arc_file = hinv_dir+'hyposvi_real_locate_pr.arc'
#in_hinv_sum_file = hinv_dir+'hyposvi_real_locate_pr.sum'
#in_match_catalog_file = hinv_dir+'us_events_MATCH_TEST10_EQT_20200107_20200108.txt'
#out_mag_plot_dir = hinv_dir+'MagPlots/'
#out_calib_plot_file = 'hyposvi_magnitude_calibration_EQT_20200107_20200108.pdf'
#out_match_catalog_file = hinv_dir+'hyposvi_events_MATCH_magcat_EQT_20200107_20200108.txt'
#out_hinv_arc_file = hinv_dir+'hyposvi_real_magcat_locate_pr.arc' # catalog magnitudes filled into HYPOINVERSE arc file
#out_hinv_sum_file = hinv_dir+'hyposvi_real_magcat_locate_pr.sum' # catalog magnitudes filled into HYPOINVERSE sum file

base_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20230101/'
hinv_dir = base_dir+'REAL_VELZHANG/DIRECTHYPOSVI/'
in_station_file = base_dir+'station_list_edited.json'
in_hinv_arc_file = hinv_dir+'hyposvi_real_locate_pr.arc'
in_hinv_sum_file = hinv_dir+'hyposvi_real_locate_pr.sum'
in_match_catalog_file = hinv_dir+'us_events_MATCH_EQT_20180101_20230101.txt'
out_mag_plot_dir = hinv_dir+'MagPlots/'
out_calib_plot_file = 'hyposvi_magnitude_calibration_EQT_20180101_20230101.pdf'
out_match_catalog_file = hinv_dir+'us_events_MATCH_magcat_EQT_20180101_20230101.txt'
out_hinv_arc_file = hinv_dir+'hyposvi_real_magcat_locate_pr.arc' # catalog magnitudes filled into HYPOINVERSE arc file
out_hinv_sum_file = hinv_dir+'hyposvi_real_magcat_locate_pr.sum' # catalog magnitudes filled into HYPOINVERSE sum file


num_i = sum(1 for line in open(in_hinv_sum_file))
print("Number of events: ", num_i)

# number of parallel blocks
#nblock_i = 2
#nblock_i = 4
#nblock_i = 32
#nblock_i = 48
#nblock_i = 72
nblock_i = 84

# number of elements per block
blocksize_i = int(num_i) // int(nblock_i)
remain_i = int(num_i) % int(nblock_i)

print("num_i = ", num_i)
print("nblock_i = ", nblock_i)
print("blocksize_i = ", blocksize_i)
print("remain_i = ", remain_i)

# starting and ending indices for each block
i_start = blocksize_i*np.arange(0,nblock_i)
i_end = i_start + blocksize_i
i_end[-1] += remain_i
print("i_start = ", i_start, ", i_end = ", i_end)


#### Parallel multi-block calculation
block_indices = get_list_of_block_indices(i_start, i_end, nblock_i)
print("len(block_indices) = ", len(block_indices))

num_cores = multiprocessing.cpu_count()
print("num_cores = ", num_cores)

pool = multiprocessing.Pool(processes=num_cores)
results = pool.map(run_compute_local_magnitude, block_indices)
# Combine returned dictionaries (with local magnitudes) into one: map_events
map_events = {}
for result in results:
   map_events.update(result)


### Now we have gathered local magnitudes for all events - no more parallel processing
### Outputs

# After magnitude calibration, show that magnitudes match for catalog events only: save arrays for plots
stations_ = json.load(open(in_station_file))
map_cat_events = utils_mag.get_catalog_events_map(in_match_catalog_file, stations_)
all_catalog_mag = []
all_catalog_ML = []
for ev_id in map_cat_events: # Loop over catalog events only
   if ((ev_id in map_events) and (len(map_events[ev_id]) > 6)): # ML exists for this event, last item in map for all events
      all_catalog_mag.append(map_cat_events[ev_id][3]) # Get magnitude from catalog
      all_catalog_ML.append(map_events[ev_id][6]) # Get ML from map for all events

# After magnitude calibration, show that magnitudes match: plot catalog magnitude vs calculated ML
equal_line = np.linspace(-1, 7, 100)
plt.figure(figsize=(6,8))
plt.scatter(all_catalog_mag, np.asarray(all_catalog_ML), facecolors='none', edgecolors='k', linewidths=0.1)
plt.plot(equal_line, equal_line, '--', color='k')
plt.axis('scaled')
plt.xlim([0, 7])
plt.ylim([0, 7])
plt.xticks([0, 1, 2, 3, 4, 5, 6, 7])
plt.yticks([0, 1, 2, 3, 4, 5, 6, 7])
plt.xlabel('Catalog magnitude $M_{cat}$')
plt.ylabel('Local magnitude $M_{L}$')
plt.tight_layout()
if not os.path.exists(out_mag_plot_dir):
   os.makedirs(out_mag_plot_dir)
plt.savefig(out_mag_plot_dir+out_calib_plot_file)

# Output magnitudes to file: matching catalog events
fout_match = open(out_match_catalog_file, 'w')
with open(in_match_catalog_file, 'r') as fin:
   for line in fin:
      ev_id = int(utils_hyp.get_event_id_hypoinverse_file(line))
      if (ev_id in map_cat_events):
         ev_mag = 0.0
         if (len(map_events[ev_id]) > 6): # ML exists for this event, last item in map for all events
            ev_mag = int(round(100*map_events[ev_id][6]))
         fout_match.write(('%s%3d%s') % (line[0:147], ev_mag, line[150:]))
fout_match.close()

# Output magnitudes to HYPOINVERSE files: all events
fout_arc = open(out_hinv_arc_file, 'w')
fout_sum = open(out_hinv_sum_file, 'w')
with open(in_hinv_arc_file, 'r') as fin:
   for line in fin:
      if ((line[0:2] == '19') or (line[0:2] == '20')): # Replace magnitude in line
         ev_id = int(utils_hyp.get_event_id_hypoinverse_file(line))
         if (ev_id in map_events):
            ev_mag = 0.0
            if (len(map_events[ev_id]) > 6): # ML exists for this event, last item in map for all events
               ev_mag = int(round(100*map_events[ev_id][6]))
            fout_arc.write(('%s%3d%s') % (line[0:147], ev_mag, line[150:]))
            fout_sum.write(('%s%3d%s') % (line[0:147], ev_mag, line[150:]))
      else:
         if (ev_id in map_events):
            fout_arc.write(('%s') % line)
fout_arc.close()
fout_sum.close()

