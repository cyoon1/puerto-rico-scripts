#!/bin/bash

# Plot Puerto Rico earthquakes on a map

#gmt gmtset BASEMAP_TYPE plain
#gmt gmtset HEADER_FONT_SIZE 24p
#gmt gmtset LABEL_FONT_SIZE 16p # smaller font for labels
##gmt gmtset PLOT_DEGREE_FORMAT +DF
#gmt gmtset PLOT_DEGREE_FORMAT -DF
#gmt gmtset GRID_PEN_PRIMARY 0.1p
#
gmt gmtset MAP_FRAME_TYPE plain     # map outline - single line with ticks
gmt gmtset FORMAT_GEO_MAP DD        # labels on map - decimal degrees

out_color_filename=depthcolors.cpt
out_color_file=../Plots/${out_color_filename}
out_color_time_filename=timecolors.cpt
out_color_time_file=../Plots/${out_color_time_filename}

#in_cat_file=../Catalog/test_catalog_puerto_rico.txt
#out_map_cat_file=../Plots/puerto_rico_small_map
#in_cat_file=../Catalog/catalog_new_puerto_rico_2018_2020.txt
#out_map_cat_file=../Plots/puerto_rico_2018_2020_small_map
#in_cat_file=../Catalog/catalog_new_puerto_rico_2018_20191227.txt
#out_map_cat_file=../Plots/puerto_rico_2018_20191227_small_map
#in_cat_file=../Catalog/catalog_new_puerto_rico_20191227_2020.txt
#out_map_cat_file=../Plots/puerto_rico_20191227_2020_small_map

in_cat_file=../Catalog/catalog_new_puerto_rico_20200107_20200114.txt
out_map_cat_depth_file=../Plots/puerto_rico_depth_20200107_20200114_small_map
out_map_cat_time_file=../Plots/puerto_rico_time_20200107_20200114_small_map
####in_eqt_hinv_file=../Catalog/TestEQTransformer_HYPOINVERSE_puerto_rico_catalog_20200107_20200114.txt
####out_map_eqt_hinv_depth_file=../Plots/puerto_rico_eqt_20200107_20200114_small_map
#####in_sta_file=../Catalog/pr_stations.txt
####in_cat_file=../Catalog/catalog_new_puerto_rico_2018_2020.txt
####out_map_cat_file=../Plots/puerto_rico_2018_2020_small_map
#in_cat_file=../Catalog/catalog_new_puerto_rico_2018_2020.txt
#out_map_cat_depth_file=../Plots/puerto_rico_depth_2018_2020_small_map
#out_map_cat_time_file=../Plots/puerto_rico_time_2018_2020_small_map

#in_cat_missed_file=../Catalog/LargeAreaEQTransformer_HYPOINVERSE_puerto_rico_MISSED_20200107_20200114.txt
in_cat_missed_file=../Catalog/LargeAreaEQTransformerModel2_REAL_HYPOINVERSE_puerto_rico_MISSED_20200107_20200114.txt
out_map_cat_missed_depth_file=../Plots/puerto_rico_catalog_missed_depth_20200107_20200114_small_map
out_map_cat_missed_time_file=../Plots/puerto_rico_catalog_missed_time_20200107_20200114_small_map
#in_cat_missed_file=../Catalog/FullEQTransformer_HYPOINVERSE_puerto_rico_MISSED_2018_2020.txt
#out_map_cat_missed_depth_file=../Plots/puerto_rico_catalog_missed_depth_2018_2020_small_map
#out_map_cat_missed_time_file=../Plots/puerto_rico_catalog_missed_time_2018_2020_small_map
#in_cat_missed_file=../Catalog/FullEQTransformer_REAL_HYPOINVERSE_puerto_rico_MISSED_2018_2020.txt
#out_map_cat_missed_depth_file=../Plots/puerto_rico_catalog_REAL_missed_depth_2018_2020_small_map
#out_map_cat_missed_time_file=../Plots/puerto_rico_catalog_REAL_missed_time_2018_2020_small_map

#in_eqt_hinv_file=../Catalog/LargeAreaEQTransformer_REAL_HYPOINVERSE_puerto_rico_catalog_20200107_20200114.txt
in_eqt_hinv_file=../Catalog/LargeAreaEQTransformerModel2_REAL_HYPOINVERSE_puerto_rico_catalog_20200107_20200114.txt
out_map_eqt_hinv_depth_file=../Plots/puerto_rico_eqt_REAL_hinv_depth_20200107_20200114_small_map_large
out_map_eqt_hinv_time_file=../Plots/puerto_rico_eqt_REAL_hinv_time_20200107_20200114_small_map_large
###in_eqt_hinv_file=../Catalog/LargeAreaEQTransformer_HYPOINVERSE_puerto_rico_catalog_20200107_20200114.txt
###out_map_eqt_hinv_depth_file=../Plots/puerto_rico_eqt_hinv_depth_20200107_20200114_small_map_large
###out_map_eqt_hinv_time_file=../Plots/puerto_rico_eqt_hinv_time_20200107_20200114_small_map_large
####in_eqt_hinv_file=../Catalog/LargeAreaEQTransformer_HYPOINVERSE_puerto_rico_catalog_2018_2020.txt
####out_map_eqt_hinv_depth_file=../Plots/puerto_rico_eqt_hinv_2018_2020_small_map_large
#in_eqt_hinv_file=../Catalog/FullEQTransformer_HYPOINVERSE_puerto_rico_catalog_2018_2020.txt
#out_map_eqt_hinv_depth_file=../Plots/puerto_rico_eqt_hinv_depth_2018_2020_small_map_large
#out_map_eqt_hinv_time_file=../Plots/puerto_rico_eqt_hinv_time_2018_2020_small_map_large
#in_eqt_hinv_file=../Catalog/FullEQTransformer_REAL_HYPOINVERSE_puerto_rico_catalog_2018_2020.txt
#out_map_eqt_hinv_depth_file=../Plots/puerto_rico_eqt_REAL_hinv_depth_2018_2020_small_map_large
#out_map_eqt_hinv_time_file=../Plots/puerto_rico_eqt_REAL_hinv_time_2018_2020_small_map_large

###in_sta_file=../Catalog/pr_stations_large.txt
in_sta_file=../Catalog/FullEQTransformer_pr_stations_large.txt

#in_eqt_hypoDD_file=../LargeAreaEQTransformer/HYPODD/hypoDD.reloc
#out_map_eqt_hypoDD_depth_file=../Plots/puerto_rico_eqt_hypoDD_20200107_20200114_small_map_large
#in_eqt_hypoDD_file=../LargeAreaEQTransformer/HYPODD/hypoDD_dtcc_only.reloc
#out_map_eqt_hypoDD_depth_file=../Plots/puerto_rico_eqt_hypoDD_dtcc_only_20200107_20200114_small_map_large
#in_eqt_hypoDD_file=../LargeAreaEQTransformer/HYPODD/hypoDD_dtct_dtcc_obsct4.reloc
#out_map_eqt_hypoDD_depth_file=../Plots/puerto_rico_eqt_hypoDD_dtct_dtcc_obsct4_20200107_20200114_small_map_large
#in_eqt_hypoDD_file=../LargeAreaEQTransformer/HYPODD/hypoDD_dtct_dtcc_obsct4_obscc4.reloc
in_eqt_hypoDD_file=../Catalog/LargeAreaEQTransformer_HYPODD_puerto_rico_catalog_20200107_20200114.txt
out_map_eqt_hypoDD_depth_file=../Plots/puerto_rico_eqt_hypoDD_depth_dtct_dtcc_obsct4_obscc4_20200107_20200114_small_map_large
out_map_eqt_hypoDD_time_file=../Plots/puerto_rico_eqt_hypoDD_time_dtct_dtcc_obsct4_obscc4_20200107_20200114_small_map_large

#in_eqt_growclust_file=../LargeAreaEQTransformer/GrowClust/OUT/out.growclust_cat
in_eqt_growclust_file=../Catalog/LargeAreaEQTransformer_GrowClust_puerto_rico_catalog_20200107_20200114.txt
out_map_eqt_growclust_depth_file=../Plots/puerto_rico_eqt_growclust_depth_20200107_20200114_small_map_large
out_map_eqt_growclust_time_file=../Plots/puerto_rico_eqt_growclust_time_20200107_20200114_small_map_large

#in_cat_file=../Catalog/catalog_new_puerto_rico_2018_2020.txt
#in_cat_file=../Catalog/catalog_new_puerto_rico_2018_2020.txt
#out_map_cat_file=../Plots/puerto_rico_2018_2020_large_map
#in_cat_file=../Catalog/catalog_new_puerto_rico_2018_20191227.txt
#out_map_cat_file=../Plots/puerto_rico_2018_20191227_large_map
#in_cat_file=../Catalog/catalog_new_puerto_rico_20191227_2020.txt
#out_map_cat_file=../Plots/puerto_rico_20191227_2020_large_map

min_lat=17
max_lat=19
min_lon=-68
max_lon=-65

min_lat=17.5
max_lat=18.5
min_lon=-67.5
max_lon=-66.0

min_lat=17.6
max_lat=18.3
min_lon=-67.3
max_lon=-66.4

reg=-R${min_lon}/${max_lon}/${min_lat}/${max_lat}
proj=-JM16
echo ${reg}
lat_lon_spacing=0.1

region_inset=-R-75/-62/15/22
projection_inset=-JM2.0i

# Cross section parameters
strike1_angle=15
strike1_length=30
strike1_proj_width=10
strike1_center_lat=17.9
strike1_center_lon=-66.85
strike1_bproj=-JX30/15
strike1_brange=-R-30/30/-30/0

strike2_angle=95
strike2_length=40
strike2_proj_width=10
strike2_center_lat=17.93
strike2_center_lon=-66.85
strike2_bproj=-JX40/15
strike2_brange=-R-40/40/-30/0



##### COLOR BY DEPTH #####

gmt makecpt -Cinferno -I -T0/20/0.01 > ${out_color_file}

gmt begin ${out_map_cat_depth_file}
   gmt basemap ${proj} ${reg} -Ba${lat_lon_spacing}fg -BneWS -U
   gmt coast ${proj} ${reg} -W0.25p,black -Na -G255 -Sdeepskyblue -Lg-66.6/17.65+c-66.5/17.65+w20+f+lkm
   awk '{print $2, $3, $4, $5*$5*0.01 + 0.0200}' ${in_cat_file} | gmt plot ${proj} ${reg} -Sc -W0.01c -G255 -BneWS -: -C${out_color_file}
   awk '{print $2 " " $3 " " $4}' ${in_sta_file} | gmt plot ${proj} ${reg} -Si0.4 -W0.1c,black -Ggray -:
   gmt colorbar -C${out_color_file} -Dx6.4i/2.4i/2.5i/0.15i -Bxaf+l"Depth (km)"

   # Project seismicity along one direction
   awk '{print $3, $2, $4, $6, $5, $1}' ${in_cat_file} | gmt project -Q -C${strike1_center_lon}/${strike1_center_lat} -A${strike1_angle} -W-${strike1_proj_width}/${strike1_proj_width} -L-${strike1_length}/${strike1_length} > ${out_map_cat_depth_file}_seismicity_time_projection${strike1_angle}.txt # plot all events
   awk '{print $3, $2, $4, $6, $5, $1}' ${in_cat_file} | gmt project -Q -C${strike2_center_lon}/${strike2_center_lat} -A${strike2_angle} -W-${strike2_proj_width}/${strike2_proj_width} -L-${strike2_length}/${strike2_length} > ${out_map_cat_depth_file}_seismicity_time_projection${strike2_angle}.txt # plot all events

   # Projection lines
   gmt project -Q -G1 -C${strike1_center_lon}/${strike1_center_lat} -A${strike1_angle} -L-${strike1_length}/${strike1_length} | gmt plot ${proj} ${reg} -W2,0/0/0,- -Gblack
   gmt text ${proj} ${reg} -F+f24 << EOF
-66.82 18.15 A'
-66.95 17.65 A
EOF
   gmt project -Q -G1 -C${strike2_center_lon}/${strike2_center_lat} -A${strike2_angle} -L-${strike2_length}/${strike2_length} | gmt plot ${proj} ${reg} -W2,0/0/0,- -Gblack
   gmt text ${proj} ${reg} -F+f24 << EOF
-67.25 17.95 B
-66.45 17.90 B'
EOF

   # Add inset
   gmt basemap ${region_inset} ${projection_inset} -Bnews -X0.05i -Y0.05i
   gmt coast ${region_inset} ${projection_inset} -Dl -G255 -Na -W1
   gmt plot ${region_inset} ${projection_inset} -W2,red << EOF
${min_lon} ${min_lat}
${min_lon} ${max_lat}
${max_lon} ${max_lat}
${max_lon} ${min_lat}
${min_lon} ${min_lat}
EOF
gmt end show

gmt begin ${out_map_cat_missed_depth_file}
   gmt basemap ${proj} ${reg} -Ba${lat_lon_spacing}fg -BneWS -U
   gmt coast ${proj} ${reg} -W0.25p,black -Na -G255 -Sdeepskyblue -Lg-66.6/17.65+c-66.5/17.65+w20+f+lkm
   awk '{print $2, $3, $4, $5*$5*0.01 + 0.0200}' ${in_cat_missed_file} | gmt plot ${proj} ${reg} -Sc -W0.01c -G255 -BneWS -: -C${out_color_file}
   awk '{print $2 " " $3 " " $4}' ${in_sta_file} | gmt plot ${proj} ${reg} -Si0.4 -W0.1c,black -Ggray -:
   gmt colorbar -C${out_color_file} -Dx6.4i/2.4i/2.5i/0.15i -Bxaf+l"Depth (km)"

   # Project seismicity along one direction
   awk '{print $3, $2, $4, $6, $5, $1}' ${in_cat_missed_file} | gmt project -Q -C${strike1_center_lon}/${strike1_center_lat} -A${strike1_angle} -W-${strike1_proj_width}/${strike1_proj_width} -L-${strike1_length}/${strike1_length} > ${out_map_cat_missed_depth_file}_seismicity_time_projection${strike1_angle}.txt # plot all events
   awk '{print $3, $2, $4, $6, $5, $1}' ${in_cat_missed_file} | gmt project -Q -C${strike2_center_lon}/${strike2_center_lat} -A${strike2_angle} -W-${strike2_proj_width}/${strike2_proj_width} -L-${strike2_length}/${strike2_length} > ${out_map_cat_missed_depth_file}_seismicity_time_projection${strike2_angle}.txt # plot all events

   # Projection lines
   gmt project -Q -G1 -C${strike1_center_lon}/${strike1_center_lat} -A${strike1_angle} -L-${strike1_length}/${strike1_length} | gmt plot ${proj} ${reg} -W2,0/0/0,- -Gblack
   gmt text ${proj} ${reg} -F+f24 << EOF
-66.82 18.15 A'
-66.95 17.65 A
EOF
   gmt project -Q -G1 -C${strike2_center_lon}/${strike2_center_lat} -A${strike2_angle} -L-${strike2_length}/${strike2_length} | gmt plot ${proj} ${reg} -W2,0/0/0,- -Gblack
   gmt text ${proj} ${reg} -F+f24 << EOF
-67.25 17.95 B
-66.45 17.90 B'
EOF

   # Add inset
   gmt basemap ${region_inset} ${projection_inset} -Bnews -X0.05i -Y0.05i
   gmt coast ${region_inset} ${projection_inset} -Dl -G255 -Na -W1
   gmt plot ${region_inset} ${projection_inset} -W2,red << EOF
${min_lon} ${min_lat}
${min_lon} ${max_lat}
${max_lon} ${max_lat}
${max_lon} ${min_lat}
${min_lon} ${min_lat}
EOF
gmt end show

gmt begin ${out_map_eqt_hinv_depth_file}
   gmt basemap ${proj} ${reg} -Ba${lat_lon_spacing}fg -BneWS -U
   gmt coast ${proj} ${reg} -W0.25p,black -Na -G255 -Sdeepskyblue -Lg-66.6/17.65+c-66.5/17.65+w20+f+lkm
   awk '{print $2, $3, $4, $5*$5*0.01 + 0.0200}' ${in_eqt_hinv_file} | gmt plot ${proj} ${reg} -Sc -W0.01c -G255 -BneWS -: -C${out_color_file}
   awk '{print $2 " " $3 " " $4}' ${in_sta_file} | gmt plot ${proj} ${reg} -Si0.4 -W0.1c,black -Ggray -:
   gmt colorbar -C${out_color_file} -Dx6.4i/2.4i/2.5i/0.15i -Bxaf+l"Depth (km)"

   # Project seismicity along one direction
   awk '{print $3, $2, $4, $6, $5, $1}' ${in_eqt_hinv_file} | gmt project -Q -C${strike1_center_lon}/${strike1_center_lat} -A${strike1_angle} -W-${strike1_proj_width}/${strike1_proj_width} -L-${strike1_length}/${strike1_length} > ${out_map_eqt_hinv_depth_file}_seismicity_time_projection${strike1_angle}.txt # plot all events
   awk '{print $3, $2, $4, $6, $5, $1}' ${in_eqt_hinv_file} | gmt project -Q -C${strike2_center_lon}/${strike2_center_lat} -A${strike2_angle} -W-${strike2_proj_width}/${strike2_proj_width} -L-${strike2_length}/${strike2_length} > ${out_map_eqt_hinv_depth_file}_seismicity_time_projection${strike2_angle}.txt # plot all events

   # Projection lines
   gmt project -Q -G1 -C${strike1_center_lon}/${strike1_center_lat} -A${strike1_angle} -L-${strike1_length}/${strike1_length} | gmt plot ${proj} ${reg} -W2,0/0/0,- -Gblack
   gmt text ${proj} ${reg} -F+f24 << EOF
-66.82 18.15 A'
-66.95 17.65 A
EOF
   gmt project -Q -G1 -C${strike2_center_lon}/${strike2_center_lat} -A${strike2_angle} -L-${strike2_length}/${strike2_length} | gmt plot ${proj} ${reg} -W2,0/0/0,- -Gblack
   gmt text ${proj} ${reg} -F+f24 << EOF
-67.25 17.95 B
-66.45 17.90 B'
EOF

   # Add inset
   gmt basemap ${region_inset} ${projection_inset} -Bnews -X0.05i -Y0.05i
   gmt coast ${region_inset} ${projection_inset} -Dl -G255 -Na -W1
   gmt plot ${region_inset} ${projection_inset} -W2,red << EOF
${min_lon} ${min_lat}
${min_lon} ${max_lat}
${max_lon} ${max_lat}
${max_lon} ${min_lat}
${min_lon} ${min_lat}
EOF
gmt end show

gmt begin ${out_map_eqt_hypoDD_depth_file}
   gmt basemap ${proj} ${reg} -Ba${lat_lon_spacing}fg -BneWS -U
   gmt coast ${proj} ${reg} -W0.25p,black -Na -G255 -Sdeepskyblue -Lg-66.6/17.65+c-66.5/17.65+w20+f+lkm
   awk '{print $2, $3, $4, $5*$5*0.01 + 0.0200}' ${in_eqt_hypoDD_file} | gmt plot ${proj} ${reg} -Sc -W0.01c -G255 -BneWS -: -C${out_color_file}
#   awk '{print $2, $3, $4, $17*$17*0.01 + 0.0200}' ${in_eqt_hypoDD_file} | gmt plot ${proj} ${reg} -Sc -W0.01c -G255 -BneWS -: -C${out_color_file}
   awk '{print $2 " " $3 " " $4}' ${in_sta_file} | gmt plot ${proj} ${reg} -Si0.4 -W0.1c,black -Ggray -:
   gmt colorbar -C${out_color_file} -Dx6.4i/2.4i/2.5i/0.15i -Bxaf+l"Depth (km)"

   # Project seismicity along one direction
   awk '{print $3, $2, $4, $6, $5, $1}' ${in_eqt_hypoDD_file} | gmt project -Q -C${strike1_center_lon}/${strike1_center_lat} -A${strike1_angle} -W-${strike1_proj_width}/${strike1_proj_width} -L-${strike1_length}/${strike1_length} > ${out_map_eqt_hypoDD_depth_file}_seismicity_time_projection${strike1_angle}.txt # plot all events
   awk '{print $3, $2, $4, $6, $5, $1}' ${in_eqt_hypoDD_file} | gmt project -Q -C${strike2_center_lon}/${strike2_center_lat} -A${strike2_angle} -W-${strike2_proj_width}/${strike2_proj_width} -L-${strike2_length}/${strike2_length} > ${out_map_eqt_hypoDD_depth_file}_seismicity_time_projection${strike2_angle}.txt # plot all events

   # Projection lines
   gmt project -Q -G1 -C${strike1_center_lon}/${strike1_center_lat} -A${strike1_angle} -L-${strike1_length}/${strike1_length} | gmt plot ${proj} ${reg} -W2,0/0/0,- -Gblack
   gmt text ${proj} ${reg} -F+f24 << EOF
-66.82 18.15 A'
-66.95 17.65 A
EOF
   gmt project -Q -G1 -C${strike2_center_lon}/${strike2_center_lat} -A${strike2_angle} -L-${strike2_length}/${strike2_length} | gmt plot ${proj} ${reg} -W2,0/0/0,- -Gblack
   gmt text ${proj} ${reg} -F+f24 << EOF
-67.25 17.95 B
-66.45 17.90 B'
EOF

   # Add inset
   gmt basemap ${region_inset} ${projection_inset} -Bnews -X0.05i -Y0.05i
   gmt coast ${region_inset} ${projection_inset} -Dl -G255 -Na -W1
   gmt plot ${region_inset} ${projection_inset} -W2,red << EOF
${min_lon} ${min_lat}
${min_lon} ${max_lat}
${max_lon} ${max_lat}
${max_lon} ${min_lat}
${min_lon} ${min_lat}
EOF
gmt end show

gmt begin ${out_map_eqt_growclust_depth_file}
   gmt basemap ${proj} ${reg} -Ba${lat_lon_spacing}fg -BneWS -U
   gmt coast ${proj} ${reg} -W0.25p,black -Na -G255 -Sdeepskyblue -Lg-66.6/17.65+c-66.5/17.65+w20+f+lkm
   awk '{print $2, $3, $4, $5*$5*0.01 + 0.0200}' ${in_eqt_growclust_file} | gmt plot ${proj} ${reg} -Sc -W0.01c -G255 -BneWS -: -C${out_color_file}
#   awk '{print $8, $9, $10, $11*$11*0.01 + 0.0200}' ${in_eqt_growclust_file} | gmt plot ${proj} ${reg} -Sc -W0.01c -G255 -BneWS -: -C${out_color_file}
   awk '{print $2 " " $3 " " $4}' ${in_sta_file} | gmt plot ${proj} ${reg} -Si0.4 -W0.1c,black -Ggray -:
   gmt colorbar -C${out_color_file} -Dx6.4i/2.4i/2.5i/0.15i -Bxaf+l"Depth (km)"

   # Project seismicity along one direction
   awk '{print $3, $2, $4, $6, $5, $1}' ${in_eqt_growclust_file} | gmt project -Q -C${strike1_center_lon}/${strike1_center_lat} -A${strike1_angle} -W-${strike1_proj_width}/${strike1_proj_width} -L-${strike1_length}/${strike1_length} > ${out_map_eqt_growclust_depth_file}_seismicity_time_projection${strike1_angle}.txt # plot all events
   awk '{print $3, $2, $4, $6, $5, $1}' ${in_eqt_growclust_file} | gmt project -Q -C${strike2_center_lon}/${strike2_center_lat} -A${strike2_angle} -W-${strike2_proj_width}/${strike2_proj_width} -L-${strike2_length}/${strike2_length} > ${out_map_eqt_growclust_depth_file}_seismicity_time_projection${strike2_angle}.txt # plot all events

   # Projection lines
   gmt project -Q -G1 -C${strike1_center_lon}/${strike1_center_lat} -A${strike1_angle} -L-${strike1_length}/${strike1_length} | gmt plot ${proj} ${reg} -W2,0/0/0,- -Gblack
   gmt text ${proj} ${reg} -F+f24 << EOF
-66.82 18.15 A'
-66.95 17.65 A
EOF
   gmt project -Q -G1 -C${strike2_center_lon}/${strike2_center_lat} -A${strike2_angle} -L-${strike2_length}/${strike2_length} | gmt plot ${proj} ${reg} -W2,0/0/0,- -Gblack
   gmt text ${proj} ${reg} -F+f24 << EOF
-67.25 17.95 B
-66.45 17.90 B'
EOF

   # Add inset
   gmt basemap ${region_inset} ${projection_inset} -Bnews -X0.05i -Y0.05i
   gmt coast ${region_inset} ${projection_inset} -Dl -G255 -Na -W1
   gmt plot ${region_inset} ${projection_inset} -W2,red << EOF
${min_lon} ${min_lat}
${min_lon} ${max_lat}
${max_lon} ${max_lat}
${max_lon} ${min_lat}
${min_lon} ${min_lat}
EOF
gmt end show

##### COLOR BY TIME #####

gmt makecpt -Cviridis -I -T0/7/0.01 > ${out_color_time_file} # LargeAreaEQTransformer - 7 days
start_date=2020-01-07
#gmt makecpt -Cviridis -I -T0/1035/0.01 > ${out_color_time_file} # FullEQTransformer - 1035 days
#gmt makecpt -Cviridis -I -T699/1035/0.01 > ${out_color_time_file} # FullEQTransformer - 699-1035 days (2019-12-01 to 2020-11-01)
#start_date=2018-01-01

gmt begin ${out_map_cat_time_file}
   gmt basemap ${proj} ${reg} -Ba${lat_lon_spacing}fg -BneWS -U
   gmt coast ${proj} ${reg} -W0.25p,black -Na -G255 -Sdeepskyblue -Lg-66.6/17.65+c-66.5/17.65+w20+f+lkm
   awk '{print $2, $3, $1/86400., $5*$5*0.01 + 0.0200}' ${in_cat_file} | gmt plot ${proj} ${reg} -Sc -W0.01c -G255 -BneWS -: -C${out_color_time_file}
   awk '{print $2 " " $3 " " $4}' ${in_sta_file} | gmt plot ${proj} ${reg} -Si0.4 -W0.1c,black -Ggray -:
   gmt colorbar -C${out_color_time_file} -Dx6.4i/2.4i/2.5i/0.15i -Bxaf+l"Days since "${start_date}

   # Projection lines
   gmt project -Q -G1 -C${strike1_center_lon}/${strike1_center_lat} -A${strike1_angle} -L-${strike1_length}/${strike1_length} | gmt plot ${proj} ${reg} -W2,0/0/0,- -Gblack
   gmt text ${proj} ${reg} -F+f24 << EOF
-66.82 18.15 A'
-66.95 17.65 A
EOF
   gmt project -Q -G1 -C${strike2_center_lon}/${strike2_center_lat} -A${strike2_angle} -L-${strike2_length}/${strike2_length} | gmt plot ${proj} ${reg} -W2,0/0/0,- -Gblack
   gmt text ${proj} ${reg} -F+f24 << EOF
-67.25 17.95 B
-66.45 17.90 B'
EOF

   # Add inset
   gmt basemap ${region_inset} ${projection_inset} -Bnews -X0.05i -Y0.05i
   gmt coast ${region_inset} ${projection_inset} -Dl -G255 -Na -W1
   gmt plot ${region_inset} ${projection_inset} -W2,red << EOF
${min_lon} ${min_lat}
${min_lon} ${max_lat}
${max_lon} ${max_lat}
${max_lon} ${min_lat}
${min_lon} ${min_lat}
EOF
gmt end show

gmt begin ${out_map_cat_time_file}_seismicity_time_projection${strike1_angle}
   gmt basemap ${strike1_bproj} ${strike1_brange} -Ba2f -BneWS -U
   awk '{print $4, $3*(-1.0), $10/86400., $9*$9*0.01 + 0.0200}' ${out_map_cat_depth_file}_seismicity_time_projection${strike1_angle}.txt | gmt plot ${strike1_bproj} ${strike1_brange} -Bxa2+l"Length along cross-section A-A' (km)" -Bya2+l"Depth (km)" -BneWS -Sc -W0.01c -G255 -C${out_color_time_file}
   gmt colorbar -C${out_color_time_file} -Dx11.9i/3.0i/2.5i/0.15i -Bxaf+l"Days since "${start_date}
gmt end show

gmt begin ${out_map_cat_time_file}_seismicity_time_projection${strike2_angle}
   gmt basemap ${strike2_bproj} ${strike2_brange} -Ba4f -BneWS -U
   awk '{print $4, $3*(-1.0), $10/86400., $9*$9*0.01 + 0.0200}' ${out_map_cat_depth_file}_seismicity_time_projection${strike2_angle}.txt | gmt plot ${strike2_bproj} ${strike2_brange} -Bxa4+l"Length along cross-section B-B' (km)" -Bya2+l"Depth (km)" -BneWS -Sc -W0.01c -G255 -C${out_color_time_file}
   gmt colorbar -C${out_color_time_file} -Dx15.8i/3.0i/2.5i/0.15i -Bxaf+l"Days since "${start_date}
gmt end show

gmt begin ${out_map_cat_missed_time_file}
   gmt basemap ${proj} ${reg} -Ba${lat_lon_spacing}fg -BneWS -U
   gmt coast ${proj} ${reg} -W0.25p,black -Na -G255 -Sdeepskyblue -Lg-66.6/17.65+c-66.5/17.65+w20+f+lkm
   awk '{print $2, $3, $1/86400., $5*$5*0.01 + 0.0200}' ${in_cat_missed_file} | gmt plot ${proj} ${reg} -Sc -W0.01c -G255 -BneWS -: -C${out_color_time_file}
   awk '{print $2 " " $3 " " $4}' ${in_sta_file} | gmt plot ${proj} ${reg} -Si0.4 -W0.1c,black -Ggray -:
   gmt colorbar -C${out_color_time_file} -Dx6.4i/2.4i/2.5i/0.15i -Bxaf+l"Days since "${start_date}

   # Projection lines
   gmt project -Q -G1 -C${strike1_center_lon}/${strike1_center_lat} -A${strike1_angle} -L-${strike1_length}/${strike1_length} | gmt plot ${proj} ${reg} -W2,0/0/0,- -Gblack
   gmt text ${proj} ${reg} -F+f24 << EOF
-66.82 18.15 A'
-66.95 17.65 A
EOF
   gmt project -Q -G1 -C${strike2_center_lon}/${strike2_center_lat} -A${strike2_angle} -L-${strike2_length}/${strike2_length} | gmt plot ${proj} ${reg} -W2,0/0/0,- -Gblack
   gmt text ${proj} ${reg} -F+f24 << EOF
-67.25 17.95 B
-66.45 17.90 B'
EOF

   # Add inset
   gmt basemap ${region_inset} ${projection_inset} -Bnews -X0.05i -Y0.05i
   gmt coast ${region_inset} ${projection_inset} -Dl -G255 -Na -W1
   gmt plot ${region_inset} ${projection_inset} -W2,red << EOF
${min_lon} ${min_lat}
${min_lon} ${max_lat}
${max_lon} ${max_lat}
${max_lon} ${min_lat}
${min_lon} ${min_lat}
EOF
gmt end show

gmt begin ${out_map_cat_missed_time_file}_seismicity_time_projection${strike1_angle}
   gmt basemap ${strike1_bproj} ${strike1_brange} -Ba2f -BneWS -U
   awk '{print $4, $3*(-1.0), $10/86400., $9*$9*0.01 + 0.0200}' ${out_map_cat_missed_depth_file}_seismicity_time_projection${strike1_angle}.txt | gmt plot ${strike1_bproj} ${strike1_brange} -Bxa2+l"Length along cross-section A-A' (km)" -Bya2+l"Depth (km)" -BneWS -Sc -W0.01c -G255 -C${out_color_time_file}
   gmt colorbar -C${out_color_time_file} -Dx11.9i/3.0i/2.5i/0.15i -Bxaf+l"Days since "${start_date}
gmt end show

gmt begin ${out_map_cat_missed_time_file}_seismicity_time_projection${strike2_angle}
   gmt basemap ${strike2_bproj} ${strike2_brange} -Ba4f -BneWS -U
   awk '{print $4, $3*(-1.0), $10/86400., $9*$9*0.01 + 0.0200}' ${out_map_cat_missed_depth_file}_seismicity_time_projection${strike2_angle}.txt | gmt plot ${strike2_bproj} ${strike2_brange} -Bxa4+l"Length along cross-section B-B' (km)" -Bya2+l"Depth (km)" -BneWS -Sc -W0.01c -G255 -C${out_color_time_file}
   gmt colorbar -C${out_color_time_file} -Dx15.8i/3.0i/2.5i/0.15i -Bxaf+l"Days since "${start_date}
gmt end show

gmt begin ${out_map_eqt_hinv_time_file}
   gmt basemap ${proj} ${reg} -Ba${lat_lon_spacing}fg -BneWS -U
   gmt coast ${proj} ${reg} -W0.25p,black -Na -G255 -Sdeepskyblue -Lg-66.6/17.65+c-66.5/17.65+w20+f+lkm
   awk '{print $2, $3, $1/86400., $5*$5*0.01 + 0.0200}' ${in_eqt_hinv_file} | gmt plot ${proj} ${reg} -Sc -W0.01c -G255 -BneWS -: -C${out_color_time_file}
   awk '{print $2 " " $3 " " $4}' ${in_sta_file} | gmt plot ${proj} ${reg} -Si0.4 -W0.1c,black -Ggray -:
   gmt colorbar -C${out_color_time_file} -Dx6.4i/2.4i/2.5i/0.15i -Bxaf+l"Days since "${start_date}

   # Projection lines
   gmt project -Q -G1 -C${strike1_center_lon}/${strike1_center_lat} -A${strike1_angle} -L-${strike1_length}/${strike1_length} | gmt plot ${proj} ${reg} -W2,0/0/0,- -Gblack
   gmt text ${proj} ${reg} -F+f24 << EOF
-66.82 18.15 A'
-66.95 17.65 A
EOF
   gmt project -Q -G1 -C${strike2_center_lon}/${strike2_center_lat} -A${strike2_angle} -L-${strike2_length}/${strike2_length} | gmt plot ${proj} ${reg} -W2,0/0/0,- -Gblack
   gmt text ${proj} ${reg} -F+f24 << EOF
-67.25 17.95 B
-66.45 17.90 B'
EOF

   # Add inset
   gmt basemap ${region_inset} ${projection_inset} -Bnews -X0.05i -Y0.05i
   gmt coast ${region_inset} ${projection_inset} -Dl -G255 -Na -W1
   gmt plot ${region_inset} ${projection_inset} -W2,red << EOF
${min_lon} ${min_lat}
${min_lon} ${max_lat}
${max_lon} ${max_lat}
${max_lon} ${min_lat}
${min_lon} ${min_lat}
EOF
gmt end show

gmt begin ${out_map_eqt_hinv_time_file}_seismicity_time_projection${strike1_angle}
   gmt basemap ${strike1_bproj} ${strike1_brange} -Ba2f -BneWS -U
   awk '{print $7, $3*(-1.0), $6/86400., $5*$5*0.01 + 0.0200}' ${out_map_eqt_hinv_depth_file}_seismicity_time_projection${strike1_angle}.txt | gmt plot ${strike1_bproj} ${strike1_brange} -Bxa2+l"Length along cross-section A-A' (km)" -Bya2+l"Depth (km)" -BneWS -Sc -W0.01c -G255 -C${out_color_time_file}
   gmt colorbar -C${out_color_time_file} -Dx11.9i/3.0i/2.5i/0.15i -Bxaf+l"Days since "${start_date}
gmt end show

gmt begin ${out_map_eqt_hinv_time_file}_seismicity_time_projection${strike2_angle}
   gmt basemap ${strike2_bproj} ${strike2_brange} -Ba4f -BneWS -U
   awk '{print $7, $3*(-1.0), $6/86400., $5*$5*0.01 + 0.0200}' ${out_map_eqt_hinv_depth_file}_seismicity_time_projection${strike2_angle}.txt | gmt plot ${strike2_bproj} ${strike2_brange} -Bxa4+l"Length along cross-section B-B' (km)" -Bya2+l"Depth (km)" -BneWS -Sc -W0.01c -G255 -C${out_color_time_file}
   gmt colorbar -C${out_color_time_file} -Dx15.8i/3.0i/2.5i/0.15i -Bxaf2+l"Days since "${start_date}
gmt end show

gmt begin ${out_map_eqt_hypoDD_time_file}
   gmt basemap ${proj} ${reg} -Ba${lat_lon_spacing}fg -BneWS -U
   gmt coast ${proj} ${reg} -W0.25p,black -Na -G255 -Sdeepskyblue -Lg-66.6/17.65+c-66.5/17.65+w20+f+lkm
   awk '{print $2, $3, $1/86400., $5*$5*0.01 + 0.0200}' ${in_eqt_hypoDD_file} | gmt plot ${proj} ${reg} -Sc -W0.01c -G255 -BneWS -: -C${out_color_time_file}
   awk '{print $2 " " $3 " " $4}' ${in_sta_file} | gmt plot ${proj} ${reg} -Si0.4 -W0.1c,black -Ggray -:
   gmt colorbar -C${out_color_time_file} -Dx6.4i/2.4i/2.5i/0.15i -Bxaf+l"Days since "${start_date}

   # Projection lines
   gmt project -Q -G1 -C${strike1_center_lon}/${strike1_center_lat} -A${strike1_angle} -L-${strike1_length}/${strike1_length} | gmt plot ${proj} ${reg} -W2,0/0/0,- -Gblack
   gmt text ${proj} ${reg} -F+f24 << EOF
-66.82 18.15 A'
-66.95 17.65 A
EOF
   gmt project -Q -G1 -C${strike2_center_lon}/${strike2_center_lat} -A${strike2_angle} -L-${strike2_length}/${strike2_length} | gmt plot ${proj} ${reg} -W2,0/0/0,- -Gblack
   gmt text ${proj} ${reg} -F+f24 << EOF
-67.25 17.95 B
-66.45 17.90 B'
EOF

   # Add inset
   gmt basemap ${region_inset} ${projection_inset} -Bnews -X0.05i -Y0.05i
   gmt coast ${region_inset} ${projection_inset} -Dl -G255 -Na -W1
   gmt plot ${region_inset} ${projection_inset} -W2,red << EOF
${min_lon} ${min_lat}
${min_lon} ${max_lat}
${max_lon} ${max_lat}
${max_lon} ${min_lat}
${min_lon} ${min_lat}
EOF
gmt end show

gmt begin ${out_map_eqt_hypoDD_time_file}_seismicity_time_projection${strike1_angle}
   gmt basemap ${strike1_bproj} ${strike1_brange} -Ba2f -BneWS -U
   awk '{print $7, $3*(-1.0), $6/86400., $5*$5*0.01 + 0.0200}' ${out_map_eqt_hypoDD_depth_file}_seismicity_time_projection${strike1_angle}.txt | gmt plot ${strike1_bproj} ${strike1_brange} -Bxa2+l"Length along cross-section A-A' (km)" -Bya2+l"Depth (km)" -BneWS -Sc -W0.01c -G255 -C${out_color_time_file}
   gmt colorbar -C${out_color_time_file} -Dx11.9i/3.0i/2.5i/0.15i -Bxaf+l"Days since "${start_date}
gmt end show

gmt begin ${out_map_eqt_hypoDD_time_file}_seismicity_time_projection${strike2_angle}
   gmt basemap ${strike2_bproj} ${strike2_brange} -Ba4f -BneWS -U
   awk '{print $7, $3*(-1.0), $6/86400., $5*$5*0.01 + 0.0200}' ${out_map_eqt_hypoDD_depth_file}_seismicity_time_projection${strike2_angle}.txt | gmt plot ${strike2_bproj} ${strike2_brange} -Bxa4+l"Length along cross-section B-B' (km)" -Bya2+l"Depth (km)" -BneWS -Sc -W0.01c -G255 -C${out_color_time_file}
   gmt colorbar -C${out_color_time_file} -Dx15.8i/3.0i/2.5i/0.15i -Bxaf+l"Days since "${start_date}
gmt end show

gmt begin ${out_map_eqt_growclust_time_file}
   gmt basemap ${proj} ${reg} -Ba${lat_lon_spacing}fg -BneWS -U
   gmt coast ${proj} ${reg} -W0.25p,black -Na -G255 -Sdeepskyblue -Lg-66.6/17.65+c-66.5/17.65+w20+f+lkm
   awk '{print $2, $3, $1/86400., $5*$5*0.01 + 0.0200}' ${in_eqt_growclust_file} | gmt plot ${proj} ${reg} -Sc -W0.01c -G255 -BneWS -: -C${out_color_time_file}
   awk '{print $2 " " $3 " " $4}' ${in_sta_file} | gmt plot ${proj} ${reg} -Si0.4 -W0.1c,black -Ggray -:
   gmt colorbar -C${out_color_time_file} -Dx6.4i/2.4i/2.5i/0.15i -Bxaf+l"Days since "${start_date}

   # Projection lines
   gmt project -Q -G1 -C${strike1_center_lon}/${strike1_center_lat} -A${strike1_angle} -L-${strike1_length}/${strike1_length} | gmt plot ${proj} ${reg} -W2,0/0/0,- -Gblack
   gmt text ${proj} ${reg} -F+f24 << EOF
-66.82 18.15 A'
-66.95 17.65 A
EOF
   gmt project -Q -G1 -C${strike2_center_lon}/${strike2_center_lat} -A${strike2_angle} -L-${strike2_length}/${strike2_length} | gmt plot ${proj} ${reg} -W2,0/0/0,- -Gblack
   gmt text ${proj} ${reg} -F+f24 << EOF
-67.25 17.95 B
-66.45 17.90 B'
EOF

   # Add inset
   gmt basemap ${region_inset} ${projection_inset} -Bnews -X0.05i -Y0.05i
   gmt coast ${region_inset} ${projection_inset} -Dl -G255 -Na -W1
   gmt plot ${region_inset} ${projection_inset} -W2,red << EOF
${min_lon} ${min_lat}
${min_lon} ${max_lat}
${max_lon} ${max_lat}
${max_lon} ${min_lat}
${min_lon} ${min_lat}
EOF
gmt end show

gmt begin ${out_map_eqt_growclust_time_file}_seismicity_time_projection${strike1_angle}
   gmt basemap ${strike1_bproj} ${strike1_brange} -Ba2f -BneWS -U
   awk '{print $7, $3*(-1.0), $6/86400., $5*$5*0.01 + 0.0200}' ${out_map_eqt_growclust_depth_file}_seismicity_time_projection${strike1_angle}.txt | gmt plot ${strike1_bproj} ${strike1_brange} -Bxa2+l"Length along cross-section A-A' (km)" -Bya2+l"Depth (km)" -BneWS -Sc -W0.01c -G255 -C${out_color_time_file}
   gmt colorbar -C${out_color_time_file} -Dx11.9i/3.0i/2.5i/0.15i -Bxaf+l"Days since "${start_date}
gmt end show

gmt begin ${out_map_eqt_growclust_time_file}_seismicity_time_projection${strike2_angle}
   gmt basemap ${strike2_bproj} ${strike2_brange} -Ba4f -BneWS -U
   awk '{print $7, $3*(-1.0), $6/86400., $5*$5*0.01 + 0.0200}' ${out_map_eqt_growclust_depth_file}_seismicity_time_projection${strike2_angle}.txt | gmt plot ${strike2_bproj} ${strike2_brange} -Bxa4+l"Length along cross-section B-B' (km)" -Bya2+l"Depth (km)" -BneWS -Sc -W0.01c -G255 -C${out_color_time_file}
   gmt colorbar -C${out_color_time_file} -Dx15.8i/3.0i/2.5i/0.15i -Bxaf+l"Days since "${start_date}
gmt end show





# basemap
#gmt basemap $proj $reg -G0/150/255 -B0.5/0.5neWS -P -K -O > $out_map_file.ps # with lat/lon
#gmt coast $proj $reg -G255 -W0.25p -Df -Na -W -K -O  >> $out_map_file.ps
#
## Plot catalog eq
#awk '{print $2, $3, $4, $5*$5*0.01 + 0.0000}' ${in_cat_file} | gmt psxy -J $reg -Sc -W0.02c/0/0 -Gblue -B200 -C${out_color_file} -: -V -O -K >> $out_map_file.ps
#gmt psscale -C${out_color_file} -D6.4i/2.4i/2.5i/0.15i -O -K -B5:"Depth (km)": >> $out_map_file.ps # color by depth

# Mainshock
#gmt psxy -J $reg $proj -Sa -Gyellow -W6,0/0/0 -: -V -O -K << END >> $out_map_file.ps
#16.264 -98.457 1
#END

# Add stations
#set stationfile = stations_detect_Ometepec.dat
#awk '{print $2 " " $3 " " $4}' ${stationfile} | gmt psxy -J $reg -Si0.4 -W0/0/0 -Gblack -: -V -O -K >> $out_map_file.ps
#awk '{print $2 " " $3 " 12 0 0 CB " $1}' ${stationfile} | pstext -J $reg -G0/0/0 -D0.0c/0.2c -: -V -O -K >> $out_map_file.ps

# Add scale and compass
#gmt psbasemap $proj $reg -Lf13.4/43.15/43.15/20k+l -V -O -K >> $out_map_file.ps
#gmt basemap $proj $reg -Lf-98.7/16.9/16.9/100k+l -V -O >> $out_map_file.ps

#ps2eps -f -g -l $out_map_file.ps
#gv $out_map_file.eps
