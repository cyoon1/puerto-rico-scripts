import json
import sys
import time
import numpy as np
import utils_hypoinverse as utils_hyp
import utils_magnitude as utils_mag

# Compute local magnitude (Wood-Anderson) for EQTransformer events, following magnitude calibration from matching catalog events


def main(EV_IND_FIRST, EV_IND_LAST):

#   in_hinv_arc_file = '../LargeAreaEQTransformer/association/merged_locate_pr.arc' # use the merged file
##   in_hinv_arc_file = '../LargeAreaEQTransformer/association/SMALLTEST_merged_magcat_locate_pr.arc' # use the merged file
#   in_station_file = '../LargeAreaEQTransformer/station_list_edited.json'
#   #in_calib_file = '../LargeAreaEQTransformer/association/magnitude_calibration_20200107_20200114.txt'
#   in_calib_file = '../LargeAreaEQTransformer/association/constrainwindow120_magnitude_calibration_20200107_20200114.txt'
#   #in_calib_file = '../FullEQTransformer/association/magnitude_calibration_2018_2020.txt'
#   #in_event_file_dir = '../LargeAreaEQTransformer/EventFiles/'
#   in_event_file_dir = '../LargeAreaEQTransformer/EventFiles120/'
#   in_inv_dir = '../LargeAreaEQTransformer/downloads_mseeds/StationXML/'

#   in_hinv_arc_file = '../LargeAreaEQTransformer/REAL/Events/merged_real_locate_pr.arc' # use the merged file
#   in_station_file = '../LargeAreaEQTransformer/station_list_edited.json'
#   in_calib_file = '../LargeAreaEQTransformer/REAL/Events/constrainwindow120_magnitude_calibration_20200107_20200114.txt'
#   in_event_file_dir = '/media/yoon/INT01/PuertoRico/LargeAreaEQTransformer/REALEventFiles120/'
#   in_inv_dir = '../LargeAreaEQTransformer/StationXML/'
##   in_event_file_dir = '../LargeAreaEQTransformer/REALEventFiles120/'
##   in_inv_dir = '../LargeAreaEQTransformer/downloads_mseeds/StationXML/'

#   in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/association/merged_locate_pr.arc' # use the merged file
#   in_station_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/station_list_edited.json'
#   in_calib_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/association/constrainwindow120_magnitude_calibration_2018_2020.txt'
#   in_event_file_dir = '/media/yoon/INT01/PuertoRico/FullEQTransformer/EventFiles120/'
#   in_inv_dir = '/media/yoon/INT01/PuertoRico/FullEQTransformer/StationXML/'

#   in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/REAL/HYPOINVERSE/merged_real_locate_pr.arc' # use the merged file
#   in_station_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/station_list_edited.json'
#   in_calib_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/REAL/HYPOINVERSE/constrainwindow120_magnitude_calibration_2018_2020.txt'
#   in_event_file_dir = '/media/yoon/INT01/PuertoRico/FullEQTransformer/REALEventFiles120/'
#   in_inv_dir = '/media/yoon/INT01/PuertoRico/FullEQTransformer/StationXML/'

#   in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/station_list_edited.json'
#   in_inv_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/StationXML/'
#   in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELPRSN/merged_real_locate_pr.arc' # use the merged file
#   in_calib_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELPRSN/magnitude_calibration_us_EQT_20200107_20200114.txt'
#   in_event_file_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELPRSN/EventFiles/'

#   in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/station_list_edited.json'
#   in_inv_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/StationXML/'
#   in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/merged_real_locate_pr.arc' # use the merged file
#   in_calib_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/magnitude_calibration_us_EQT_20200107_20200114.txt'
#   in_event_file_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/downloads_mseeds/'
##   in_event_file_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/EventFiles/'

#   in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/station_list_edited.json'
#   in_inv_dir = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/StationXML/'
#   in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELPRSN/merged_real_locate_pr.arc' # use the merged file
#   in_calib_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELPRSN/magnitude_calibration_us_EQT_20191228_20200114.txt'
#   in_event_file_dir = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELPRSN/EventFiles/'

#   in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/station_list_edited.json'
#   in_inv_dir = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/StationXML/'
#   in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELZHANG/merged_real_locate_pr.arc' # use the merged file
#   in_calib_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELZHANG/magnitude_calibration_us_EQT_20191228_20200114.txt'
#   in_event_file_dir = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELZHANG/EventFiles/'

#   in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/station_list_edited.json'
#   in_inv_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/StationXML/'
#   in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELPRSN/merged_real_locate_pr.arc' # use the merged file
#   in_calib_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELPRSN/magnitude_calibration_us_EQT_20180101_20210601.txt'
#   in_event_file_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELPRSN/EventFiles/'

#   in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/station_list_edited.json'
#   in_inv_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/StationXML/'
#   in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/merged_real_locate_pr.arc' # use the merged file
#   in_calib_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/magnitude_calibration_us_EQT_20180101_20210601.txt'
#   in_event_file_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/EventFiles/'

#   in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/station_list_edited.json'
#   in_inv_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/StationXML/'
#   in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/merged_real_locate_pr.arc' # use the merged file
#   in_calib_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/magnitude_calibration_us_EQT_20180101_20211001.txt'
#   in_event_file_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/EventFiles/'

#   in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/station_list_edited.json'
#   in_inv_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/StationXML/'
#   in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/merged_real_locate_pr.arc' # use the merged file
#   in_calib_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/magnitude_calibration_us_EQT_20180101_20220101.txt'
#   in_event_file_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/downloads_mseeds/'

#   in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/station_list_edited.json'
#   in_inv_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/StationXML/'
#   in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/merged_real_locate_pr.arc' # use the merged file
#   in_calib_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/magnitude_calibration_us_EQT_20180101_20220101.txt'
#   in_event_file_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/downloads_mseeds/'

#   base_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200108/'
#   hinv_dir = base_dir+'REAL_VELZHANG/DIRECTHYPOSVI_TEST10/'
#   in_station_file = base_dir+'station_list_edited.json'
#   in_inv_dir = base_dir+'StationXML/'
#   in_event_file_dir = base_dir+'downloads_mseeds/'
#   in_hinv_arc_file = hinv_dir+'hyposvi_real_locate_pr.arc'
#   in_calib_file = hinv_dir+'hyposvi_magnitude_calibration_us_EQT_20200107_20200108.txt'

   base_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20230101/'
   hinv_dir = base_dir+'REAL_VELZHANG/DIRECTHYPOSVI/'
   in_station_file = base_dir+'station_list_edited.json'
   in_inv_dir = base_dir+'StationXML/'
   in_event_file_dir = base_dir+'downloads_mseeds/'
   in_hinv_arc_file = hinv_dir+'hyposvi_real_locate_pr.arc'
   in_calib_file = hinv_dir+'hyposvi_magnitude_calibration_us_EQT_20180101_20230101.txt'

   # Get station data
   stations_ = json.load(open(in_station_file))

   # Get station inventory for instrument response
   inv_map = utils_mag.get_station_inventory(in_inv_dir, stations_)

   # Get all event data from HYPOINVERSE arc file
   map_events = utils_mag.get_events_map(in_hinv_arc_file, stations_)

   # Get all event phase data from HYPOINVERSE arc file
   [event_dict, ev_id_list] = utils_hyp.get_event_phase_data_hypoinverse_file(in_hinv_arc_file)

   # Distance correction, slope and intercept - get this from matching catalog events
   # Run calibrate_local_magnitude_wood_anderson.py
   #   dist_corr_slope =  0.725917698378 # TEST 20200107-20200114
   #   dist_corr_icpt =  3.76802638352 # TEST 20200107-20200114
   #   dist_corr_slope = 1.38657435857 # PHASETEST 20200107-20200114
   #   dist_corr_icpt = 2.58957042452 # PHASETEST 20200107-20200114
   #dist_corr_slope = 1.41556597881 # NOFILTERPHASETEST 20200107-20200114
   #dist_corr_icpt = 2.49115637769 # NOFILTERPHASETEST 20200107-20200114
   #dist_corr_slope = 1.18039588465 # NOFILTERPHASETEST 20200107-20200114 StationXML
   #dist_corr_icpt = 3.00651850763 # NOFILTERPHASETEST 20200107-20200114 StationXML

   # Distance correction, slope (common) and intercept (per station) - get this from matching catalog events
   order_sta = []
   dist_corr_icpt = []
   with open(in_calib_file, 'r') as fin:
      for line in fin:
         if (line[0] == 'k'):
            dist_corr_slope = float(line.split()[1])
         else:
            split_line = line.split()
            order_sta.append(split_line[0])
            dist_corr_icpt.append(float(split_line[1]))

   # Loop over all events
   t0 = time.time()
   map_return_events = {}
   for kk in range(EV_IND_FIRST, EV_IND_LAST):
      ev_id = ev_id_list[kk]
      median_ML_Richter = utils_mag.calculate_local_magnitude(in_event_file_dir, event_dict, ev_id, order_sta, map_events,
         inv_map, dist_corr_slope, dist_corr_icpt)
      map_return_events[ev_id] = map_events[ev_id]
      map_return_events[ev_id].append(median_ML_Richter) # ML is the last item in the map values
   print("Total runtime magnitude calculation: ", time.time()-t0)

   return map_return_events


if __name__ == "__main__":

   if len(sys.argv) != 3:
      print("Usage: python PARTIALcompute_local_magnitude_wood_anderson.py <start_ev_ind> <end_ev_ind>")
      sys.exit(1)

   EV_IND_FIRST = int(sys.argv[1])
   EV_IND_LAST = int(sys.argv[2])
   print("PROCESSING:", EV_IND_FIRST, EV_IND_LAST)

   main(EV_IND_FIRST, EV_IND_LAST)


