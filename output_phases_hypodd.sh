#!/bin/bash

# Output hypoDD station and phase files
# hypoDD programs need to be installed first

sta_dir=/media/yoon/INT01/PuertoRico/EQT_20180101_20230101
in_hinv_sta_file=pr_stations.sta
out_hdd_sta_file=pr_stations_hypoDD.txt

in_ph_dir=${sta_dir}/REAL_VELZHANG/DIRECTHYPOSVI
in_hinv_ph_file=combined_hyposvi_real_magcat_locate_keep_pr.arc
out_hdd_ph_file=pr_phases_hypoDD.txt

cd ${sta_dir}
hista2ddsta < ${in_hinv_sta_file} > ${out_hdd_sta_file}

cp ${in_ph_dir}/${in_hinv_ph_file} combined_pr.arc # limit on file name length for ncsn2pha, use temp file
ncsn2pha combined_pr.arc ${out_hdd_ph_file}
mv ${out_hdd_ph_file} ${in_ph_dir}
rm combined_pr.arc

