import numpy as np
import subprocess
import multiprocessing


# Script to parallelize output plots of waveforms and cutting event files

def get_list_of_block_indices(i_start, i_end, nblock_i):
   block_indices = []
   for iblock in range(nblock_i):
      i_now_start = i_start[iblock]
      i_now_end = i_end[iblock]
      print("\n # i_now_start = ", i_now_start, ", i_now_end = ", i_now_end)

      block_indices.append([i_now_start, i_now_end])
   return block_indices


def run_partial_script(block_index):
   print("block_index = ", block_index)

#   print('python PARTIALcut_event_files.py %s %s' % (block_index[0], block_index[1]))
#   process = subprocess.Popen(('python PARTIALcut_event_files.py %s %s' % (block_index[0], block_index[1])), stdout=subprocess.PIPE, shell=True)

   print('python CPARTIALplot_event_waveform_gathers.py %s %s' % (block_index[0], block_index[1]))
   process = subprocess.Popen(('python CPARTIALplot_event_waveform_gathers.py %s %s' % (block_index[0], block_index[1])), stdout=subprocess.PIPE, shell=True)

   output, error = process.communicate()
   print(output.decode('UTF-8').strip())


# i: number of events
#num_i = 100939
#num_i = 17950
#num_i = 12499
#num_i = 13931
#num_i = 523408
#num_i = 693408

## EQT_20200107_20200114
##num_i = 7924 # HYPOINVERSE_VELPRSN
#num_i = 7926 # HYPOINVERSE_VELZHANG

## EQT_20191228_20200114
#num_i = 9132 # HYPOINVERSE_VELPRSN
##num_i = 9169 # HYPOINVERSE_VELZHANG

# EQT_20180101_20210601
#num_i = 96047 # HYPOINVERSE_VELPRSN
#num_i = 96236 # HYPOINVERSE_VELZHANG

# EQT_20180101_20211001
#num_i = 100906 # HYPOINVERSE_VELZHANG

# EQT_20200107_20200108
#num_i = 1188 # TEST00
#num_i = 1325 # TEST01
#num_i = 930 # TEST02
#num_i = 1354 # TEST03
#num_i = 1419 # TEST04
#num_i = 1529 # TEST05
#num_i = 1612 # TEST06
#num_i = 1296 # TEST07
#num_i = 1257 # TEST09
#num_i = 1587 # TEST10
#num_i = 928 # TEST11
#num_i = 1774 # TEST12

#num_i = 5094
num_i = 160497

# number of parallel blocks in each dimension
#nblock_i = 1
#nblock_i = 4
#nblock_i = 8
#nblock_i = 20
nblock_i = 48
#nblock_i = 72
#nblock_i = 84

# number of elements per block
blocksize_i = int(num_i) // int(nblock_i)
remain_i = int(num_i) % int(nblock_i)

print("num_i = ", num_i)
print("nblock_i = ", nblock_i)
print("blocksize_i = ", blocksize_i)
print("remain_i = ", remain_i)

# starting and ending indices for each block
i_start = blocksize_i*np.arange(0,nblock_i)
i_end = i_start + blocksize_i
i_end[-1] += remain_i
print("i_start = ", i_start, ", i_end = ", i_end)


#### Parallel multi-block calculation
block_indices = get_list_of_block_indices(i_start, i_end, nblock_i)
print("len(block_indices) = ", len(block_indices))

num_cores = multiprocessing.cpu_count()
print("num_cores = ", num_cores)

pool = multiprocessing.Pool(processes=num_cores)
pool.map(run_partial_script, block_indices)

