import numpy as np

base_dir = '../Catalog/EQT_20180101_20230101/'
input_file = base_dir+'EQT_20180101_20230101_REAL_VELZHANG_DIRECTHYPOSVI_diam.txt'
input_unc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20230101/Eiko_out/EQT_20180101_20230101_REAL_DIRECTHYPOSVI_CombinedCatalog.csv'
output_file = base_dir+'EQT_20180101_20230101_REAL_VELZHANG_DIRECTHYPOSVI_diam_uncertainty.txt'

unc_map = {}
with open(input_unc_file, 'r') as funcert:
   for line in funcert:
      if line.startswith('EventID'):
         continue
      split_line = line.split(',')
      curr_evid = int(split_line[0])
      unc_map[curr_evid] = [float(split_line[5]), float(split_line[6]), float(split_line[7]), float(split_line[8])]

fout = open(output_file, 'w')
with open(input_file, 'r') as fin:
   for line in fin:
      split_line = line.strip('\n').split()
      ev_id = int(split_line[6])
      ev_unc = unc_map[ev_id]
      fout.write(('%s %10.8f %10.8f %10.8f %10.8f\n') % (line.strip('\n'), ev_unc[0], ev_unc[1], ev_unc[2], ev_unc[3]))
fout.close()
