from obspy import read
from obspy.io.sac import SACTrace
import datetime
import sys
import os
import json
import glob
import utils_hypoinverse as utils_hyp



if len(sys.argv) != 3:
   print("Usage: python PARTIALcut_event_files.py <start_ind> <end_ind>")
   sys.exit(1)

IND_FIRST = int(sys.argv[1])
IND_LAST = int(sys.argv[2])
print("PROCESSING:", IND_FIRST, IND_LAST)
   
# Inputs
###in_hinv_arc_file = '../LargeAreaEQTransformer/association/merged_locate_pr.arc' # use the merged file
##in_hinv_arc_file = '../LargeAreaEQTransformer/REAL/Events/merged_real_locate_pr.arc' # use the merged file
##in_hinv_arc_file = '../LargeAreaEQTransformer/20200107_20200114_Model1/REAL/Events/merged_real_magmiss_locate_pr.arc' # use the merged file
##in_station_file = '../LargeAreaEQTransformer/station_list_edited.json'
##in_mseed_dir = '../LargeAreaEQTransformer/downloads_mseeds/'
##in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/association/merged_locate_pr.arc' # use the merged file
##in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/REAL/HYPOINVERSE/merged_real_locate_pr.arc' # use the merged file
##in_station_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/station_list_edited.json'
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/20180101_20201101_Model1/REAL/HYPOINVERSE/merged_real_magmiss_locate_pr.arc' # use the merged file
#in_station_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/20180101_20201101_Model1/station_list_edited_20180101_20201101.json'
#in_mseed_dir = '/media/yoon/INT01/PuertoRico/FullEQTransformer/downloads_mseeds/'
##wtime_before = 30 # time window before origin time (s)
##wtime_after = 60 # time window after origin time (s)
##wtime_before = 15 # time window before origin time (s)
##wtime_after = 45 # time window after origin time (s)
#wtime_before = 15 # time window before origin time (s)
#wtime_after = 105 # time window after origin time (s)


# Outputs
#out_dir = '../LargeAreaEQTransformer/EventFiles/'
#out_dir = '../LargeAreaEQTransformer/EventFiles120/'
#out_dir = '../LargeAreaEQTransformer/EventFiles120Filtered/'
#out_dir = '../LargeAreaEQTransformer/REALEventFiles120/'
#out_dir = '/media/yoon/INT01/PuertoRico/LargeAreaEQTransformer/REALEventFiles120/'
#out_dir = '/media/yoon/INT01/PuertoRico/LargeAreaEQTransformer/20200107_20200114_Model1/MissedREALEventFiles120/'
#out_dir = '/media/yoon/INT01/PuertoRico/FullEQTransformer/EventFiles/'
#out_dir = '/media/yoon/INT01/PuertoRico/FullEQTransformer/EventFiles120/'
#out_dir = '/media/yoon/INT01/PuertoRico/FullEQTransformer/EventFiles120Filtered/'
#out_dir = '/media/yoon/INT01/PuertoRico/FullEQTransformer/REALEventFiles120/'
#out_dir = '/media/yoon/INT01/PuertoRico/FullEQTransformer/20180101_20201101_Model1/MissedREALEventFiles120/'


#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/station_list_edited.json'
#in_mseed_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/downloads_mseeds/'
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELPRSN/merged_real_locate_pr.arc' # use the merged file
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELPRSN/EventFiles/'

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/station_list_edited.json'
#in_mseed_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/downloads_mseeds/'
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELPRSN/merged_real_magmiss_locate_pr.arc' # use the merged file
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELPRSN/MissedEventFiles/'

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/station_list_edited.json'
#in_mseed_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/downloads_mseeds/'
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/merged_real_locate_pr.arc' # use the merged file
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/EventFiles/'

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/station_list_edited.json'
#in_mseed_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/downloads_mseeds/'
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/merged_real_magmiss_locate_pr.arc' # use the merged file
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/MissedEventFiles/'

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/station_list_edited.json'
#in_mseed_dir = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/downloads_mseeds/'
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELPRSN/merged_real_locate_pr.arc' # use the merged file
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELPRSN/EventFiles/'

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/station_list_edited.json'
#in_mseed_dir = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/downloads_mseeds/'
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELPRSN/merged_real_magmiss_locate_pr.arc' # use the merged file
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELPRSN/MissedEventFiles/'

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/station_list_edited.json'
#in_mseed_dir = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/downloads_mseeds/'
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELZHANG/merged_real_locate_pr.arc' # use the merged file
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELZHANG/EventFiles/'

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/station_list_edited.json'
#in_mseed_dir = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/downloads_mseeds/'
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELZHANG/merged_real_magmiss_locate_pr.arc' # use the merged file
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELZHANG/MissedEventFiles/'

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/station_list_edited.json'
#in_mseed_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/downloads_mseeds/'
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELPRSN/merged_real_locate_pr.arc' # use the merged file
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELPRSN/EventFiles/'

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/station_list_edited.json'
#in_mseed_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/downloads_mseeds/'
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELPRSN/merged_real_magmiss_locate_pr.arc' # use the merged file
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELPRSN/MissedEventFiles/'

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/station_list_edited.json'
#in_mseed_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/downloads_mseeds/'
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/merged_real_locate_pr.arc' # use the merged file
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/EventFiles/'

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/station_list_edited.json'
#in_mseed_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/downloads_mseeds/'
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/merged_real_magmiss_locate_pr.arc' # use the merged file
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/MissedEventFiles/'

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/station_list_edited.json'
#in_mseed_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/downloads_mseeds/'
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/merged_real_magmiss_locate_pr.arc' # use the merged file
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/MissedEventFiles/'

base_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200108/'
hinv_dir = base_dir+'REAL_VELZHANG/DIRECTHYPOSVI_TEST10/'
in_station_file = base_dir+'station_list_edited.json'
in_mseed_dir = base_dir+'downloads_mseeds/'
in_hinv_arc_file = hinv_dir+'combined_hyposvi_real_magcat_locate_pr.arc'
out_dir = hinv_dir+'EventFiles/'





wtime_before = 15 # time window before origin time (s)
wtime_after = 105 # time window after origin time (s)

if not os.path.exists(out_dir):
   os.makedirs(out_dir)

# Read in and store event and phase data from HYPOINVERSE arc file
[event_dict, ev_id_list] = utils_hyp.get_event_phase_data_hypoinverse_file(in_hinv_arc_file)


print("\n ------------------- OUTPUT CUT EVENT FILES --------------------------\n")
stations_ = json.load(open(in_station_file))
for kk in range(IND_FIRST, IND_LAST):
   curr_ev = event_dict[ev_id_list[kk]]

   # Get time window for event files
   ev_origin_time = curr_ev['event'][0]
   start_time = ev_origin_time - wtime_before
   end_time = ev_origin_time + wtime_after
   print(kk, ev_id_list[kk], ev_origin_time, start_time, end_time)

   # Read in time window for event files from daylong mseed files
#   st_all = Stream()
   for sta,val in stations_.items():
      net = val['network']
      ev_origin_date = datetime.datetime(ev_origin_time.year, ev_origin_time.month, ev_origin_time.day)
      origin_date_str = datetime.datetime.strftime(ev_origin_date, '%Y%m%d')
      mseed_files = glob.glob(in_mseed_dir+'*/'+sta+'/'+net+'.'+sta+'*__'+origin_date_str+'T000000Z'+'__*')
      if (len(mseed_files) > 0):
         st = read(in_mseed_dir+'*/'+sta+'/'+net+'.'+sta+'*__'+origin_date_str+'T000000Z'+'__*mseed')
         st_slice = st.slice(start_time, end_time)

#         # Filter the cut data
#         st_slice.detrend(type='demean')
#         st_slice.detrend(type='linear')
#         st_slice.filter('bandpass', freqmin=2, freqmax=10, corners=2, zerophase=False)

         # Output event files in SAC format, in their own event directory
         out_ev_dir = out_dir+str(ev_id_list[kk])+'/'
         if not os.path.exists(out_ev_dir):
            os.makedirs(out_ev_dir)
         output_event_name = out_ev_dir+'event'+format(ev_id_list[kk],'06d')+'_'+ev_origin_time.strftime('%Y%m%dT%H%M%S.%f')
         for tr in st_slice:
            output_file_name = output_event_name+'_'+tr.stats.network+'.'+tr.stats.station+'.'+tr.stats.channel+'.sac'
            tr.write(output_file_name, format='SAC')

