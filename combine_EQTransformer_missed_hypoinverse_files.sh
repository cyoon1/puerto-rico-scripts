#!/bin/bash

# combine_EQTransformer_missed_hypoinverse_files.sh
# combine hypoinverse arc/sum files from EQTransformer and missed catalog events (should already have magnitudes)

#event_dir=../LargeAreaEQTransformer/20200107_20200114_Model1/REAL/Events/
#event_dir=/media/yoon/INT01/PuertoRico/FullEQTransformer/20180101_20201101_Model1/REAL/HYPOINVERSE/

#event_dir=/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELPRSN/
#event_dir=/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/
#event_dir=/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELPRSN/
#event_dir=/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELZHANG/
#event_dir=/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELPRSN/
#event_dir=/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/
#event_dir=/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/
#event_dir=/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/
#event_dir=/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/
#event_dir=/media/yoon/INT01/PuertoRico/EQT_20200107_20200108/REAL_VELZHANG/DIRECTHYPOSVI_TEST10/
event_dir=/media/yoon/INT01/PuertoRico/EQT_20180101_20230101/REAL_VELZHANG/DIRECTHYPOSVI/

cd ${event_dir}

#cat merged_real_magcat_locate_pr.arc merged_real_magcatmiss_locate_pr.arc > combined_real_magcat_locate_pr.arc
#cat merged_real_magcat_locate_pr.sum merged_real_magcatmiss_locate_pr.sum > combined_real_magcat_locate_pr.sum

#cat hyposvi_real_magcat_locate_pr.arc hyposvi_cat_real_magcatmiss_locate_pr.arc > combined_hyposvi_real_magcat_locate_pr.arc
#cat hyposvi_real_magcat_locate_pr.sum hyposvi_cat_real_magcatmiss_locate_pr.sum > combined_hyposvi_real_magcat_locate_pr.sum

cat hyposvi_real_magcat_locate_keep_pr.arc hyposvi_cat_real_magcatmiss_locate_pr.arc > combined_hyposvi_real_magcat_locate_keep_pr.arc
cat hyposvi_real_magcat_locate_keep_pr.sum hyposvi_cat_real_magcatmiss_locate_pr.sum > combined_hyposvi_real_magcat_locate_keep_pr.sum

