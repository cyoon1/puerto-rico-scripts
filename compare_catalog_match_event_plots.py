from obspy.geodetics.base import gps2dist_azimuth
import utils_hypoinverse as utils_hyp
import datetime
import math
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rcParams
from matplotlib.gridspec import GridSpec

#rcParams.update({'font.size': 24})
rcParams.update({'font.size': 20})
rcParams['font.sans-serif'] = "Helvetica"
rcParams['font.family'] = "sans-serif"

# Plot catalog vs quakes2AWS
def plot_compare_vs_catalog(cat_arr, new_arr, size_plot_arr, x_min, x_max, y_min, y_max, x_ticks, y_ticks, x_label, y_label, out_file):
#   size_plot_arr = np.multiply(size_arr, 10)
   equal_line = np.linspace(x_min, x_max, 100)
   plt.figure(num=0, figsize=(6,6))
   plt.clf()
   plt.scatter(cat_arr, new_arr, size_plot_arr, facecolors='none', edgecolors='k', linewidths=0.1)
   plt.plot(equal_line, equal_line, '--', color='k', linewidth=0.8)
   plt.axis('scaled')
   plt.xlim([x_min, x_max])
   plt.ylim([y_min, y_max])
   plt.xticks(x_ticks)
   plt.yticks(y_ticks)
   plt.xlabel(x_label)
   plt.ylabel(y_label)
   plt.tight_layout()
   plt.savefig(out_file)


# Plot (quakes2AWS-catalog) residual vs catalog
def plot_compare_residual_vs_catalog(cat_arr, res_arr, size_plot_arr, x_min, x_max, y_min, y_max, x_ticks, y_ticks, x_label, y_label, res_bins, out_file):
#   size_plot_arr = np.multiply(size_arr, 10)
   mean_res_arr = np.mean(res_arr)
   std_res_arr = np.std(res_arr)
   print("Min residual: ", min(res_arr), out_file)
   print("Max residual: ", max(res_arr), out_file)
   print("Mean residual: ", mean_res_arr, out_file)
   print("Std residual: ", std_res_arr, out_file)
   print("-------------------------")
   equal_line = np.linspace(x_min, x_max, 100)
   zero_line = np.zeros(100)
   fig = plt.figure(figsize=(8,6))

   # https://stackoverflow.com/questions/37008112/matplotlib-plotting-histogram-plot-just-above-scatter-plot
   gs = GridSpec(5,8)
   ax_joint = fig.add_subplot(gs[0:5,0:6])
   ax_marg_y = fig.add_subplot(gs[0:5,6:8], sharey=ax_joint)

   ax_joint.scatter(cat_arr, res_arr, size_plot_arr, facecolors='none', edgecolors='k', linewidths=0.1)
   ax_joint.plot(equal_line, zero_line, '--', color='k', linewidth=0.8)
   ax_marg_y.hist(res_arr, bins=res_bins, orientation="horizontal", color=['white'], edgecolor='black', linewidth=0.3)

   ax_joint.set_xlim([x_min, x_max])
   ax_joint.set_ylim([y_min, y_max])
   ax_joint.set_xticks(x_ticks)
   ax_joint.set_yticks(y_ticks)
   ax_joint.set_xlabel(x_label)
   ax_joint.set_ylabel(y_label)

   # Turn off tick labels on marginals
   plt.setp(ax_marg_y.get_yticklabels(), visible=False)

   # Statistics
   text_res = "Mean: "+f"{mean_res_arr:.2f}"+"\nStd: "+f"{std_res_arr:.2f}"
   plt.figtext(0.85, 0.72, text_res, rotation=270, fontsize=16)

   # Set labels on marginals
   ax_marg_y.set_xlabel('Number of\n events')

   plt.tight_layout()
   plt.savefig(out_file)



#in_match_file = '../EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/us_events_MATCH_magcat_EQT_20180101_20220101.txt'
#alg1_str = 'us Catalog'
#alg2_str = 'New'
#catalog_start_time = '2018-01-01T00:00:00'
#out_dir = '../EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/CompPlots/'
#out_str = 'EQT_20180101_20220101_MATCH_uscatalog_new'

#in_match_file = '../EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/events_MATCH_magcat_EQT_20180101_20220101.txt'
#alg1_str = ' Catalog'
#alg2_str = 'New'
#catalog_start_time = '2018-01-01T00:00:00'
#out_dir = '../EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/CompPlots/'
#out_str = 'EQT_20180101_20220101_MATCH_catalog_new'

#in_match_file = '../EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/us_events_MATCH_magcat_EQT_20180101_20220101.txt'
#alg1_str = 'us Catalog'
#alg2_str = 'New'
#catalog_start_time = '2018-01-01T00:00:00'
#out_dir = '../EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/CompPlots/'
#out_str = 'EQT_20180101_20220101_MATCH_uscatalog_new'

#in_match_file = '../EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/events_MATCH_magcat_EQT_20180101_20220101.txt'
#alg1_str = ' Catalog'
#alg2_str = 'New'
#catalog_start_time = '2018-01-01T00:00:00'
#out_dir = '../EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/CompPlots/'
#out_str = 'EQT_20180101_20220101_MATCH_catalog_new'

#in_match_file = '../Catalog/QuakeFlow/us_events_MATCH_magcat_QuakeFlow_GAMMA_20180501_20211101.txt'
#alg1_str = 'us Catalog'
#alg2_str = 'QuakeFlow GAMMA'
#catalog_start_time = '2018-01-01T00:00:00'
#out_dir = '../EQT_20180101_20220101/QuakeFlow/'
#out_str = 'QuakeFlow_GAMMA_20180501_20211101_MATCH_uscatalog_new'

#in_match_file = '../Catalog/QuakeFlow/events_MATCH_magcat_QuakeFlow_GAMMA_20180501_20211101.txt'
#alg1_str = 'Catalog'
#alg2_str = 'QuakeFlow GAMMA'
#catalog_start_time = '2018-01-01T00:00:00'
#out_dir = '../EQT_20180101_20220101/QuakeFlow/'
#out_str = 'QuakeFlow_GAMMA_20180501_20211101_MATCH_catalog_new'

#in_match_file = '../Catalog/QuakeFlow/us_events_MATCH_magcat_QuakeFlow_HYPODD_20180501_20211101.txt'
#alg1_str = 'us Catalog'
#alg2_str = 'QuakeFlow HYPODD'
#catalog_start_time = '2018-01-01T00:00:00'
#out_dir = '../EQT_20180101_20220101/QuakeFlow/'
#out_str = 'QuakeFlow_HYPODD_20180501_20211101_MATCH_uscatalog_new'

#in_match_file = '../Catalog/QuakeFlow/events_MATCH_magcat_QuakeFlow_HYPODD_20180501_20211101.txt'
#alg1_str = 'Catalog'
#alg2_str = 'QuakeFlow HYPODD'
#catalog_start_time = '2018-01-01T00:00:00'
#out_dir = '../EQT_20180101_20220101/QuakeFlow/'
#out_str = 'QuakeFlow_HYPODD_20180501_20211101_MATCH_catalog_new'

#in_match_file = '../EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/QuakeFlow_GAMMA_events_MATCH_magcat_EQT_20180101_20220101.txt'
#alg1_str = 'QuakeFlow GAMMA'
#alg2_str = 'EQT-REAL-HYPOINVERSE'
#catalog_start_time = '2018-01-01T00:00:00'
#out_dir = '../EQT_20180101_20220101/QuakeFlow/'
#out_str = 'QuakeFlow_GAMMA_EQT_20180501_20211101_MATCH_catalog_new'

#in_match_file = '../EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/QuakeFlow_HYPODD_events_MATCH_magcat_EQT_20180101_20220101.txt'
#alg1_str = 'QuakeFlow HYPODD'
#alg2_str = 'EQT-REAL-HYPOINVERSE'
#catalog_start_time = '2018-01-01T00:00:00'
#out_dir = '../EQT_20180101_20220101/QuakeFlow/'
#out_str = 'QuakeFlow_HYPODD_EQT_20180501_20211101_MATCH_catalog_new'

in_match_file = '../EQT_20180101_20230101/REAL_VELZHANG/DIRECTHYPOSVI/events_MATCH_magcat_DIRECTHYPOSVI_EQT_20180101_20230101.txt'
alg1_str = ' Catalog'
alg2_str = 'HypoSVI'
catalog_start_time = '2018-01-01T00:00:00'
out_dir = '../EQT_20180101_20230101/REAL_VELZHANG/DIRECTHYPOSVI/CompPlots/'
out_str = 'EQT_20180101_20230101_MATCH_catalog_hyposvi'

new_arr_ot = []
new_arr_lat = []
new_arr_ot = []
new_arr_lat = []
new_arr_lon = []
new_arr_depth = []
new_arr_mag = []

cat_arr_ot = []
cat_arr_lat = []
cat_arr_lon = []
cat_arr_depth = []
cat_arr_mag = []

epidist_new_cat = []
hypodist_new_cat = []
diff_arr_ot = []
diff_arr_lat = []
diff_arr_lon = []
diff_arr_depth = []
diff_arr_mag = []

min_lat = 17.6
max_lat = 18.3
min_lon = -67.3
max_lon = -66.4
min_depth=-1.
max_depth=40.

catalog_ref_time = datetime.datetime.strptime(catalog_start_time, '%Y-%m-%dT%H:%M:%S')
with open(in_match_file, 'r') as fin:
   for line in fin:

      #------- HYPOINVERSE MATCH FILE-------
      year = int(line[0:4])
      month = int(line[4:6])
      day = int(line[6:8])
      hour = int(line[8:10])
      minute = int(line[10:12])
      second = float(line[12:14]) + 0.01*float(line[14:16])
      origin_time_nosec = datetime.datetime(year, month, day, hour, minute)
      origin_delta = datetime.timedelta(seconds=second)
      origin_time = origin_time_nosec + origin_delta
      [lat_deg, lon_deg, depth_km] = utils_hyp.get_lat_lon_depth_hypoinverse_file(line) #CY

#      new_ot = (datetime.datetime.strptime(split_line[1], "%Y-%m-%dT%H:%M:%S.%f") - catalog_ref_time).total_seconds()
      new_ot = (origin_time - catalog_ref_time).total_seconds()
      new_lat = lat_deg
      new_lon = lon_deg
      new_depth = depth_km
      new_mag = 0.01*float(line[147:150])

#      cat_line = line[180:]
      cat_line = line[151:]
      split_line = cat_line.split()
      cat_ot = (datetime.datetime.strptime(split_line[0], "%Y-%m-%dT%H:%M:%S.%f") - catalog_ref_time).total_seconds()
      cat_lat = float(split_line[1])
      cat_lon = float(split_line[2])
      cat_depth = float(split_line[3])
      cat_mag = float(split_line[4])


#      #------- QUAKEFLOW GAMMA MATCH FILE-------
#      split_line = line.split()
#      origin_time = datetime.datetime.strptime(split_line[0], '%Y-%m-%dT%H:%M:%S.%f')
#      lat_deg = float(split_line[3])
#      lon_deg = float(split_line[2])
#      depth_km = 0.001*float(split_line[4])
#      mag = float(split_line[1])
#
#      new_ot = (origin_time - catalog_ref_time).total_seconds()
#      new_lat = lat_deg
#      new_lon = lon_deg
#      new_depth = depth_km
#      new_mag = mag
#
#      cat_ot = (datetime.datetime.strptime(split_line[8], "%Y-%m-%dT%H:%M:%S.%f") - catalog_ref_time).total_seconds()
#      cat_lat = float(split_line[9])
#      cat_lon = float(split_line[10])
#      cat_depth = float(split_line[11])
#      cat_mag = float(split_line[12])


#      #------- QUAKEFLOW HYPODD MATCH FILE-------
#      split_line = line.split()
#      year = int(split_line[10])
#      month = int(split_line[11])
#      day = int(split_line[12])
#      hour = int(split_line[13])
#      minute = int(split_line[14])
#      second = float(split_line[15])
#      origin_time_nosec = datetime.datetime(year, month, day, hour, minute)
#      origin_delta = datetime.timedelta(seconds=second)
#      origin_time = origin_time_nosec + origin_delta
#
#      lat_deg = float(split_line[1])
#      lon_deg = float(split_line[2])
#      depth_km = float(split_line[3])
#      mag = float(split_line[16])
#
#      new_ot = (origin_time - catalog_ref_time).total_seconds()
#      new_lat = lat_deg
#      new_lon = lon_deg
#      new_depth = depth_km
#      new_mag = mag
#
#      cat_ot = (datetime.datetime.strptime(split_line[24], "%Y-%m-%dT%H:%M:%S.%f") - catalog_ref_time).total_seconds()
#      cat_lat = float(split_line[25])
#      cat_lon = float(split_line[26])
#      cat_depth = float(split_line[27])
#      cat_mag = float(split_line[28])


      # Check if catalog event (lat, lon) is inside our box
#      if ((cat_lat >= min_lat) and (cat_lat <= max_lat) and (cat_lon >= min_lon) and (cat_lon <= max_lon)):
      if ((cat_lat >= min_lat) and (cat_lat <= max_lat) and (cat_lon >= min_lon) and (cat_lon <= max_lon) and (cat_depth >= min_depth) and (cat_depth <= max_depth)):
         new_arr_ot.append(new_ot)
         new_arr_lat.append(new_lat)
         new_arr_lon.append(new_lon)
         new_arr_depth.append(new_depth)
         new_arr_mag.append(new_mag)

         cat_arr_ot.append(cat_ot)
         cat_arr_lat.append(cat_lat)
         cat_arr_lon.append(cat_lon)
         cat_arr_depth.append(cat_depth)
         cat_arr_mag.append(cat_mag)

#         diff_arr_ot.append((new_ot - cat_ot).total_seconds())
         diff_arr_ot.append(new_ot - cat_ot)
         diff_arr_lat.append(new_lat - cat_lat)
         diff_arr_lon.append(new_lon - cat_lon)
         diff_arr_depth.append(new_depth - cat_depth)
         diff_arr_mag.append(new_mag - cat_mag)

         [epi_dist, azAB, azBA] = gps2dist_azimuth(new_lat, new_lon, cat_lat, cat_lon)
         epidist_new_cat.append(0.001*epi_dist) #km
         hypo_dist = math.sqrt((0.001*epi_dist)**2 + (new_depth-cat_depth)**2)
         hypodist_new_cat.append(hypo_dist) #km

print("Number of events: ", len(cat_arr_mag))

size_plot_depth_sq = np.multiply(diff_arr_depth, diff_arr_depth)
#size_plot_dist_arr = np.multiply(hypodist_new_cat, 15)
size_plot_dist_arr = np.multiply(size_plot_depth_sq, 5)
size_plot_mag_sq = np.multiply(cat_arr_mag, cat_arr_mag)
size_plot_mag_arr = np.multiply(size_plot_mag_sq, 5)

# Bins for residual histograms
res_bins_mag = np.arange(-4, 4.0001, 0.05)
res_bins_latlon = np.arange(-0.3, 0.30001, 0.005)
res_bins_depth = np.arange(-20, 20.0001, 0.5)
res_bins_ot = np.arange(-5, 5.0001, 0.1)

# Compare magnitudes - sized by hypocentral distance
plot_compare_vs_catalog(cat_arr_mag, new_arr_mag, size_plot_mag_arr, 0, 7, 0, 7,
   [0, 1, 2, 3, 4, 5, 6, 7], [0, 1, 2, 3, 4, 5, 6, 7],
   alg1_str+' magnitude', alg2_str+' magnitude',
   out_dir+out_str+'_magnitude_compare.pdf')
plot_compare_residual_vs_catalog(cat_arr_mag, diff_arr_mag, size_plot_mag_arr, 0, 7, -2, 2,
   [0, 1, 2, 3, 4, 5, 6, 7], [-2, -1, 0, 1, 2],
   alg1_str+' magnitude', 'Residual magnitude:\n['+alg2_str+' - '+alg1_str+']', res_bins_mag,
   out_dir+out_str+'_magnitude_residual.pdf')

# Compare latitudes - sized by magnitude
#plot_compare_vs_catalog(cat_arr_lat, new_arr_lat, 33, 37, 33, 37,
#   [33, 34, 35, 36, 37], [33, 34, 35, 36, 37],
#plot_compare_vs_catalog(cat_arr_lat, new_arr_lat, 33, 34, 33, 34,
#   [33, 33.5, 34], [33, 33.5, 34],
plot_compare_vs_catalog(cat_arr_lat, new_arr_lat, size_plot_mag_arr, 17.6, 18.3, 17.6, 18.3,
   [17.7, 17.9, 18.1, 18.3], [17.7, 17.9, 18.1, 18.3],
   alg1_str+' latitude (deg)', alg2_str+' latitude (deg)',
   out_dir+out_str+'_latitude_compare.pdf')
#plot_compare_residual_vs_catalog(cat_arr_lat, diff_arr_lat, 33, 37, -0.1, 0.1,
#   [33, 34, 35, 36, 37], [-0.1, -0.05, 0, 0.05, 0.1],
#plot_compare_residual_vs_catalog(cat_arr_lat, diff_arr_lat, 33, 34, -0.1, 0.1,
#   [33, 33.5, 34], [-0.1, -0.05, 0, 0.05, 0.1],
plot_compare_residual_vs_catalog(cat_arr_lat, diff_arr_lat, size_plot_mag_arr, 17.6, 18.3, -0.3, 0.3,
   [17.7, 17.9, 18.1, 18.3], [-0.3, -0.2, -0.1, 0, 0.1, 0.2, 0.3],
   alg1_str+' latitude (deg)', 'Residual latitude (deg):\n['+alg2_str+' - '+alg1_str+']', res_bins_latlon,
   out_dir+out_str+'_latitude_residual.pdf')

# Compare longitudes - sized by magnitude
#plot_compare_vs_catalog(cat_arr_lon, new_arr_lon, -120, -115, -120, -115,
plot_compare_vs_catalog(cat_arr_lon, new_arr_lon, size_plot_mag_arr, -67.3, -66.4, -67.3, -66.4,
   [-67.3, -67, -66.7, -66.4], [-67.3, -67, -66.7, -66.4], 
   alg1_str+' longitude (deg)', alg2_str+' longitude (deg)',
   out_dir+out_str+'_longitude_compare.pdf')
#plot_compare_residual_vs_catalog(cat_arr_lon, diff_arr_lon, -120, -115, -0.1, 0.1,
plot_compare_residual_vs_catalog(cat_arr_lon, diff_arr_lon, size_plot_mag_arr, -67.3, -66.4, -0.3, 0.3,
   [-67.3, -67, -66.7, -66.4], [-0.3, -0.2, -0.1, 0, 0.1, 0.2, 0.3],
   alg1_str+' longitude (deg)', 'Residual longitude (deg):\n['+alg2_str+' - '+alg1_str+']', res_bins_latlon,
   out_dir+out_str+'_longitude_residual.pdf')

# Compare depths - sized by magnitude
plot_compare_vs_catalog(cat_arr_depth, new_arr_depth, size_plot_mag_arr, -5, 40, -5, 40,
   [0, 10, 20, 30, 40], [0, 10, 20, 30, 40],
#plot_compare_vs_catalog(cat_arr_depth, new_arr_depth, size_plot_mag_arr, -5, 50, -5, 50,
#   [0, 10, 20, 30, 40, 50], [0, 10, 20, 30, 40, 50],
   alg1_str+' depth (km)', alg2_str+' depth (km)',
   out_dir+out_str+'_depth_compare.pdf')
plot_compare_residual_vs_catalog(cat_arr_depth, diff_arr_depth, size_plot_mag_arr, -5, 40, -40, 40,
   [0, 10, 20, 30, 40], [-40, -20, 0, 20, 40],
#plot_compare_residual_vs_catalog(cat_arr_depth, diff_arr_depth, size_plot_mag_arr, -5, 50, -40, 40,
#   [0, 10, 20, 30, 40, 50], [-40, -20, 0, 20, 40],
   alg1_str+' depth (km)', 'Residual depth (km):\n['+alg2_str+' - '+alg1_str+']', res_bins_depth,
   out_dir+out_str+'_depth_residual.pdf')

# Compare origin times
#plot_compare_vs_catalog(cat_arr_ot, new_arr_ot, size_plot_mag_arr, 0, 126230400, 0, 126230400,
##   [0, 19200, 38400, 57600], [0, 19200, 38400, 57600],
#   [0, 31536000, 63072000, 94694400, 126230400], [0, 31536000, 63072000, 94694400, 126230400], 
#   alg1_str+' origin time (s)', alg2_str+' origin time (s)',
#   out_dir+out_str+'_origintime_compare.pdf')
#plot_compare_residual_vs_catalog(cat_arr_ot, diff_arr_ot, size_plot_mag_arr, 0, 126230400, -4, 4,
##   [0, 19200, 38400, 57600], [-4, -2, 0, 2, 4],
#   [0, 31536000, 63072000, 94694400, 126230400], [-4, -2, 0, 2, 4],
#   alg1_str+' origin time (s)', 'Residual origin time (s):\n['+alg2_str+' - '+alg1_str+']', res_bins_ot,
#   out_dir+out_str+'_origintime_residual.pdf')

plot_compare_vs_catalog(cat_arr_ot, new_arr_ot, size_plot_mag_arr, 0, 157766400, 0, 157766400,
#   [0, 19200, 38400, 57600], [0, 19200, 38400, 57600],
   [0, 31536000, 63072000, 94694400, 126230400, 157766400], [0, 31536000, 63072000, 94694400, 126230400, 157766400], 
   alg1_str+' origin time (s)', alg2_str+' origin time (s)',
   out_dir+out_str+'_origintime_compare.pdf')
plot_compare_residual_vs_catalog(cat_arr_ot, diff_arr_ot, size_plot_mag_arr, 0, 157766400, -4, 4,
#   [0, 19200, 38400, 57600], [-4, -2, 0, 2, 4],
   [0, 31536000, 63072000, 94694400, 126230400, 157766400], [-4, -2, 0, 2, 4],
   alg1_str+' origin time (s)', 'Residual origin time (s):\n['+alg2_str+' - '+alg1_str+']', res_bins_ot,
   out_dir+out_str+'_origintime_residual.pdf')

## Compare origin times
#plot_compare_vs_catalog(cat_arr_ot, new_arr_ot, 72000, 87000, 72000, 87000,
#   [72000, 77000, 82000, 87000], [72000, 77000, 82000, 87000],
#   alg1_str+' origin time (s)', alg2_str+' origin time (s)',
#   out_dir+out_str+'_origintime_compare.pdf')
#plot_compare_residual_vs_catalog(cat_arr_ot, diff_arr_ot, 72000, 87000, -4, 4,
#   [72000, 77000, 82000, 87000], [-4, -2, 0, 2, 4],
#   alg1_str+' origin time (s)', 'Residual origin time (s):\n['+alg2_str+' - '+alg1_str,
#   out_dir+out_str+'_origintime_residual.pdf')

## Compare catalog origin time vs depth - tradeoff?
#plt.figure(num=2, figsize=(6,6))
#plt.clf()
#plt.scatter(cat_arr_ot, cat_arr_depth, facecolors='none', edgecolors='k', linewidths=0.5)
#plt.xlim([0, 57600])
##plt.xlim([72000, 87000])
#plt.ylim([0, 20])
#plt.xticks([0, 19200, 38400, 57600])
##plt.xticks([72000, 77000, 82000, 87000])
#plt.yticks([0, 5, 10, 15, 20])
#plt.xlabel(alg1_str+' origin time (s)')
#plt.ylabel(alg1_str+' depth (km)')
#plt.tight_layout()
#plt.savefig(out_dir+out_str+'_MATCH_comcat_view_origintime_vs_depth.pdf')
#
## Compare gpdbinder origin time vs depth - tradeoff?
#plt.figure(num=2, figsize=(6,6))
#plt.clf()
#plt.scatter(new_arr_ot, new_arr_depth, facecolors='none', edgecolors='k', linewidths=0.5)
#plt.xlim([0, 57600])
##plt.xlim([72000, 87000])
#plt.ylim([0, 20])
#plt.xticks([0, 19200, 38400, 57600])
##plt.xticks([72000, 77000, 82000, 87000])
#plt.yticks([0, 5, 10, 15, 20])
#plt.xlabel(alg2_str+' origin time (s)')
#plt.ylabel(alg2_str+' depth (km)')
#plt.tight_layout()
#plt.savefig(out_dir+out_str+'_MATCH_gpdbinder_view_origintime_vs_depth.pdf')

## Compare residuals - origin time vs depth - tradeoff?
#plt.figure(num=2, figsize=(6,6))
#plt.clf()
#plt.scatter(diff_arr_ot, diff_arr_depth, size_plot_mag_arr, facecolors='none', edgecolors='k', linewidths=0.5)
#plt.xlim([-4, 4])
#plt.ylim([-40, 40])
#plt.xticks([-4, -2, 0, 2, 4])
#plt.yticks([-40, -20, 0, 20, 40])
#plt.xlabel('Residual origin time (s):\n['+alg2_str+' - '+alg1_str)
#plt.ylabel('Residual depth (km):\n['+alg2_str+' - '+alg1_str)
#plt.tight_layout()
#plt.savefig(out_dir+out_str+'_residual_origintime_vs_depth.pdf')
#
## Compare residuals - origin time vs epidist - tradeoff?
#plt.figure(num=2, figsize=(6,6))
#plt.clf()
#plt.scatter(diff_arr_ot, epidist_new_cat, size_plot_mag_arr, facecolors='none', edgecolors='k', linewidths=0.5)
#plt.xlim([-4, 4])
#plt.ylim([0, 20])
#plt.xticks([-4, -2, 0, 2, 4])
#plt.yticks([0, 10, 20])
#plt.xlabel('Residual origin time (s):\n['+alg2_str+' - '+alg1_str)
#plt.ylabel('Epicentral distance (km):\n['+alg2_str+' - '+alg1_str)
#plt.tight_layout()
#plt.savefig(out_dir+out_str+'_residual_origintime_vs_epidist.pdf')
#
## Compare residuals - origin time vs hypodist - tradeoff?
#plt.figure(num=2, figsize=(6,6))
#plt.clf()
#plt.scatter(diff_arr_ot, hypodist_new_cat, size_plot_mag_arr, facecolors='none', edgecolors='k', linewidths=0.5)
#plt.xlim([-4, 4])
#plt.ylim([0, 60])
#plt.xticks([-4, -2, 0, 2, 4])
#plt.yticks([0, 30, 60])
#plt.xlabel('Residual origin time (s):\n['+alg2_str+' - '+alg1_str)
#plt.ylabel('Epicentral distance (km):\n['+alg2_str+' - '+alg1_str)
#plt.tight_layout()
#plt.savefig(out_dir+out_str+'_residual_origintime_vs_hypodist.pdf')
#
## Compare depth vs residual magnitude - correlation?
#plt.figure(num=2, figsize=(6,6))
#plt.clf()
#plt.scatter(cat_arr_depth, diff_arr_mag, size_plot_mag_arr, facecolors='none', edgecolors='k', linewidths=0.5)
#plt.xlim([-5, 20])
#plt.ylim([-2, 2])
#plt.xticks([-5, 0, 5, 10, 15, 20])
#plt.yticks([-2, -1, 0, 1, 2])
#plt.xlabel('Catalog depth (km)')
#plt.ylabel('Residual magnitude:\n['+alg2_str+' - '+alg1_str+']')
#plt.tight_layout()
#plt.savefig(out_dir+out_str+'_depth_vs_residual_magnitude.pdf')
#
## Compare residual depth vs residual magnitude - correlation?
#plt.figure(num=2, figsize=(6,6))
#plt.clf()
#plt.scatter(diff_arr_depth, diff_arr_mag, size_plot_mag_arr, facecolors='none', edgecolors='k', linewidths=0.5)
#plt.xlim([-20, 20])
#plt.ylim([-2, 2])
#plt.xticks([-20, -10, 0, 10, 20])
#plt.yticks([-2, -1, 0, 1, 2])
#plt.xlabel('Residual depth (km):\n['+alg2_str+' - '+alg1_str+']')
#plt.ylabel('Residual magnitude:\n['+alg2_str+' - '+alg1_str+']')
#plt.tight_layout()
#plt.savefig(out_dir+out_str+'_residual_depth_vs_residual_magnitude.pdf')

