#!/bin/bash

# Plot Puerto Rico earthquakes on a map

#gmt gmtset BASEMAP_TYPE plain
#gmt gmtset HEADER_FONT_SIZE 24p
#gmt gmtset LABEL_FONT_SIZE 16p # smaller font for labels
##gmt gmtset PLOT_DEGREE_FORMAT +DF
#gmt gmtset PLOT_DEGREE_FORMAT -DF
#gmt gmtset GRID_PEN_PRIMARY 0.1p
#
gmt gmtset MAP_FRAME_TYPE plain     # map outline - single line with ticks
gmt gmtset FORMAT_GEO_MAP DD        # labels on map - decimal degrees

gmt gmtset LABEL_FONT_SIZE 20p
gmt gmtset FONT_ANNOT 20p
#gmt gmtset LABEL_FONT_SIZE 32p #poster
#gmt gmtset FONT_ANNOT 32p #poster

in_dir=../Catalog/EQT_20180101_20230101
out_dir=../EQT_20180101_20230101/Plots

out_color_file=${out_dir}/depthcolors.cpt
out_color_time_file=${out_dir}/timecolors.cpt

##in_eqt_hinv_file=../Catalog/LargeAreaEQTransformer_REAL_HYPOINVERSE_puerto_rico_catalog_20200107_20200114.txt
##in_eqt_hinv_file=../Catalog/LargeAreaEQTransformerModel2_REAL_HYPOINVERSE_puerto_rico_catalog_20200107_20200114.txt
##out_map_eqt_hinv_depth_file=../Plots/puerto_rico_eqt_REAL_hinv_depth_20200107_20200114_small_map_large
##out_map_eqt_hinv_time_file=../Plots/puerto_rico_eqt_REAL_hinv_time_20200107_20200114_small_map_large
####in_eqt_hinv_file=../Catalog/LargeAreaEQTransformer_HYPOINVERSE_puerto_rico_catalog_20200107_20200114.txt
####out_map_eqt_hinv_depth_file=../Plots/puerto_rico_eqt_hinv_depth_20200107_20200114_small_map_large
####out_map_eqt_hinv_time_file=../Plots/puerto_rico_eqt_hinv_time_20200107_20200114_small_map_large
#####in_eqt_hinv_file=../Catalog/LargeAreaEQTransformer_HYPOINVERSE_puerto_rico_catalog_2018_2020.txt
#####out_map_eqt_hinv_depth_file=../Plots/puerto_rico_eqt_hinv_2018_2020_small_map_large
##in_eqt_hinv_file=../Catalog/FullEQTransformer_HYPOINVERSE_puerto_rico_catalog_2018_2020.txt
##out_map_eqt_hinv_depth_file=../Plots/puerto_rico_eqt_hinv_depth_2018_2020_small_map_large
##out_map_eqt_hinv_time_file=../Plots/puerto_rico_eqt_hinv_time_2018_2020_small_map_large
##in_eqt_hinv_file=../Catalog/FullEQTransformer_REAL_HYPOINVERSE_puerto_rico_catalog_2018_2020.txt
##out_map_eqt_hinv_depth_file=../Plots/puerto_rico_eqt_REAL_hinv_depth_2018_2020_small_map_large
##out_map_eqt_hinv_time_file=../Plots/puerto_rico_eqt_REAL_hinv_time_2018_2020_small_map_large
##in_eqt_hinv_file=../Catalog/FullEQTransformer_REAL_HYPOINVERSE_puerto_rico_catalog_combined_2018_2020.txt
#in_eqt_hinv_file=../Catalog/FullEQTransformer_REAL_HYPOINVERSE_puerto_rico_catalog_combined_2018_2020_diam.txt
#out_map_eqt_hinv_depth_file=../Plots/FullEQTransformer_REAL_HYPOINVERSE_puerto_rico_catalog_combined_2018_2020_depth
#out_map_eqt_hinv_time_file=../Plots/FullEQTransformer_REAL_HYPOINVERSE_puerto_rico_catalog_combined_2018_2020_time

###in_eqt_hinv_file=../Catalog/EQT_20180101_20210601/EQT_20180101_20210601_REAL_HYPOINVERSE_VELZHANG_combined_real_magcat_locate_pr_diam.txt
###out_map_eqt_hinv_depth_file=../EQT_20180101_20210601/Plots/EQT_20180101_20210601_REAL_HYPOINVERSE_VELZHANG_combined_depth
###out_map_eqt_hinv_time_file=../EQT_20180101_20210601/Plots/EQT_20180101_20210601_REAL_HYPOINVERSE_VELZHANG_combined_time

#in_eqt_gc_file=../Catalog/EQT_20200107_20200114/LargeAreaEQTransformer_GrowClust_puerto_rico_catalog_20200107_20200114_maxdt2_diam.txt
#out_map_eqt_gc_depth_file=../EQT_20200107_20200114/Plots/EQT_20200107_20200114_REAL_HYPOINVERSE_VELZHANG_GrowClust_combined_depth
#out_map_eqt_gc_time_file=../EQT_20200107_20200114/Plots/EQT_20200107_20200114_REAL_HYPOINVERSE_VELZHANG_GrowClust_combined_time

#in_eqt_gc_file=../Catalog/EQT_20200107_20200114/LargeAreaEQTransformer_GrowClust_puerto_rico_catalog_20200107_20200114_maxdt2_cc070_diam.txt
#out_map_eqt_gc_depth_file=../EQT_20200107_20200114/Plots/EQT_20200107_20200114_REAL_HYPOINVERSE_VELZHANG_GrowClust_maxdt2_cc070_combined_depth
#out_map_eqt_gc_time_file=../EQT_20200107_20200114/Plots/EQT_20200107_20200114_REAL_HYPOINVERSE_VELZHANG_GrowClust_maxdt2_cc070_combined_time

# From Stasis
#in_eqt_gc_file=../Catalog/EQT_20200107_20200114/EQT_20200107_20200114_REAL_HYPOINVERSE_VELZHANG_GrowClust_maxdt2_cc060_diam.txt
#out_map_eqt_gc_depth_file=../EQT_20200107_20200114/Plots/EQT_20200107_20200114_REAL_HYPOINVERSE_VELZHANG_GrowClust_maxdt2_cc060_combined_depth
#out_map_eqt_gc_time_file=../EQT_20200107_20200114/Plots/EQT_20200107_20200114_REAL_HYPOINVERSE_VELZHANG_GrowClust_maxdt2_cc060_combined_time

#in_eqt_gc_file=../Catalog/EQT_20200107_20200114/EQT_20200107_20200114_REAL_GrowClust_VELPRSN_maxdt2_diam.txt
#out_map_eqt_gc_depth_file=../EQT_20200107_20200114/Plots/EQT_20200107_20200114_REAL_GrowClust_VELPRSN_depth
#out_map_eqt_gc_time_file=../EQT_20200107_20200114/Plots/EQT_20200107_20200114_REAL_GrowClust_VELPRSN_time

#in_eqt_gc_file=../Catalog/EQT_20200107_20200114/EQT_20200107_20200114_REAL_GrowClust_VELZHANG_maxdt2_diam.txt
#out_map_eqt_gc_depth_file=../EQT_20200107_20200114/Plots/EQT_20200107_20200114_REAL_GrowClust_VELZHANG_depth
#out_map_eqt_gc_time_file=../EQT_20200107_20200114/Plots/EQT_20200107_20200114_REAL_GrowClust_VELZHANG_time

#in_eqt_gc_file=../Catalog/EQT_20180101_20210601/EQT_20180101_20210601_REAL_GrowClust_VELZHANG_maxdt2_diam.txt
#out_map_eqt_gc_depth_file=../EQT_20180101_20210601/Plots/EQT_20180101_20210601_REAL_GrowClust_VELZHANG_depth
#out_map_eqt_gc_time_file=../EQT_20180101_20210601/Plots/EQT_20180101_20210601_REAL_GrowClust_VELZHANG_time

#in_eqt_gc_file=../Catalog/EQT_20180101_20211001/EQT_20180101_20211001_REAL_NonCluster_GrowClust_VELZHANG_maxdt2_diam.txt
#out_map_eqt_gc_depth_file=../EQT_20180101_20211001/Plots/EQT_20180101_20211001_REAL_NonCluster_GrowClust_VELZHANG_depth
#out_map_eqt_gc_time_file=../EQT_20180101_20211001/Plots/EQT_20180101_20211001_REAL_NonCluster_GrowClust_VELZHANG_time

#in_eqt_gc_file=../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_NonCluster_GrowClust_VELPRSN_maxdt2_diam.txt
#out_map_eqt_gc_depth_file=../EQT_20180101_20220101/Plots/EQT_20180101_20220101_3REAL_NonCluster_GrowClust_VELPRSN_depth
#out_map_eqt_gc_time_file=../EQT_20180101_20220101/Plots/EQT_20180101_20220101_3REAL_NonCluster_GrowClust_VELPRSN_time

#in_eqt_gc_file=../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_NonCluster_GrowClust_VELZHANG_maxdt2_diam.txt
#out_map_eqt_gc_depth_file=../EQT_20180101_20220101/Plots/EQT_20180101_20220101_3REAL_NonCluster_GrowClust_VELZHANG_depth
#out_map_eqt_gc_time_file=../EQT_20180101_20220101/Plots/EQT_20180101_20220101_3REAL_NonCluster_GrowClust_VELZHANG_time

in_eqt_gc_file=${in_dir}/EQT_20180101_20230101_REAL_VELZHANG_NonCluster_GrowClust_kNN5000_stasis_diam.txt
out_map_eqt_gc_depth_file=${out_dir}/EQT_20180101_20230101_REAL_VELZHANG_NonCluster_GrowClust_kNN5000_stasis_depth
out_map_eqt_gc_time_file=${out_dir}/EQT_20180101_20230101_REAL_VELZHANG_NonCluster_GrowClust_kNN5000_stasis_time

#in_sta_file=../Catalog/FullEQTransformer_pr_stations_large.txt
#in_sta_file=../Catalog/EQT_20180101_20210601_pr_stations.txt
in_temp_sta_file=../Catalog/FullEQTransformer_pr_stations_large_temporary.txt
in_sta_file=${in_dir}/EQT_20180101_20230101_pr_stations.txt

#min_lat=17
#max_lat=19
#min_lon=-68
#max_lon=-65
#
#min_lat=17.5
#max_lat=18.5
#min_lon=-67.5
#max_lon=-66.0

min_lat=17.6
max_lat=18.3
min_lon=-67.3
max_lon=-66.4

reg=-R${min_lon}/${max_lon}/${min_lat}/${max_lat}
proj=-JM16
echo ${reg}
lat_lon_spacing=0.3
#lat_lon_spacing=0.1

region_inset=-R-75/-62/15/22
projection_inset=-JM2.0i

# Cross section parameters
strike1_angle=13
strike1_length=30
strike1_proj_width=10
strike1A_proj_width=5
strike1_center_lat=17.93
strike1_center_lon=-66.85
strike1_bproj=-JX30/15
strike1_brange=-R-30/30/-30/0

strike2_angle=98
strike2_length=40
strike2_proj_width=10
strike2B_proj_width=5
strike2_center_lat=17.93
strike2_center_lon=-66.85
strike2_bproj=-JX40/15
strike2_brange=-R-40/40/-30/0

strike3_angle=13
strike3_length=30
strike3_proj_width=5
strike3_center_lat=17.92
strike3_center_lon=-66.74
strike3_bproj=-JX30/15
strike3_brange=-R-30/30/-30/0

strike4_angle=13
strike4_length=30
strike4_proj_width=5
strike4_center_lat=17.9
strike4_center_lon=-66.6
strike4_bproj=-JX30/15
strike4_brange=-R-30/30/-30/0

strike5_angle=13
strike5_length=30
strike5_proj_width=5
strike5_center_lat=17.94
strike5_center_lon=-66.96
strike5_bproj=-JX30/15
strike5_brange=-R-30/30/-30/0

strike6_angle=13
strike6_length=30
strike6_proj_width=5
strike6_center_lat=17.96
strike6_center_lon=-67.09
strike6_bproj=-JX30/15
strike6_brange=-R-30/30/-30/0

strike7_angle=98
strike7_length=40
strike7_proj_width=5
strike7_center_lat=18.02
strike7_center_lon=-66.83
strike7_bproj=-JX40/15
strike7_brange=-R-40/40/-30/0

strike8_angle=98
strike8_length=40
strike8_proj_width=5
strike8_center_lat=17.82
strike8_center_lon=-66.88
strike8_bproj=-JX40/15
strike8_brange=-R-40/40/-30/0

# Magnitude threshold for plotting non-clustered events
mag_thresh=3

mag_plot_thresh=3

# Time slice ranges

# Entire time range
#FIRST_DAY=0 # 2020-01-07
#LAST_DAY=7 # 2020-01-14

#FIRST_DAY=0 # 2018-01-01
#LAST_DAY=1247 # 2021-06-01

# Entire Model1 time range
#FIRST_DAY=0 # 2018-01-01
#LAST_DAY=1035 # 2020-11-01

FIRST_DAY=0 # 2018-01-01
#LAST_DAY=1369 # 2021-10-01
#LAST_DAY=1461 # 2022-01-01
LAST_DAY=1826 # 2023-01-01

#FIRST_DAY=726 # 2019-12-28
#LAST_DAY=750 # 2020-01-21

# 0. Before start of sequence
#FIRST_DAY=0 # 2018-01-01
#LAST_DAY=726 # 2019-12-28

# 1. Between start of sequence and mainshock
#FIRST_DAY=726 # 2019-12-28
#LAST_DAY=736 # 2020-01-07

# 2. Day 0-3 after mainshock
#FIRST_DAY=736 # 2020-01-07
#LAST_DAY=739 # 2020-01-10

# 3. Day 3-7 after mainshock
#FIRST_DAY=739 # 2020-01-10
#LAST_DAY=742 # 2020-01-13

# 4. Day 7-14 after mainshock
#FIRST_DAY=742 # 2020-01-13
#LAST_DAY=750 # 2020-01-21

## 5. Day 14 after mainshock - to 2020-05-02 (aftershock on NE-SW and W-E)
#FIRST_DAY=750 # 2020-01-21
##LAST_DAY=761 # 2020-02-01
##LAST_DAY=790 # 2020-03-01
#LAST_DAY=852 # 2020-05-02

## 6. 2020-05 M5.4 event and aftershocks - to 2020-06-13
#FIRST_DAY=852 # 2020-05-02
##LAST_DAY=859 # 2020-05-09
#LAST_DAY=894 # 2020-06-13

## 7. 2020-06-13 to 2020-06-28 to 2020-07-03 M4.5,4.8,4.5,4.9,5.3 event and aftershocks
#FIRST_DAY=894 # 2020-06-13
##LAST_DAY=956 # 2020-08-12
#LAST_DAY=995 # 2020-09-22

## 8. 2020-09-22 to 2020-12-24 (far west smaller aftershocks)
#FIRST_DAY=995 # 2020-09-22
##LAST_DAY=1041 # 2020-11-07
#LAST_DAY=1088 # 2020-12-24

# 9. 2020-12-24 to 2021-01-07 (M4.8,4.7 and aftershocks on NE-SW)
#FIRST_DAY=1088 # 2020-12-24
#LAST_DAY=1102 # 2021-01-07

# 10. 2021-01-07 to 2021-06-24 (quiet time)
#FIRST_DAY=1102 # 2021-01-07
#LAST_DAY=1270 # 2021-06-24

# 11. 2021-06-24 to 2021-08-24 (far west smaller aftershocks)
#FIRST_DAY=1270 # 2021-06-24
#LAST_DAY=1331 # 2021-08-24

# 12. 2021-08-24 to 2022-01-01 (quiet time)
#FIRST_DAY=1331 # 2021-08-24
#LAST_DAY=1461 # 2022-01-01

# 13. 2022-01-01 to 2023-01-01 (year 3)
#FIRST_DAY=1461 # 2022-01-01
#LAST_DAY=1826 # 2023-01-01


##### COLOR BY DEPTH #####

gmt makecpt -Cinferno -I -T0/20/0.01 > ${out_color_file}

#gmt begin ${out_map_eqt_gc_depth_file}
#gmt begin ${out_map_eqt_gc_depth_file}_${FIRST_DAY}_${LAST_DAY}
gmt begin ${out_map_eqt_gc_depth_file}_${FIRST_DAY}_${LAST_DAY}_${mag_plot_thresh}
   gmt basemap ${proj} ${reg} -Ba${lat_lon_spacing}f0.1g -BneWS #-U
   gmt coast ${proj} ${reg} -W0.25p,black -Na -G170 -Slightskyblue -Lg-66.6/17.68+c-66.5/17.68+w20+f+lkm
#   awk '{print $2, $3, $4, $5*$5*0.01 + 0.0200}' ${in_eqt_gc_file} | gmt plot ${proj} ${reg} -Sc -W0.01c -G255 -BneWS -: -C${out_color_file}
#   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $2, $3, $4, $5*$5*0.01 + 0.0200}' ${in_eqt_gc_file} | gmt plot ${proj} ${reg} -Sc -W0.01c -G255 -BneWS -: -C${out_color_file}
#   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $2, $3, $4, $7/6.26172}' ${in_eqt_gc_file} | gmt plot ${proj} ${reg} -Sc -W0.01c -G255 -BneWS -: -C${out_color_file}
#   sort -nk1,1 ${in_eqt_gc_file} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $2, $3, $4, $7/6.26172}' | gmt plot ${proj} ${reg} -Sc -W0.01c -G255 -BneWS -: -C${out_color_file}
##   sort -nk1,1 ${in_eqt_gc_file} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $2, $3, $4, $7/6.26172 + 0.03}' | gmt plot ${proj} ${reg} -Sc -W0.01c -G255 -BneWS -: -C${out_color_file}
#   sort -nk1,1 ${in_eqt_gc_file} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} -v MT=${mag_thresh} '{if (($1/86400.) >= FD && ($1/86400.) <= LD && $5 >= MT) print $2, $3, $4, $7/6.26172 + 0.03}' | gmt plot ${proj} ${reg} -Sc -W0.9+cl -BneWS -: -C${out_color_file}
   sort -nk2,2 ${in_eqt_gc_file} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} -v MT=${mag_plot_thresh} '{if (($2/86400.) >= FD && ($2/86400.) <= LD && $6 >= MT) print $3, $4, $5, $8/6.26172 + 0.03}' | gmt plot ${proj} ${reg} -Sc -W0.9+cl -BneWS -: -C${out_color_file}
   awk '{print $2 " " $3 " " $4}' ${in_sta_file} | gmt plot ${proj} ${reg} -Si0.4 -W0.1c,black -Gwhite -:
   awk '{print $2 " " $3 " " $4}' ${in_temp_sta_file} | gmt plot ${proj} ${reg} -Si0.4 -W0.1c,blue -Gyellow -:
#   gmt colorbar -C${out_color_file} -Dx6.4i/2.4i/2.5i/0.15i -Bxaf+l"Depth (km)"

   # Project seis along one direction
#   awk '{print $3, $2, $4, $6, $5, $1}' ${in_eqt_gc_file} | gmt project -Q -C${strike1_center_lon}/${strike1_center_lat} -A${strike1_angle} -W-${strike1_proj_width}/${strike1_proj_width} -L-${strike1_length}/${strike1_length} > ${out_map_eqt_gc_depth_file}_seis_proj${strike1_angle}.txt # plot all events
#   awk '{print $3, $2, $4, $6, $5, $1}' ${in_eqt_gc_file} | gmt project -Q -C${strike2_center_lon}/${strike2_center_lat} -A${strike2_angle} -W-${strike2_proj_width}/${strike2_proj_width} -L-${strike2_length}/${strike2_length} > ${out_map_eqt_gc_depth_file}_seis_proj${strike2_angle}.txt # plot all events
#   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $3, $2, $4, $6, $5, $1}' ${in_eqt_gc_file} | gmt project -Q -C${strike1_center_lon}/${strike1_center_lat} -A${strike1_angle} -W-${strike1_proj_width}/${strike1_proj_width} -L-${strike1_length}/${strike1_length} > ${out_map_eqt_gc_depth_file}_${FIRST_DAY}_${LAST_DAY}_seis_proj${strike1_angle}.txt # plot all events
#   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $3, $2, $4, $6, $5, $1}' ${in_eqt_gc_file} | gmt project -Q -C${strike2_center_lon}/${strike2_center_lat} -A${strike2_angle} -W-${strike2_proj_width}/${strike2_proj_width} -L-${strike2_length}/${strike2_length} > ${out_map_eqt_gc_depth_file}_${FIRST_DAY}_${LAST_DAY}_seis_proj${strike2_angle}.txt # plot all events
#   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} -v MT=${mag_thresh} '{if (($1/86400.) >= FD && ($1/86400.) <= LD && $5 >= MT) print $3, $2, $4, $6, $5, $1, $7}' ${in_eqt_gc_file} | gmt project -Q -C${strike1_center_lon}/${strike1_center_lat} -A${strike1_angle} -W-${strike1_proj_width}/${strike1_proj_width} -L-${strike1_length}/${strike1_length} > ${out_map_eqt_gc_depth_file}_${FIRST_DAY}_${LAST_DAY}_seis_proj${strike1_angle}.txt # plot all events
#   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} -v MT=${mag_thresh} '{if (($1/86400.) >= FD && ($1/86400.) <= LD && $5 >= MT) print $3, $2, $4, $6, $5, $1, $7}' ${in_eqt_gc_file} | gmt project -Q -C${strike2_center_lon}/${strike2_center_lat} -A${strike2_angle} -W-${strike2_proj_width}/${strike2_proj_width} -L-${strike2_length}/${strike2_length} > ${out_map_eqt_gc_depth_file}_${FIRST_DAY}_${LAST_DAY}_seis_proj${strike2_angle}.txt # plot all events
   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} -v MT=${mag_thresh} '{if (($2/86400.) >= FD && ($2/86400.) <= LD && $6 >= MT) print $4, $3, $5, $7, $6, $2, $8}' ${in_eqt_gc_file} | gmt project -Q -C${strike1_center_lon}/${strike1_center_lat} -A${strike1_angle} -W-${strike1_proj_width}/${strike1_proj_width} -L-${strike1_length}/${strike1_length} > ${out_map_eqt_gc_depth_file}_${FIRST_DAY}_${LAST_DAY}_seis_proj${strike1_angle}.txt # plot all events
   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} -v MT=${mag_thresh} '{if (($2/86400.) >= FD && ($2/86400.) <= LD && $6 >= MT) print $4, $3, $5, $7, $6, $2, $8}' ${in_eqt_gc_file} | gmt project -Q -C${strike2_center_lon}/${strike2_center_lat} -A${strike2_angle} -W-${strike2_proj_width}/${strike2_proj_width} -L-${strike2_length}/${strike2_length} > ${out_map_eqt_gc_depth_file}_${FIRST_DAY}_${LAST_DAY}_seis_proj${strike2_angle}.txt # plot all events

#   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} -v MT=${mag_thresh} '{if (($1/86400.) >= FD && ($1/86400.) <= LD && $5 >= MT) print $3, $2, $4, $6, $5, $1, $7}' ${in_eqt_gc_file} | gmt project -Q -C${strike1_center_lon}/${strike1_center_lat} -A${strike1_angle} -W-${strike1A_proj_width}/${strike1A_proj_width} -L-${strike1_length}/${strike1_length} > ${out_map_eqt_gc_depth_file}_${FIRST_DAY}_${LAST_DAY}_seis_projA_${strike1_angle}.txt # plot all events
#   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} -v MT=${mag_thresh} '{if (($1/86400.) >= FD && ($1/86400.) <= LD && $5 >= MT) print $3, $2, $4, $6, $5, $1, $7}' ${in_eqt_gc_file} | gmt project -Q -C${strike2_center_lon}/${strike2_center_lat} -A${strike2_angle} -W-${strike2B_proj_width}/${strike2B_proj_width} -L-${strike2_length}/${strike2_length} > ${out_map_eqt_gc_depth_file}_${FIRST_DAY}_${LAST_DAY}_seis_projB_${strike2_angle}.txt # plot all events
#   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} -v MT=${mag_thresh} '{if (($1/86400.) >= FD && ($1/86400.) <= LD && $5 >= MT) print $3, $2, $4, $6, $5, $1, $7}' ${in_eqt_gc_file} | gmt project -Q -C${strike3_center_lon}/${strike3_center_lat} -A${strike3_angle} -W-${strike3_proj_width}/${strike3_proj_width} -L-${strike3_length}/${strike3_length} > ${out_map_eqt_gc_depth_file}_${FIRST_DAY}_${LAST_DAY}_seis_projC_${strike3_angle}.txt # plot all events
#   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} -v MT=${mag_thresh} '{if (($1/86400.) >= FD && ($1/86400.) <= LD && $5 >= MT) print $3, $2, $4, $6, $5, $1, $7}' ${in_eqt_gc_file} | gmt project -Q -C${strike4_center_lon}/${strike4_center_lat} -A${strike4_angle} -W-${strike4_proj_width}/${strike4_proj_width} -L-${strike4_length}/${strike4_length} > ${out_map_eqt_gc_depth_file}_${FIRST_DAY}_${LAST_DAY}_seis_projD_${strike4_angle}.txt # plot all events
#   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} -v MT=${mag_thresh} '{if (($1/86400.) >= FD && ($1/86400.) <= LD && $5 >= MT) print $3, $2, $4, $6, $5, $1, $7}' ${in_eqt_gc_file} | gmt project -Q -C${strike5_center_lon}/${strike5_center_lat} -A${strike5_angle} -W-${strike5_proj_width}/${strike5_proj_width} -L-${strike5_length}/${strike5_length} > ${out_map_eqt_gc_depth_file}_${FIRST_DAY}_${LAST_DAY}_seis_projE_${strike5_angle}.txt # plot all events
#   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} -v MT=${mag_thresh} '{if (($1/86400.) >= FD && ($1/86400.) <= LD && $5 >= MT) print $3, $2, $4, $6, $5, $1, $7}' ${in_eqt_gc_file} | gmt project -Q -C${strike6_center_lon}/${strike6_center_lat} -A${strike6_angle} -W-${strike6_proj_width}/${strike6_proj_width} -L-${strike6_length}/${strike6_length} > ${out_map_eqt_gc_depth_file}_${FIRST_DAY}_${LAST_DAY}_seis_projF_${strike6_angle}.txt # plot all events
#   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} -v MT=${mag_thresh} '{if (($1/86400.) >= FD && ($1/86400.) <= LD && $5 >= MT) print $3, $2, $4, $6, $5, $1, $7}' ${in_eqt_gc_file} | gmt project -Q -C${strike7_center_lon}/${strike7_center_lat} -A${strike7_angle} -W-${strike7_proj_width}/${strike7_proj_width} -L-${strike7_length}/${strike7_length} > ${out_map_eqt_gc_depth_file}_${FIRST_DAY}_${LAST_DAY}_seis_projG_${strike7_angle}.txt # plot all events
#   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} -v MT=${mag_thresh} '{if (($1/86400.) >= FD && ($1/86400.) <= LD && $5 >= MT) print $3, $2, $4, $6, $5, $1, $7}' ${in_eqt_gc_file} | gmt project -Q -C${strike8_center_lon}/${strike8_center_lat} -A${strike8_angle} -W-${strike8_proj_width}/${strike8_proj_width} -L-${strike8_length}/${strike8_length} > ${out_map_eqt_gc_depth_file}_${FIRST_DAY}_${LAST_DAY}_seis_projH_${strike8_angle}.txt # plot all events
#
   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} -v MT=${mag_thresh} '{if (($2/86400.) >= FD && ($2/86400.) <= LD && $6 >= MT) print $4, $3, $5, $7, $6, $2, $8}' ${in_eqt_gc_file} | gmt project -Q -C${strike1_center_lon}/${strike1_center_lat} -A${strike1_angle} -W-${strike1A_proj_width}/${strike1A_proj_width} -L-${strike1_length}/${strike1_length} > ${out_map_eqt_gc_depth_file}_${FIRST_DAY}_${LAST_DAY}_seis_projA_${strike1_angle}.txt # plot all events
   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} -v MT=${mag_thresh} '{if (($2/86400.) >= FD && ($2/86400.) <= LD && $6 >= MT) print $4, $3, $5, $7, $6, $2, $8}' ${in_eqt_gc_file} | gmt project -Q -C${strike2_center_lon}/${strike2_center_lat} -A${strike2_angle} -W-${strike2B_proj_width}/${strike2B_proj_width} -L-${strike2_length}/${strike2_length} > ${out_map_eqt_gc_depth_file}_${FIRST_DAY}_${LAST_DAY}_seis_projB_${strike2_angle}.txt # plot all events
   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} -v MT=${mag_thresh} '{if (($2/86400.) >= FD && ($2/86400.) <= LD && $6 >= MT) print $4, $3, $5, $7, $6, $2, $8}' ${in_eqt_gc_file} | gmt project -Q -C${strike3_center_lon}/${strike3_center_lat} -A${strike3_angle} -W-${strike3_proj_width}/${strike3_proj_width} -L-${strike3_length}/${strike3_length} > ${out_map_eqt_gc_depth_file}_${FIRST_DAY}_${LAST_DAY}_seis_projC_${strike3_angle}.txt # plot all events
   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} -v MT=${mag_thresh} '{if (($2/86400.) >= FD && ($2/86400.) <= LD && $6 >= MT) print $4, $3, $5, $7, $6, $2, $8}' ${in_eqt_gc_file} | gmt project -Q -C${strike4_center_lon}/${strike4_center_lat} -A${strike4_angle} -W-${strike4_proj_width}/${strike4_proj_width} -L-${strike4_length}/${strike4_length} > ${out_map_eqt_gc_depth_file}_${FIRST_DAY}_${LAST_DAY}_seis_projD_${strike4_angle}.txt # plot all events
   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} -v MT=${mag_thresh} '{if (($2/86400.) >= FD && ($2/86400.) <= LD && $6 >= MT) print $4, $3, $5, $7, $6, $2, $8}' ${in_eqt_gc_file} | gmt project -Q -C${strike5_center_lon}/${strike5_center_lat} -A${strike5_angle} -W-${strike5_proj_width}/${strike5_proj_width} -L-${strike5_length}/${strike5_length} > ${out_map_eqt_gc_depth_file}_${FIRST_DAY}_${LAST_DAY}_seis_projE_${strike5_angle}.txt # plot all events
   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} -v MT=${mag_thresh} '{if (($2/86400.) >= FD && ($2/86400.) <= LD && $6 >= MT) print $4, $3, $5, $7, $6, $2, $8}' ${in_eqt_gc_file} | gmt project -Q -C${strike6_center_lon}/${strike6_center_lat} -A${strike6_angle} -W-${strike6_proj_width}/${strike6_proj_width} -L-${strike6_length}/${strike6_length} > ${out_map_eqt_gc_depth_file}_${FIRST_DAY}_${LAST_DAY}_seis_projF_${strike6_angle}.txt # plot all events
   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} -v MT=${mag_thresh} '{if (($2/86400.) >= FD && ($2/86400.) <= LD && $6 >= MT) print $4, $3, $5, $7, $6, $2, $8}' ${in_eqt_gc_file} | gmt project -Q -C${strike7_center_lon}/${strike7_center_lat} -A${strike7_angle} -W-${strike7_proj_width}/${strike7_proj_width} -L-${strike7_length}/${strike7_length} > ${out_map_eqt_gc_depth_file}_${FIRST_DAY}_${LAST_DAY}_seis_projG_${strike7_angle}.txt # plot all events
   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} -v MT=${mag_thresh} '{if (($2/86400.) >= FD && ($2/86400.) <= LD && $6 >= MT) print $4, $3, $5, $7, $6, $2, $8}' ${in_eqt_gc_file} | gmt project -Q -C${strike8_center_lon}/${strike8_center_lat} -A${strike8_angle} -W-${strike8_proj_width}/${strike8_proj_width} -L-${strike8_length}/${strike8_length} > ${out_map_eqt_gc_depth_file}_${FIRST_DAY}_${LAST_DAY}_seis_projH_${strike8_angle}.txt # plot all events

   # Projection lines
   gmt project -Q -G1 -C${strike1_center_lon}/${strike1_center_lat} -A${strike1_angle} -L-${strike1_length}/${strike1_length} | gmt plot ${proj} ${reg} -W1,0/0/0,-. -Gblack
   gmt text ${proj} ${reg} -F+f24 << EOF
-66.82 18.16 A'
-66.935 17.67 A
EOF
   gmt project -Q -G1 -C${strike2_center_lon}/${strike2_center_lat} -A${strike2_angle} -L-${strike2_length}/${strike2_length} | gmt plot ${proj} ${reg} -W1,0/0/0,-. -Gblack
   gmt text ${proj} ${reg} -F+f24 << EOF
-67.25 17.98 B
-66.45 17.88 B'
EOF
#   gmt project -Q -G1 -C${strike3_center_lon}/${strike3_center_lat} -A${strike3_angle} -L-${strike3_length}/${strike3_length} | gmt plot ${proj} ${reg} -W1,0/0/0,-. -Gblack
#   gmt text ${proj} ${reg} -F+f24 << EOF
#-66.71 18.15 C'
#-66.83 17.66 C
#EOF
#   gmt project -Q -G1 -C${strike4_center_lon}/${strike4_center_lat} -A${strike4_angle} -L-${strike4_length}/${strike4_length} | gmt plot ${proj} ${reg} -W1,0/0/0,-. -Gblack
#   gmt text ${proj} ${reg} -F+f24 << EOF
#-66.57 18.14 D'
#-66.69 17.65 D
#EOF
#   gmt project -Q -G1 -C${strike5_center_lon}/${strike5_center_lat} -A${strike5_angle} -L-${strike5_length}/${strike5_length} | gmt plot ${proj} ${reg} -W1,0/0/0,-. -Gblack
#   gmt text ${proj} ${reg} -F+f24 << EOF
#-66.93 18.17 E'
#-67.05 17.68 E
#EOF
#   gmt project -Q -G1 -C${strike6_center_lon}/${strike6_center_lat} -A${strike6_angle} -L-${strike6_length}/${strike6_length} | gmt plot ${proj} ${reg} -W1,0/0/0,-. -Gblack
#   gmt text ${proj} ${reg} -F+f24 << EOF
#-67.06 18.195 F'
#-67.18 17.70 F
#EOF
#   gmt project -Q -G1 -C${strike7_center_lon}/${strike7_center_lat} -A${strike7_angle} -L-${strike7_length}/${strike7_length} | gmt plot ${proj} ${reg} -W1,0/0/0,-. -Gblack
#   gmt text ${proj} ${reg} -F+f24 << EOF
#-67.23 18.07 G
#-66.43 17.97 G'
#EOF
#   gmt project -Q -G1 -C${strike8_center_lon}/${strike8_center_lat} -A${strike8_angle} -L-${strike8_length}/${strike8_length} | gmt plot ${proj} ${reg} -W1,0/0/0,-. -Gblack
#   gmt text ${proj} ${reg} -F+f24 << EOF
#-67.27 17.87 H
#-66.47 17.77 H'
#EOF

#   # Add inset
#   gmt basemap ${region_inset} ${projection_inset} -Bnews -X0.05i -Y0.05i
#   gmt coast ${region_inset} ${projection_inset} -Dl -G255 -Na -W1
#   gmt plot ${region_inset} ${projection_inset} -W2,red << EOF
#${min_lon} ${min_lat}
#${min_lon} ${max_lat}
#${max_lon} ${max_lat}
#${max_lon} ${min_lat}
#${min_lon} ${min_lat}

#   # Add inset
#   gmt basemap ${region_inset} ${projection_inset} -Bnews -X0.05i -Y0.05i
#   gmt coast ${region_inset} ${projection_inset} -Dl -G255 -Na -W1
#   gmt plot ${region_inset} ${projection_inset} -W2,red << EOF
#${min_lon} ${min_lat}
#${min_lon} ${max_lat}
#${max_lon} ${max_lat}
#${max_lon} ${min_lat}
#${min_lon} ${min_lat}
#EOF
gmt end show


##### COLOR BY TIME #####

#gmt makecpt -Cviridis -I -T0/7/0.01 > ${out_color_time_file} # LargeAreaEQTransformer - 7 days
#start_date=2020-01-07
#gmt makecpt -Cviridis -I -T0/1035/0.01 > ${out_color_time_file} # FullEQTransformer - 1035 days
#gmt makecpt -Cviridis -I -T699/1035/0.01 > ${out_color_time_file} # FullEQTransformer - 699-1035 days (2019-12-01 to 2020-11-01)
#gmt makecpt -Crainbow -I -T0/726/0.01 > ${out_color_time_file} # FullEQTransformer - 0-726 days (2018-01-01 to 2019-12-28)
#gmt makecpt -Cviridis -I -T726/1035/0.01 > ${out_color_time_file} # FullEQTransformer - 726-1035 days (2019-12-28 to 2020-11-01)
###gmt makecpt -Cviridis -I -T726/1247/0.01 > ${out_color_time_file} # FullEQTransformer - 726-1247 days (2019-12-28 to 2021-06-01)
#gmt makecpt -Cviridis -I -T726/1369/0.01 > ${out_color_time_file} # FullEQTransformer - 726-1369 days (2019-12-28 to 2021-10-01)
#gmt makecpt -Cviridis -I -T726/1461/0.01 > ${out_color_time_file} # FullEQTransformer - 726-1461 days (2019-12-28 to 2022-01-01)
gmt makecpt -Cviridis -I -T726/1826/0.01 > ${out_color_time_file} # FullEQTransformer - 726-1826 days (2019-12-28 to 2023-01-01)
start_date=2018-01-01

#gmt begin ${out_map_eqt_gc_time_file}
gmt begin ${out_map_eqt_gc_time_file}_${FIRST_DAY}_${LAST_DAY}_${mag_plot_thresh}
   gmt basemap ${proj} ${reg} -Ba${lat_lon_spacing}f0.1g -BneWS #-U
   gmt coast ${proj} ${reg} -W0.25p,black -Na -G170 -Slightskyblue -Lg-66.6/17.68+c-66.5/17.68+w20+f+lkm
#   awk '{print $2, $3, $1/86400., $5*$5*0.01 + 0.0200}' ${in_eqt_gc_file} | gmt plot ${proj} ${reg} -Sc -W0.01c -G255 -BneWS -: -C${out_color_time_file}
#   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $2, $3, $1/86400., $5*$5*0.01 + 0.0200}' ${in_eqt_gc_file} | gmt plot ${proj} ${reg} -Sc -W0.01c -G255 -BneWS -: -C${out_color_time_file}
#   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $2, $3, $1/86400., $7/6.26172}' ${in_eqt_gc_file} | gmt plot ${proj} ${reg} -Sc -W0.01c -G255 -BneWS -: -C${out_color_time_file}
#   sort -nk1,1 ${in_eqt_gc_file} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $2, $3, $1/86400., $7/6.26172}' | gmt plot ${proj} ${reg} -Sc -W0.01c -G255 -BneWS -: -C${out_color_time_file}
##   sort -nk1,1 ${in_eqt_gc_file} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $2, $3, $1/86400., $7/6.26172 + 0.03}' | gmt plot ${proj} ${reg} -Sc -W0.01c -G255 -BneWS -: -C${out_color_time_file}
#   sort -nk1,1 ${in_eqt_gc_file} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} -v MT=${mag_thresh} '{if (($1/86400.) >= FD && ($1/86400.) <= LD && $5 >= MT) print $2, $3, $1/86400., $7/6.26172 + 0.03}' | gmt plot ${proj} ${reg} -Sc -W0.9+cl -BneWS -: -C${out_color_time_file}
   sort -nk2,2 ${in_eqt_gc_file} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} -v MT=${mag_plot_thresh} '{if (($2/86400.) >= FD && ($2/86400.) <= LD && $6 >= MT) print $3, $4, $2/86400., $8/6.26172 + 0.03}' | gmt plot ${proj} ${reg} -Sc -W0.9+cl -BneWS -: -C${out_color_time_file}
   awk '{print $2 " " $3 " " $4}' ${in_sta_file} | gmt plot ${proj} ${reg} -Si0.4 -W0.1c,black -Gwhite -:
   awk '{print $2 " " $3 " " $4}' ${in_temp_sta_file} | gmt plot ${proj} ${reg} -Si0.4 -W0.1c,blue -Gyellow -:
#   gmt colorbar -C${out_color_time_file} -Dx6.4i/2.4i/2.5i/0.15i -Bxaf+l"Days since "${start_date}

   # Projection lines
   gmt project -Q -G1 -C${strike1_center_lon}/${strike1_center_lat} -A${strike1_angle} -L-${strike1_length}/${strike1_length} | gmt plot ${proj} ${reg} -W1,0/0/0,-. -Gblack
   gmt text ${proj} ${reg} -F+f24 << EOF
-66.82 18.16 A'
-66.935 17.67 A
EOF
   gmt project -Q -G1 -C${strike2_center_lon}/${strike2_center_lat} -A${strike2_angle} -L-${strike2_length}/${strike2_length} | gmt plot ${proj} ${reg} -W1,0/0/0,-. -Gblack
   gmt text ${proj} ${reg} -F+f24 << EOF
-67.25 17.98 B
-66.45 17.88 B'
EOF

#   # Add inset
#   gmt basemap ${region_inset} ${projection_inset} -Bnews -X0.05i -Y0.05i
#   gmt coast ${region_inset} ${projection_inset} -Dl -G255 -Na -W1
#   gmt plot ${region_inset} ${projection_inset} -W2,red << EOF
#${min_lon} ${min_lat}
#${min_lon} ${max_lat}
#${max_lon} ${max_lat}
#${max_lon} ${min_lat}
#${min_lon} ${min_lat}
#EOF
gmt end show

#gmt gmtset LABEL_FONT_SIZE 32p
#gmt gmtset FONT_ANNOT 32p
gmt gmtset LABEL_FONT_SIZE 56p #poster
gmt gmtset FONT_ANNOT 56p #poster

#gmt begin ${out_map_eqt_gc_time_file}_seis_proj${strike1_angle}
#gmt begin ${out_map_eqt_gc_time_file}_${FIRST_DAY}_${LAST_DAY}_seis_proj${strike1_angle}
gmt begin ${out_map_eqt_gc_time_file}_${FIRST_DAY}_${LAST_DAY}_${mag_plot_thresh}_seis_proj${strike1_angle}
   gmt basemap ${strike1_bproj} ${strike1_brange} -Ba10f5 -BneWS+g170 #-U
#   awk '{print $7, $3*(-1.0), $6/86400., $5*$5*0.01 + 0.0200}' ${out_map_eqt_gc_depth_file}_seis_proj${strike1_angle}.txt | gmt plot ${strike1_bproj} ${strike1_brange} -Bxa2+l"Length along cross-section A-A' (km)" -Bya2+l"Depth (km)" -BneWS -Sc -W0.01c -G255 -C${out_color_time_file}
#   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($6/86400.) >= FD && ($6/86400.) <= LD) print $7, $3*(-1.0), $6/86400., $5*$5*0.01 + 0.0200}' ${out_map_eqt_gc_depth_file}_${FIRST_DAY}_${LAST_DAY}_seis_proj${strike1_angle}.txt | gmt plot ${strike1_bproj} ${strike1_brange} -Bxa2+l"Length along cross-section A-A' (km)" -Bya2+l"Depth (km)" -BneWS -Sc -W0.01c -G255 -C${out_color_time_file}
#   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($6/86400.) >= FD && ($6/86400.) <= LD) print $8, $3*(-1.0), $6/86400., $7}' ${out_map_eqt_gc_depth_file}_${FIRST_DAY}_${LAST_DAY}_seis_proj${strike1_angle}.txt | gmt plot ${strike1_bproj} ${strike1_brange} -Bxa2+l"Length along cross-section A-A' (km)" -Bya2+l"Depth (km)" -BneWS -Sc -W0.01c -G255 -C${out_color_time_file}
   sort -nk6,6 ${out_map_eqt_gc_depth_file}_${FIRST_DAY}_${LAST_DAY}_seis_proj${strike1_angle}.txt | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($6/86400.) >= FD && ($6/86400.) <= LD) print $8, $3*(-1.0), $6/86400., $7}' | gmt plot ${strike1_bproj} ${strike1_brange} -Bxa10+l"Length along cross-section A-A' (km)" -Bya10+l"Depth (km)" -BneWS -Sc -W1.2+cl -C${out_color_time_file}
#   gmt colorbar -C${out_color_time_file} -Dx11.9i/3.0i/2.5i/0.15i -Bxaf+l"Days since "${start_date}
gmt end show

#gmt begin ${out_map_eqt_gc_time_file}_seis_proj${strike2_angle}
gmt begin ${out_map_eqt_gc_time_file}_${FIRST_DAY}_${LAST_DAY}_${mag_plot_thresh}_seis_proj${strike2_angle}
   gmt basemap ${strike2_bproj} ${strike2_brange} -Ba10f5 -BneWS+g170 #-U
#   awk '{print $7, $3*(-1.0), $6/86400., $5*$5*0.01 + 0.0200}' ${out_map_eqt_gc_depth_file}_seis_proj${strike2_angle}.txt | gmt plot ${strike2_bproj} ${strike2_brange} -Bxa4+l"Length along cross-section B-B' (km)" -Bya2+l"Depth (km)" -BneWS -Sc -W0.01c -G255 -C${out_color_time_file}
#   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($6/86400.) >= FD && ($6/86400.) <= LD) print $7, $3*(-1.0), $6/86400., $5*$5*0.01 + 0.0200}' ${out_map_eqt_gc_depth_file}_${FIRST_DAY}_${LAST_DAY}_seis_proj${strike2_angle}.txt | gmt plot ${strike2_bproj} ${strike2_brange} -Bxa4+l"Length along cross-section B-B' (km)" -Bya2+l"Depth (km)" -BneWS -Sc -W0.01c -G255 -C${out_color_time_file}
#   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($6/86400.) >= FD && ($6/86400.) <= LD) print $8, $3*(-1.0), $6/86400., $7}' ${out_map_eqt_gc_depth_file}_${FIRST_DAY}_${LAST_DAY}_seis_proj${strike2_angle}.txt | gmt plot ${strike2_bproj} ${strike2_brange} -Bxa4+l"Length along cross-section B-B' (km)" -Bya2+l"Depth (km)" -BneWS -Sc -W0.01c -G255 -C${out_color_time_file}
   sort -nk6,6 ${out_map_eqt_gc_depth_file}_${FIRST_DAY}_${LAST_DAY}_seis_proj${strike2_angle}.txt | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($6/86400.) >= FD && ($6/86400.) <= LD) print $8, $3*(-1.0), $6/86400., $7}' | gmt plot ${strike2_bproj} ${strike2_brange} -Bxa10+l"Length along cross-section B-B' (km)" -Bya10+l"Depth (km)" -BneWS -Sc -W1.2+cl -C${out_color_time_file}
#   gmt colorbar -C${out_color_time_file} -Dx15.8i/3.0i/2.5i/0.15i -Bxaf2+l"Days since "${start_date}
gmt end show

#gmt begin ${out_map_eqt_gc_time_file}_${FIRST_DAY}_${LAST_DAY}_seis_projA_${strike1_angle}
#   gmt basemap ${strike1_bproj} ${strike1_brange} -Ba10f5 -BneWS+g170 #-U
#   sort -nk6,6 ${out_map_eqt_gc_depth_file}_${FIRST_DAY}_${LAST_DAY}_seis_projA_${strike1_angle}.txt | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($6/86400.) >= FD && ($6/86400.) <= LD) print $8, $3*(-1.0), $6/86400., $7}' | gmt plot ${strike1_bproj} ${strike1_brange} -Bxa10+l"Length along cross-section A-A' (km)" -Bya10+l"Depth (km)" -BneWS -Sc -W1.2+cl -C${out_color_time_file}
##   gmt colorbar -C${out_color_time_file} -Dx11.9i/3.0i/2.5i/0.15i -Bxaf+l"Days since "${start_date}
#gmt end show
#
##gmt begin ${out_map_eqt_gc_time_file}_seis_projB_${strike2_angle}
#gmt begin ${out_map_eqt_gc_time_file}_${FIRST_DAY}_${LAST_DAY}_seis_projB_${strike2_angle}
#   gmt basemap ${strike2_bproj} ${strike2_brange} -Ba10f5 -BneWS+g170 #-U
#   sort -nk6,6 ${out_map_eqt_gc_depth_file}_${FIRST_DAY}_${LAST_DAY}_seis_projB_${strike2_angle}.txt | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($6/86400.) >= FD && ($6/86400.) <= LD) print $8, $3*(-1.0), $6/86400., $7}' | gmt plot ${strike2_bproj} ${strike2_brange} -Bxa10+l"Length along cross-section B-B' (km)" -Bya10+l"Depth (km)" -BneWS -Sc -W1.2+cl -C${out_color_time_file}
##   gmt colorbar -C${out_color_time_file} -Dx15.8i/3.0i/2.5i/0.15i -Bxaf2+l"Days since "${start_date}
#gmt end show
#
##gmt begin ${out_map_eqt_gc_time_file}_${FIRST_DAY}_${LAST_DAY}_seis_projC_${strike3_angle}
#gmt begin ${out_map_eqt_gc_time_file}_${FIRST_DAY}_${LAST_DAY}_seis_projC_${strike3_angle}
#   gmt basemap ${strike3_bproj} ${strike3_brange} -Ba10f5 -BneWS+g170 #-U
#   sort -nk6,6 ${out_map_eqt_gc_depth_file}_${FIRST_DAY}_${LAST_DAY}_seis_projC_${strike3_angle}.txt | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($6/86400.) >= FD && ($6/86400.) <= LD) print $8, $3*(-1.0), $6/86400., $7}' | gmt plot ${strike3_bproj} ${strike3_brange} -Bxa10+l"Length along cross-section C-C' (km)" -Bya10+l"Depth (km)" -BneWS -Sc -W1.2+cl -C${out_color_time_file}
##   gmt colorbar -C${out_color_time_file} -Dx11.9i/3.0i/2.5i/0.15i -Bxaf+l"Days since "${start_date}
#gmt end show
#
#gmt begin ${out_map_eqt_gc_time_file}_${FIRST_DAY}_${LAST_DAY}_seis_projD_${strike4_angle}
#   gmt basemap ${strike4_bproj} ${strike4_brange} -Ba10f5 -BneWS+g170 #-U
#   sort -nk6,6 ${out_map_eqt_gc_depth_file}_${FIRST_DAY}_${LAST_DAY}_seis_projD_${strike4_angle}.txt | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($6/86400.) >= FD && ($6/86400.) <= LD) print $8, $3*(-1.0), $6/86400., $7}' | gmt plot ${strike4_bproj} ${strike4_brange} -Bxa10+l"Length along cross-section D-D' (km)" -Bya10+l"Depth (km)" -BneWS -Sc -W1.2+cl -C${out_color_time_file}
##   gmt colorbar -C${out_color_time_file} -Dx11.9i/3.0i/2.5i/0.15i -Bxaf+l"Days since "${start_date}
#gmt end show
#
#gmt begin ${out_map_eqt_gc_time_file}_${FIRST_DAY}_${LAST_DAY}_seis_projE_${strike5_angle}
#   gmt basemap ${strike5_bproj} ${strike5_brange} -Ba10f5 -BneWS+g170 #-U
#   sort -nk6,6 ${out_map_eqt_gc_depth_file}_${FIRST_DAY}_${LAST_DAY}_seis_projE_${strike5_angle}.txt | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($6/86400.) >= FD && ($6/86400.) <= LD) print $8, $3*(-1.0), $6/86400., $7}' | gmt plot ${strike5_bproj} ${strike5_brange} -Bxa10+l"Length along cross-section E-E' (km)" -Bya10+l"Depth (km)" -BneWS -Sc -W1.2+cl -C${out_color_time_file}
##   gmt colorbar -C${out_color_time_file} -Dx11.9i/3.0i/2.5i/0.15i -Bxaf+l"Days since "${start_date}
#gmt end show
#
#gmt begin ${out_map_eqt_gc_time_file}_${FIRST_DAY}_${LAST_DAY}_seis_projF_${strike6_angle}
#   gmt basemap ${strike6_bproj} ${strike6_brange} -Ba10f5 -BneWS+g170 #-U
#   sort -nk6,6 ${out_map_eqt_gc_depth_file}_${FIRST_DAY}_${LAST_DAY}_seis_projF_${strike6_angle}.txt | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($6/86400.) >= FD && ($6/86400.) <= LD) print $8, $3*(-1.0), $6/86400., $7}' | gmt plot ${strike6_bproj} ${strike6_brange} -Bxa10+l"Length along cross-section F-F' (km)" -Bya10+l"Depth (km)" -BneWS -Sc -W1.2+cl -C${out_color_time_file}
##   gmt colorbar -C${out_color_time_file} -Dx11.9i/3.0i/2.5i/0.15i -Bxaf+l"Days since "${start_date}
#gmt end show
#
#gmt begin ${out_map_eqt_gc_time_file}_${FIRST_DAY}_${LAST_DAY}_seis_projG_${strike7_angle}
#   gmt basemap ${strike7_bproj} ${strike7_brange} -Ba10f5 -BneWS+g170 #-U
#   sort -nk6,6 ${out_map_eqt_gc_depth_file}_${FIRST_DAY}_${LAST_DAY}_seis_projG_${strike7_angle}.txt | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($6/86400.) >= FD && ($6/86400.) <= LD) print $8, $3*(-1.0), $6/86400., $7}' | gmt plot ${strike7_bproj} ${strike7_brange} -Bxa10+l"Length along cross-section G-G' (km)" -Bya10+l"Depth (km)" -BneWS -Sc -W1.2+cl -C${out_color_time_file}
##   gmt colorbar -C${out_color_time_file} -Dx15.8i/3.0i/2.5i/0.15i -Bxaf2+l"Days since "${start_date}
#gmt end show
#
#gmt begin ${out_map_eqt_gc_time_file}_${FIRST_DAY}_${LAST_DAY}_seis_projH_${strike8_angle}
#   gmt basemap ${strike8_bproj} ${strike8_brange} -Ba10f5 -BneWS+g170 #-U
#   sort -nk6,6 ${out_map_eqt_gc_depth_file}_${FIRST_DAY}_${LAST_DAY}_seis_projH_${strike8_angle}.txt | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($6/86400.) >= FD && ($6/86400.) <= LD) print $8, $3*(-1.0), $6/86400., $7}' | gmt plot ${strike8_bproj} ${strike8_brange} -Bxa10+l"Length along cross-section H-H' (km)" -Bya10+l"Depth (km)" -BneWS -Sc -W1.2+cl -C${out_color_time_file}
##   gmt colorbar -C${out_color_time_file} -Dx15.8i/3.0i/2.5i/0.15i -Bxaf2+l"Days since "${start_date}
#gmt end show
