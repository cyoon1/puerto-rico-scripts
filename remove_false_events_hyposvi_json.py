#dv='cuda:2'
dv='cuda'
# $ gpustat (find which GPUs free)
# $ export CUDA_VISIBLE_DEVICES=5,6,7
# $ echo $CUDA_VISIBLE_DEVICES
# $ CUDA_VISIBLE_DEVICES=5 python plot_hyposvi_locations.py


# EikoNet
from EikoNet import model    as md
from EikoNet import database as db

# HypoSVI
from HypoSVI import location as lc
import pandas as pd

import numpy as np
#import os

# Plot HypoSVI locations
# Code from Jonny 2020-03-10

#####PATH = '/atomic-data/cyoon/PuertoRico/Eiko_out'
#PATH = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/Eiko_out'
#PATH = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200108/Eiko_out'
PATH = '/media/yoon/INT01/PuertoRico/EQT_20180101_20230101/Eiko_out'

xmin               = [-67.5,17.5,-2] #Lat,Long,Depth
#xmax               = [-65.0,18.5,20] #Lat,Long,Depth #PRSN
xmax               = [-65.0,18.5,41] #Lat,Long,Depth #ZHANG
projection         = "+proj=utm +zone=19 +north +ellps=WGS84 +datum=WGS84 +units=km +no_defs"

#vp_dir = 'VP_PRSN'
#vp_file = vp_dir+'/VP_PRSN.csv'
#vp_model_file = 'Model_Epoch_00014_ValLoss_0.006514014532663545.pt'
##vp_model_file = 'Model_Epoch_00014_ValLoss_0.0032528631574705357.pt'
#vs_dir = 'VS_PRSN'
#vs_file = vs_dir+'/VS_PRSN.csv'
#vs_model_file = 'Model_Epoch_00014_ValLoss_0.0060166874305254085.pt'
##vs_model_file = 'Model_Epoch_00014_ValLoss_0.0035651540033575287.pt'

vp_dir = 'VP_ZHANG'
vp_file = vp_dir+'/VP_ZHANG.csv'
vp_model_file = 'Model_Epoch_00015_ValLoss_0.0027984082313688625.pt'
vs_dir = 'VS_ZHANG'
vs_file = vs_dir+'/VS_ZHANG.csv'
vs_model_file = 'Model_Epoch_00015_ValLoss_0.004144702160983046.pt'

in_hyposvi_dir = 'EQT_20180101_20230101_REAL_VELZHANG_PickErrorRES_Events'
out_catalog_plot_file = 'EQT_20180101_20230101_REAL_DIRECTHYPOSVI_KeepCatalog.png'
out_catalog_csv_file = 'EQT_20180101_20230101_REAL_DIRECTHYPOSVI_KeepCatalog.csv'

in_false_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20230101/REAL_VELZHANG/DIRECTHYPOSVI/'
in_false_file = in_false_dir+'events_NEW_magcat_false_DIRECTHYPOSVI_EQT_20180101_20230101.txt'



# ------------- VP Velocity Model ----------
vm_vp = db.Graded1DVelocity(('{}/'+vp_file).format(PATH),xmin=xmin,xmax=xmax,projection=projection)
model_VP  = md.Model(('{}/'+vp_dir).format(PATH),vm_vp,device=dv)
##model_VP  = md.Model(('{}/'+vp_dir).format(PATH),vm_vp)
model_VP.load(('{}/'+vp_dir+'/'+vp_model_file).format(PATH))

# ------------- VS Velocity Model ----------
vm_vs = db.Graded1DVelocity(('{}/'+vs_file).format(PATH),xmin=xmin,xmax=xmax,projection=projection)
model_VS  = md.Model(('{}/'+vs_dir).format(PATH),vm_vs,device=dv)
##model_VS  = md.Model(('{}/'+vs_dir).format(PATH),vm_vs)
model_VS.load(('{}/'+vs_dir+'/'+vs_model_file).format(PATH))


# ------ HypoSVI -------
PATH_EVT = PATH+'/'+in_hyposvi_dir
#PATH_MISSED_EVT = PATH+'/'+in_missed_hyposvi_dir

false_id_list = np.loadtxt(in_false_file, usecols=(6), dtype='int', unpack=True)
print('len(false_id_list): ', len(false_id_list))

EVT_org = lc.IO_JSON('{}/Catalogue.json'.format(PATH_EVT),rw_type='r')
print('Original, len(EVT_org) = ', len(EVT_org))

# Remove false events from json
for ev_id in false_id_list:
   ev_str = str(ev_id)
   EVT_org.pop(ev_str, None)

print('Removed false events, len(EVT_org) = ', len(EVT_org))

# Output modified (no false events) HypoSVI dictionary to file
lc.IO_JSON('{}/KeepCatalogue.json'.format(PATH_EVT), EVT_org, rw_type='w')

#EVT_in_all = lc.IO_JSON('{}/CombinedCatalogue.json'.format(PATH), rw_type='r')
#print('len(EVT_in_all) = ', len(EVT_in_all))

keys = []
for key in EVT_org.keys():
   try:
      loc = EVT_org[key]['location']
      keys.append(key)
      continue
   except:
      continue
EVT= { your_key: EVT_org[your_key] for your_key in keys }

LocMethod = lc.HypoSVI([model_VP,model_VS],Phases=['P','S'],device=dv)
LocMethod.plot_info['CataloguePlot']['Minimum Phase Picks']                                           = 8
LocMethod.plot_info['CataloguePlot']['Maximum Location Uncertainty (km)']                             = 30
LocMethod.plot_info['CataloguePlot']['Num Std to define errorbar']                                    = 2
#LocMethod.plot_info['CataloguePlot']['Event Info - [Size, Color, Marker, Alpha]']                     = [0.05,'r','*',0.05]
#LocMethod.plot_info['CataloguePlot']['Event Errorbar - [On/Off(Bool),Linewidth,Color,Alpha]']         = [True,0.1,'r',0.1]
#LocMethod.plot_info['CataloguePlot']['Station Marker - [Size,Color,Names On/Off(Bool)]']              = [150,'b',True]
LocMethod.plot_info['CataloguePlot']['Event Info - [Size, Color, Marker, Alpha]']                     = [0.5,'r','*',0.5]
LocMethod.plot_info['CataloguePlot']['Event Errorbar - [On/Off(Bool),Linewidth,Color,Alpha]']         = [True,0.3,'r',0.3]
LocMethod.plot_info['CataloguePlot']['Station Marker - [Size,Color,Names On/Off(Bool)]']              = [50,'b',True]

Stations       = pd.read_csv('{}/pr_st.out'.format(PATH),sep=r'\s+')
#Stations       = pd.read_csv('{}/mendo_st.out'.format(PATH),sep=r'\s+')
Stations       = Stations.drop_duplicates(['Network','Station'],keep='last').reset_index(drop=True)

LocMethod.CataloguePlot(filepath=(('{}/'+out_catalog_plot_file).format(PATH_EVT)), Events=EVT,
                        user_xmin=[None,None,-1], user_xmax=[None,None,None], Stations=Stations)

LocMethod.Events2CSV(savefile=(('{}/'+out_catalog_csv_file).format(PATH_EVT)))

