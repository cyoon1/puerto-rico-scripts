import glob
import os

# Check that all catalog events have been downloaded into directory as xml files
#in_catalog_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/CatalogEventsDownload20220111/'
#in_evid_file = '/home/yoon/Documents/PuertoRico/Catalog/EQT_20180101_20220101/event_id_catalog.txt'
in_catalog_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20230101/CatalogEventsDownload20230206/'
in_evid_file = '/home/yoon/Documents/PuertoRico/Catalog/EQT_20180101_20230101/event_id_catalog.txt'

# Read catalog event ids from file, initialize to set
evid_file_set = set()
with open(in_evid_file, 'r') as fin:
   for line in fin:
      evid_file_set.add(line.strip())
print("Number of items in full evid_file_set: ", len(evid_file_set))

# Get catalog event id xml files from directory
evid_noext = [os.path.splitext(val)[0] for val in glob.glob(in_catalog_dir+"*.xml")]
evid_files = [os.path.basename(val) for val in evid_noext]
evid_dir_set = set(evid_files)
print("Number of items in full evid_dir_set: ", len(evid_dir_set))

# Set differences
print("Number of items in evid_file_set but not in evid_dir_set: ", evid_file_set-evid_dir_set)
print("Number of items in evid_dir_set but not in evid_file_set: ", evid_dir_set-evid_file_set)

