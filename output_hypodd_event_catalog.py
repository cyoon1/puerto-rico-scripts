import datetime

# Write out relocated HYPODD event catalogs for plotting in GMT

#catalog_start_time = '2020-01-07T00:00:00'
##in_hypodd_file = '../LargeAreaEQTransformer/HYPODD/hypoDD.reloc'
#in_hypodd_file = '../LargeAreaEQTransformer/20200107_20200114_Model1/HYPODD/dtct_hypoDD.reloc'
#out_hypodd_file = '../Catalog/LargeAreaEQTransformer_HYPODD_puerto_rico_catalog_20200107_20200114.txt'

#catalog_start_time = '2018-01-01T00:00:00'
#in_hypodd_file = '../Catalog/QuakeFlow/hypoDD_catalog.txt'
#out_hypodd_file = '../Catalog/QuakeFlow/20180501_20211101_QuakeFlow_HYPODD_Catalog.txt'

#catalog_start_time = '2018-01-01T00:00:00'
#in_hypodd_file = '../Catalog/QuakeFlow/events_MATCH_magcat_QuakeFlow_HYPODD_20180501_20211101.txt'
#out_hypodd_file = '../Catalog/QuakeFlow/QuakeFlow_HYPODD_20180501_20211101_events_MATCH_magcat.txt'

#catalog_start_time = '2018-01-01T00:00:00'
#in_hypodd_file = '../Catalog/QuakeFlow/events_NEW_magcat_QuakeFlow_HYPODD_20180501_20211101.txt'
#out_hypodd_file = '../Catalog/QuakeFlow/QuakeFlow_HYPODD_20180501_20211101_events_NEW_magcat.txt'

#catalog_start_time = '2018-01-01T00:00:00'
#in_hypodd_file = '../Catalog/QuakeFlow/us_events_MATCH_magcat_QuakeFlow_HYPODD_20180501_20211101.txt'
#out_hypodd_file = '../Catalog/QuakeFlow/QuakeFlow_HYPODD_20180501_20211101_us_events_MATCH_magcat.txt'

#catalog_start_time = '2018-01-01T00:00:00'
#in_hypodd_file = '../Catalog/QuakeFlow/us_events_NEW_magcat_QuakeFlow_HYPODD_20180501_20211101.txt'
#out_hypodd_file = '../Catalog/QuakeFlow/QuakeFlow_HYPODD_20180501_20211101_us_events_NEW_magcat.txt'

catalog_start_time = '2020-01-07T00:00:00'
#in_hypodd_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200108/REAL_VELZHANG/HYPODD_TEST10/python_dtcc_hypoDD.reloc'
#out_hypodd_file = '../Catalog/EQT_20200107_20200108/EQT_20200107_20200108_REAL_VELZHANG_HYPODD_python_dtcc_TEST10.txt'
#in_hypodd_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200108/REAL_VELZHANG/HYPODD_TEST10/stasis_dtcc_hypoDD.reloc'
#out_hypodd_file = '../Catalog/EQT_20200107_20200108/EQT_20200107_20200108_REAL_VELZHANG_HYPODD_stasis_dtcc_TEST10.txt'
#in_hypodd_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200108/REAL_VELZHANG/HYPODD_TEST10/python_dtct_dtcc_hypoDD.reloc'
#out_hypodd_file = '../Catalog/EQT_20200107_20200108/EQT_20200107_20200108_REAL_VELZHANG_HYPODD_python_dtct_dtcc_TEST10.txt'
in_hypodd_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200108/REAL_VELZHANG/HYPODD_TEST10/stasis_dtct_dtcc_hypoDD.reloc'
out_hypodd_file = '../Catalog/EQT_20200107_20200108/EQT_20200107_20200108_REAL_VELZHANG_HYPODD_stasis_dtct_dtcc_TEST10.txt'

catalog_ref_time = datetime.datetime.strptime(catalog_start_time, '%Y-%m-%dT%H:%M:%S')
fout = open(out_hypodd_file, 'w')
with open(in_hypodd_file, 'r') as fin:
   for line in fin:
      split_line = line.split()
      year = int(split_line[10])
      month = int(split_line[11])
      day = int(split_line[12])
      hour = int(split_line[13])
      minute = int(split_line[14])
      second = float(split_line[15])
      origin_time_nosec = datetime.datetime(year, month, day, hour, minute)
      origin_delta = datetime.timedelta(seconds=second)
      origin_time = origin_time_nosec + origin_delta
      origin_str = datetime.datetime.strftime(origin_time, '%Y-%m-%dT%H:%M:%S.%f')
      num_sec = (origin_time - catalog_ref_time).total_seconds()
      lat_deg = split_line[1]
      lon_deg = split_line[2]
      depth_km = split_line[3]
      mag = split_line[16]
      evid = split_line[0]
      fout.write(("%s %f %s %s %s %s %s\n") % (origin_str, num_sec, lat_deg, lon_deg, depth_km, mag, evid))
#      fout.write(("%f %s %s %s %s %s\n") % (num_sec, lat_deg, lon_deg, depth_km, mag, evid))
fout.close()
